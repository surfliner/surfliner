apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "comet.fullname" . }}-env
  labels:
    app.kubernetes.io/name: {{ include "comet.name" . }}
    helm.sh/chart: {{ include "comet.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
data:
  AUTH_METHOD: {{ .Values.auth.method }}
  {{- if (or .Values.fits.enabled .Values.externalFits.enabled) }}
  CH12N_TOOL: "fits_servlet"
  {{- else }}
  CH12N_TOOL: "fits"
  {{- end }}
  COMET_ENABLE_BULKRAX: {{ if .Values.bulkrax.enabled }}"true"{{ else }}"false"{{ end }}
  COMET_DISCOVERY_PLATFORMS: {{ .Values.discovery.platforms | default "shoreline,tidewater" }}
  COMET_WORKFLOWS_SUBPATH: {{ .Values.workflows_subpath | default "" }}
  COMET_DEFAULT_ACTIVE_WORKFLOW_NAME: {{ .Values.default_active_workflow_name | default "" }}
  DATABASE_COMMAND: {{ .Values.db_command }}
  DB_NAME: {{ template "common.postgresql.database" . }}
  DB_USERNAME: {{ template "common.postgresql.username" . }}
  DISCOVER_PLATFORM_DAYLIGHT_URL_BASE: {{ .Values.discovery.daylight_url_base }}
  DISCOVER_PLATFORM_TIDEWATER_URL_BASE: {{ .Values.discovery.tidewater_url_base }}
  DISCOVER_PLATFORM_SHORELINE_URL_BASE: {{ .Values.discovery.shoreline_url_base }}
  {{- if .Values.ezid.enabled }}
  EZID_SHOULDER: {{ .Values.ezid.shoulder | default "ark:/99999/fk4" }}
  EZID_USERNAME: {{ .Values.ezid.username | default "apitest" }}
  {{- end }}
  {{- if .Values.fits.enabled }}
  FITS_SERVLET_URL: http://{{ template "common.fits.host" . }}:{{ .Values.fits.servicePort | default 8080 }}{{ .Values.fits.subPath | default "/fits" }}
  {{- else if .Values.externalFits.enabled }}
  FITS_SERVLET_URL: {{ .Values.externalFits.url }}
  {{- end }}
  HYRAX_BRANDING_PATH: "/app/samvera/branding"
  {{- if .Values.derivativesVolume.enabled }}
  HYRAX_DERIVATIVES_PATH: "/app/samvera/derivatives"
  {{- end }}
  HYRAX_CACHE_PATH: "/app/samvera/file_cache"
  HYRAX_UPLOAD_PATH: "/app/samvera/uploads"
  {{- if .Values.iiif.enabled }}
  IIIF_BASE_URL: {{ .Values.iiif.base_url }}
  {{- end }}
  {{- if .Values.memcached.enabled }}
  MEMCACHED_HOST: {{ template "comet.memcached.fullname" . }}
  {{- end }}
  METADATA_API_URL_BASE: {{ .Values.metadata.api.url_base }}
  METADATA_DATABASE_NAME: {{ .Values.metadata.database }}
  METADATA_MODELS: {{ .Values.metadata.models }}
  {{- if .Values.otel.enabled }}
  OTEL_EXPORTER_OTLP_ENDPOINT: {{ .Values.otel.endpoint }}
  {{- else }}
  OTEL_SDK_DISABLED: "true"
  {{- end }}
  PGDATABASE: {{ template "common.postgresql.database" . }}
  POSTGRESQL_HOST: {{ template "common.postgresql.host" . }}
  POSTGRESQL_PORT: {{ template "common.postgresql.port" . }}
  {{- if .Values.rabbitmq.enabled }}
  RABBITMQ_ENABLED: "true"
  RABBITMQ_HOST: {{ .Values.rabbitmq.host }}
  RABBITMQ_NODE_PORT_NUMBER: {{ .Values.rabbitmq.port | default "5672" | quote }}
  RABBITMQ_TOPIC: {{ .Values.rabbitmq.topic }}
  RABBITMQ_USERNAME: {{ .Values.rabbitmq.username | default "surfliner" }}
  {{- else }}
  RABBITMQ_ENABLED: "false"
  {{- end }}
  RACK_ENV: production
  RAILS_ENV: production
  {{- if .Values.rails_log_to_stdout }}
  RAILS_LOG_TO_STDOUT: "true"
  {{- end }}
  RAILS_QUEUE: {{ .Values.rails_queue }}
  {{- if .Values.redis.enabled }}
  REDIS_HOST: {{ template "comet.redis.fullname" . }}-master
  REDIS_PORT: {{ .Values.redis.master.containerPorts.redis | default 6379 | quote }}
  {{- end }}
  REPOSITORY_S3_BUCKET: {{ .Values.s3.repository.bucket }}
  REPOSITORY_S3_EXTERNAL_ENDPOINT: {{ .Values.s3.repository.external_endpoint }}
  REPOSITORY_S3_FORCE_PATH_STYLE: {{ .Values.s3.repository.force_path_style | default "false" | quote }}
  REPOSITORY_S3_HOST: {{ .Values.s3.repository.host }}
  REPOSITORY_S3_PORT: {{ .Values.s3.repository.port | default 9000 | quote }}
  REPOSITORY_S3_PROTOCOL: {{ .Values.s3.repository.protocol | default "http" }}
  REPOSITORY_S3_REGION: {{ .Values.s3.repository.region | default "us-east-1" }}
  {{- if .Values.bulkrax.create_and_validate.enabled }}
  SHOW_CREATE_AND_VALIDATE: "true"
  {{- end }}
  {{- if .Values.minio.enabled }}
  MINIO_ENDPOINT: {{ template "comet.minio.fullname" . }}
  {{- end }}
  SKIP_HYRAX_ENGINE_SEED: {{  .Values.skipHyraxEngineSeed | default 0 | quote }}
  SOLR_ADMIN_USER: {{ template "common.solr.username" . }}
  SOLR_COLLECTION_NAME: {{ .Values.solr_collection | default "hyrax" | quote }}
  SOLR_CONFIGSET_NAME: {{ template "comet.fullname" . }}
  SOLR_HOST: {{ template "common.solr.fullname" . }}
  SOLR_PORT: {{ template "common.solr.port" . }}
  {{- if .Values.solr_replication.enabled }}
  SOLR_COLLECTION_REPLICATION_SHARDS: {{ .Values.solr_replication.shards | quote }}
  SOLR_COLLECTION_REPLICATION_FACTOR: {{ .Values.solr_replication.factor | quote }}
  {{- end }}
  STAGING_AREA_S3_BUCKET: {{ .Values.s3.staging_area.bucket }}
  STAGING_AREA_S3_FORCE_PATH_STYLE: {{ .Values.s3.staging_area.force_path_style | default "false" | quote }}
  STAGING_AREA_S3_HOST: {{ .Values.s3.staging_area.host }}
  STAGING_AREA_S3_PORT: {{ .Values.s3.staging_area.port | default 9000 | quote }}
  STAGING_AREA_S3_PROTOCOL: {{ .Values.s3.staging_area.protocol | default "http" }}
  STAGING_AREA_S3_REGION: {{ .Values.s3.staging_area.region | default "us-east-1" }}
  {{- if .Values.s3.staging_area.example_files }}
  STAGING_AREA_S3_EXAMPLE_FILES: "1"
  {{- end }}
