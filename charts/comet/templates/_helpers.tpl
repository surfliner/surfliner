{{/*
Expand the name of the chart.
*/}}
{{- define "comet.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "comet.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "comet.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "comet.labels" -}}
helm.sh/chart: {{ include "comet.chart" . }}
{{ include "comet.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "comet.selectorLabels" -}}
app.kubernetes.io/name: {{ include "comet.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Worker Selector labels
*/}}
{{- define "comet.workerSelectorLabels" -}}
app.kubernetes.io/name: {{ include "comet.name" . }}-worker
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "comet.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "comet.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "comet.memcached.fullname" -}}
{{- printf "%s-%s" .Release.Name "memcached" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "comet.minio.fullname" -}}
{{- printf "%s-%s" .Release.Name "minio" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "comet.redis.fullname" -}}
{{- printf "%s-%s" .Release.Name "redis" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "comet.sharedPvcAccessModes" -}}
{{- if .Values.worker.enabled }}
accessModes:
  - ReadWriteMany
{{- else }}
accessModes:
  - ReadWriteOnce
{{- end }}
{{- end -}}

{{/* Setup the volume configuration for a given volume */}}
{{- define "comet.volume" -}}
- name: {{ .volume.name }}
{{- if and .volume.enabled .volume.existingClaim }}
  persistentVolumeClaim:
    claimName: {{ .volume.existingClaim }}
{{- else if .volume.enabled }}
  persistentVolumeClaim:
    claimName: {{ include "comet.fullname" .context }}-{{ .volume.name }}
{{ else }}
  emptyDir: {}
{{ end }}
{{- end -}}

{{/* Setup the volumeMount configuration for a given volume */}}
{{- define "comet.volumeMount" -}}
{{- range $mount := .mounts }}
- name: {{ $.name }}
  mountPath: {{ $mount.mountPath }}
  {{- if $mount.subPath }}
  subPath: {{ $mount.subPath }}
  {{- end }}
{{- end }}
{{- end -}}
