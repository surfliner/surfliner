# Comet Helm chart

Prior to deployment, create a
[`Secret`](https://kubernetes.io/docs/concepts/configuration/secret/) with the
name you set in the `existingSecret.secretName` field, and populate it with the
key/value pairs listed in
[`secrets-static.yaml`](./templates/secrets-static.yaml).  (You can pass those
secrets directly as Helm chart variables, but you shouldn’t.)

- [Parameters](#parameters)
    - [Image source parameters](#image-source-parameters)
    - [Traffic exposure parameters](#traffic-exposure-parameters)
    - [Service Account parameters](#service-account-parameters)
    - [Secrets and authentication parameters](#secrets-and-authentication-parameters)
    - [General application parameters](#general-application-parameters)
    - [S3 parameters](#s3-parameters)
    - [Additional variable parameters](#additional-variable-parameters)
    - [Persistent storage parameters](#persistent-storage-parameters)
    - [Resources and scheduling parameters](#resources-and-scheduling-parameters)
    - [Scaling parameters](#scaling-parameters)
    - [Background worker parameters](#background-worker-parameters)
    - [Solr parameters](#solr-parameters)
    - [External Solr configuration (if `solr.enabled=false`)](#external-solr-configuration-if-solrenabledfalse)
    - [FITS parameters](#fits-parameters)
    - [External FITS parameters (if `fits.enabled=false`)](#external-fits-parameters-if-fitsenabledfalse)
    - [Memcached parameters](#memcached-parameters)
    - [MinIO parameters](#minio-parameters)
    - [PostgreSQL parameters](#postgresql-parameters)
    - [External PostgreSQL parameters (if `postgresql.enabled=false`)](#external-postgresql-parameters-if-postgresqlenabledfalse)
    - [RabbitMQ parameters](#rabbitmq-parameters)
    - [Redis parameters](#redis-parameters)

## Parameters

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                               |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- |
| `image.repository` | Image repository for the Rails web application                                                                                                           | `registry.gitlab.com/surfliner/surfliner/comet_web` |
| `image.pullPolicy` | Pull policy for the Rails web application image                                                                                                          | `IfNotPresent`                                      |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `""`                                                |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                |
| `nameOverride`     | Override the default chart name                                                                                                                          | `""`                                                |
| `fullnameOverride` | Override the `fullname` used by the chart                                                                                                                | `""`                                                |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value       |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP` |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `3000`      |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `false`     |
| `ingress.className`   | The Ingress class to use                                                                                                                                | `""`        |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`        |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `[]`        |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`        |

### Service Account parameters

| Name                         | Description                                                                                                             | Value  |
| ---------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create`      | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.automount`   | Automatically mount a ServiceAccount's API credentials?                                                                 | `true` |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                               | `{}`   |
| `serviceAccount.name`        | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### Secrets and authentication parameters

| Name                        | Description                                                                                                                                                                                                         | Value    |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| `existingSecret.enabled`    | Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide) | `false`  |
| `existingSecret.secretName` | The name of the existing secret, if enabled                                                                                                                                                                         | `""`     |
| `auth.method`               | The method used to authenticate users (`google` or `developer`)                                                                                                                                                     | `google` |

### General application parameters

| Name                                  | Description                                                                                                              | Value                                  |
| ------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ | -------------------------------------- |
| `bulkrax.enabled`                     | Whether to enable bulk import functionality                                                                              | `true`                                 |
| `bulkrax.create_and_validate.enabled` | Whether to display the "Create and Validate" button in Bulkrax (see [#821](https://github.com/samvera/bulkrax/pull/821)) | `true`                                 |
| `db_command`                          | the `rake` task(s) to run during database setup                                                                          | `db:create db:migrate`                 |
| `skipHyraxEngineSeed`                 | Whether to skip loading the Hyrax engine database seed file                                                              | `false`                                |
| `skipDbMigrateSeed`                   | Whether to skip running the `db-setup` initContainer                                                                     | `false`                                |
| `ezid.enabled`                        | Whether to enable EzID integration                                                                                       | `true`                                 |
| `ezid.shoulder`                       | The EzID shoulder to use                                                                                                 | `ark:/99999/fk4`                       |
| `ezid.username`                       | The username for the EzID account being used                                                                             | `apitest`                              |
| `iiif.enabled`                        | Whether to enabled IIIF                                                                                                  | `true`                                 |
| `iiif.base_url`                       | The base URL (including protocol) for the IIIF server                                                                    | `https://iiif.cool`                    |
| `discovery.tidewater_url_base`        | The base URL (including protocol) for the Tidewater discovery API                                                        | `http://oai.cool/item?source_iri=`     |
| `discovery.shoreline_url_base`        | The base URL (including protocol) for the Shoreline discovery API                                                        | `http://geodata.cool/item?source_iri=` |
| `metadata.api.url_base`               | The base URL (including protocol) for the Superskunk metadata API                                                        | `http://metadata.cool/resources`       |
| `metadata.database`                   | The name of the database that Comet and Superskunk use for metadata storage                                              | `comet_metadata`                       |
| `metadata.models`                     | The metadata model(s) that Comet should load                                                                             | `ucsb-metadata`                        |
| `otel.enabled`                        | Whether to enable OpenTelemetry                                                                                          | `true`                                 |
| `otel.endpoint`                       | the OTel endpoint                                                                                                        | `telemetry.cool`                       |
| `rails_log_to_stdout`                 | Whether to output logs to stdout/stderr                                                                                  | `true`                                 |
| `embargoRelease.enabled`              | Whether to enable the cron job that releases expired embargos                                                            | `true`                                 |
| `embargoRelease.schedule`             | The cron schedule                                                                                                        | `0 0 * * *`                            |
| `leaseRelease.enabled`                | Whether to enable the cron job that releases expired leases                                                              | `true`                                 |
| `leaseRelease.schedule`               | The cron schedule                                                                                                        | `0 0 * * *`                            |

### S3 parameters

| Name                               | Description                                                                          | Value                                 |
| ---------------------------------- | ------------------------------------------------------------------------------------ | ------------------------------------- |
| `s3.repository.bucket`             | The name of the bucket for repository file storage                                   | `production-repository-files`         |
| `s3.repository.host`               | The hostname of the S3 server                                                        | `production-repository-files.s3.cool` |
| `s3.repository.force_path_style`   | Whether to use the server/bucket style instead of the bucket.server subdomain format | `false`                               |
| `s3.repository.port`               | The port that the S3 server is listening on                                          | `9000`                                |
| `s3.repository.protocol`           | The protocol (http or https) that the S3 server uses                                 | `http`                                |
| `s3.repository.region`             | The region of the S3 bucket                                                          | `us-west-2`                           |
| `s3.staging_area.bucket`           | The name of the bucket holding data intended for ingest                              | `production-batch-source`             |
| `s3.staging_area.example_files`    | Whether to ingest sample data into Comet on deploy                                   | `false`                               |
| `s3.staging_area.host`             | The hostname of the S3 server                                                        | `production-batch-source.minio.cool`  |
| `s3.staging_area.force_path_style` | Whether to use the server/bucket style instead of the bucket.server subdomain format | `false`                               |
| `s3.staging_area.port`             | The port that the S3 server is listening on                                          | `9000`                                |
| `s3.staging_area.protocol`         | The protocol (http or https) that the S3 server uses                                 | `http`                                |
| `s3.staging_area.region`           | The region of the S3 bucket                                                          | `us-west-2`                           |

### Additional variable parameters

| Name                          | Description                                                                                                                              | Value |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `extraEnvVars`                | An array of optional environment variables to inject into the Rails pods                                                                 | `[]`  |
| `extraInitContainers`         | An array of optional [`initContainer`](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/) definitions to run on deploy | `[]`  |
| `extraEnvFrom`                | An array of optional ConfigMaps containing additional environment variables, to inject into the Rails pods                               | `[]`  |
| `extraContainerConfiguration` | An array of additional config options for the container spec                                                                             | `[]`  |
| `extraDeploy`                 | An array of optional extra objects to deploy with the release (evaluated as a template)                                                  | `[]`  |
| `applicationExistingClaim`    | An existing volume containing a Hyrax-based application (must be a `ReadWriteMany` volume if workers are enabled)                        | `""`  |

### Persistent storage parameters

| Name                                    | Description                                                       | Value                      |
| --------------------------------------- | ----------------------------------------------------------------- | -------------------------- |
| `brandingVolume.enabled`                | Whether to enable persistent storage for the "branding" volume    | `true`                     |
| `brandingVolume.name`                   | Name to use for the "uploads" volume                              | `branding`                 |
| `brandingVolume.existingClaim`          | An existing `PersistentVolumeClaim` to use                        | `""`                       |
| `brandingVolume.size`                   | The size of the PVC to create                                     | `2Gi`                      |
| `brandingVolume.storageClass`           | The storage class to use for the PVC                              | `""`                       |
| `brandingVolume.mounts[0].mountPath`    | Branding volume mountPath                                         | `/app/samvera/branding`    |
| `derivativesVolume.enabled`             | Whether to enable persistent storage for the "derivatives" volume | `true`                     |
| `derivativesVolume.name`                | Name to use for the "uploads" volume                              | `derivatives`              |
| `derivativesVolume.existingClaim`       | An existing `PersistentVolumeClaim` to use                        | `""`                       |
| `derivativesVolume.size`                | The size of the PVC to create                                     | `10Gi`                     |
| `derivativesVolume.storageClass`        | The storage class to use for the PVC                              | `""`                       |
| `derivativesVolume.mounts[0].mountPath` | Derivatives volume mountPath                                      | `/app/samvera/derivatives` |
| `uploadsVolume.enabled`                 | Whether to enable persistent storage for the "uploads" volume     | `true`                     |
| `uploadsVolume.name`                    | Name to use for the "uploads" volume                              | `uploads`                  |
| `uploadsVolume.existingClaim`           | An existing `PersistentVolumeClaim` to use                        | `""`                       |
| `uploadsVolume.size`                    | The size of the PVC to create                                     | `20Gi`                     |
| `uploadsVolume.storageClass`            | The storage class to use for the PVC                              | `""`                       |
| `uploadsVolume.mounts[0].mountPath`     | Uploads volume file_cache mountPath                               | `/app/samvera/file_cache`  |
| `uploadsVolume.mounts[0].subPath`       | Uploads volume file_cache subPath                                 | `file_cache`               |
| `uploadsVolume.mounts[1].mountPath`     | Uploads volume uploads mountPath                                  | `/app/samvera/uploads`     |
| `uploadsVolume.mounts[1].subPath`       | Uploads volume uploads subPath                                    | `uploads`                  |

### Resources and scheduling parameters

| Name                                 | Description                                                                                                                                                                                                                         | Value   |
| ------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| `resources`                          | The resource requests and limits                                                                                                                                                                                                    | `{}`    |
| `extraVolumeMounts`                  | An array of optional `volumeMounts` for the Rails deployment                                                                                                                                                                        | `[]`    |
| `extraVolumes`                       | An array of optional [`Volumes`](https://kubernetes.io/docs/concepts/storage/volumes/) for the Rails deployment                                                                                                                     | `[]`    |
| `nodeSelector`                       | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                                                                                                                   | `{}`    |
| `tolerations`                        | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                                                                                                                 | `[]`    |
| `affinity`                           | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods                                                                                                   | `{}`    |
| `podAnnotations`                     | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods                                                                                            | `{}`    |
| `podLabels`                          | Default [labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/) to apply to Rails application pods                                                                                                      | `{}`    |
| `podSecurityContext`                 | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods                                                                                               | `{}`    |
| `securityContext`                    | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers                                                                                         | `{}`    |
| `livenessProbe.enabled`              | Whether to enable a [liveness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for the Rails application                                                               | `true`  |
| `livenessProbe.failureThreshold`     | The number of failures required to trigger an unhealthy status                                                                                                                                                                      | `3`     |
| `livenessProbe.initialDelaySeconds`  | Seconds to wait after the container starts before initiating checks                                                                                                                                                                 | `5`     |
| `livenessProbe.path`                 | The path to make HTTP requests to                                                                                                                                                                                                   | `/`     |
| `livenessProbe.periodSeconds`        | How often to perform the check                                                                                                                                                                                                      | `10`    |
| `livenessProbe.successThreshold`     | The number of successes required to trigger a healthy status                                                                                                                                                                        | `1`     |
| `livenessProbe.timeoutSeconds`       | Seconds after which the probe times out                                                                                                                                                                                             | `5`     |
| `readinessProbe.enabled`             | Whether to enable a [readiness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for the Rails application                                                              | `true`  |
| `readinessProbe.failureThreshold`    | The number of failures required to trigger an unready status                                                                                                                                                                        | `3`     |
| `readinessProbe.initialDelaySeconds` | Seconds to wait after the container starts before initiating checks                                                                                                                                                                 | `5`     |
| `readinessProbe.path`                | The path to make HTTP requests to                                                                                                                                                                                                   | `/`     |
| `readinessProbe.periodSeconds`       | How often to perform the check                                                                                                                                                                                                      | `10`    |
| `readinessProbe.successThreshold`    | The number of successes required to trigger a ready status                                                                                                                                                                          | `1`     |
| `readinessProbe.timeoutSeconds`      | Seconds after which the probe times out                                                                                                                                                                                             | `5`     |
| `startupProbe.enabled`               | Whether to enable a [startup probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for the Rails application (consider enabling if you are experiencing slow startup times) | `false` |
| `startupProbe.failureThreshold`      | The number of failures required to trigger an unready status                                                                                                                                                                        | `3`     |
| `startupProbe.initialDelaySeconds`   | Seconds to wait after the container starts before initiating checks                                                                                                                                                                 | `30`    |
| `startupProbe.path`                  | The path to make HTTP requests to                                                                                                                                                                                                   | `/`     |
| `startupProbe.periodSeconds`         | How often to perform the check                                                                                                                                                                                                      | `10`    |
| `startupProbe.successThreshold`      | The number of successes required to trigger a ready status                                                                                                                                                                          | `1`     |
| `startupProbe.timeoutSeconds`        | Seconds after which the probe times out                                                                                                                                                                                             | `5`     |

### Scaling parameters

| Name                                            | Description                                                                                               | Value   |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------- |
| `replicaCount`                                  | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `1`     |
| `autoscaling.enabled`                           | Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)               | `false` |
| `autoscaling.minReplicas`                       | The minimum number of replicas                                                                            | `1`     |
| `autoscaling.maxReplicas`                       | The maximum number of replicas                                                                            | `100`   |
| `autoscaling.targetCPUUtilizationPercentage`    |                                                                                                           | `80`    |
| `autoscaling.targetMemoryUtilizationPercentage` |                                                                                                           | `80`    |

### Background worker parameters

| Name                            | Description                                                                                                                                              | Value                                                  |
| ------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ |
| `rails_queue`                   | The background jobs processor to use                                                                                                                     | `sidekiq`                                              |
| `worker.enabled`                | Whether to enable background workers                                                                                                                     | `true`                                                 |
| `worker.enabled`                | Whether to enable background jobs processing                                                                                                             | `true`                                                 |
| `worker.enabled`                | Whether to enable background jobs processing                                                                                                             | `true`                                                 |
| `worker.replicaCount`           | How many workers to run simultaneously                                                                                                                   | `3`                                                    |
| `worker.image.repository`       | Image repository for the worker application                                                                                                              | `registry.gitlab.com/surfliner/surfliner/comet_worker` |
| `worker.image.pullPolicy`       | Pull policy for the worker application image                                                                                                             | `IfNotPresent`                                         |
| `worker.image.tag`              | Overrides the image tag whose default is the chart appVersion.                                                                                           | `""`                                                   |
| `worker.extraInitContainers`    | An array of optional [`initContainer`](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/) definitions to run on deploy                 | `[]`                                                   |
| `worker.extraVolumeMounts`      | An array of optional `volumeMounts` for the worker deployment                                                                                            | `[]`                                                   |
| `worker.extraVolumes`           | An array of optional [`Volumes`](https://kubernetes.io/docs/concepts/storage/volumes/) for the worker deployment                                         | `[]`                                                   |
| `worker.imagePullSecrets`       | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                   |
| `worker.podSecurityContext`     | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) for the worker pods                                   | `{}`                                                   |
| `worker.nodeSelector`           | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the worker pods                                       | `{}`                                                   |
| `worker.tolerations`            | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the worker pods                                     | `[]`                                                   |
| `worker.affinity`               | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the worker pods                       | `{}`                                                   |
| `worker.resources`              | The [resource spec](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) for the worker deployment                            | `{}`                                                   |
| `worker.readinessProbe.enabled` | Whether to enable readiness probes on the worker pods                                                                                                    | `false`                                                |

### Solr parameters

| Name                                  | Description                                                            | Value                 |
| ------------------------------------- | ---------------------------------------------------------------------- | --------------------- |
| `solr_collection`                     | The name of the Solr collection to create                              | `hyrax`               |
| `loadSolrConfigSet`                   | Whether to run the configset management initContainer                  | `true`                |
| `solr_replication.enabled`            | Whether to enable replication in Solr                                  | `true`                |
| `solr_replication.shards`             | The number of shards to use                                            | `2`                   |
| `solr_replication.factor`             | The replication factor to use                                          | `3`                   |
| `solr.enabled`                        | Whether to deploy Solr                                                 | `true`                |
| `solr.image.repository`               | The image repository for Solr                                          | `bitnami/solr`        |
| `solr.containerPorts.http`            | The HTTP port for the containers to listen on                          | `8983`                |
| `solr.auth.enabled`                   | Whether to require authentication when connecting to Solr              | `true`                |
| `solr.auth.adminUsername`             | The name of the Solr admin user                                        | `admin`               |
| `solr.auth.adminPassword`             | The password for the Solr admin user                                   | `admin`               |
| `solr.auth.existingSecret`            | The name of the pre-generated secret containing Solr admin credentials | `""`                  |
| `solr.auth.existingSecretPasswordKey` | The key used for the Solr admin password in an existing Secret         | `SOLR_ADMIN_PASSWORD` |
| `solr.coreNames`                      | The default cores to create (ignored if `cloudEnabled`)                | `hyrax`               |
| `solr.cloudBootstrap`                 | Whether to enable cloud bootstrap                                      | `true`                |
| `solr.cloudEnabled`                   | Whether to run in cloud mode (as opposed to standalone)                | `true`                |
| `solr.persistence.enabled`            | Whether to enable persistent storage for Solr                          | `true`                |
| `solr.zookeeper.enabled`              | Whether to deploy ZooKeeper                                            | `true`                |
| `solr.zookeeper.persistence.enabled`  | Whether to enable persistent storage for ZooKeeper                     | `true`                |

### External Solr configuration (if `solr.enabled=false`)

| Name                    | Description                                                                       | Value   |
| ----------------------- | --------------------------------------------------------------------------------- | ------- |
| `externalSolr.host`     | The hostname of the external Solr cluster                                         | `""`    |
| `externalSolr.password` | The password to use to authenticate to the external Solr cluster                  | `""`    |
| `externalSolr.port`     | The port that the external Solr cluster is listening on                           | `8983`  |
| `externalSolr.runMode`  | The "mode" that the external Solr cluster is running in (`cloud` or `standalone`) | `cloud` |
| `externalSolr.username` | The username to use to authenticate to the external Solr cluster                  | `""`    |

### FITS parameters

| Name               | Description                                      | Value   |
| ------------------ | ------------------------------------------------ | ------- |
| `fits.enabled`     | Whether to deploy FITS                           | `true`  |
| `fits.servicePort` | The port that the deployed FITS should listen on | `8080`  |
| `fits.subPath`     | The path of the FITS endpoint                    | `/fits` |

### External FITS parameters (if `fits.enabled=false`)

| Name                   | Description                                   | Value   |
| ---------------------- | --------------------------------------------- | ------- |
| `externalFits.enabled` | Whether to use an external FITS service       | `false` |
| `externalFits.url`     | The endpoint URL of the external FITS service | `""`    |

### Memcached parameters

| Name                         | Description                        | Value               |
| ---------------------------- | ---------------------------------- | ------------------- |
| `memcached.enabled`          | Whether to deploy memcached        | `false`             |
| `memcached.image.repository` | The image repository for Memcached | `bitnami/memcached` |

### MinIO parameters

| Name                        | Description                                    | Value              |
| --------------------------- | ---------------------------------------------- | ------------------ |
| `minio.enabled`             | Whether to deploy MinIO                        | `false`            |
| `minio.image.repository`    | The image repository for MinIO                 | `bitnami/minio`    |
| `minio.auth.rootUser`       | The name of the root/admin user for MinIO      | `hyrax-access-key` |
| `minio.auth.rootPassword`   | the password for the root/admin user           | `hyrax-secret-key` |
| `minio.persistence.enabled` | Whether to enable persistent storage for MinIO | `false`            |

### PostgreSQL parameters

| Name                                               | Description                                          | Value                |
| -------------------------------------------------- | ---------------------------------------------------- | -------------------- |
| `postgresql.enabled`                               | Whether to deploy PostgreSQL                         | `true`               |
| `postgresql.image.repository`                      | The image repository for PostgreSQL                  | `bitnami/postgresql` |
| `postgresql.auth.database`                         | The name of the default database to create on deploy | `hyrax`              |
| `postgresql.auth.password`                         | the password for the PostgreSQL user                 | `hyrax_pass`         |
| `postgresql.auth.username`                         | The name of the PostgreSQL user                      | `hyrax`              |
| `postgresql.containerPorts.postgresql`             | The port to use for the container                    | `5432`               |
| `postgresql.primary.service.ports.postgresql`      | The port to use for the primary service              | `5432`               |
| `postgresql.readReplicas.service.ports.postgresql` | The port to use for the replica service              | `5432`               |

### External PostgreSQL parameters (if `postgresql.enabled=false`)

| Name                          | Description                                                  | Value  |
| ----------------------------- | ------------------------------------------------------------ | ------ |
| `externalPostgresql.username` | The name of the PostgreSQL user                              | `""`   |
| `externalPostgresql.password` | the password for the PostgreSQL user                         | `""`   |
| `externalPostgresql.database` | The name of the database to use                              | `""`   |
| `externalPostgresql.port`     | The port that the external PostgreSQL server is listening on | `5432` |

### RabbitMQ parameters

| Name                | Description                                       | Value          |
| ------------------- | ------------------------------------------------- | -------------- |
| `rabbitmq.enabled`  | Whether to enable the message queue               | `true`         |
| `rabbitmq.host`     | The hostname of the RabbitMQ server               | `rabbits.cool` |
| `rabbitmq.port`     | The port that the RabbitMQ server is listening on | `5672`         |
| `rabbitmq.topic`    | The topic that the RabbitMQ server is using       | `rabbit.prod`  |
| `rabbitmq.username` | The username to authenticate to RabbitMQ with     | `bnuuy`        |

### Redis parameters

| Name                                   | Description                                                       | Value            |
| -------------------------------------- | ----------------------------------------------------------------- | ---------------- |
| `redis.enabled`                        | Whether to deploy Redis                                           | `true`           |
| `redis.image.repository`               | The image repository for Redis                                    | `bitnami/redis`  |
| `redis.auth.enabled`                   | Whether to use password authentication for Redis                  | `true`           |
| `redis.auth.existingSecret`            | The name of the pre-generated secret containing Redis credentials | `""`             |
| `redis.auth.existingSecretPasswordKey` | The key used for the Redis password in an existing Secret         | `REDIS_PASSWORD` |
