# Default values for daylight.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

## @section Image source parameters

# This sets the container image; more information can be found here:
# https://kubernetes.io/docs/concepts/containers/images/
#
## @param image.repository Image repository for the application
## @param image.name Name of the application image
## @param image.pullPolicy Pull policy for the application image
## @param image.tag Overrides the image tag whose default is the chart appVersion.
image:
  repository: registry.gitlab.com/surfliner/surfliner/daylight-backend_web
  name: daylight-backend_web
  pullPolicy: IfNotPresent
  tag: stable

## @param imagePullSecrets An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
imagePullSecrets: []

## @param nameOverride Override the default chart name
nameOverride: ""
## @param fullnameOverride Override the `fullname` used by the chart
fullnameOverride: ""

## @section Traffic exposure parameters
#
## @param service.type The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application
## @param service.port The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service
service:
  type: ClusterIP
  port: 80

## @param ingress.enabled Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)
## @param ingress.className The Ingress class to use
## @param ingress.annotations Additional annotations for the ingress
## @param ingress.hosts An array of hosts to manage with the ingress
## @param ingress.tls An array of TLS options
ingress:
  enabled: false
  className: ""
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts: []
    # - host: chart-example.local
    #   paths:
    #     - path: /
    #       pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

## @section Service Account parameters
#
## @param serviceAccount.create Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created
## @param serviceAccount.annotations Annotations to add to the service account
## @param serviceAccount.name The name of the service account to use (if not set and create is true, a name is generated using the fullname template)
serviceAccount:
  create: true
  annotations: {}
  name: ""

## @section Secrets and authentication parameters

## @param existingSecret.enabled Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide)
## @param existingSecret.name The name of the existing secret, if enabled
existingSecret:
  enabled: false
  name: ""

## @section General application parameters
#
## @param daylight.disableSolrConfigInit Whether to skip the Solr setup step
## @param daylight.rails.environment
## @param daylight.rails.log_level
## @param daylight.rails.log_to_stdout
## @param daylight.rails.serve_static_files
## @param daylight.iiif_manifest_template The template for generating IIIF manifest URLs
## @param daylight.components An array of frontend components to use (hashes with `name` and `uri` keys)
daylight:
  disableSolrConfigInit: false
  rails:
    environment: production
    log_level: info
    log_to_stdout: true
    serve_static_files: true
  iiif_manifest_template: "https://manage-staging.digital.library.ucsb.edu/concern/generic_objects/%s/manifest.json"
  components: []
    # - name: spike
    #   uri: "https://daylight-spike.eks.dld.library.ucsb.edu"
    # - name: object_viewer
    #   uri: "http://clover"

## @section Consumer application parameters
#
## @param consumer.enabled Whether to enable the consumer application
## @param consumer.replicaCount How many consumers to run simultaneously
## @param consumer.image.repository Image repository for the consumer application
## @param consumer.imagePullPolicy Pull policy for the consumer application image
## @param consumer.image.tag Overrides the image tag whose default is the chart appVersion.
#
## @param consumer.api.base_uri The base API endpoint
#
## @param consumer.rabbitmq.host The RabbitMQ host to connect to
## @param consumer.rabbitmq.password
## @param consumer.rabbitmq.port
## @param consumer.rabbitmq.queue The RabbitMQ queue to connect to
## @param consumer.rabbitmq.routing_key The RabbitMQ routing key to use
## @param consumer.rabbitmq.topic The RabbitMQ topic to subscribe to
## @param consumer.rabbitmq.username
consumer:
  enabled: false
  replicaCount: 2
  imagePullPolicy: IfNotPresent
  image:
    repository: registry.gitlab.com/surfliner/surfliner/daylight-metadata_consumer
    tag: stable
  api:
    base_uri: http://surfliner-superskunk-stage.superskunk_staging
  rabbitmq:
    host: rabbitmq
    password: surfliner
    port: 5672
    queue: daylight
    routing_key: surfliner.metadata.daylight
    topic: surfliner.metadata
    username: surfliner

## @section Scaling parameters

## @param replicaCount The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy
replicaCount: 2

## @param autoscaling.enabled Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)
## @param autoscaling.minReplicas The minimum number of replicas
## @param autoscaling.maxReplicas The maximum number of replicas
## @param autoscaling.targetCPUUtilizationPercentage
## @param autoscaling.targetMemoryUtilizationPercentage
autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  targetMemoryUtilizationPercentage: 80

## @section Resources and scheduling parameters
#
## @param affinity The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods
affinity: {}

## @param nodeSelector The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods
nodeSelector: {}

## @param tolerations The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods
tolerations: []

## @param podAnnotations Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods
podAnnotations: {}

## @param podSecurityContext The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods
podSecurityContext: {}
  # fsGroup: 2000

## @param securityContext The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers
securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

## @param resources The resource requests and limits
resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi



## @section Solr parameters
#
# FIXME: this is a workaround for the fact that setting SOLR_COLLECTION in the
# bitnami solr context creates the collection before we can set the
# configuration for it: https://github.com/bitnami/charts/issues/21832
#
## @param solr_collection The name of the Solr collection to create
solr_collection: daylight

## @param solr.enabled Whether to deploy Solr
## @param solr.image.repository The image repository for Solr
## @skip solr.image.tag The Solr image tag to use
## @param solr.auth.enabled Whether to require authentication when connecting to Solr
## @param solr.auth.adminUsername The name of the Solr admin user
## @param solr.auth.adminPassword The password for the Solr admin user
## @param solr.auth.existingSecret The name of the pre-generated secret containing Solr admin credentials
## @param solr.auth.existingSecretPasswordKey The key used for the Solr admin password in an existing Secret
## @param solr.cloudBootstrap Whether to enable cloud bootstrap
## @param solr.cloudEnabled Whether to run in cloud mode (as opposed to standalone)
## @param solr.persistence.enabled Whether to enable persistent storage for Solr
## @param solr.zookeeper.enabled Whether to deploy ZooKeeper
## @param solr.zookeeper.persistence.enabled Whether to enable persistent storage for ZooKeeper
solr:
  enabled: true
  image:
    repository: bitnami/solr
    tag: 8.11.4-debian-12-r3
  auth:
    enabled: true
    adminUsername: admin
    adminPassword: admin
    existingSecret: ""
    existingSecretPasswordKey: SOLR_ADMIN_PASSWORD
  cloudBootstrap: true
  cloudEnabled: true
  persistence:
    enabled: true
  zookeeper:
    enabled: true
    persistence:
      enabled: true

## @section External Solr configuration (if `solr.enabled=false`)
#
## @param externalSolr.host The hostname of the external Solr cluster
## @param externalSolr.password The password to use to authenticate to the external Solr cluster
## @param externalSolr.port The port that the external Solr cluster is listening on
## @param externalSolr.runMode The "mode" that the external Solr cluster is running in (`cloud` or `standalone`)
## @param externalSolr.username The username to use to authenticate to the external Solr cluster
externalSolr:
  host: ""
  password: ""
  port: "8983"
  runMode: cloud
  username: ""
