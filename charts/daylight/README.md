# Daylight

To deploy manually, create a custom values file if desired, then:

```
helm install my-daylight oci://registry.gitlab.com/surfliner/surfliner/charts/daylight --namespace my-namespace [--values custom-values.yaml]
```

## Parameters

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                                          |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------- |
| `image.repository` | Image repository for the application                                                                                                                     | `registry.gitlab.com/surfliner/surfliner/daylight-backend_web` |
| `image.name`       | Name of the application image                                                                                                                            | `daylight-backend_web`                                         |
| `image.pullPolicy` | Pull policy for the application image                                                                                                                    | `IfNotPresent`                                                 |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                                       |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                           |
| `nameOverride`     | Override the default chart name                                                                                                                          | `""`                                                           |
| `fullnameOverride` | Override the `fullname` used by the chart                                                                                                                | `""`                                                           |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value       |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP` |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`        |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `false`     |
| `ingress.className`   | The Ingress class to use                                                                                                                                | `""`        |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`        |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `[]`        |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`        |

### Service Account parameters

| Name                         | Description                                                                                                             | Value  |
| ---------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create`      | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                               | `{}`   |
| `serviceAccount.name`        | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### Secrets and authentication parameters

| Name                     | Description                                                                                                                                                                                                         | Value   |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| `existingSecret.enabled` | Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide) | `false` |
| `existingSecret.name`    | The name of the existing secret, if enabled                                                                                                                                                                         | `""`    |

### General application parameters

| Name                                | Description                                                                | Value                                                                                      |
| ----------------------------------- | -------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| `daylight.disableSolrConfigInit`    | Whether to skip the Solr setup step                                        | `false`                                                                                    |
| `daylight.rails.environment`        |                                                                            | `production`                                                                               |
| `daylight.rails.log_level`          |                                                                            | `info`                                                                                     |
| `daylight.rails.log_to_stdout`      |                                                                            | `true`                                                                                     |
| `daylight.rails.serve_static_files` |                                                                            | `true`                                                                                     |
| `daylight.iiif_manifest_template`   | The template for generating IIIF manifest URLs                             | `https://manage-staging.digital.library.ucsb.edu/concern/generic_objects/%s/manifest.json` |
| `daylight.components`               | An array of frontend components to use (hashes with `name` and `uri` keys) | `[]`                                                                                       |

### Consumer application parameters

| Name                            | Description                                                    | Value                                                                |
| ------------------------------- | -------------------------------------------------------------- | -------------------------------------------------------------------- |
| `consumer.enabled`              | Whether to enable the consumer application                     | `false`                                                              |
| `consumer.replicaCount`         | How many consumers to run simultaneously                       | `2`                                                                  |
| `consumer.image.repository`     | Image repository for the consumer application                  | `registry.gitlab.com/surfliner/surfliner/daylight-metadata_consumer` |
| `consumer.imagePullPolicy`      | Pull policy for the consumer application image                 | `IfNotPresent`                                                       |
| `consumer.image.tag`            | Overrides the image tag whose default is the chart appVersion. | `stable`                                                             |
| `consumer.api.base_uri`         | The base API endpoint                                          | `http://surfliner-superskunk-stage.superskunk_staging`               |
| `consumer.rabbitmq.host`        | The RabbitMQ host to connect to                                | `rabbitmq`                                                           |
| `consumer.rabbitmq.password`    |                                                                | `surfliner`                                                          |
| `consumer.rabbitmq.port`        |                                                                | `5672`                                                               |
| `consumer.rabbitmq.queue`       | The RabbitMQ queue to connect to                               | `daylight`                                                           |
| `consumer.rabbitmq.routing_key` | The RabbitMQ routing key to use                                | `surfliner.metadata.daylight`                                        |
| `consumer.rabbitmq.topic`       | The RabbitMQ topic to subscribe to                             | `surfliner.metadata`                                                 |
| `consumer.rabbitmq.username`    |                                                                | `surfliner`                                                          |

### Scaling parameters

| Name                                            | Description                                                                                               | Value   |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------- |
| `replicaCount`                                  | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `2`     |
| `autoscaling.enabled`                           | Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)               | `false` |
| `autoscaling.minReplicas`                       | The minimum number of replicas                                                                            | `1`     |
| `autoscaling.maxReplicas`                       | The maximum number of replicas                                                                            | `100`   |
| `autoscaling.targetCPUUtilizationPercentage`    |                                                                                                           | `80`    |
| `autoscaling.targetMemoryUtilizationPercentage` |                                                                                                           | `80`    |

### Resources and scheduling parameters

| Name                 | Description                                                                                                                                 | Value |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `affinity`           | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods           | `{}`  |
| `nodeSelector`       | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                           | `{}`  |
| `tolerations`        | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                         | `[]`  |
| `podAnnotations`     | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods    | `{}`  |
| `podSecurityContext` | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods       | `{}`  |
| `securityContext`    | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers | `{}`  |
| `resources`          | The resource requests and limits                                                                                                            | `{}`  |

### Solr parameters

| Name                                  | Description                                                            | Value                 |
| ------------------------------------- | ---------------------------------------------------------------------- | --------------------- |
| `solr_collection`                     | The name of the Solr collection to create                              | `daylight`            |
| `solr.enabled`                        | Whether to deploy Solr                                                 | `true`                |
| `solr.image.repository`               | The image repository for Solr                                          | `bitnami/solr`        |
| `solr.auth.enabled`                   | Whether to require authentication when connecting to Solr              | `true`                |
| `solr.auth.adminUsername`             | The name of the Solr admin user                                        | `admin`               |
| `solr.auth.adminPassword`             | The password for the Solr admin user                                   | `admin`               |
| `solr.auth.existingSecret`            | The name of the pre-generated secret containing Solr admin credentials | `""`                  |
| `solr.auth.existingSecretPasswordKey` | The key used for the Solr admin password in an existing Secret         | `SOLR_ADMIN_PASSWORD` |
| `solr.cloudBootstrap`                 | Whether to enable cloud bootstrap                                      | `true`                |
| `solr.cloudEnabled`                   | Whether to run in cloud mode (as opposed to standalone)                | `true`                |
| `solr.persistence.enabled`            | Whether to enable persistent storage for Solr                          | `true`                |
| `solr.zookeeper.enabled`              | Whether to deploy ZooKeeper                                            | `true`                |
| `solr.zookeeper.persistence.enabled`  | Whether to enable persistent storage for ZooKeeper                     | `true`                |

### External Solr configuration (if `solr.enabled=false`)

| Name                    | Description                                                                       | Value   |
| ----------------------- | --------------------------------------------------------------------------------- | ------- |
| `externalSolr.host`     | The hostname of the external Solr cluster                                         | `""`    |
| `externalSolr.password` | The password to use to authenticate to the external Solr cluster                  | `""`    |
| `externalSolr.port`     | The port that the external Solr cluster is listening on                           | `8983`  |
| `externalSolr.runMode`  | The "mode" that the external Solr cluster is running in (`cloud` or `standalone`) | `cloud` |
| `externalSolr.username` | The username to use to authenticate to the external Solr cluster                  | `""`    |
