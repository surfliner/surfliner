{{- define "common.fits.fullname" -}}
{{- printf "%s-%s" .Release.Name "fits" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "common.fits.host" -}}
{{- if .Values.fits.enabled }}
{{- include "common.fits.fullname" . }}
{{- else }}
{{- .Values.externalFitsHost | default "NO_FITS_HOST_DEFINED" }}
{{- end }}
{{- end -}}
