{{- define "common.postgresql.fullname" -}}
{{- if .Values.postgresql.enabled -}}
{{- printf "%s-%s" .Release.Name "postgresql" | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Values.postgresql.postgresqlHostname -}}
{{- end -}}
{{- end -}}

{{- define "common.postgresql.host" -}}
{{- if .Values.postgresql.enabled -}}
{{- include "common.postgresql.fullname" . -}}
{{- else -}}
{{- .Values.externalPostgresql.host }}
{{- end -}}
{{- end -}}

{{- define "common.postgresql.database" -}}
{{- if .Values.postgresql.enabled -}}
{{- .Values.postgresql.auth.database -}}
{{- else -}}
{{- .Values.externalPostgresql.database | default ( include "common.fullname" . ) -}}
{{- end -}}
{{- end -}}

{{- define "common.postgresql.username" -}}
{{- if .Values.postgresql.enabled -}}
{{- .Values.postgresql.auth.username -}}
{{- else -}}
{{- .Values.externalPostgresql.username | default "postgres" -}}
{{- end -}}
{{- end -}}

{{- define "common.postgresql.password" -}}
{{- if .Values.postgresql.enabled -}}
{{- .Values.postgresql.auth.password -}}
{{- else -}}
{{- .Values.externalPostgresql.password -}}
{{- end -}}
{{- end -}}

{{- define "common.postgresql.port" -}}
{{- if .Values.postgresql.enabled -}}
{{- .Values.postgresql.primary.service.ports.postgresql | default "5432" | quote -}}
{{- else -}}
{{- .Values.externalPostgresql.port | default "5432" | quote -}}
{{- end -}}
{{- end -}}

{{- define "common.postgresql.url" -}}
{{- printf "postgresql://%s:%s@%s/%s?pool=5" ( include "common.postgresql.username" . ) ( include "common.postgresql.password" . ) ( include "common.postgresql.host" . ) ( include "common.postgresql.database" . ) -}}
{{- end -}}
