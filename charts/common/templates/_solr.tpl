{{- define "common.solr.cloudEnabled" -}}
{{- if .Values.solr.enabled -}}
{{- .Values.solr.cloudEnabled }}
{{- else -}}
{{- eq "cloud" (lower .Values.externalSolr.runMode) }}
{{- end -}}
{{- end -}}

{{- define "common.solr.fullname" -}}
{{- if .Values.solr.enabled -}}
{{- printf "%s-%s" .Release.Name "solr" | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Values.externalSolr.host | default "solr" -}}
{{- end -}}
{{- end -}}

{{- define "common.solr.password" -}}
{{- if .Values.solr.enabled -}}
{{- .Values.solr.auth.adminPassword }}
{{- else -}}
{{- .Values.externalSolr.password -}}
{{- end -}}
{{- end -}}

{{- define "common.solr.port" -}}
{{- if .Values.solr.enabled -}}
{{- .Values.solr.service.ports.http | quote -}}
{{- else -}}
{{- .Values.externalSolr.port | quote -}}
{{- end -}}
{{- end -}}

{{- define "common.solr.username" -}}
{{- if .Values.solr.enabled -}}
{{- .Values.solr.auth.adminUsername }}
{{- else -}}
{{- .Values.externalSolr.username -}}
{{- end -}}
{{- end -}}
