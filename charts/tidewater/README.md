# Tidewater

Tidewater is a Helm chart that leverages the [tidewater][tidewater] container
image to support easy deployment via Helm and Kubernetes.

The Tidewater chart also contains an optional `Deployment` template for the
tidewater consumer script. The purpose of this script is to listen and consume
events published by Comet to a RabbitMQ cluster.

## TL;DR;

```console
$ git clone https://gitlab.com/surfliner/surfliner.git
$ helm dep update charts/tidewater
$ helm install my-release charts/tidewater
```

## Introduction

This chart bootstraps a [tidewater][tidewater] deployment on a
[Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh)
package manager.

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm install my-release charts/tidewater
```

The command deploys Tidewater on the Kubernetes cluster in the default
configuration. The [Parameters](#parameters) section lists the parameters that
can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and
deletes the release.

## Tidewater Signing Keypair

In order to be able to successfully authenticate to the [Superskunk][superskunk]
application, tidewater needs to sign requests using a keypair. There is a
default keypair in a `Secret` template in the Helm chart, however for production
purposes it is expected that an external `Secret` will be created and referenced
by name as shown below.

To create that secret, you might do the following:

Create the key, let's assume we save it to `/tmp/tidwater`

```sh
$ ssh-keygen -t rsa -b 4096 -C "tidewater@ucsd.edu" -f /tmp/tidewater
$ ls /tmp
tidewater tidewater.pub
```

Create a `pem` (or `PKCS8`) public key:

```sh
$ ssh-keygen -f /tmp/tidewater -e -m pem > /tmp/tidewater-pem.pub
```

Create your secret by coping the template and replacing the `base64` encoded
values for the `ssh-publickey` and `ssh-privatekey`. For the `ssh-publickey` it
is important to use the `pem` or `PKCS8` public key. Note it's critical that the
`base64`-encoded value be on a single line (no newlines or carriage returns).

Example:

```sh
cat /tmp/tidewater-pem.pub | base64 -w0 | xclip
```

An alternative to hand-creating the `Secret` is to do it directly via the
command line. You might do something like:

```sh
kubectl -n tidewater-staging create secret generic surfliner-tidewater-staging-keypair --from-file=ssh-privatekey=/tmp/tidewater --from-file=ssh-publickey=/tmp/tidewater-pem.pub
```

## Parameters

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                                   |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------- |
| `image.repository` | Image repository for the Rails web application                                                                                                           | `registry.gitlab.com/surfliner/surfliner/tidewater_web` |
| `image.name`       | Name of the Rails web application image                                                                                                                  | `tidewater_web`                                         |
| `image.pullPolicy` | Pull policy for the Rails web application image                                                                                                          | `Always`                                                |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                                |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                    |
| `nameOverride`     | Override the default chart name                                                                                                                          | `""`                                                    |
| `fullnameOverride` | Override the `fullname` used by the chart                                                                                                                | `""`                                                    |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value                 |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP`           |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`                  |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`                |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`                  |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `["tidewater.local"]` |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`                  |

### Secrets and authentication parameters

| Name                     | Description                                                                                                                                                                                                         | Value       |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `existingSecret.enabled` | Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide) | `false`     |
| `existingSecret.name`    | The name of the existing secret, if enabled                                                                                                                                                                         | `tidewater` |

### General application parameters

| Name                       | Description                            | Value        |
| -------------------------- | -------------------------------------- | ------------ |
| `rails.db_setup_command`   | The command to run during migrations   | `db:migrate` |
| `rails.environment`        |                                        | `production` |
| `rails.log_to_stdout`      |                                        | `true`       |
| `rails.max_threads`        |                                        | `5`          |
| `rails.serve_static_files` | Whether Puma should serve static files | `true`       |

### OAI-PMH endpoint parameters

| Name                  | Description | Value                                     |
| --------------------- | ----------- | ----------------------------------------- |
| `oai.adminEmail`      |             | `tidewater@example.com`                   |
| `oai.metadataProfile` |             | `tag:surfliner.gitlab.io,2022:api/oai_dc` |
| `oai.repositoryName`  |             | `tidewater`                               |
| `oai.sampleId`        |             | `13900`                                   |

### Keypair parameters (for signing requests)

| Name                             | Description | Value               |
| -------------------------------- | ----------- | ------------------- |
| `keypair.mountPath`              |             | `/keys`             |
| `keypair.existingSecret.enabled` |             | `false`             |
| `keypair.existingSecret.name`    |             | `tidewater-keypair` |

### Consumer application parameters

| Name                              | Description                                                              | Value                          |
| --------------------------------- | ------------------------------------------------------------------------ | ------------------------------ |
| `consumer.enabled`                | Whether to enable the consumer application                               | `true`                         |
| `consumer.replicaCount`           | How many consumers to run simultaneously                                 | `1`                            |
| `consumer.logLevel`               |                                                                          | `info`                         |
| `consumer.existingSecret.enabled` |                                                                          | `false`                        |
| `consumer.existingSecret.name`    |                                                                          | `tidewater-consumer`           |
| `consumer.rabbitmq.host`          | The RabbitMQ host to connect to                                          | `rabbitmq`                     |
| `consumer.rabbitmq.password`      |                                                                          | `surfliner`                    |
| `consumer.rabbitmq.port`          |                                                                          | `5672`                         |
| `consumer.rabbitmq.queue`         | The RabbitMQ queue to connect to                                         | `tidewater`                    |
| `consumer.rabbitmq.routing_key`   | The RabbitMQ routing key to use                                          | `surfliner.metadata.tidewater` |
| `consumer.rabbitmq.topic`         | The RabbitMQ topic to subscribe to                                       | `surfliner.metadata`           |
| `consumer.rabbitmq.username`      |                                                                          | `surfliner`                    |
| `extraContainerConfiguration`     | An array of additional config options for the container spec             | `[]`                           |
| `extraEnvVars`                    | An array of optional environment variables to inject into the Rails pods | `[]`                           |

### Scaling parameters

| Name           | Description                                                                                               | Value |
| -------------- | --------------------------------------------------------------------------------------------------------- | ----- |
| `replicaCount` | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `1`   |

### Resources and scheduling parameters

| Name                 | Description                                                                                                                                 | Value |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `resources`          | The resource requests and limits                                                                                                            | `{}`  |
| `extraVolumeMounts`  | An array of optional `volumeMounts` for the Rails deployment                                                                                | `[]`  |
| `extraVolumes`       | An array of optional [`Volumes`](https://kubernetes.io/docs/concepts/storage/volumes/) for the Rails deployment                             | `[]`  |
| `affinity`           | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods           | `{}`  |
| `nodeSelector`       | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                           | `{}`  |
| `tolerations`        | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                         | `[]`  |
| `podAnnotations`     | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods    | `{}`  |
| `podSecurityContext` | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods       | `{}`  |
| `securityContext`    | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers | `{}`  |

### PostgreSQL parameters

| Name                                          | Description                                             | Value                |
| --------------------------------------------- | ------------------------------------------------------- | -------------------- |
| `postgresql.enabled`                          | Whether to deploy PostgreSQL                            | `true`               |
| `postgresql.image.repository`                 | The image repository for PostgreSQL                     | `bitnami/postgresql` |
| `postgresql.auth.database`                    | The name of the default database to create on deploy    | `tidewater_db`       |
| `postgresql.auth.password`                    | the password for the PostgreSQL user                    | `tidewater_pass`     |
| `postgresql.auth.username`                    | The name of the PostgreSQL user                         | `tidewater`          |
| `postgresql.auth.postgresPassword`            | the password for the PostgreSQL admin (`postgres`) user | `tidewater_admin`    |
| `postgresql.primary.service.ports.postgresql` | The port to use for the primary service                 | `5432`               |
| `postgresql.primary.persistence.size`         | The size of the PVC to create for the primary DB        | `10Gi`               |
| `postgresql.primary.resources.cpu`            |                                                         | `1000m`              |
| `postgresql.primary.resources.memory`         |                                                         | `1Gi`                |
| `postgresql.readReplicas.persistence.size`    | The size of the PVC to create for the replica DB        | `10Gi`               |
| `postgresql.readReplicas.resources.cpu`       |                                                         | `1000m`              |
| `postgresql.readReplicas.resources.memory`    |                                                         | `1Gi`                |


[logger-severity]:https://ruby-doc.org/stdlib-3.0.0/libdoc/logger/rdoc/Logger/Severity.html
[oai-spec]:https://www.openarchives.org/OAI/openarchivesprotocol.html
[oai-repository]:https://www.openarchives.org/OAI/openarchivesprotocol.html#Repository
[tidewater]:https://gitlab.com/surfliner/surfliner/-/tree/trunk/tidewater
