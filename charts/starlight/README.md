# Starlight

Starlight is a Helm chart that leverages the [starlight][starlight] container
image to support easy deployment via Helm and Kubernetes.

## Introduction

This chart bootstraps a [starlight][starlight] deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ git clone https://gitlab.com/surfliner/surfliner.git
$ cd surfliner
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm repo update
$ helm dependency update charts/starlight
$ helm install --atomic --set starlight.sample_data=true my-release charts/starlight
```

These commands deploy Starlight on the Kubernetes cluster in the default configuration with an admin account and a skeleton sample exhibit. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Data Import and Export

The Starlight Helm chart contains optional configuration to enable the import
and/or export of data for a Helm chart deployment. Some use cases for this
functionality are:

Import:
* Migrating an existing non-k8s Starlight environment to a new Starlight
    environment managed by the Helm chart in k8s.
* Populating a local deployment with an export of an existing Starlight
    environment to explore the range of features. An example of this might be
    the Starlight documentation instance.
* Populating CI/CD Review applications with real data from an existing Starlight
    environment for review by Subject Matter Experts and other stakeholders.

Export:
* Nightly exports of the images and database for recovery. Note, this should not
    necessarily replace any backup strategy you would normally use for your
    applications.
* Export of a Starlight instance, such as the documentation site, for use in
    import use cases elsewhere, as noted above.

Note: If setting up an S3 bucket for import or export, which relies on the `sync` option
in the `aws-cli` tool, you will need to ensure that you set a `ListBucket`
statement as shown below:

```json
{
    "Version": "2012-10-17",
    "Id": "Policy1600725558503",
    "Statement": [
        {
            "Sid": "Stmt000001",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::111111111111:user/StarlightProdData"
            },
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::starlight-backup-test/*"
        },
        {
            "Sid": "Stmt000002",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::111111111111:user/StarlightProdData"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::starlight-backup-test"
        }
    ]
}
```

## Parameters

### Image source parameters

| Name                    | Description                                                                                                                                              | Value                                                    |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------- |
| `image.repository`      | Image repository for the Rails web application                                                                                                           | `registry.gitlab.com/surfliner/surfliner/starlight_web`  |
| `image.name`            | Name of the Rails web application image                                                                                                                  | `starlight_web`                                          |
| `image.pullPolicy`      | Pull policy for the Rails web application image                                                                                                          | `Always`                                                 |
| `image.tag`             | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                                 |
| `util.image.repository` | Image repository for the utility application                                                                                                             | `registry.gitlab.com/surfliner/surfliner/surfliner-util` |
| `util.image.name`       | Name of the utility application image                                                                                                                    | `surfliner-util`                                         |
| `util.image.pullPolicy` | Pull policy for the utility image                                                                                                                        | `Always`                                                 |
| `util.image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                                 |
| `imagePullSecrets`      | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                     |
| `nameOverride`          | Override the default chart name                                                                                                                          | `""`                                                     |
| `fullnameOverride`      | Override the `fullname` used by the chart                                                                                                                | `""`                                                     |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value                 |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP`           |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`                  |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`                |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`                  |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `["starlight.local"]` |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`                  |

### Service Account parameters

| Name                    | Description                                                                                                             | Value  |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create` | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.name`   | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### Secrets and authentication parameters

| Name                     | Description                                                                                                                                                                                                         | Value       |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `existingSecret.enabled` | Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide) | `false`     |
| `existingSecret.name`    | The name of the existing secret, if enabled                                                                                                                                                                         | `starlight` |

### General application parameters

| Name                                                   | Description                                                                                | Value                       |
| ------------------------------------------------------ | ------------------------------------------------------------------------------------------ | --------------------------- |
| `starlight.disableSolrConfigInit`                      | Whether to skip the Solr setup step                                                        | `false`                     |
| `starlight.allow_robots`                               | Whether to allow crawling via robots.txt                                                   | `false`                     |
| `starlight.loadSolrConfigSet`                          | Whether to run Solr configset maintenance init container                                   | `true`                      |
| `starlight.application.name`                           | The application name to use in the UI                                                      | `Starlight`                 |
| `starlight.application.themes`                         | The UI themes to load                                                                      | `ucsb,surfliner,ucsd`       |
| `starlight.analytics`                                  |                                                                                            | `{}`                        |
| `starlight.port`                                       | The HTTP port for the Rails application to listen on                                       | `3000`                      |
| `starlight.rails.db_adapter`                           | The ActiveRecord database adapter to use                                                   | `postgresql`                |
| `starlight.rails.db_setup_command`                     | The command to run during migrations                                                       | `db:migrate`                |
| `starlight.rails.environment`                          |                                                                                            | `production`                |
| `starlight.rails.log_level`                            |                                                                                            | `info`                      |
| `starlight.rails.log_to_stdout`                        |                                                                                            | `true`                      |
| `starlight.rails.max_threads`                          |                                                                                            | `5`                         |
| `starlight.rails.queue`                                | The background jobs adapter to use                                                         | `sidekiq`                   |
| `starlight.rails.serve_static_files`                   | Whether Puma should serve static files                                                     | `true`                      |
| `starlight.sample_data`                                | Whether to load sample data on deploy                                                      | `false`                     |
| `starlight.sitemaps.enabled`                           |                                                                                            | `true`                      |
| `starlight.storage.enabled`                            | Whether to enable S3 storage                                                               | `true`                      |
| `starlight.storage.accessKey`                          |                                                                                            | `starlight-access-key`      |
| `starlight.storage.acl`                                | The default ACL to apply to uploaded files                                                 | `bucket-owner-full-control` |
| `starlight.storage.asset_host_public`                  | Whether generated S3 URLs are public or need auth parameters                               | `false`                     |
| `starlight.storage.bucket`                             |                                                                                            | `starlight`                 |
| `starlight.storage.endpointUrl`                        | Optional for AWS, required for MinIO                                                       | `""`                        |
| `starlight.storage.region`                             |                                                                                            | `us-west-2`                 |
| `starlight.storage.secretKey`                          |                                                                                            | `starlight-secret-key`      |
| `starlight.backups.import.enabled`                     | Whether to import an existing backup on deployment                                         | `false`                     |
| `starlight.backups.import.force`                       | Whether to override existing data                                                          | `false`                     |
| `starlight.backups.import.solrReindex`                 | Whether to run a full Solr reindex                                                         | `false`                     |
| `starlight.backups.import.dbBackupSource`              | S3 or local file path for importing psql database backup source                            | `""`                        |
| `starlight.backups.import.dbBackupDestination`         | S3 or local file path for DB backup destination file                                       | `""`                        |
| `starlight.backups.import.oldAppUrl`                   | URL for previous system, used for IIIF URL migration                                       | `""`                        |
| `starlight.backups.import.dbBackupSource`              | S3 or local file path for psql database backup destination file                            | `""`                        |
| `starlight.backups.import.sourcePath`                  | S3 or local directory path for importing image upload backup source                        | `""`                        |
| `starlight.backups.export.enabled`                     | Whether to install a cron job to run data exports                                          | `false`                     |
| `starlight.backups.export.dbBackupSource`              | S3 or local file path for DB backup source file                                            | `""`                        |
| `starlight.backups.export.dbBackupDestination`         | S3 or local file path for DB backup destination file                                       | `""`                        |
| `starlight.backups.export.destinationPath`             | S3 path for image backups                                                                  | `""`                        |
| `starlight.backups.export.schedule`                    |                                                                                            | `30 8 * * *`                |
| `starlight.backups.export.sourcePath`                  | S3 or local directory path to source path                                                  | `""`                        |
| `starlight.email.from_address`                         | Required field                                                                             | `starlight@example.edu`     |
| `starlight.email.delivery_method`                      | Set to `smtp` in production                                                                | `letter_opener_web`         |
| `starlight.email.smtp_settings`                        | See <https://guides.rubyonrails.org/action_mailer_basics.html#action-mailer-configuration> | `{}`                        |
| `starlight.auth.method`                                | Set to [`google`](../../docs/themanual/auth/README.md) in production                       | `developer`                 |
| `starlight.auth.google.api_id`                         |                                                                                            | `""`                        |
| `starlight.auth.google.api_secret`                     |                                                                                            | `""`                        |
| `starlight.iiif.external.enabled`                      | Whether to use Cantaloupe instead of riiif                                                 | `false`                     |
| `deviseGuestsCronJob.enabled`                          | Whether to periodically remove Devise guest accounts                                       | `false`                     |
| `deviseGuestsCronJob.schedule`                         |                                                                                            | `0 8 * * *`                 |
| `deviseGuestsCronJob.backoffLimit`                     |                                                                                            | `3`                         |
| `deviseGuestsCronJob.successfulJobsHistoryLimit`       |                                                                                            | `5`                         |
| `deviseGuestsCronJob.failedJobsHistoryLimit`           |                                                                                            | `10`                        |
| `blacklightSearchesCronJob.enabled`                    | Whether to periodically remove blacklight search entries                                   | `false`                     |
| `blacklightSearchesCronJob.schedule`                   |                                                                                            | `0 3 * * *`                 |
| `blacklightSearchesCronJob.backoffLimit`               |                                                                                            | `3`                         |
| `blacklightSearchesCronJob.successfulJobsHistoryLimit` |                                                                                            | `5`                         |
| `blacklightSearchesCronJob.failedJobsHistoryLimit`     |                                                                                            | `10`                        |

### Telemetry parameters

| Name                      | Description | Value                              |
| ------------------------- | ----------- | ---------------------------------- |
| `telemetry.enabled`       |             | `false`                            |
| `telemetry.otel_endpoint` |             | `http://otel-collector.local:4318` |

### Worker parameters

| Name                                       | Description | Value  |
| ------------------------------------------ | ----------- | ------ |
| `workerReadinessProbe.enabled`             |             | `true` |
| `workerReadinessProbe.initialDelaySeconds` |             | `10`   |
| `workerReadinessProbe.periodSeconds`       |             | `2`    |
| `workerReadinessProbe.timeoutSeconds`      |             | `1`    |
| `workerReadinessProbe.failureThreshold`    |             | `10`   |
| `workerReadinessProbe.successThreshold`    |             | `2`    |

### Additional variable parameters

| Name                          | Description                                                              | Value |
| ----------------------------- | ------------------------------------------------------------------------ | ----- |
| `extraEnvVars`                | An array of optional environment variables to inject into the Rails pods | `[]`  |
| `extraContainerConfiguration` | An array of additional config options for the container spec             | `[]`  |

### Scaling parameters

| Name                                         | Description                                                                                                                  | Value   |
| -------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | ------- |
| `workerReplicaCount`                         | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy for the worker pod | `1`     |
| `autoscaling.enabled`                        | Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)                                  | `false` |
| `autoscaling.minReplicas`                    | The minimum number of replicas                                                                                               | `2`     |
| `autoscaling.maxReplicas`                    | The maximum number of replicas                                                                                               | `20`    |
| `autoscaling.targetCPUUtilizationPercentage` |                                                                                                                              | `80`    |

### Resources and scheduling parameters

| Name                              | Description                                                                                                                                 | Value    |
| --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| `affinity`                        | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods           | `{}`     |
| `nodeSelector`                    | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                           | `{}`     |
| `tolerations`                     | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                         | `[]`     |
| `podAnnotations`                  | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods    | `{}`     |
| `podSecurityContext`              | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods       | `{}`     |
| `securityContext`                 | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers | `{}`     |
| `resources.requests.cpu`          |                                                                                                                                             | `500m`   |
| `resources.requests.memory`       |                                                                                                                                             | `500Mi`  |
| `resources.limits.cpu`            |                                                                                                                                             | `1000m`  |
| `resources.limits.memory`         |                                                                                                                                             | `1000Mi` |
| `workerResources.requests.cpu`    |                                                                                                                                             | `200m`   |
| `workerResources.requests.memory` |                                                                                                                                             | `200Mi`  |
| `workerResources.limits.cpu`      |                                                                                                                                             | `500m`   |
| `workerResources.limits.memory`   |                                                                                                                                             | `500Mi`  |
| `extraVolumeMounts`               | An array of optional `volumeMounts` for the Rails deployment                                                                                | `[]`     |
| `extraVolumes`                    | An array of optional [`Volumes`](https://kubernetes.io/docs/concepts/storage/volumes/) for the Rails deployment                             | `[]`     |

### Solr parameters

| Name                                  | Description                                                            | Value                 |
| ------------------------------------- | ---------------------------------------------------------------------- | --------------------- |
| `solr_collection`                     | The name of the Solr collection to create                              | `starlight`           |
| `solr.enabled`                        | Whether to deploy Solr                                                 | `true`                |
| `solr.image.repository`               | The image repository for Solr                                          | `bitnami/solr`        |
| `solr.auth.enabled`                   | Whether to require authentication when connecting to Solr              | `true`                |
| `solr.auth.adminUsername`             | The name of the Solr admin user                                        | `admin`               |
| `solr.auth.adminPassword`             | The password for the Solr admin user                                   | `admin`               |
| `solr.auth.existingSecret`            | The name of the pre-generated secret containing Solr admin credentials | `""`                  |
| `solr.auth.existingSecretPasswordKey` | The key used for the Solr admin password in an existing Secret         | `SOLR_ADMIN_PASSWORD` |
| `solr.coreNames`                      | The default cores to create (ignored if `cloudEnabled`)                | `starlight`           |
| `solr.cloudBootstrap`                 | Whether to enable cloud bootstrap                                      | `true`                |
| `solr.cloudEnabled`                   | Whether to run in cloud mode (as opposed to standalone)                | `true`                |
| `solr.persistence.enabled`            | Whether to enable persistent storage for Solr                          | `true`                |
| `solr.zookeeper.enabled`              | Whether to deploy ZooKeeper                                            | `true`                |
| `solr.zookeeper.persistence.enabled`  | Whether to enable persistent storage for ZooKeeper                     | `true`                |

### External Solr configuration (if `solr.enabled=false`)

| Name                    | Description                                                                       | Value   |
| ----------------------- | --------------------------------------------------------------------------------- | ------- |
| `externalSolr.host`     | The hostname of the external Solr cluster                                         | `""`    |
| `externalSolr.password` | The password to use to authenticate to the external Solr cluster                  | `""`    |
| `externalSolr.port`     | The port that the external Solr cluster is listening on                           | `8983`  |
| `externalSolr.runMode`  | The "mode" that the external Solr cluster is running in (`cloud` or `standalone`) | `cloud` |
| `externalSolr.username` | The username to use to authenticate to the external Solr cluster                  | `""`    |

### Memcached parameters

| Name                            | Description                          | Value               |
| ------------------------------- | ------------------------------------ | ------------------- |
| `memcached.enabled`             | Whether to deploy memcached          | `true`              |
| `memcached.image.repository`    | The image repository for Memcached   | `bitnami/memcached` |
| `memcached.architecture`        |                                      | `high-availability` |
| `memcached.persistence.enabled` | Whether to enable persistent storage | `true`              |
| `memcached.persistence.size`    |                                      | `8Gi`               |

### MinIO parameters

| Name                        | Description                                    | Value                  |
| --------------------------- | ---------------------------------------------- | ---------------------- |
| `minio.enabled`             | Whether to deploy MinIO                        | `false`                |
| `minio.image.repository`    | The image repository for MinIO                 | `bitnami/minio`        |
| `minio.auth.rootUser`       | The name of the root/admin user for MinIO      | `starlight-access-key` |
| `minio.auth.rootPassword`   | the password for the root/admin user           | `starlight-secret-key` |
| `minio.defaultBuckets`      | Buckets (with permissions) to create on deploy | `starlight:download`   |
| `minio.persistence.enabled` | Whether to enable persistent storage for MinIO | `false`                |

### PostgreSQL parameters

| Name                                     | Description                                             | Value                |
| ---------------------------------------- | ------------------------------------------------------- | -------------------- |
| `postgresql.enabled`                     | Whether to deploy PostgreSQL                            | `true`               |
| `postgresql.image.repository`            | The image repository for PostgreSQL                     | `bitnami/postgresql` |
| `postgresql.architecture`                |                                                         | `standalone`         |
| `postgresql.auth.database`               | The name of the default database to create on deploy    | `starlight_db`       |
| `postgresql.auth.password`               | the password for the PostgreSQL user                    | `starlight_pass`     |
| `postgresql.auth.postgresPassword`       | the password for the admin `postgres` user              | `starlight_admin`    |
| `postgresql.auth.username`               | The name of the PostgreSQL user                         | `starlight`          |
| `postgresql.containerPorts.postgresql`   | The port to use for the container                       | `5432`               |
| `postgresql.primary.persistence.enabled` | Whether to enable persistent storage for the primary DB | `false`              |
| `postgresql.primary.persistence.size`    |                                                         | `10Gi`               |

### Redis parameters

| Name                                   | Description                                                       | Value            |
| -------------------------------------- | ----------------------------------------------------------------- | ---------------- |
| `redis.enabled`                        | Whether to deploy Redis                                           | `true`           |
| `redis.image.repository`               | The image repository for Redis                                    | `bitnami/redis`  |
| `redis.auth.enabled`                   | Whether to use password authentication for Redis                  | `false`          |
| `redis.auth.existingSecret`            | The name of the pre-generated secret containing Redis credentials | `""`             |
| `redis.auth.existingSecretPasswordKey` | The key used for the Redis password in an existing Secret         | `REDIS_PASSWORD` |


[starlight]:https://gitlab.com/surfliner/surfliner/-/tree/trunk/starlight
