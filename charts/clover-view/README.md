# Clover View

To deploy manually, create a custom values file if desired, then:

```
helm install my-clover oci://registry.gitlab.com/surfliner/surfliner/charts/clover-view --namespace my-namespace [--values custom-values.yaml]
```

## Parameters

### Chart metadata parameters

| Name               | Description                               | Value |
| ------------------ | ----------------------------------------- | ----- |
| `nameOverride`     | Override the default chart name           | `""`  |
| `fullnameOverride` | Override the `fullname` used by the chart | `""`  |

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                                          |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------- |
| `image.name`       | Name of the Clover application image                                                                                                                     | `daylight-clover_view`                                         |
| `image.pullPolicy` | Pull policy for the image                                                                                                                                | `IfNotPresent`                                                 |
| `image.repository` | Image repository for Clover                                                                                                                              | `registry.gitlab.com/surfliner/surfliner/daylight-clover_view` |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                                       |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                           |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value       |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP` |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`        |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `false`     |
| `ingress.className`   | The ingress class to use; falls back to cluster default                                                                                                 | `""`        |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`        |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `[]`        |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`        |

### Application parameters

| Name         | Description                                      | Value              |
| ------------ | ------------------------------------------------ | ------------------ |
| `public_url` | The publically available URL for the application | `http://localhost` |

### Service Account parameters

| Name                         | Description                                                                                                             | Value  |
| ---------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create`      | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.automount`   | Whether to automatically mount the service account token                                                                | `true` |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                               | `{}`   |
| `serviceAccount.name`        | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### Resources and scheduling parameters

| Name                          | Description                                                                                                                                                            | Value  |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ |
| `livenessProbe.enabled`       | Whether to enable a [liveness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for the Rails application  | `true` |
| `livenessProbe.httpGet.path`  | The path to make HTTP requests to                                                                                                                                      | `/`    |
| `livenessProbe.httpGet.port`  | The (named) port to make HTTP requests to                                                                                                                              | `http` |
| `readinessProbe.enabled`      | Whether to enable a [readiness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for the Rails application | `true` |
| `readinessProbe.httpGet.path` | The path to make HTTP requests to                                                                                                                                      | `/`    |
| `readinessProbe.httpGet.port` | The (named) port to make HTTP requests to                                                                                                                              | `http` |
| `affinity`                    | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods                                      | `{}`   |
| `nodeSelector`                | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                                                      | `{}`   |
| `tolerations`                 | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                                                    | `[]`   |
| `podAnnotations`              | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods                               | `{}`   |
| `podLabels`                   | Default [labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/) to apply to Rails application pods                                         | `{}`   |
| `podSecurityContext`          | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods                                  | `{}`   |
| `securityContext`             | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers                            | `{}`   |
| `resources`                   | The resource requests and limits                                                                                                                                       | `{}`   |
| `volumes`                     | An array of optional [`Volumes`](https://kubernetes.io/docs/concepts/storage/volumes/) for the deployment                                                              | `[]`   |
| `volumeMounts`                | An array of optional `volumeMounts` for the deployment                                                                                                                 | `[]`   |

### Scaling parameters

| Name                                            | Description                                                                                               | Value   |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------- |
| `replicaCount`                                  | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `1`     |
| `autoscaling.enabled`                           | Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)               | `false` |
| `autoscaling.minReplicas`                       | The minimum number of replicas                                                                            | `1`     |
| `autoscaling.maxReplicas`                       | The maximum number of replicas                                                                            | `100`   |
| `autoscaling.targetCPUUtilizationPercentage`    |                                                                                                           | `80`    |
| `autoscaling.targetMemoryUtilizationPercentage` |                                                                                                           | `80`    |
