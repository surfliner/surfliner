# Orange Empire

Orange Empire is a Helm chart that leverages a [cantaloupe][cantaloupe] container
image to support easy deployment via Helm and Kubernetes.

To deploy manually, create a custom values file if desired, then:

```
helm install my-empire oci://registry.gitlab.com/surfliner/surfliner/charts/orange-empire --namespace my-namespace [--values custom-values.yaml]
```

## Introduction

This chart bootstraps a [cantaloupe][cantaloupe] deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release charts/orange-empire
```

The command deploys Orange Empire on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

### Chart metadata parameters

| Name               | Description                               | Value |
| ------------------ | ----------------------------------------- | ----- |
| `nameOverride`     | Override the default chart name           | `""`  |
| `fullnameOverride` | Override the `fullname` used by the chart | `""`  |

### Image source parameters

| Name                    | Description                                                                                                                                              | Value                                                    |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------- |
| `image.repository`      | Image repository for Cantaloupe                                                                                                                          | `uclalibrary/cantaloupe`                                 |
| `image.pullPolicy`      | Pull policy for the image                                                                                                                                | `IfNotPresent`                                           |
| `util.image.name`       | Name of the utility image                                                                                                                                | `surfliner-util`                                         |
| `util.image.repository` | Repository for the utility image                                                                                                                         | `registry.gitlab.com/surfliner/surfliner/surfliner-util` |
| `util.image.pullPolicy` | Pull policy for the image                                                                                                                                | `Always`                                                 |
| `util.image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                                 |
| `imagePullSecrets`      | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                     |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value       |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP` |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`        |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`      |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`        |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `[]`        |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`        |

### Application parameters

| Name                                                 | Description                                                                                                                                            | Value                     |
| ---------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------- |
| `existingSecret.enabled`                             | Whether to load environment variables from a Secret generated in advance                                                                               | `false`                   |
| `existingSecret.name`                                | The name of the pre-existing secret                                                                                                                    | `orange-empire`           |
| `noPreAuth`                                          | Optional array of services that should skip the `pre_authorize` step of the delegation service                                                         | `["starlight"]`           |
| `superskunkApiBase`                                  | Base URL (including protocol) of the Superskunk API                                                                                                    | `http://superskunk`       |
| `cantaloupe.port`                                    | The port for the Cantaloupe server to listen on                                                                                                        | `8182`                    |
| `cantaloupe.admin.endpointEnabled`                   | Whether to enable the admin API endpoint                                                                                                               | `true`                    |
| `cantaloupe.admin.secret`                            | The name of the Secret containing admin credentials for Cantaloupe                                                                                     | `admin-password`          |
| `cantaloupe.cacheServer.derivativeEnabled`           | Whether to enable caching of generated derivatives                                                                                                     | `true`                    |
| `cantaloupe.cacheServer.derivative`                  | The [kind of cache](https://cantaloupe-project.github.io/manual/5.0/caching.html#Implementations) to use for generated derivatives                     | `S3Cache`                 |
| `cantaloupe.cacheServer.derivativeTTLSeconds`        | The TTL for the derivative cache                                                                                                                       | `2592000`                 |
| `cantaloupe.cacheServer.purgeMissing`                | Whether to delete the cache for images that can't be found                                                                                             | `true`                    |
| `cantaloupe.delegate.enabled`                        | Whether to enable the [delegate script](https://cantaloupe-project.github.io/manual/5.0/delegate-system.html#Delegate%20Script)                        | `true`                    |
| `cantaloupe.delegate.mountPath`                      | The path at which to write the delegate script                                                                                                         | `/mnt/cantaloupe`         |
| `cantaloupe.delegate.filename`                       | The filename of the delegate script                                                                                                                    | `delegate.rb`             |
| `cantaloupe.logLevel`                                |                                                                                                                                                        | `debug`                   |
| `cantaloupe.processorSelectionStrategy`              | See <https://cantaloupe-project.github.io/manual/5.0/processors.html#Selection%20Strategies>                                                           | `ManualSelectionStrategy` |
| `cantaloupe.manualProcessorJP2`                      | The [processor](https://cantaloupe-project.github.io/manual/5.0/processors.html#KakaduNativeProcessor) to use for JPEG2000                             | `KakaduNativeProcessor`   |
| `cantaloupe.sourceStatic`                            | The [source](https://cantaloupe-project.github.io/manual/5.0/sources.html#Static%20Source) to supply all requests                                      | `S3Source`                |
| `cantaloupe.java.heapSize`                           |                                                                                                                                                        | `1g`                      |
| `cantaloupe.s3.cache.accessKeyId`                    | The access key ID for the S3 cache bucket                                                                                                              | `cantaloupe-access-key`   |
| `cantaloupe.s3.cache.bucketName`                     | The name of the S3 cache bucket                                                                                                                        | `cantaloupe-cache`        |
| `cantaloupe.s3.cache.secretKey`                      | The secret key for the S3 cache bucket                                                                                                                 | `cantaloupe-secret`       |
| `cantaloupe.s3.source.accessKeyId`                   | The access key ID for the S3 [source](https://cantaloupe-project.github.io/manual/5.0/sources.html) bucket                                             | `cantaloupe-access-key`   |
| `cantaloupe.s3.source.bucketMap`                     | A string mapping services that will be making requests to the bucket that should be used for those requests; e.g., "service1:bucket1,service2:bucket2" | `""`                      |
| `cantaloupe.s3.source.basicLookupStrategyPathSuffix` | Path or extension added to the end of identifiers                                                                                                      | `""`                      |
| `cantaloupe.s3.source.secretKey`                     | The secret key for the S3 source bucket                                                                                                                | `cantaloupe-secret`       |

### Service Account parameters

| Name                         | Description                                                                                                             | Value  |
| ---------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create`      | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                               | `{}`   |
| `serviceAccount.name`        | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### MinIO parameters

| Name                        | Description                                    | Value                                                |
| --------------------------- | ---------------------------------------------- | ---------------------------------------------------- |
| `minio.enabled`             | Whether to deploy MinIO                        | `true`                                               |
| `minio.image.repository`    | The image repository for MinIO                 | `bitnami/minio`                                      |
| `minio.defaultBuckets`      | The buckets to create on deploy                | `cantaloupe-source:download,cantaloupe-cache:public` |
| `minio.auth.rootUser`       | The name of the root/admin user for MinIO      | `cantaloupe-access-key`                              |
| `minio.auth.rootPassword`   | the password for the root/admin user           | `cantaloupe-secret`                                  |
| `minio.persistence.enabled` | Whether to enable persistent storage for MinIO | `false`                                              |

### Resources and scheduling parameters

| Name                 | Description                                                                                                                                 | Value |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `affinity`           | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods           | `{}`  |
| `nodeSelector`       | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                           | `{}`  |
| `tolerations`        | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                         | `[]`  |
| `podAnnotations`     | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods    | `{}`  |
| `podSecurityContext` | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods       | `{}`  |
| `securityContext`    | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers | `{}`  |
| `resources`          | The resource requests and limits                                                                                                            | `{}`  |

### Scaling parameters

| Name                                            | Description                                                                                               | Value   |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------- |
| `replicaCount`                                  | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `1`     |
| `autoscaling.enabled`                           | Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)               | `false` |
| `autoscaling.minReplicas`                       | The minimum number of replicas                                                                            | `1`     |
| `autoscaling.maxReplicas`                       | The maximum number of replicas                                                                            | `100`   |
| `autoscaling.targetCPUUtilizationPercentage`    |                                                                                                           | `80`    |
| `autoscaling.targetMemoryUtilizationPercentage` |                                                                                                           | `80`    |

[cantaloupe]:https://github.com/UCLALibrary/docker-cantaloupe
[delegate]:https://cantaloupe-project.github.io/manual/5.0/delegate-system.html
