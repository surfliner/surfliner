# Default values for orange-empire.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

## @section Chart metadata parameters
#
## @param nameOverride Override the default chart name
nameOverride: ""
## @param fullnameOverride Override the `fullname` used by the chart
fullnameOverride: ""

## @section Image source parameters

# This sets the container image; more information can be found here:
# https://kubernetes.io/docs/concepts/containers/images/
#
## @param image.repository Image repository for Cantaloupe
## @param image.pullPolicy Pull policy for the image
## @skip image.tag Overrides the image tag whose default is the chart appVersion.
image:
  repository: uclalibrary/cantaloupe
  pullPolicy: IfNotPresent
  tag: 5.0.6-1

## @param util.image.name Name of the utility image
## @param util.image.repository Repository for the utility image
## @param util.image.pullPolicy Pull policy for the image
## @param util.image.tag Overrides the image tag whose default is the chart appVersion.
util:
  image:
    name: surfliner-util
    repository: registry.gitlab.com/surfliner/surfliner/surfliner-util
    pullPolicy: Always
    tag: stable

## @param imagePullSecrets An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
imagePullSecrets: []

## @section Traffic exposure parameters
#
## @param service.type The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application
## @param service.port The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service
service:
  type: ClusterIP
  port: 80

## @param ingress.enabled Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)
## @param ingress.annotations Additional annotations for the ingress
## @param ingress.hosts An array of hosts to manage with the ingress
## @param ingress.tls An array of TLS options
ingress:
  enabled: true
  annotations: {}
    # cert-manager.io/cluster-issuer: letsencrypt-prod
    # kubernetes.io/ingress.class: nginx
  hosts: []
    # - host: chart.local
    #   paths:
    #     - path: /
    #       pathType: ImplementationSpecific
  tls: []
    # - hosts:
    #     - iiif-test.eks.dld.library.ucsb.edu
    #   secretName: tls-secret

## @section Application parameters
#
## @param existingSecret.enabled Whether to load environment variables from a Secret generated in advance
## @param existingSecret.name The name of the pre-existing secret
existingSecret:
  enabled: false
  name: orange-empire

## @param noPreAuth Optional array of services that should skip the `pre_authorize` step of the delegation service
noPreAuth:
  - starlight

## @param superskunkApiBase Base URL (including protocol) of the Superskunk API
superskunkApiBase: http://superskunk

cantaloupe:
  ## @param cantaloupe.port The port for the Cantaloupe server to listen on
  port: 8182
  ## @param cantaloupe.admin.endpointEnabled Whether to enable the admin API endpoint
  ## @param cantaloupe.admin.secret The name of the Secret containing admin credentials for Cantaloupe
  admin:
    endpointEnabled: true
    secret: admin-password
  ## @param cantaloupe.cacheServer.derivativeEnabled Whether to enable caching of generated derivatives
  ## @param cantaloupe.cacheServer.derivative The [kind of cache](https://cantaloupe-project.github.io/manual/5.0/caching.html#Implementations) to use for generated derivatives
  ## @param cantaloupe.cacheServer.derivativeTTLSeconds The TTL for the derivative cache
  ## @param cantaloupe.cacheServer.purgeMissing Whether to delete the cache for images that can't be found
  cacheServer:
    derivativeEnabled: true
    derivative: S3Cache
    derivativeTTLSeconds: "2592000"
    purgeMissing: true
  ## @param cantaloupe.delegate.enabled Whether to enable the [delegate script](https://cantaloupe-project.github.io/manual/5.0/delegate-system.html#Delegate%20Script)
  ## @param cantaloupe.delegate.mountPath The path at which to write the delegate script
  ## @param cantaloupe.delegate.filename The filename of the delegate script
  delegate:
    enabled: true
    mountPath: /mnt/cantaloupe
    filename: delegate.rb
  ## @param cantaloupe.logLevel
  logLevel: debug
  ## @param cantaloupe.processorSelectionStrategy See <https://cantaloupe-project.github.io/manual/5.0/processors.html#Selection%20Strategies>
  processorSelectionStrategy: ManualSelectionStrategy
  ## @param cantaloupe.manualProcessorJP2 The [processor](https://cantaloupe-project.github.io/manual/5.0/processors.html#KakaduNativeProcessor) to use for JPEG2000
  manualProcessorJP2: KakaduNativeProcessor
  ## @param cantaloupe.sourceStatic The [source](https://cantaloupe-project.github.io/manual/5.0/sources.html#Static%20Source) to supply all requests
  sourceStatic: S3Source
  ## @param cantaloupe.java.heapSize
  java:
    heapSize: 1g
  s3:
    ## @param cantaloupe.s3.cache.accessKeyId The access key ID for the S3 cache bucket
    ## @param cantaloupe.s3.cache.bucketName The name of the S3 cache bucket
    ## @param cantaloupe.s3.cache.secretKey The secret key for the S3 cache bucket
    cache:
      accessKeyId: cantaloupe-access-key
      bucketName: cantaloupe-cache
      secretKey: cantaloupe-secret
    ## @param cantaloupe.s3.source.accessKeyId The access key ID for the S3 [source](https://cantaloupe-project.github.io/manual/5.0/sources.html) bucket
    ## @param cantaloupe.s3.source.bucketMap A string mapping services that will be making requests to the bucket that should be used for those requests; e.g., "service1:bucket1,service2:bucket2"
    ## @param cantaloupe.s3.source.basicLookupStrategyPathSuffix Path or extension added to the end of identifiers
    ## @param cantaloupe.s3.source.secretKey The secret key for the S3 source bucket
    source:
      accessKeyId: cantaloupe-access-key
      bucketMap: ""
      basicLookupStrategyPathSuffix: ""
      secretKey: cantaloupe-secret

## @section Service Account parameters
#
## @param serviceAccount.create Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created
## @param serviceAccount.annotations Annotations to add to the service account
## @param serviceAccount.name The name of the service account to use (if not set and create is true, a name is generated using the fullname template)
serviceAccount:
  create: true
  annotations: {}
  name: ""

## @section MinIO parameters

# Configuration for minio chart
# This is currently only used for Review applications in CI/CD pipelines
# see: https://github.com/bitnami/charts/blob/master/bitnami/minio/README.md
#
minio:
  ## @param minio.enabled Whether to deploy MinIO
  enabled: true
  ## @param minio.image.repository The image repository for MinIO
  ## @skip minio.image.tag The MinIO image tag to use
  image:
    repository: bitnami/minio
    tag: 2025.1.20-debian-12-r0
  ## @param minio.defaultBuckets The buckets to create on deploy
  defaultBuckets: cantaloupe-source:download,cantaloupe-cache:public
  ## @param minio.auth.rootUser The name of the root/admin user for MinIO
  ## @param minio.auth.rootPassword the password for the root/admin user
  auth:
    rootUser: cantaloupe-access-key
    rootPassword: cantaloupe-secret
    ## @param minio.persistence.enabled Whether to enable persistent storage for MinIO
  persistence:
    enabled: false

## @section Resources and scheduling parameters
#
## @param affinity The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods
affinity: {}

## @param nodeSelector The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods
nodeSelector: {}

## @param tolerations The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods
tolerations: []

## @param podAnnotations Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods
podAnnotations: {}

## @param podSecurityContext The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods
podSecurityContext: {}
  # fsGroup: 2000

## @param securityContext The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers
securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

## @param resources The resource requests and limits
resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

## @section Scaling parameters
#
## @param replicaCount The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy
replicaCount: 1

## @param autoscaling.enabled Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)
## @param autoscaling.minReplicas The minimum number of replicas
## @param autoscaling.maxReplicas The maximum number of replicas
## @param autoscaling.targetCPUUtilizationPercentage
## @param autoscaling.targetMemoryUtilizationPercentage
autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  targetMemoryUtilizationPercentage: 80
