{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "shoreline.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "shoreline.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "shoreline.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "shoreline.labels" -}}
app.kubernetes.io/name: {{ include "shoreline.name" . }}
helm.sh/chart: {{ include "shoreline.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Supports using an existing secret instead of one built using the Chart
*/}}
{{- define "shoreline.secretName" -}}
{{ include "common.secretName" . }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "shoreline.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
{{ default (include "shoreline.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
{{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{- define "shoreline.email.fullname" -}}
{{- printf "%s-%s" .Release.Name "email" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "shoreline.geoserver.hostname" -}}
{{- if .Values.geoserver.enabled -}}
{{- .Values.geoserver.ingress.hosts | first -}}
{{- else -}}
{{- .Values.geoserver.geoserverHostname -}}
{{- end -}}
{{- end -}}

{{- define "shoreline.geoserver.internal_hostname" -}}
{{- if .Values.geoserver.enabled -}}
{{- printf "%s-%s" .Release.Name "geoserver" | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Values.geoserver.geoserverHostname -}}
{{- end -}}
{{- end -}}

{{- define "shoreline.geoserver.port" -}}
{{- if (and .Values.geoserver.enabled .Values.geoserver.ingress.tls) -}}
{{- "443" -}}
{{- else -}}
{{- .Values.geoserver.service.port | default "80" -}}
{{- end -}}
{{- end -}}

{{- define "shoreline.geoserver.scheme" -}}
{{- if .Values.geoserver.enabled -}}
{{- if .Values.geoserver.ingress.tls -}}
{{- "https" -}}
{{- else -}}
{{- "http" -}}
{{- end -}}
{{- else -}}
{{- .Values.geoserver.geoserverScheme | default "http" -}}
{{- end -}}
{{- end -}}

{{- define "shoreline.geoserver.internal_url" -}}
{{- if .Values.geoserver.enabled -}}
{{- printf "http://%s" (include "shoreline.geoserver.internal_hostname" . ) -}}
{{- else -}}
{{- include "shoreline.geoserver.url" . -}}
{{- end -}}
{{- end -}}

{{- define "shoreline.geoserver.url" -}}
{{- $scheme := ( include "shoreline.geoserver.scheme" .) -}}
{{- $hostname := ( include "shoreline.geoserver.hostname" .) -}}
{{- $port := ( include "shoreline.geoserver.port" .) -}}
{{- printf "%s://%s:%s" $scheme $hostname $port -}}
{{- end -}}

{{- define "shoreline.postgresql.fullname" -}}
{{ include "common.postgresql.fullname" . }}
{{- end -}}

{{- define "shoreline.minio.fullname" -}}
{{- printf "%s-%s" .Release.Name "minio" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "shoreline.solr.cloudEnabled" -}}
{{ include "common.solr.cloudEnabled" . }}
{{- end -}}

{{- define "shoreline.solr.fullname" -}}
{{ include "common.solr.fullname" . }}
{{- end -}}

{{- define "shoreline.zk.fullname" -}}
{{ include "common.zk.fullname" . }}
{{- end -}}
