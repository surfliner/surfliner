# Shoreline

## Introduction

This chart bootstraps a
[Shoreline](https://gitlab.com/surfliner/surfliner/-/tree/trunk/shoreline)
deployment on a [Kubernetes](http://kubernetes.io) cluster using the
[Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `shoreline`:

```console
$ git clone https://gitlab.com/surfliner/surfliner.git
$ cd surfliner
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm repo update
$ helm dependency update charts/shoreline
$ helm install --wait --set consumer.enabled=false shoreline charts/shoreline
```

These commands deploy Shoreline on the Kubernetes cluster in the standalone,
default configuration.  If integration with Comet is required, remove the '--set
consumer.enabled=false' and install or upgrade Shoreline after Comet has been
installed. The [Parameters](#parameters) section lists the parameters that can
be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## GeoServer

If [GeoServer](../geoserver) is enabled via `geoserver.enabled` then it is
desirable for ingest network traffic to be routing internally in Kubernetes to
avoid cost for moving data in a cloud environment.

We use two environment variables to distinguish between Geoserver traffic routed
internally and externally.

- `GEOSERVER_URL` – Used for Solr document indexing and Leaflet to render and
  pull data for clients/users.

- `GEOSERVER_INTERNAL_URL` – Used for ingest of content into Geoserver. If
  `geoserver.enabled=true` then this will use the internal hostname for the
  deployment. If `false` this value will be the same as `GEOSERVER_URL`

## SolrCloud vs. Standalone Solr

You have the option to run an instance of Solr either as a part of the helm
install or not managed by the chart (external). This is configured using the
`solr.enabled` value.

When set to `true`, the Solr environment that comes with the chart is SolrCloud,
and the chart will automatically set-up and configure the application to
integrate with this instance.

When set to `false`, you have the option to run an instance of Solr using either
SolrCloud or standalone. This is configured using the `solrRunMode` value. You
also need to set additional values to configure the application to integrate
with the external Solr instance (reference the solr parameters chart above).

Differences to note about SolrCloud and Standalone:

- Collections versus Cores
  - SolrCloud uses the concept of _collections_ whereas Standalone uses
    _cores_. This is why we have the two values `solr.collectionName` and
    `solr.coreName` - be sure to se these accordingly.

- Zookeeper
  - SolrCloud comprises a set of Solr nodes and Zookeeper nodes. If you are
    running Standalone Solr, zookeeper nodes are not present and those values
    are not needed.

## Solr Authentication

If you are using an external Solr environment, you have the option to use or
disable Basic Authentication for the appliation to access Solr. Be sure to set
the `solr.auth` parameters appropriately.

## Parameters

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                               |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- |
| `image.repository` | Image repository for the Rails web application                                                                                                           | `registry.gitlab.com/surfliner/surfliner/shoreline` |
| `image.name`       | Name of the Rails web application image                                                                                                                  | `shoreline`                                         |
| `image.pullPolicy` | Pull policy for the Rails web application image                                                                                                          | `Always`                                            |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                            |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                |
| `nameOverride`     | Override the default chart name                                                                                                                          | `""`                                                |
| `fullnameOverride` | Override the `fullname` used by the chart                                                                                                                | `""`                                                |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value                 |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP`           |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`                  |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`                |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`                  |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `["shoreline.local"]` |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`                  |

### Service Account parameters

| Name                    | Description                                                                                                             | Value  |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create` | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.name`   | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### Secrets and authentication parameters

| Name                     | Description                                                                                                                                                                                                         | Value       |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `existingSecret.enabled` | Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide) | `false`     |
| `existingSecret.name`    | The name of the existing secret, if enabled                                                                                                                                                                         | `shoreline` |

### General application parameters

| Name                                  | Description                                                                                | Value                       |
| ------------------------------------- | ------------------------------------------------------------------------------------------ | --------------------------- |
| `shoreline.disableSolrConfigInit`     | Whether to skip the Solr setup step                                                        | `false`                     |
| `shoreline.db_setup_command`          | The command to run during migrations                                                       | `db:migrate`                |
| `shoreline.geoblacklightDownloadPath` | The cache directory for GeoBlacklight-generated downloads                                  | `/data/tmp/cache/downloads` |
| `shoreline.theme`                     | The UI theme to use                                                                        | `ucsb`                      |
| `shoreline.sample_data`               | Whether to ingest fixture data during deployment                                           | `false`                     |
| `shoreline.suppressTools`             | Whether to hide the tools UI elements                                                      | `false`                     |
| `shoreline.email.contact_email`       |                                                                                            | `shoreline@example.edu`     |
| `shoreline.email.delivery_method`     | Set to `smtp` in production                                                                | `letter_opener_web`         |
| `shoreline.email.smtp_settings`       | See <https://guides.rubyonrails.org/action_mailer_basics.html#action-mailer-configuration> | `{}`                        |

### Telemetry parameters

| Name                      | Description | Value                              |
| ------------------------- | ----------- | ---------------------------------- |
| `telemetry.enabled`       |             | `false`                            |
| `telemetry.otel_endpoint` |             | `http://otel-collector.local:4318` |

### Consumer application parameters

| Name                              | Description                                | Value                          |
| --------------------------------- | ------------------------------------------ | ------------------------------ |
| `consumer.enabled`                | Whether to enable the consumer application | `true`                         |
| `consumer.replicaCount`           | How many consumers to run simultaneously   | `1`                            |
| `consumer.existingSecret.enabled` |                                            | `false`                        |
| `consumer.existingSecret.name`    |                                            | `shoreline-consumer`           |
| `consumer.rabbitmq.host`          | The RabbitMQ host to connect to            | `rabbitmq`                     |
| `consumer.rabbitmq.password`      |                                            | `surfliner`                    |
| `consumer.rabbitmq.port`          |                                            | `5672`                         |
| `consumer.rabbitmq.queue`         | The RabbitMQ queue to connect to           | `shoreline`                    |
| `consumer.rabbitmq.routing_key`   | The RabbitMQ routing key to use            | `surfliner.metadata.shoreline` |
| `consumer.rabbitmq.topic`         | The RabbitMQ topic to subscribe to         | `surfliner.metadata`           |
| `consumer.rabbitmq.username`      |                                            | `surfliner`                    |

### Scaling parameters

| Name           | Description                                                                                               | Value |
| -------------- | --------------------------------------------------------------------------------------------------------- | ----- |
| `replicaCount` | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `1`   |

### Resources and scheduling parameters

| Name                                     | Description                                                                                                                                 | Value            |
| ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| `affinity`                               | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods           | `{}`             |
| `nodeSelector`                           | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                           | `{}`             |
| `tolerations`                            | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                         | `[]`             |
| `podAnnotations`                         | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods    | `{}`             |
| `podSecurityContext.runAsUser`           |                                                                                                                                             | `10000`          |
| `podSecurityContext.runAsGroup`          |                                                                                                                                             | `10001`          |
| `podSecurityContext.fsGroup`             |                                                                                                                                             | `10001`          |
| `podSecurityContext.fsGroupChangePolicy` |                                                                                                                                             | `OnRootMismatch` |
| `securityContext`                        | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers | `{}`             |
| `resources`                              | The resource requests and limits                                                                                                            | `{}`             |

### GeoServer parameters

| Name                                                                        | Description                            | Value                                                    |
| --------------------------------------------------------------------------- | -------------------------------------- | -------------------------------------------------------- |
| `geoserver.enabled`                                                         | Whether to deploy GeoServer            | `true`                                                   |
| `geoserver.admin.username`                                                  |                                        | `admin`                                                  |
| `geoserver.admin.password`                                                  |                                        | `shorelinegeo`                                           |
| `geoserver.persistence.enabled`                                             | Whether to allocate persistent storage | `true`                                                   |
| `geoserver.ingress.enabled`                                                 |                                        | `true`                                                   |
| `geoserver.ingress.annotations.nginx.ingress.kubernetes.io/proxy-body-size` |                                        | `{"nginx.ingress.kubernetes.io/proxy-body-size":"512m"}` |
| `geoserver.ingress.hosts`                                                   |                                        | `["geoserver.local"]`                                    |
| `geoserver.ingress.tls`                                                     |                                        | `[]`                                                     |

### Solr parameters

| Name                                  | Description                                                            | Value                 |
| ------------------------------------- | ---------------------------------------------------------------------- | --------------------- |
| `solr_collection`                     | The name of the Solr collection to create                              | `shoreline`           |
| `solr.enabled`                        | Whether to deploy Solr                                                 | `true`                |
| `solr.image.repository`               | The image repository for Solr                                          | `bitnami/solr`        |
| `solr.auth.enabled`                   | Whether to require authentication when connecting to Solr              | `true`                |
| `solr.auth.adminUsername`             | The name of the Solr admin user                                        | `admin`               |
| `solr.auth.adminPassword`             | The password for the Solr admin user                                   | `admin`               |
| `solr.auth.existingSecret`            | The name of the pre-generated secret containing Solr admin credentials | `""`                  |
| `solr.auth.existingSecretPasswordKey` | The key used for the Solr admin password in an existing Secret         | `SOLR_ADMIN_PASSWORD` |
| `solr.cloudBootstrap`                 | Whether to enable cloud bootstrap                                      | `true`                |
| `solr.cloudEnabled`                   | Whether to run in cloud mode (as opposed to standalone)                | `true`                |
| `solr.coreNames`                      | The default cores to create (ignored if `cloudEnabled`)                | `shoreline`           |
| `solr.persistence.enabled`            | Whether to enable persistent storage for Solr                          | `true`                |
| `solr.zookeeper.enabled`              | Whether to deploy ZooKeeper                                            | `true`                |
| `solr.zookeeper.persistence.enabled`  | Whether to enable persistent storage for ZooKeeper                     | `true`                |

### External Solr configuration (if `solr.enabled=false`)

| Name                    | Description                                                                       | Value   |
| ----------------------- | --------------------------------------------------------------------------------- | ------- |
| `externalSolr.host`     | The hostname of the external Solr cluster                                         | `""`    |
| `externalSolr.password` | The password to use to authenticate to the external Solr cluster                  | `""`    |
| `externalSolr.port`     | The port that the external Solr cluster is listening on                           | `8983`  |
| `externalSolr.runMode`  | The "mode" that the external Solr cluster is running in (`cloud` or `standalone`) | `cloud` |
| `externalSolr.username` | The username to use to authenticate to the external Solr cluster                  | `""`    |

### PostgreSQL parameters

| Name                                     | Description                                             | Value                 |
| ---------------------------------------- | ------------------------------------------------------- | --------------------- |
| `postgresql.enabled`                     | Whether to deploy PostgreSQL                            | `true`                |
| `postgresql.image.repository`            | The image repository for PostgreSQL                     | `bitnami/postgresql`  |
| `postgresql.architecture`                |                                                         | `standalone`          |
| `postgresql.auth.database`               | The name of the default database to create on deploy    | `shoreline-discovery` |
| `postgresql.auth.password`               | the password for the PostgreSQL user                    | `shorelinepass`       |
| `postgresql.auth.username`               | The name of the PostgreSQL user                         | `shoreline-discovery` |
| `postgresql.containerPorts.postgresql`   | The port to use for the container                       | `5432`                |
| `postgresql.primary.persistence.enabled` | Whether to enable persistent storage for the primary DB | `true`                |
| `postgresql.primary.persistence.size`    |                                                         | `10Gi`                |

### MinIO parameters

| Name                        | Description                                    | Value                               |
| --------------------------- | ---------------------------------------------- | ----------------------------------- |
| `minio.enabled`             | Whether to deploy MinIO                        | `false`                             |
| `minio.image.repository`    | The image repository for MinIO                 | `bitnami/minio`                     |
| `minio.auth.rootUser`       | The name of the root/admin user for MinIO      | `shoreline-access-key`              |
| `minio.auth.rootPassword`   | the password for the root/admin user           | `shoreline-secret-key`              |
| `minio.defaultBuckets`      | Buckets (with permissions) to create on deploy | `shoreline-staging-area-dev:public` |
| `minio.persistence.enabled` | Whether to enable persistent storage for MinIO | `false`                             |
