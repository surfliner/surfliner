# Superskunk

## TL;DR;

```console
$ git clone https://gitlab.com/surfliner/surfliner.git
$ helm dep update charts/superskunk
$ helm install my-release charts/superskunk
```

## Introduction

This chart bootstraps a
[Superskunk](https://gitlab.com/surfliner/surfliner/-/tree/trunk/superskunk)
deployment on a [Kubernetes](http://kubernetes.io) cluster using the
[Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release charts/superskunk
```

The command deploys Superskunk on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                                |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------- |
| `image.repository` | Image repository for the Rails web application                                                                                                           | `registry.gitlab.com/surfliner/surfliner/superskunk` |
| `image.name`       | Name of the Rails web application image                                                                                                                  | `superskunk`                                         |
| `image.pullPolicy` | Pull policy for the Rails web application image                                                                                                          | `Always`                                             |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                             |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                                 |
| `nameOverride`     | Override the default chart name                                                                                                                          | `""`                                                 |
| `fullnameOverride` | Override the `fullname` used by the chart                                                                                                                | `""`                                                 |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value       |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP` |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`        |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`      |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`        |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `[]`        |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`        |

### Service Account parameters

| Name                         | Description                                                                                                             | Value  |
| ---------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ------ |
| `serviceAccount.create`      | Specifies whether a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) should be created | `true` |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                               | `{}`   |
| `serviceAccount.name`        | The name of the service account to use (if not set and create is true, a name is generated using the fullname template) | `""`   |

### Secrets and authentication parameters

| Name                     | Description                                                                                                                                                                                                         | Value        |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------ |
| `existingSecret.enabled` | Whether to use an existing secret for populating auth-related environment variables (see [templates/secrets-static.yaml](./templates/secrets-static.yaml) for the values that a pre-existing secret should provide) | `false`      |
| `existingSecret.name`    | The name of the existing secret, if enabled                                                                                                                                                                         | `superskunk` |

### General application parameters

| Name                                   | Description                                                      | Value               |
| -------------------------------------- | ---------------------------------------------------------------- | ------------------- |
| `superskunk.allow_robots`              | Allow crawling in robots.txt config                              | `false`             |
| `superskunk.metadata_models`           | M3 models to load                                                | `["test-metadata"]` |
| `superskunk.port`                      |                                                                  | `3000`              |
| `superskunk.db.standalone`             | Whether to deploy with its own DB instead of connecting to Comet | `false`             |
| `superskunk.db.metadata_database_name` |                                                                  | `comet_metadata`    |
| `superskunk.rails.environment`         |                                                                  | `production`        |
| `superskunk.rails.log_to_stdout`       |                                                                  | `true`              |
| `superskunk.rails.serve_static_files`  |                                                                  | `true`              |
| `superskunk.comet_base`                | Base URL of Comet instance, for building queries                 | `http://comet:3000` |
| `superskunk.comet_external_base`       | If using an internal host for comet_base                         | `""`                |

### Additional variable parameters

| Name           | Description                                                              | Value |
| -------------- | ------------------------------------------------------------------------ | ----- |
| `extraEnvVars` | An array of optional environment variables to inject into the Rails pods | `[]`  |

### Consumer key parameters

| Name                          | Description                                                               | Value           |
| ----------------------------- | ------------------------------------------------------------------------- | --------------- |
| `consumers.keysFileMountPath` | Path to mount file container app/key yaml file for application            | `/config`       |
| `consumers.mountPath`         | Path to mount consumer public keys                                        | `/keys`         |
| `consumers.publicKey`         | Name of publicKey in the provided `Secrets`                               | `ssh-publickey` |
| `consumers.defaultKeyEnabled` | Whether to use the default provided key for `tidewater` (for review apps) | `false`         |
| `consumers.keys`              | Array of `name` and `secretName` pairs for each consumer                  | `[]`            |

### Scaling parameters

| Name                                            | Description                                                                                               | Value   |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------- |
| `replicaCount`                                  | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy | `1`     |
| `autoscaling.enabled`                           | Whether to enable [autoscaling](https://kubernetes.io/docs/concepts/workloads/autoscaling/)               | `false` |
| `autoscaling.minReplicas`                       | The minimum number of replicas                                                                            | `1`     |
| `autoscaling.maxReplicas`                       | The maximum number of replicas                                                                            | `100`   |
| `autoscaling.targetCPUUtilizationPercentage`    |                                                                                                           | `80`    |
| `autoscaling.targetMemoryUtilizationPercentage` |                                                                                                           | `80`    |

### Resources and scheduling parameters

| Name                 | Description                                                                                                                                 | Value |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `affinity`           | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods           | `{}`  |
| `nodeSelector`       | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                           | `{}`  |
| `tolerations`        | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                         | `[]`  |
| `podAnnotations`     | Default [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) to apply to Rails application pods    | `{}`  |
| `podSecurityContext` | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods       | `{}`  |
| `securityContext`    | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers | `{}`  |
| `resources`          | The resource requests and limits                                                                                                            | `{}`  |

### PostgreSQL parameters

| Name                                                | Description                                          | Value                |
| --------------------------------------------------- | ---------------------------------------------------- | -------------------- |
| `postgresql.enabled`                                | Whether to deploy PostgreSQL                         | `true`               |
| `postgresql.image.repository`                       | The image repository for PostgreSQL                  | `bitnami/postgresql` |
| `postgresql.postgresqlHostname`                     | External PSQL host if `enabled=false`                | `""`                 |
| `postgresql.auth.database`                          | The name of the default database to create on deploy | `comet_metadata`     |
| `postgresql.auth.password`                          | the password for the PostgreSQL user                 | `superskunk_pass`    |
| `postgresql.auth.postgresPassword`                  | the password for the admin `postgres` user           | `superskunk_admin`   |
| `postgresql.auth.username`                          | The name of the PostgreSQL user                      | `superskunk`         |
| `postgresql.primary.service.ports.postgresql`       | The port to use for the primary service              | `5432`               |
| `postgresql.primary.persistence.size`               | The size of the PVC to create for the primary DB     | `10Gi`               |
| `postgresql.primary.resources.requests.cpu`         |                                                      | `1000m`              |
| `postgresql.primary.resources.requests.memory`      |                                                      | `1Gi`                |
| `postgresql.readReplicas.persistence.size`          | The size of the PVC to create for the replica DB     | `10Gi`               |
| `postgresql.readReplicas.resources.requests.cpu`    |                                                      | `1000m`              |
| `postgresql.readReplicas.resources.requests.memory` |                                                      | `1Gi`                |

