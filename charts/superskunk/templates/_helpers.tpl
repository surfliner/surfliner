{{/*
Expand the name of the chart.
*/}}
{{- define "superskunk.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "superskunk.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "superskunk.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "superskunk.labels" -}}
helm.sh/chart: {{ include "superskunk.chart" . }}
{{ include "superskunk.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "superskunk.selectorLabels" -}}
app.kubernetes.io/name: {{ include "superskunk.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "superskunk.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "superskunk.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Supports using an existing secret instead of one built using the Chart
*/}}
{{- define "superskunk.secretName" -}}
{{ include "common.secretName" . }}
{{- end -}}

{{/*
Services
*/}}
{{/*
Create a default fully qualified postgresql name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
Alternatively, if postgresqlHostname is set, we use that which allows for an external database
*/}}
{{- define "superskunk.postgresql.fullname" -}}
{{- if .Values.superskunk.db.standalone -}}
{{ include "common.postgresql.fullname" . }}
{{- else -}}
{{- .Values.postgresql.postgresqlHostname -}}
{{- end -}}
{{- end -}}

{{/*
Set the appropriate database name based on using standalone or external Comet database
*/}}
{{- define "superskunk.postgresql.database" -}}
{{- if .Values.superskunk.db.standalone -}}
{{- .Values.postgresql.auth.database -}}
{{- else -}}
{{- .Values.superskunk.db.metadata_database_name -}}
{{- end -}}
{{- end -}}
