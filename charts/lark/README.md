# Lark Helm chart

## Parameters

### Image source parameters

| Name               | Description                                                                                                                                              | Value                                     |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| `image.repository` | Image repository for the application                                                                                                                     | `registry.gitlab.com/surfliner/surfliner` |
| `image.name`       | Name of the application image                                                                                                                            | `lark_web`                                |
| `image.pullPolicy` | Pull policy for the application image                                                                                                                    | `Always`                                  |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion.                                                                                           | `stable`                                  |
| `imagePullSecrets` | An array of optional secrets to use to [authenticate image pulls](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) | `[]`                                      |
| `nameOverride`     | Override the default chart name                                                                                                                          | `""`                                      |
| `fullnameOverride` | Override the `fullname` used by the chart                                                                                                                | `""`                                      |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value            |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP`      |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`             |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`           |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`             |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `["lark.local"]` |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`             |

### General application parameters

| Name                         | Description                              | Value                                                      |
| ---------------------------- | ---------------------------------------- | ---------------------------------------------------------- |
| `lark.import_data.enabled`   | Whether to import data during deployment | `false`                                                    |
| `lark.import_data.data_file` | The path to the CSV to import            | `/home/lark/app/spec/fixtures/ucsd-batch-2-concept-v4.csv` |
| `persistence_adapters.index` | The storage adapter to use for indexing  | `solr`                                                     |
| `persistence_adapters.event` | The storage adapter to use for events    | `sql`                                                      |

### EzID parameters

| Name            | Description                                   | Value            |
| --------------- | --------------------------------------------- | ---------------- |
| `ezid.enabled`  | Whether to authenticate with the EzID service | `true`           |
| `ezid.shoulder` | The EzID shoulder to use                      | `ark:/99999/fk4` |
| `ezid.username` | The EzID user to authenticate as              | `apitest`        |
| `ezid.password` | The EzID password to authenticate with        | `ezid-password`  |

### Solr parameters

| Name                                  | Description                                                            | Value                 |
| ------------------------------------- | ---------------------------------------------------------------------- | --------------------- |
| `solr_collection`                     | The name of the Solr collection to create                              | `lark`                |
| `solr.enabled`                        | Whether to deploy Solr                                                 | `true`                |
| `solr.image.repository`               | The image repository for Solr                                          | `bitnami/solr`        |
| `solr.auth.enabled`                   | Whether to require authentication when connecting to Solr              | `true`                |
| `solr.auth.adminUsername`             | The name of the Solr admin user                                        | `admin`               |
| `solr.auth.adminPassword`             | The password for the Solr admin user                                   | `admin`               |
| `solr.auth.existingSecret`            | The name of the pre-generated secret containing Solr admin credentials | `""`                  |
| `solr.auth.existingSecretPasswordKey` | The key used for the Solr admin password in an existing Secret         | `SOLR_ADMIN_PASSWORD` |
| `solr.cloudEnabled`                   | Whether to run in cloud mode (as opposed to standalone)                | `true`                |
| `solr.persistence.enabled`            | Whether to enable persistent storage for Solr                          | `true`                |
| `solr.zookeeper.enabled`              | Whether to deploy ZooKeeper                                            | `true`                |
| `solr.zookeeper.persistence.enabled`  | Whether to enable persistent storage for ZooKeeper                     | `true`                |

### External Solr configuration (if `solr.enabled=false`)

| Name                    | Description                                                                       | Value   |
| ----------------------- | --------------------------------------------------------------------------------- | ------- |
| `externalSolr.host`     | The hostname of the external Solr cluster                                         | `""`    |
| `externalSolr.password` | The password to use to authenticate to the external Solr cluster                  | `""`    |
| `externalSolr.port`     | The port that the external Solr cluster is listening on                           | `8983`  |
| `externalSolr.runMode`  | The "mode" that the external Solr cluster is running in (`cloud` or `standalone`) | `cloud` |
| `externalSolr.username` | The username to use to authenticate to the external Solr cluster                  | `""`    |

### PostgreSQL parameters

| Name                                          | Description                                             | Value                |
| --------------------------------------------- | ------------------------------------------------------- | -------------------- |
| `postgresql.enabled`                          | Whether to deploy PostgreSQL                            | `true`               |
| `postgresql.image.repository`                 | The image repository for PostgreSQL                     | `bitnami/postgresql` |
| `postgresql.auth.database`                    | The name of the default database to create on deploy    | `lark-events`        |
| `postgresql.auth.password`                    | the password for the PostgreSQL user                    | `larkpass`           |
| `postgresql.auth.postgresPassword`            | the password for the PostgreSQL admin (`postgres`) user | `adminpass`          |
| `postgresql.auth.username`                    | The name of the PostgreSQL user                         | `lark`               |
| `postgresql.primary.service.ports.postgresql` | The port to use for the primary service                 | `5432`               |
| `postgresql.primary.persistence.size`         | The size of the PVC to create for the primary DB        | `10Gi`               |
| `postgresql.primary.resources.cpu`            |                                                         | `1000m`              |
| `postgresql.primary.resources.memory`         |                                                         | `1Gi`                |
| `postgresql.readReplicas.persistence.size`    | The size of the PVC to create for the replica DB        | `10Gi`               |
| `postgresql.readReplicas.resources.cpu`       |                                                         | `1000m`              |
| `postgresql.readReplicas.resources.memory`    |                                                         | `1Gi`                |

### Resources and scheduling parameters

| Name           | Description                                                                                                                       | Value |
| -------------- | --------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `resources`    | The resource requests and limits                                                                                                  | `{}`  |
| `nodeSelector` | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                 | `{}`  |
| `tolerations`  | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods               | `[]`  |
| `affinity`     | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods | `{}`  |
| `replicaCount` | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy                         | `1`   |
