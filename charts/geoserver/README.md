# GeoServer Helm chart

To deploy manually, create a custom values file if desired, then:

```
helm install my-geoserver oci://registry.gitlab.com/surfliner/surfliner/charts/geoserver --namespace my-namespace [--values custom-values.yaml]
```

## Parameters

### Chart metadata parameters

| Name               | Description                               | Value |
| ------------------ | ----------------------------------------- | ----- |
| `nameOverride`     | Override the default chart name           | `""`  |
| `fullnameOverride` | Override the `fullname` used by the chart | `""`  |

### Image source parameters

| Name               | Description                                                    | Value               |
| ------------------ | -------------------------------------------------------------- | ------------------- |
| `image.repository` | Image repository for GeoServer                                 | `kartoza/geoserver` |
| `image.pullPolicy` | Pull policy for the image                                      | `IfNotPresent`      |
| `image.tag`        | Overrides the image tag whose default is the chart appVersion. | `2.26.1`            |

### Traffic exposure parameters

| Name                  | Description                                                                                                                                             | Value                 |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| `service.type`        | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types) to use for the Rails application | `ClusterIP`           |
| `service.port`        | The [port](https://kubernetes.io/docs/concepts/services-networking/service/#field-spec-ports) used by the Rails service                                 | `80`                  |
| `ingress.enabled`     | Whether to enable an [Ingress](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)                                              | `true`                |
| `ingress.annotations` | Additional annotations for the ingress                                                                                                                  | `{}`                  |
| `ingress.hosts`       | An array of hosts to manage with the ingress                                                                                                            | `["geoserver.local"]` |
| `ingress.tls`         | An array of TLS options                                                                                                                                 | `[]`                  |

### Application parameters

| Name                  | Description                                                                                                                                                         | Value        |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------ |
| `admin.password`      | The password for the admin user                                                                                                                                     | `geoserver`  |
| `admin.username`      | The name of the the admin user                                                                                                                                      | `admin`      |
| `dataDirectory`       | The path to use for the GeoServer data directory                                                                                                                    | `/data`      |
| `existingData`        | Whether the deploy is using [existing data](https://github.com/kartoza/docker-geoserver#upgrading-image-to-use-a-specific-version), and should not rewrite password | `false`      |
| `healthz`             | The health check endpoint                                                                                                                                           | `/geoserver` |
| `persistence.enabled` | Whether to enable persistent storage                                                                                                                                | `true`       |
| `port`                | The port that the server will listen on                                                                                                                             | `8080`       |

### JVM parameters (see <https://github.com/kartoza/docker-geoserver/blob/master/.env#L16-L19> and <https://docs.geoserver.org/stable/en/user/production/container.html#optimize-your-jvm>)

| Name                | Description | Value |
| ------------------- | ----------- | ----- |
| `jvm.initialMemory` |             | `2G`  |
| `jvm.maximumMemory` |             | `4G`  |

### Resources and scheduling parameters

| Name                 | Description                                                                                                                                                                                       | Value      |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- |
| `affinity`           | The [affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/) of the Rails pods                                                                 | `{}`       |
| `nodeSelector`       | The [node selectors](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for the Rails pods                                                                                 | `{}`       |
| `podSecurityContext` | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application pods                                                             | `{}`       |
| `priorityClassName`  | The name of the [`PriorityClass`](https://kubernetes.io/docs/concepts/scheduling-eviction/pod-priority-preemption/#pod-priority) to be applied to the deployed pods                               | `""`       |
| `replicaCount`       | The number of [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) to deploy                                                                                         | `1`        |
| `resources`          | The resource requests and limits                                                                                                                                                                  | `{}`       |
| `securityContext`    | The [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to apply to Rails application containers                                                       | `{}`       |
| `tolerations`        | The [tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for the Rails pods                                                                               | `[]`       |
| `updateStrategy`     | The [pod replacement strategy](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy) (`Recreate` is recommendend over rolling update to support volume detach/reattach) | `Recreate` |
