# Tidewater

Please see [CONTRIBUTING.md][contributing] for information about contributing to
this project, and [the manual](../docs/themanual) for technical documentation.
By participating, you agree to abide by the [UC Principles of
Community][principles].

## Developing

Run `make build && make up` or, from the root directory of the repository,
```
docker compose --profile tidewater build
docker compose --profile tidewater up
```

Run `docker-compose --profile tidewater --profile queue up` to run Tidewater
with the RabbitMQ service (this is optional and will not always be desirable or
necessary)

The application will be available at <http://localhost:3004>.

See the `docker-compose` [CLI reference][cli-reference] for more on commands.

[cli-reference]: https://docs.docker.com/compose/reference/overview/

## Documentation

See [the Tidewater chapter of the Surfliner manual][tidewater-manual].

### Tidewater Consumer script

Tidewater Consumer script consumes Comet data like OaiSet, OaiItem, and OaiEntry to the consumer. The script listens on a RabbitmMQ topic and routing key for messages from Comet.  It uses the payload resourceUrl to get the data. ResourceUrl is expected to be a URL to superskunk API for accessing the data itself. It is intended to be independent from the Rails application.  It could be extracted into it's own project if needed.

See [Tidewater Consumer script][helm-chart-tidewater] for more information regarding configuration and how it is deployed.

[contributing]: ../CONTRIBUTING.md
[principles]: https://ucnet.universityofcalifornia.edu/working-at-uc/our-values/principles-of-community.html
[tidewater-manual]: ../docs/themanual/tidewater/
[helm-chart-tidewater]: ../charts/tidewater#tidewater-consumer
