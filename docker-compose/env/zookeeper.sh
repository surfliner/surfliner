#############
# Zookeeper #
#############

# https://github.com/bitnami/containers/tree/main/bitnami/zookeeper#customizable-environment-variables
ALLOW_ANONYMOUS_LOGIN=yes
ZOO_CLIENT_PASSWORD=admin
ZOO_CLIENT_USER=admin
ZOO_SERVERS=zk:2888:3888
