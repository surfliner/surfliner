#########
# Minio #
#########
MINIO_DEFAULT_BUCKETS=comet-repository:download,comet-staging:public,cantaloupe-cache:public,shoreline-staging-area-dev:public,starlight:download
MINIO_ROOT_PASSWORD=cantaloupe-secret
MINIO_ROOT_USER=cantaloupe-access-key
