#############
# Shoreline #
#############

APP_URL=http://localhost
CONTACT_EMAIL=repo-admin@example.org
# applications that use ActiveMailer determine their submission method with
# this; should be `smtp` in production
DELIVERY_METHOD=letter_opener_web
GEOBLACKLIGHT_DOWNLOAD_PATH=tmp
# TODO used inconsistently versus RAILS_LOG_LEVEL
LOG_LEVEL=debug
# telemetry
OTEL_EXPORTER_OTLP_ENDPOINT=http://jaeger:4318
RACK_ENV=development
RAILS_ENV=development
RAILS_LOG_LEVEL=debug
RAILS_LOG_TO_STDOUT=true
RAILS_MAX_THREADS=5
RAILS_QUEUE=sidekiq
RAILS_SERVE_STATIC_FILES=1
SHORELINE_SUPPRESS_TOOLS=false
SHORELINE_THEME=ucsb

# ==
# S3
# ==
#
# TODO: applications should migrate to specific named variables, and these
# should only be used to configure Minio itself
MINIO_ENDPOINT=minio
MINIO_PORT=9000
MINIO_ROOT_PASSWORD=cantaloupe-secret
MINIO_ROOT_USER=cantaloupe-access-key
STAGING_AREA_S3_BUCKET=shoreline-staging-area-dev

# ===============
# APIs/Messaging
# ===============
RABBITMQ_HOST=rabbitmq
RABBITMQ_NODE_PORT_NUMBER=5672
RABBITMQ_PASSWORD=bitnami
RABBITMQ_TOPIC=surfliner.metadata
RABBITMQ_USERNAME=user
RABBITMQ_QUEUE=shoreline
RABBITMQ_SHORELINE_ROUTING_KEY=surfliner.metadata.shoreline
SHORELINE_METADATA_PROFILE=tag:surfliner.gitlab.io,2022:api/aardvark
# used by clients communicating with Tidewater
USER_AGENT_PRODUCT_NAME=surfliner.shoreline

# ======================
# Database configuration
# ======================
DATABASE_ADAPTER=postgresql
# https://github.com/DatabaseCleaner/database_cleaner/blob/f458536ac6bb09cf93f4f4b53b9fce3741b6c0c8/README.markdown?plain=1#L330
DATABASE_CLEANER_ALLOW_REMOTE_DATABASE_URL=true
DATABASE_COMMAND="db:create db:migrate"
DB_ADAPTER=postgresql
DB_ADAPTER=postgresql
DB_NAME=shoreline-discovery
DB_PASSWORD=cluster_admin
DB_USERNAME=postgres
POSTGRESQL_DATABASE=shoreline-discovery
POSTGRESQL_HOST=postgresql
POSTGRESQL_PASSWORD=cluster_admin
POSTGRESQL_PORT=5432
POSTGRESQL_USERNAME=postgres

# =========
# GeoServer
# =========

# https://github.com/kartoza/docker-geoserver?tab=readme-ov-file#upgrading-image-to-use-a-specific-version
EXISTING_DATA_DIR=false
GEOSERVER_ADMIN_PASSWORD=geoserver
GEOSERVER_ADMIN_USER=admin
GEOSERVER_DATA_DIR=/data
GEOSERVER_HOST=localhost
GEOSERVER_INTERNAL_HOST=geoserver
GEOSERVER_INTERNAL_URL=http://geoserver:8080
GEOSERVER_PORT=8080
GEOSERVER_URL=http://localhost:8080
GEOSERVERUSERS_GID=0
GEOSERVERUSER_UID=0
GEOSERVER_WORKSPACE=public

# ====
# Solr
# ====
SOLR_ADMIN_PASSWORD=admin
SOLR_ADMIN_USER=admin
SOLR_COLLECTION_NAME=shoreline-discovery
SOLR_HOST=solr
SOLR_PORT=8983
