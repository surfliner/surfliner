##############
# PostgreSQL #
############ #

# https://github.com/bitnami/containers/tree/main/bitnami/postgresql#customizable-environment-variables
POSTGRESQL_HOST=postgres
POSTGRESQL_PASSWORD=cluster_admin
POSTGRESQL_PORT=5432
POSTGRESQL_POSTGRES_PASSWORD=cluster_admin
POSTGRESQL_USERNAME=postgres
