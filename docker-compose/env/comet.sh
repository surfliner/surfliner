#########
# Comet #
#########

# this is used to toggle between the development authentication (managed by
# comet/config/role_map.yml) and production auth (`google` -- see
# docs/themanual/auth.md)
AUTH_METHOD=developer
COMET_ENABLE_BULKRAX=true
# directory where Bulkrax caches generated exports for downloading
BULKRAX_EXPORT_PATH=/home/comet/app/exports
# password for the EzID service account used to mint ARKs
EZID_PASSWORD=
FITS_SERVLET_URL=http://fits:8080/fits
CH12N_TOOL=comet_fits_servlet
# used by applications connecting to Orange Empire (currently Comet and
# Starlight)
IIIF_BASE_URL=http://localhost:8182
HYRAX_DERIVATIVES_PATH=/home/comet/app/derivatives
HYRAX_ENGINE_PATH=/app/samvera/hyrax-engine
HYRAX_UPLOAD_PATH=/app/samvera/hyrax-webapp/uploads/
# TODO used inconsistently versus RAILS_LOG_LEVEL
LOG_LEVEL=debug
# specifies the M3 models to load from comet/config/metadata
METADATA_MODELS=test-metadata
# telemetry
OTEL_EXPORTER_OTLP_ENDPOINT=http://jaeger:4318
RACK_ENV=development
RAILS_ENV=development
RAILS_LOG_LEVEL=debug
RAILS_LOG_TO_STDOUT=true
RAILS_MAX_THREADS=5
RAILS_QUEUE=sidekiq
RAILS_SERVE_STATIC_FILES=1
# https://github.com/samvera/bulkrax/pull/821
SHOW_CREATE_AND_VALIDATE=true
# TODO what are these used for (comet/config/valkyrie_index.yaml)
VALKYRIE_SOLR_HOST=solr
VALKYRIE_SOLR_PORT=8983

# ===============
# APIs/Messaging
# ===============
DISCOVER_PLATFORM_SHORELINE_URL_BASE=http://localhost:3001/item?source_iri=
DISCOVER_PLATFORM_TIDEWATER_URL_BASE=http://localhost:3000/tidewater/item?source_iri=
# the Superskunk endpoint
METADATA_API_URL_BASE=http://superskunk:3000/resources
# whether or not to try to connect to the message queue for publishing events
RABBITMQ_ENABLED=true
RABBITMQ_HOST=rabbitmq
RABBITMQ_NODE_PORT_NUMBER=5672
RABBITMQ_PASSWORD=bitnami
RABBITMQ_TOPIC=surfliner.metadata
RABBITMQ_USERNAME=user

# ======================
# Database configuration
# ======================

# TODO consolidate these
#
# https://github.com/DatabaseCleaner/database_cleaner/blob/f458536ac6bb09cf93f4f4b53b9fce3741b6c0c8/README.markdown?plain=1#L330
DATABASE_CLEANER_ALLOW_REMOTE_DATABASE_URL=true
DATABASE_COMMAND="db:create db:migrate"
DATABASE_URL=postgresql://postgres:cluster_admin@postgresql/comet?pool=5
DB_ADAPTER=postgresql
DB_NAME=comet
DB_PASSWORD=cluster_admin
DB_USERNAME=postgres
POSTGRESQL_DATABASE=comet
POSTGRESQL_HOST=postgresql
POSTGRESQL_PORT=5432
# the name of the metadata DB (also used by superskunk)
METADATA_DATABASE_NAME=comet_metadata_development

# =====
# Redis
# =====
REDIS_HOST=redis
REDIS_PASSWORD=sidekickin
REDIS_URL=redis://:sidekickin@redis:6379

# ==
# S3
# ==
#
# S3 configuration for primary storage in Comet
REPOSITORY_S3_ACCESS_KEY=cantaloupe-access-key
REPOSITORY_S3_BUCKET=comet-repository
REPOSITORY_S3_EXTERNAL_ENDPOINT=http://localhost:9000
REPOSITORY_S3_FORCE_PATH_STYLE=true
REPOSITORY_S3_HOST=minio
REPOSITORY_S3_PORT=9000
REPOSITORY_S3_SECRET_KEY=cantaloupe-secret

# S3 configuration for bulkrax source files
STAGING_AREA_S3_ACCESS_KEY=cantaloupe-access-key
STAGING_AREA_S3_BUCKET=comet-staging
STAGING_AREA_S3_EXTERNAL_ENDPOINT=http://localhost:9000
STAGING_AREA_S3_FORCE_PATH_STYLE=true
STAGING_AREA_S3_HOST=minio
STAGING_AREA_S3_PORT=9000
STAGING_AREA_S3_SECRET_KEY=cantaloupe-secret

# ====
# Solr
# ====
SOLR_ADMIN_PASSWORD=admin
SOLR_ADMIN_USER=admin
SOLR_COLLECTION_NAME=comet-dev
SOLR_HOST=solr
SOLR_PORT=8983
SOLR_URL=http://admin:admin@solr:8983/solr/comet-dev

# =======
# Testing
# =======
CAPYBARA_PORT=3010
CAPYBARA_SERVER=http://comet-hyrax.comet-development:3010
CAPYBARA_WAIT_TIME=30
CHROME_HEADLESS_MODE=false
DATABASE_TEST_URL=postgresql://postgres:cluster_admin@postgresql/comet_test?pool=5
HUB_URL=http://chrome:4444/wd/hub
IN_DOCKER=true
SELENIUM_TIMEOUT=30
SKIP_SELENIUM=
