##############
# Superskunk #
##############

COMET_BASE=http://comet:3000
COMET_EXTERNAL_BASE=http://localhost:3000
METADATA_MODELS=test-metadata
RAILS_DEVELOPMENT_HOSTS=superskunk
RACK_ENV=development
RAILS_ENV=development
RAILS_LOG_LEVEL=debug
RAILS_LOG_TO_STDOUT=true
# used by clients to connect to Superskunk
SUPERSKUNK_API_BASE=http://superskunk:3000

# ======================
# Database configuration
# ======================
POSTGRESQL_HOST=postgresql
POSTGRESQL_DATABASE=comet_metadata_development
POSTGRESQL_PASSWORD=cluster_admin
POSTGRESQL_PORT=5432
POSTGRESQL_USERNAME=postgres
