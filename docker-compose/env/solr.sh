########
# Solr #
########

# https://github.com/bitnami/containers/tree/main/bitnami/solr#customizable-environment-variables
SOLR_ADMIN_PASSWORD=admin
SOLR_ADMIN_USER=admin
SOLR_CLOUD_BOOTSTRAP=yes
SOLR_COLLECTION_REPLICAS=1
SOLR_COLLECTION_SHARDS=1
SOLR_ENABLE_AUTHENTICATION=yes
SOLR_ENABLE_CLOUD_MODE=yes
SOLR_NUMBER_OF_NODES=1
SOLR_ZK_HOSTS=zk:2181
