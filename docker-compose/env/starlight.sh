#############
# Starlight #
#############

ALLOW_ROBOTS=
# this is used by Starlight to advertise its public-facing URL
APP_URL=http://localhost
# this is used to toggle between the development authentication (managed by
# comet/config/role_map.yml) and production auth (`google` -- see
# docs/themanual/auth.md)
AUTH_METHOD=developer
# applications that use ActiveMailer determine their submission method with
# this; should be `smtp` in production
DELIVERY_METHOD=letter_opener_web
# telemetry
OTEL_EXPORTER_OTLP_ENDPOINT=http://jaeger:4318
# if false, uses riiif instead of Orange Empire
EXTERNAL_IIIF=true
# the from address for alert emails
FROM_EMAIL=email@uc.edu
# used by applications connecting to Orange Empire (currently Comet and
# Starlight)
IIIF_BASE_URL=http://localhost:8182
# used for requests to Orange Empire that can be performed on the internal
# private network
IIIF_INTERNAL_BASE=http://cantaloupe:8182
PORT=3000
RACK_ENV=development
RAILS_ENV=development
RAILS_LOG_LEVEL=debug
RAILS_LOG_TO_STDOUT=true
RAILS_MAX_THREADS=5
RAILS_QUEUE=sidekiq
RAILS_SERVE_STATIC_FILES=1
SECRET_KEY_BASE=6e975aca3fb9aa34971bc073ab0f1b4587e4f3a008e99f91d7ec171e3a0ac57aeee35c725c6787543f62972159ad1825497fa51e210e8708920c7da398b3ac17
SERVER=localhost
SITEMAPS_ENABLED=
SITEMAPS_HOST=http://localhost:9001/starlight/
SPOTLIGHT_THEMES=ucsd,ucsb,surfliner

# ======================
# Database configuration
# ======================

# https://github.com/DatabaseCleaner/database_cleaner/blob/f458536ac6bb09cf93f4f4b53b9fce3741b6c0c8/README.markdown?plain=1#L330
DATABASE_CLEANER_ALLOW_REMOTE_DATABASE_URL=true
DATABASE_COMMAND="db:migrate db:test:prepare"
DATABASE_URL=postgresql://postgres:cluster_admin@postgresql/starlight?pool=5
DB_ADAPTER=postgresql
DB_NAME=starlight
DB_PASSWORD=cluster_admin
DB_USERNAME=postgres
POSTGRESQL_DATABASE=starlight
POSTGRESQL_HOST=postgresql
POSTGRESQL_PASSWORD=cluster_admin
POSTGRESQL_PORT=5432
POSTGRESQL_USERNAME=postgres

# =====
# Redis
# =====
REDIS_HOST=redis
REDIS_PASSWORD=sidekickin
REDIS_URL=redis://:sidekickin@redis:6379

# =========
# Memcached
# =========
MEMCACHED_HOST=memcached

# ==
# S3
# ==

# standard AWS S3 auth variables.  in Comet areas we have replaced these with
# ones specific to the service, but these are still used as-is by Starlight and
# Orange Empire
AWS_ACCESS_KEY_ID=cantaloupe-access-key
AWS_REGION=us-west-2
AWS_SECRET_ACCESS_KEY=cantaloupe-secret
S3_ACL=public-read
S3_ASSET_HOST_PUBLIC=true
S3_BUCKET_NAME=starlight
S3_ENDPOINT=http://minio:9000

# ====
# Solr
# ====
SOLR_COLLECTION_NAME=starlight-dev
SOLR_ADMIN_PASSWORD=admin
SOLR_ADMIN_USER=admin
SOLR_HOST=solr
SOLR_PORT=8983

# =======
# Testing
# =======
SELENIUM_URL=http://chrome:4444/wd/hub/
START_XVFB=false
TEST_POSTGRES_DB=test
TEST_SOLR_URL=http://admin:admin@solr:8983/solr/starlight-test
