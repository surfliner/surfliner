###################################
# Cantaloupe IIIF (Orange Empire) #
###################################

# standard AWS S3 auth variables.  in Comet areas we have replaced these with
# ones specific to the service, but these are still used as-is by Starlight and
# Orange Empire
AWS_ACCESS_KEY_ID=cantaloupe-access-key
AWS_SECRET_ACCESS_KEY=cantaloupe-secret
# see
# https://cantaloupe-project.github.io/manual/5.0/configuration.html#EnvironmentConfiguration
CANTALOUPE_CACHE_SERVER_DERIVATIVE_ENABLED=false
CANTALOUPE_CACHE_SERVER_DERIVATIVE=S3Cache
CANTALOUPE_DELEGATE_SCRIPT_ENABLED=true
CANTALOUPE_DELEGATE_SCRIPT_PATHNAME=/cantaloupe/delegate.rb
CANTALOUPE_ENDPOINT_ADMIN_ENABLED=true
CANTALOUPE_ENDPOINT_ADMIN_SECRET=cantaloupe-secret-admin
CANTALOUPE_LOG_APPLICATION_LEVEL=debug
CANTALOUPE_S3CACHE_ACCESS_KEY_ID=cantaloupe-access-key
CANTALOUPE_S3CACHE_BUCKET_NAME=cantaloupe-cache
CANTALOUPE_S3CACHE_ENDPOINT=http://minio:9000
CANTALOUPE_S3CACHE_SECRET_KEY=cantaloupe-secret
CANTALOUPE_S3SOURCE_ACCESS_KEY_ID=cantaloupe-access-key
CANTALOUPE_S3SOURCE_ENDPOINT=http://minio:9000
CANTALOUPE_S3SOURCE_LOOKUP_STRATEGY=ScriptLookupStrategy
CANTALOUPE_S3SOURCE_SECRET_KEY=cantaloupe-secret
CANTALOUPE_SOURCE_STATIC=S3Source
SAMPLE_IMAGE_URL=https://gitlab.com/surfliner/surfliner/-/raw/trunk/comet/spec/fixtures/image.jpg
# custom/local variables
#

# specifies which services do not need a pre-auth check on viewing permissions
CANTALOUPE_NO_PRE_AUTH=starlight
# this is used by our delegate.rb script to let us map requests from different
# services (comet, starlight) to different S3 buckets containing source images
CANTALOUPE_S3SOURCE_BUCKET_MAP=comet:comet-repository,starlight:starlight
# TODO this does not appear to be used anywhere
CANTALOUPE_STARLIGHT_PATH_PREFIX=uploads/spotlight/featured_image/image/
# used by clients to connect to Superskunk
SUPERSKUNK_API_BASE=http://superskunk:3000
