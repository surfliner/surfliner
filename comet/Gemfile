# frozen_string_literal: true

source "https://rubygems.org"

gem "bootsnap", "1.18.4", require: false
gem "bootstrap", "4.6.2"
gem "bulkrax", "6.0.1"
gem "bunny", "2.23.0"
gem "coffee-rails", "5.0.0"
gem "concurrent-ruby", "1.3.4"
gem "declarative-option", "0.1.0"
gem "devise", "4.9.4"
gem "devise-guests", "0.8.3"
gem "down", "~> 5.0"
gem "ezid-client", "1.11.0"
gem "fog-aws", "3.29.0"
gem "google-protobuf", force_ruby_platform: true
gem "hyrax", "5.0.1"
gem "jbuilder", "2.13.0"
gem "jquery-rails", "4.6.0"
gem "okcomputer", "1.18.6"
gem "omniauth-google-oauth2", "0.8.2"
gem "opentelemetry-exporter-otlp"
gem "opentelemetry-instrumentation-all"
gem "opentelemetry-sdk"
gem "pg", "1.5.9"
gem "puma", "6.5.0"
gem "rails", "6.1.7.8"
gem "rdf-edtf",
  git: "https://gitlab.com/surfliner/rdf-edtf.git",
  branch: "trunk"
gem "redis", "4.8.1"
gem "rsolr", "2.6.0"
gem "sass-rails"
gem "sidekiq", "7.3.3"
gem "surfliner_schema", path: "../gems/surfliner_schema"
gem "tinymce-rails", "5.10.9"
gem "turbolinks", "5.2.1"
gem "twitter-typeahead-rails"
gem "uglifier", "4.2.1"
gem "valkyrie-sequel", "~> 3.0"
gem "valkyrie-shrine",
  git: "https://github.com/samvera-labs/valkyrie-shrine.git",
  branch: "main"

group :development do
  gem "listen", ">= 3.0.5", "< 3.9.1"
end

group :development, :test do
  gem "byebug", "11.1.3"
  gem "capybara", "3.40.0"
  gem "capybara-screenshot", "1.0.26"
  gem "ci_reporter_rspec", "~> 1.0"
  gem "database_cleaner-active_record", "2.2.0"
  gem "equivalent-xml", "~> 0.6.0"
  gem "factory_bot_rails", "6.4.4"
  gem "pry-byebug", "3.10.1"
  gem "pry-doc", "1.5.0"
  gem "pry-rails", "0.3.11"
  gem "pry-rescue", "1.6.0"
  gem "rspec-rails", "6.1.5"
  gem "rspec_junit_formatter", "0.6.0"
  gem "scss_lint", "0.60.0", require: false
  gem "scss_lint_reporter_checkstyle", "0.2.0", require: false
  gem "selenium-webdriver", "4.25.0"
  gem "simplecov", "~> 0.22", require: false
  gem "simplecov-cobertura", "~> 2.1", require: false
  gem "standard", "1.43.0"
end
