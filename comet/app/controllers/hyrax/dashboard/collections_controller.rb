# frozen_string_literal: true

module Hyrax
  module Dashboard
    class CollectionsController < Hyrax::My::CollectionsController
      configure_blacklight do |config|
        config.search_builder_class = ::CometDashboardCollectionsSearchBuilder
      end

      with_themed_layout "dashboard"
      before_action :authenticate_user!

      load_and_authorize_resource(only: [:show],
        class: Hyrax.config.collection_class,
        instance_name: :collection)

      rescue_from Hydra::AccessDenied, CanCan::AccessDenied, with: :deny_collection_access

      # Override My Collections controller to fix breadcrumb link to dashboard collections
      def index
        add_breadcrumb t(:"hyrax.controls.home"), root_path
        add_breadcrumb t(:"hyrax.dashboard.breadcrumbs.admin"), hyrax.dashboard_path
        add_breadcrumb t(:"hyrax.admin.sidebar.collections"), hyrax.dashboard_collections_path
        collection_type_list_presenter
        managed_collections_count
        MyController.instance_method(:index).bind_call(self)
      end

      def new
        @collection = Hyrax.config.collection_class
          .new(collection_type_gid: collection_type.to_global_id)
        @form = Hyrax::Forms::ResourceForm.for(resource: @collection)
      end

      def create
        @collection = Hyrax.config.collection_class.new
        authorize! :create, @collection

        @form = Hyrax::Forms::ResourceForm.for(resource: @collection)

        create_valkyrie_collection
      end

      def destroy
        @collection = Hyrax.query_service.find_by(id: params[:id])

        destroyed = transactions["collection_resource.destroy"]
          .with_step_args("collection_resource.delete" => {user: current_user},
            "collection_resource.remove_from_membership" => {user: current_user})
          .call(@collection).value!
        Hyrax.logger.debug("Destroyed Collection: #{destroyed}")

        respond_to do |format|
          format.html do
            redirect_to hyrax.dashboard_collections_path,
              notice: t("hyrax.dashboard.my.action.collection_delete_success")
          end
          format.json { head :no_content, location: hyrax.dashboard_collections_path }
        end
      rescue => err
        Hyrax.logger.error(err)

        respond_to do |format|
          format.html do
            flash[:notice] = t("hyrax.dashboard.my.action.collection_delete_fail")
            render :edit, status: :unprocessable_entity
          end
          format.json { render json: {id: id}, status: :unprocessable_entity, location: dashboard_collection_path(@collection) }
        end
      end

      def edit
        @collection = Hyrax.query_service.find_by(id: params[:id])
        @collection_type = collection_type
        @form = Hyrax::Forms::ResourceForm.for(resource: @collection)
        @form.prepopulate!
      end

      def show
        @presenter = Comet::CollectionPresenter.new(curation_concern, current_ability)
        query_collection_members
      end

      def update
        form = Hyrax::Forms::ResourceForm.for(resource: Hyrax.query_service.find_by(id: params[:id]))

        return after_create_errors(form_err_msg(form)) unless
          form.validate(collection_params)

        upload_presentative_image(collection_id: params[:id]) if params[:representative_files].present?
        remove_representative_image(collection_id: params[:id]) if ActiveModel::Type::Boolean.new.cast(params[:remove_image_uploaded])

        @collection = transactions["change_set.update_collection"]
          .call(form)
          .value_or { |failure| after_update_errors(failure.first) }

        respond_to do |format|
          format.html { redirect_to dashboard_collection_path(@collection), notice: t("hyrax.dashboard.my.action.collection_update_success") }
          format.json { render json: @collection, status: :updated, location: dashboard_collection_path(@collection) }
        end
      end

      def repository
        blacklight_config.repository || Blacklight::Solr::Repository.new(blacklight_config)
      end

      private

      def create_valkyrie_collection
        return after_create_errors(form_err_msg(@form)) unless @form.validate(collection_params)

        result = transactions["change_set.create_collection"].with_step_args(
          "change_set.set_user_as_depositor" => {user: current_user},
          "collection_resource.apply_collection_type_permissions" => {user: current_user}
        ).call(@form)

        @collection = result.value_or { return after_create_errors(result.failure.first) }

        upload_presentative_image(collection_id: @collection.id.to_s) if params[:representative_files].present?
        Hyrax.index_adapter.save(resource: @collection)

        after_create_response
      end

      def after_create_response
        respond_to do |format|
          format.html { redirect_to(dashboard_collections_path, notice: t("hyrax.dashboard.my.action.collection_create_success")) }
          format.json { render json: @collection, status: :created, location: dashboard_collection_path(@collection) }
        end
      end

      def after_create_errors(errors)
        return after_create_errors_for_active_fedora(errors) if @collection.is_a? ActiveFedora::Base
        respond_to do |wants|
          wants.html do
            flash[:error] = errors.to_s
            render "new", status: :unprocessable_entity
          end
          wants.json do
            render_json_response(response_type: :unprocessable_entity, options: {errors: errors})
          end
        end
      end

      def form_err_msg(form)
        errmsg = []
        form.errors.messages.each do |fld, err|
          errmsg << "#{fld} #{err.to_sentence}"
        end
        errmsg.to_sentence
      end

      def collection_params
        representative_id = params[:collection][:find_representative_image]

        params.permit(collection: {})[:collection]
          .merge(params.permit(:collection_type_gid)
                   .with_defaults(collection_type_gid: default_collection_type_gid))
          .merge(member_of_collection_ids: Array(params[:parent_id]))
          .merge({representative_id: representative_id.present? ? representative_id : params[:representative_id],
                  thumbnail_id: representative_id.present? ? representative_id : params[:thumbnail_id]})
      end

      def deny_collection_access(exception)
        Hyrax.logger.info(exception)

        if current_user&.present?
          head :unauthorized
        else
          session["user_return_to"] = request.url
          redirect_to main_app.new_user_session_url, alert: exception.message
        end
      end

      def query_collection_members
        member_works
        member_subcollections if collection_type.nestable?
        parent_collections if collection_type.nestable? && action_name == "show"
      end

      # Instantiate the membership query service
      def collection_member_service
        @collection_member_service ||= Collections::CollectionMemberSearchService.new(scope: self, collection: @presenter, params: params_for_query, search_builder_class: CometCollectionMemberSearchBuilder)
      end

      # You can override this method if you need to provide additional
      # inputs to the search builder. For example:
      #   search_field: 'all_fields'
      # @return <Hash> the inputs required for the collection member search builder
      def params_for_query
        params.merge(q: params[:cq])
      end

      def member_works
        @response = collection_member_service.available_member_works
        @member_docs = @response.documents
        @members_count = @response.total
      end

      def member_subcollections
        results = collection_member_service.available_member_subcollections
        @subcollection_solr_response = results
        @subcollection_docs = results.documents
        @subcollection_count = @presenter.nil? ? 0 : @subcollection_count = @presenter.subcollection_count = results.total
      end

      def parent_collections
        page = params[:parent_collection_page].to_i
        query = Hyrax::Collections::NestedCollectionQueryService
        @presenter.parent_collections = query.parent_collections(child: @collection, scope: self, page: page)
      end

      def curation_concern
        # Query Solr for the collection.
        # run the solr query to find the collection members
        response, _docs = single_item_search_service.search_results
        curation_concern = response.documents.first
        raise CanCan::AccessDenied unless curation_concern
        curation_concern
      end

      def single_item_search_service
        Hyrax::SearchService.new(config: blacklight_config, user_params: params.except(:q, :page), scope: self, search_builder_class: SingleCollectionSearchBuilder)
      end

      # Instantiates the search builder that builds a query for a single item
      # this is useful in the show view.
      def single_item_search_builder
        search_service.search_builder
      end

      def collection_type
        id = params[:collection_type_id].presence || default_collection_type.id

        Hyrax::CollectionType.find(id)
      end
      helper_method :collection_type

      def default_collection_type
        Hyrax::CollectionType.find_or_create_default_collection_type
      end

      def default_collection_type_gid
        default_collection_type.to_global_id.to_s
      end

      ##
      # Save presentative image locally as thumbnail.
      def upload_presentative_image(collection_id:)
        file = Hyrax::UploadedFile.find(params[:representative_files])
        carrier_wave_file = file.first.uploader.file

        carrier_wave_file.move_to local_thumbnail_path(collection_id)
      end

      ##
      # Remove the representative image stored locally
      def remove_representative_image(collection_id:)
        thumbnail_path = local_thumbnail_path(collection_id)
        return unless File.exist?(thumbnail_path)

        File.delete thumbnail_path
      end

      ##
      # Retrieve the local file path for the collection's representative image
      def local_thumbnail_path(collection_id)
        @local_thumbnail_path = Hyrax::DerivativePath.derivative_path_for_reference(collection_id, Hyrax::DownloadsController::THUMBNANIL)
      end

      # The search action url for dashboard collections
      def search_action_url(...)
        dashboard_collections_url(...)
      end

      # The url of the "more" link for additional facet values for dashbord collections facet filtering.
      def search_facet_path(args = {})
        main_app.dashboard_collections_facet_path(args[:id])
      end
    end
  end
end
