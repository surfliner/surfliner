# frozen_string_literal: true

module Hyrax
  module Dashboard
    class ProjectsController < Hyrax::My::CollectionsController
      configure_blacklight do |config|
        config.search_builder_class = ::AdminSetsSearchBuilder
      end

      def index
        add_breadcrumb t(:"hyrax.controls.home"), root_path
        add_breadcrumb t(:"hyrax.dashboard.breadcrumbs.admin"), hyrax.dashboard_path
        add_breadcrumb t(:"hyrax.admin.sidebar.projects"), main_app.dashboard_projects_path
        collection_type_list_presenter
        managed_collections_count
        MyController.instance_method(:index).bind_call(self)
      end

      def collection_type_list_presenter
        @collection_type_list_presenter ||= Hyrax::SelectCollectionTypeListPresenter.new(current_user, Hyrax::CollectionType::ADMIN_SET_MACHINE_ID)
      end

      def managed_collections_count
        @managed_collection_count = Hyrax::Collections::ManagedAdminSetsService.managed_collections_count(scope: self)
      end

      # The search action url for dashboard collections
      def search_action_url(...)
        main_app.dashboard_projects_url(...)
      end

      # The url of the "more" link for additional facet values for dashbord collections facet filtering.
      def search_facet_path(args = {})
        main_app.dashboard_projects_facet_path(args[:id])
      end
    end
  end
end
