# frozen_string_literal: true

module Hyrax
  # Presents a list of works in workflow
  class Admin::WorkflowsController < ApplicationController
    before_action :ensure_authorized!
    before_action :build_breadcrumbs, only: [:index, :workflow_actions]
    with_themed_layout "dashboard"
    class_attribute :deposited_workflow_state_name

    # Works that are in this workflow state (see workflow json template) are excluded from the
    # status list and display in the "Published" tab
    self.deposited_workflow_state_name = "deposited"

    # include the render_check_all view helper method
    helper Hyrax::BatchEditsHelper

    def index
      assign_action_objects_params
      @workflow_states = workflow_state_options(actionable_objects.workflow_states&.map { |state| state.name }&.uniq)
      @importers = importer_options(actionable_objects.bulkrax_importers)
      @workflow_projects = project_options(actionable_objects.available_projects.select_options)
      @response = WorkflowResponse.new(actionable_objects.to_a, actionable_objects.total_count, current_page, per_page, under_review?, workflow_state, bulkrax_importer, workflow_project, workflow_project_id)
    end

    def workflow_actions
      batch_document_ids = params[:batch_document_ids]
      if batch_document_ids.blank?
        redirect_to(admin_workflows_path,
          alert: t("hyrax.admin.workflows.workflow_actions.missing_params"))
        return
      end

      id_and_state_pairs = CometActionableObjects.id_and_state_pairs_for(user: current_user, object_ids: batch_document_ids)
      states = id_and_state_pairs.map { |p| p&.last }.uniq

      if states.length > 1
        redirect_to(admin_workflows_path,
          alert: t("hyrax.admin.workflows.workflow_actions.mixed_states", workflow_states: states.map(&:name).join(", ")))
        return
      end

      @batch_ids = id_and_state_pairs.map { |p| p&.first }
      doc = Hyrax::SolrQueryService.new.with_ids(ids: [@batch_ids.first])
        .solr_documents.first
      @workflow = WorkflowPresenter.new(doc, current_ability) unless doc.nil?
    end

    def batch_update
      permitted = params.require(:workflow_action).permit(:name, :comment)

      object_ids = JSON.parse(params[:batch_document_ids])
      BatchWorkflowUpdateJob.perform_later(user: current_user, object_ids: object_ids, attributes: permitted)

      redirect_to(admin_workflows_path,
        notice: "#{t("hyrax.admin.workflows.workflow_actions.success")}: #{object_ids.join(", ")}")
    rescue => ex
      Hyrax.logger.error(ex)

      redirect_to("/admin/workflows/batch/workflow_actions",
        batch_document_ids: object_ids,
        alert: "#{t("hyrax.admin.workflows.workflow_actions.failed")} #{ex.message}")
    end

    private

    def ensure_authorized!
      authorize! :review, :submissions
    end

    def actionable_objects
      @actionable_objects ||= CometActionableObjects.new(user: current_user,
        workflow_state_filter: workflow_state.blank? ? nil : workflow_state,
        bulkrax_importer_filter: bulkrax_importer.blank? ? nil : bulkrax_importer,
        project_filter: workflow_project_id.blank? ? nil : workflow_project_id)
    end

    def current_page
      @page ||= params.fetch("page", 1).to_i
    end

    def per_page
      @per_page ||= params.fetch("per_page", 10).to_i
    end

    def assign_action_objects_params
      actionable_objects.page = current_page
      actionable_objects.per_page = per_page
      actionable_objects.workflow_state_filter = workflow_state
    end

    def under_review?
      @under_review = params["state"] != "published"
    end

    def workflow_state
      @workflow_state = params["state"]
    end

    def bulkrax_importer
      @bulkrax_importer = params["importer"]
    end

    def workflow_project
      Hyrax.query_service.find_by(id: params["project_id"])&.title&.first if params["project_id"].present?
    end

    def workflow_project_id
      @workflow_project_id = params["project_id"]
    end

    # Workflow state options for select2 drop-down list.
    def workflow_state_options(states)
      states.map { |state| {text: state.humanize, id: state} }
    end

    # Importer options for select2 drop-down list.
    def importer_options(importers)
      importers.map { |importer| {text: importer.name, id: importer.id} }
    end

    # Project options for select2 drop-down list.
    def project_options(projects)
      projects.map { |project| {text: project[0], id: project[1]} }
    end

    def build_breadcrumbs
      add_breadcrumb I18n.t(:"hyrax.controls.home"), root_path
      add_breadcrumb I18n.t(:"hyrax.dashboard.breadcrumbs.admin"), hyrax.dashboard_path
      add_breadcrumb I18n.t(:"hyrax.admin.sidebar.tasks"), admin_workflows_path.include?(request.path) ? "#" : admin_workflows_path
      add_breadcrumb I18n.t(:"hyrax.admin.sidebar.workflow_review"), request.path
    end

    class WorkflowResponse
      attr_reader :total_count
      attr_reader :current_page
      attr_reader :per_page
      attr_reader :docs
      attr_reader :under_review
      attr_reader :workflow_state
      attr_reader :importer
      attr_reader :workflow_project
      attr_reader :workflow_project_id

      def initialize(docs, total_count, page, per_page, under_review, workflow_state = "", importer = nil, workflow_project = "", workflow_project_id = "")
        @docs = docs
        @total_count = total_count
        @per_page = per_page.to_i
        @current_page = page.to_i
        @under_review = under_review
        @workflow_state = workflow_state
        @importer = bulkrax_importer(importer)
        @workflow_project = workflow_project
        @workflow_project_id = workflow_project_id
      end

      def total_pages
        (total_count.to_f / per_page).ceil
      end

      def limit_value
        docs.length
      end

      def viewing_under_review?
        under_review
      end

      def bulkrax_importer(importer)
        Bulkrax::Importer.find_by(id: importer) unless importer.blank?
      rescue
        nil
      end
    end
  end
end
