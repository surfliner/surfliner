# frozen_string_literal: true

module Hyrax
  ##
  # Override Hyrax's default downloads controller with a custom one for comet
  class DownloadsController < ApplicationController
    include Hyrax::LocalFileDownloadsControllerBehavior
    include Hyrax::ValkyrieDownloadsControllerBehavior

    THUMBNANIL = "thumbnail"

    before_action :authorize_download!

    def show
      if params["file"] == THUMBNANIL
        send_local_content
      else
        file_set = Hyrax.query_service.find_by(id: params.require(:id))
        send_file_contents_valkyrie(file_set)
      end
    end

    private

    def authorize_download!
      (params["file"] == THUMBNANIL) ? authorize!(:read, params[:id]) : authorize!(:download, params[:id])
    rescue CanCan::AccessDenied, Blacklight::Exceptions::RecordNotFound
      unauthorized_image = Rails.root.join("app", "assets", "images", "unauthorized.png")
      send_file unauthorized_image, status: :unauthorized
    end

    def default_use_for(file_set:)
      if Hyrax.query_service.find_parents(resource: file_set).first.is_a?(GeospatialObject)
        :preservation_file
      else
        :original_file
      end
    end

    ##
    # Return the local file path for derivatives
    # @return [String]
    def file
      @file = Hyrax::DerivativePath.derivative_path_for_reference(params[:id], params["file"])
    end

    ##
    # @param [String]
    # @return [Stringl]
    def mime_type_for(file)
      MIME::Types.type_for(File.extname(file)).first.content_type
    end
  end
end
