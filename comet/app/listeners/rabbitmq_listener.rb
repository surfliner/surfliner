# frozen_string_literal: true

##
# An event listener for publishing to RabbitMQ.
#
# Listeners have a `platform_name` which is used to generate the routing key for
# the RabbitMQ message, in conjunction with the `RABBITMQ_TOPIC` environment
# variable.
#
# Listeners should be subscribed to `Hyrax.publisher` in an initializer;
# to add a new platform, simply subscribe a new listener with an appropriate
# product name.
#
# @example subscribe a new listener
#    Hyrax.publisher.subscribe(RabbitmqListener.new(platform_name: :my_platform))
#
# @note This is not actually Rabbitmq‐specific; it could conceivably be used
#   with any discovery platform publisher regardless of what its message broker
#   is. It probably needs a rename.
#
# @see https://dry-rb.org/gems/dry-events/
class RabbitmqListener
  ##
  # @!attribute [rw] platform_name
  #   @return [Symbol]
  #
  # @!attribute [rw] publisher_class
  #   @return [.open_on]  a class yielding a {DiscoveryPlatformPublisher} from
  #     +.open_on+
  #   @see DiscoveryPlatformPublisher for the reference implementation
  attr_accessor :platform_name, :publisher_class

  ##
  # @param platform_name [Symbol]
  # @param publisher_class [.open_on] a class yielding a
  #   {DiscoveryPlatformPublisher} from +.open_on+
  def initialize(platform_name:, publisher_class: DiscoveryPlatformPublisher)
    self.platform_name = platform_name
    self.publisher_class = publisher_class
  end

  ##
  # Updates the object in the platform when metadata is updated.
  def on_object_metadata_updated(event)
    OpenTelemetry::Trace.current_span.set_attribute("surfliner.resource_id", event[:object].id)
    Hyrax.logger.debug { "Received object.metadata.updated event for #{event[:object].class} with id #{event[:object].id} targeting platform: #{platform_name}" }

    publisher_class.open_on(platform_name) do |publisher|
      publisher.update(resource: event[:object])
    end
  rescue => err
    Hyrax.logger.error(err)
  end

  ##
  # Handles unpublishing when the object has been destroyed
  #
  # @param event [{:object => GenericObject}]
  def on_object_deleted(event)
    OpenTelemetry::Trace.current_span.set_attribute("surfliner.resource_id", event[:object].id)
    Hyrax.logger.debug("Received object.deleted event for #{event[:object].class} with id #{event[:object].id} targeting platform: #{platform_name}")

    object = event[:object]
    object.state = Vocab::FedoraResourceStatus.deleted

    publisher_class.open_on(platform_name) do |publisher|
      publisher.unpublish(resource: object)
    end
  rescue => err
    Hyrax.logger.error(err)
  end
end
