module CustomQueries
  module AppData
    # A custom query for finding all binaries
    #
    # This is only implemented for the +Valkyrie::Persistence::Postgres+ adapter
    # and depends on its implementation.
    class DeleteBinaries
      def self.queries
        [:delete_binaries]
      end

      def initialize(query_service:)
        @query_service = query_service
      end

      attr_reader :query_service
      delegate :resource_factory, to: :query_service

      # This method currently deletes all binaries
      #
      # TODO: Create delete binaries function using some form of:
      # query_service.orm_class.where(internal_resource: DeletedBinary.to_s).delete_all
      def delete_binaries
        tmp_binaries = query_service.find_all_of_model(model: DeletedBinary.to_s)
        tmp_binaries.each { |resource| Valkyrie::StorageAdapter.delete(id: resource.file_identifier) if resource.file_identifier.present? }
        query_service.orm_class.where(internal_resource: DeletedBinary.to_s).delete_all
      end
    end
  end
end
