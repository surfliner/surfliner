module CustomQueries
  module AppData
    # A custom query for finding a pending relationship given an importer run id
    # and a parent id.
    #
    # This is only implemented for the +Valkyrie::Persistence::Postgres+ adapter
    # and depends on its implementation.
    class FindPendingRelationship
      def self.queries
        [:find_pending_relationships]
      end

      def initialize(query_service:)
        @query_service = query_service
      end

      attr_reader :query_service
      delegate :resource_factory, to: :query_service

      def find_pending_relationships(importer_run_id:, parent_id:)
        # This method follows the naming and general structure of the
        # +Valkyrie::Persistence::Postgres+ methods, which make use of an
        # +internal_array+ variable that they convert to JSON and pass to
        # +#run_query+.
        #
        # We’ve decided to follow those conventions for consistency here, even
        # tho we believe +internal_array+ is a confusing name.
        internal_array = {
          importer_run_id: [id: importer_run_id.to_s],
          parent_id: [id: parent_id.to_s]
        }
        query_service.run_query(query_service.find_inverse_references_with_type_query,
          internal_array.to_json, ::PendingRelationship.to_s)
      end
    end
  end
end
