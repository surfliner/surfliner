# frozen_string_literal: true

module Qa::Authorities
  class FindRepresentativeImage < Qa::Authorities::FindWorks
    self.search_builder_class = Hyrax::My::FindCollectionRepresentativeSearchBuilder

    ##
    # @Override include representative image id and title instead
    def search(_q, controller)
      # The My::FindWorksSearchBuilder expects a current_user
      return [] unless controller.current_user

      response, _docs = search_response(controller)
      docs = response.documents
      file_set_ids = docs.map { |doc| doc["hasRelatedImage_ssim"]&.first }.compact
      file_sets_map = file_set_ids.blank? ? [] : Hyrax::SolrQueryService.new
        .with_ids(ids: file_set_ids)
        .solr_documents(rows: file_set_ids.size)
        .each_with_object({}) { |fs, h| h[fs.id] = fs.title }

      docs.map do |doc|
        image_id = doc["hasRelatedImage_ssim"]&.first
        title = ["#{file_sets_map[image_id]&.first} -- #{doc.title.first}"]
        {id: image_id, label: title, value: image_id}
      end
    end
  end
end
