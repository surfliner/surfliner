module Presenters
  def self.CometObjectPresenter(model_class:)
    Class.new(::Presenters::CometObjectPresenter) do
      @model_class = model_class

      delegate(*::SchemaLoader.new.properties_for(model_class.availability).keys, to: :model)

      class << self
        attr_reader :model_class
      end
    end
  end

  class CometObjectPresenter < Hyrax::WorkShowPresenter
    # Delegate arks to the model.
    delegate :ark, to: :model

    ##
    # Current page for all members
    def current_members_page
      current_page
    end

    ##
    # Count for all items
    # @return [Integer]
    def total_members_count
      total_items
    end

    ##
    # Count for file sets
    # @return [Integer]
    def file_sets_count
      @file_sets_count ||= file_set_presenters.count
    end

    ##
    # Count for components
    # @return [Integer]
    def components_count
      @components_count ||= work_presenters.count
    end

    # @return [Integer] total number of pages of viewable filesets
    def total_component_pages
      (authorized_component_ids.size.to_f / rows_from_params.to_f).ceil
    end

    # @return [Array] authorized component IDs
    def authorized_component_ids
      authorized_item_ids - file_set_ids
    end

    # @return [Array] component list to display with Kaminari pagination
    def list_of_component_ids_to_display
      paginated_item_list(page_array: authorized_component_ids, current_page: current_component_page)
    end

    # @return [Integer] current/active componet page index
    def current_component_page
      page = request.params[:component_page].nil? ? 1 : request.params[:component_page].to_i
      (page > total_component_pages) ? total_component_pages : page
    end

    # @return [Integer] total number of pages of viewable filesets
    def total_file_set_pages
      (authorized_file_set_ids.size.to_f / rows_from_params.to_f).ceil
    end

    # @return [Array] authorized fileset IDs
    def authorized_file_set_ids
      authorized_item_ids & file_set_ids
    end

    # @return [Array] fileset list to display with Kaminari pagination
    def list_of_file_set_ids_to_display
      paginated_item_list(page_array: authorized_file_set_ids, current_page: current_file_set_page)
    end

    # @return [Integer] current/active fileset page index
    def current_file_set_page
      page = request.params[:file_set_page].nil? ? 1 : request.params[:file_set_page].to_i
      (page > total_file_set_pages) ? total_file_set_pages : page
    end

    # @return [Array] list of file set ids in the object
    def file_set_ids
      file_set_presenters.map { |fp| fp.id }
    end

    # @override add paramter 'current_page' for current page of each tab
    # Uses kaminari to paginate an array to avoid need for solr documents for items here
    # @page_array [Array, String]
    # @current_page [Integer] default for items page for all items
    def paginated_item_list(page_array:, current_page: current_members_page)
      Kaminari.paginate_array(page_array, total_count: page_array.size).page(current_page).per(rows_from_params)
    end

    # Discovery platform options that an object can unpublish from
    def unpublish_options
      resource = Hyrax.query_service.find_by(id: id)
      published_platfroms = DiscoveryPlatformService.published_platforms_for(resource).map(&:to_sym)
      return [] unless published_platfroms.length > 0
      [:all] + published_platfroms
    end

    ##
    # Provides a data structure representing the discovery links for this object.
    #
    # When the object is published in a given discovery platform, we want to
    # display that in a way that makes it possible for users to review the status
    # of those links.
    #
    # @return [[]]
    def discovery_links
      DiscoveryPlatformService.call(solr_document[:id])
    end

    # Discovery platform options that an object can publish to
    def publish_options
      resource = Hyrax.query_service.find_by(id: id)
      published_platfroms = DiscoveryPlatformService.published_platforms_for(resource).map(&:to_sym)
      available_platforms = Rails.application.config.discovery_platforms - published_platfroms
      return [] unless available_platforms.length > 0
      [:all] + available_platforms
    end

    # The label of the publish option
    # @param platform_name [String]
    # @return [String]
    def publish_option_label(platform_name)
      (platform_name == :all) ? I18n.t(:"hyrax.base.show_actions.publish.all_discovery_platforms") : platform_name
    end

    ##
    # Provides an array of property names defined on the model in the schema.
    #
    # @return [Array<Symbol>]
    def schema_property_names
      ::SchemaLoader.new.properties_for(model.class.availability).values
    end

    ##
    # Returns a new class derived from +Presenters::CometObjectPresenter+
    # suitable for presenting the provided model.
    def self.class_for(model:)
      ::Presenters::CometObjectPresenter(model_class: model.class)
    end

    # @override iclude parent collections from parent object
    # @return [Array<String>] member_of_collection_ids with current_ability access
    def member_of_authorized_parent_collections
      @member_of ||= find_member_of_collections
    end

    # Find menber of collections the object or component has, including those from its ancestors
    def find_member_of_collections
      member_of_collections = []
      obj = Hyrax.query_service.find_by(id: id)

      loop do
        member_of_collections += Hyrax::CollectionMemberService.run(obj, current_ability)
        obj = Hyrax.custom_queries.find_parent_work(resource: obj)
        break if obj.nil?
      end

      member_of_collections.map(&:id).uniq
    end
  end
end
