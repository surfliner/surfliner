module Comet
  class CollectionPresenter < Hyrax::CollectionPresenter
    include Presenters
    include Hyrax::Renderers
    # this method gets the metadata property names and corresponding values
    def schema_property_names
      resource = Hyrax.query_service.find_by(id: id)
      property_names = ::SchemaLoader.new.properties_for(Hyrax.config.collection_class.availability).keys
      property_values = {}
      property_names.each do |name|
        value = resource.send(name)
        unless value.blank?
          property_values[name] = value
        end
      end
      property_values
    end
  end
end
