# frozen_string_literal: true

module Bulkrax
  class ValkyrieObjectFactory < ObjectFactory
    include ValkyrieRecordLookupBehavior

    # @Override ensure parent objects exist before building objects
    def run
      importer_run = Bulkrax::ImporterRun.find(importer_run_id) if importer_run_id
      attributes[related_parents_parsed_mapping]&.map do |identifier|
        find_entry_and_record(identifier: identifier, importer_run: importer_run)
      rescue RecordNotFound => e
        raise(ParentNotFound, e.message || identifier)
      end

      super
    end

    def run!
      run
      return object if object.persisted?

      raise(RecordInvalid, object)
    end

    ##
    # Overridden from upstream to ignore +:work_identifier+ and +:work_identifier_search_field+; we don’t use these.
    def initialize(attributes:, source_identifier_value:, work_identifier: nil, work_identifier_search_field: nil, related_parents_parsed_mapping: nil, replace_files: false, user: nil, klass: nil, importer_run_id: nil, update_files: false)
      super
    end

    ##
    # Upstream only calls +#search_by_identifier+ if
    # +#attributes[work_identifier]+ is present. But +#search_by_identifier+
    # doesn’t actually read +#attributes+; it looks at
    # +#source_identifier_value+ directly. Use that as the final check instead.
    def find
      found = find_by_id if attributes[:id].present?
      return found if found.present?
      return search_by_identifier if source_identifier_value.present?
      nil
    end

    def find_by_id
      Hyrax.query_service.find_by(id: Valkyrie::ID.new(attributes[:id])) if attributes.key? :id
    end

    def search_by_identifier
      Hyrax.query_service.find_by_alternate_identifier(alternate_identifier: source_identifier_value)
    rescue => err
      Hyrax.logger.error(err)
      false
    end

    def form
      return @form if @form.present?

      object = @object || klass.new
      @form = Bulkrax::Forms::ResourceForm.for(resource: object)
      @form.prepopulate! unless @object.present?
      @form
    end

    def create
      attrs = transform_attributes
        .merge(alternate_ids: [source_identifier_value])
        .symbolize_keys
      # Add required collection_type_gid for transaction
      collection_type(attrs) if klass.collection?

      form.validate(attrs)

      # add the alternate ids even if the form doesn't support them
      # bulkrax really depends on it!
      form.model.try(:alternate_ids=, [source_identifier_value])

      @object = create_transaction_for(klass)
        .call(form)
        .value_or { |failure| on_create_error(failure.first.to_s) }
    end

    def update
      raise "Object doesn't exist" unless @object

      attrs = transform_attributes(update: true)
      attrs = attrs.merge(ark: @object.ark).symbolize_keys if @object.respond_to?(:ark) && @object.ark&.to_s&.present?

      update_record(attrs)
    end

    def update_record(attrs)
      form.validate(attrs)

      @object = update_transaction_for(klass, @replace_files)
        .call(form)
        .value!
    end

    # @return [Hash<Symbol>, Array<Fog::AWS::Storage::File>>]
    #   example { service_file: [s3file1, s3file2], preservation_file: [s3preservationfile] }
    def get_s3_files
      file_attrs = file_attributes
      return {} unless permitted_file_attributes.any? { |k| file_attrs.key?(k) }

      s3_bucket_name = S3Configurations::StagingArea.bucket
      s3_bucket = Rails.application.config.staging_area_s3_connection
        .directories.get(s3_bucket_name)

      results = {}

      permitted_file_attributes.each do |attr|
        urls = []
        attr_files = file_attrs[attr]

        if attr_files.blank?
          Hyrax.logger.info "No #{attr} files listed for #{attributes["source_identifier"]}"
          next
        end

        Hyrax.logger.debug "Will try to assign #{attr_files} with from attribute #{attr}"

        attr_files.map { |r| r[:url] }.map do |key|
          Hyrax.logger.debug "Loading file #{key} from s3 bucket #{s3_bucket_name}"
          url = s3_bucket.files.get_url(key, Time.now + 60)
          raise "Requested file #{key} doesn't exist in #{s3_bucket_name}" unless url

          file_wrapper = RemoteFile.new(url: url, original_filename: Pathname.new(key).basename.to_s)
          urls << file_wrapper
        end

        results[use_for_file(attr)] = urls unless urls.empty?
      end.compact

      results
    end

    ##
    # Regardless of what the Parser gives us, these are the properties we are prepared to accept.
    def permitted_attributes
      allowed = %i[title visibility]
      allowed += klass.reader.properties(availability: klass.availability).keys if klass.respond_to?(:reader) # klass is defined by +SurflinerSchema+
      [:admin_set_id, :ark].each { |attr| allowed << attr if klass.has_attribute?(attr) }
      allowed
    end

    def apply_depositor_metadata(object, user)
      object.depositor = user.email
      object = Hyrax.persister.save(resource: object)
      Hyrax.publisher.publish("object.metadata.updated", object: object, user: @user)
      object
    end

    # @Override - support multiple fields for remote file ingest
    def file_attributes(update_files = false)
      @update_files = update_files
      hash = {}
      return hash if klass.collection?
      hash[:uploaded_files] = upload_ids if attributes[:file].present?
      permitted_file_attributes.each do |a|
        hash[a] = new_remote_files(a) if attributes[a].present?
      end
      hash
    end

    # @Override - Addition of file_attribute parameter
    # @file_attribute [String] Valid attribute for remote file upload
    # Its possible to get just an array of strings here, so we need to make sure they are all hashes
    def parsed_remote_files(file_attribute)
      Hyrax.logger.info "parsed_remote_files for #{file_attribute} with values #{attributes[file_attribute]} which is a #{attributes[file_attribute].class}"
      remote_files = attributes[file_attribute] || []
      remote_files = Array(remote_files).map do |file_value|
        if file_value.is_a?(Hash)
          file_value
        elsif file_value.is_a?(String)
          name = Bulkrax::Importer.safe_uri_filename(file_value)
          {url: file_value, file_name: name}
        else
          Rails.logger.error("skipped remote file #{file_value} because we do not recognize the type")
          nil
        end
      end
      remote_files.delete(nil)
      remote_files
    end

    # @Override remove branch for FileSets replace validation with errors
    # @file_attribute [String] Valid attribute for remote file upload
    def new_remote_files(file_attribute)
      parsed_remote_files(file_attribute)
    end

    private

    def collection_type(attrs)
      return attrs if attrs["collection_type_gid"].present?

      attrs["collection_type_gid"] = Hyrax::CollectionType.find_or_create_default_collection_type.to_global_id.to_s
      attrs
    end

    # @return [Array<String> attributes which are permitted for referencing files to ingest
    def permitted_file_attributes
      ["remote_files"] + attributes.keys.keep_if { |a| a.start_with?("use:") }
    end

    # @param String header value which typicall will contain the use:<PCDMUse> as a value
    # example: use:PreservationFile
    # @return Symbol use symbol for generating RDF::URI
    def use_for_file(header)
      return :original_file if header.eql? "remote_files"
      return :original_file unless header.start_with?("use:")

      header.gsub("use:", "").underscore.to_sym
    end

    def create_transaction_for(klass)
      if klass.collection?
        Rails.logger.info "Creating a collection for #{@user}"

        Hyrax::Transactions::Container["change_set.create_collection"].with_step_args(
          "change_set.set_user_as_depositor" => {user: @user}
        )
      else
        Hyrax::Transactions::Container["work_resource.create_with_bulk_behavior"].with_step_args(
          "work_resource.add_bulkrax_files" => {files: get_s3_files, user: @user},
          "change_set.set_user_as_depositor" => {user: @user},
          "work_resource.change_depositor" => {user: @user}
        )
      end
    end

    def update_transaction_for(klass, replace_files = false)
      if klass.collection?
        Hyrax::Transactions::Container["change_set.update_collection"]
      else
        Hyrax::Transactions::Container["work_resource.update_with_bulk_behavior"].with_step_args(
          "work_resource.destroy_file_sets" => {user: @user, force_destroy: replace_files},
          "work_resource.add_bulkrax_files" => {files: get_s3_files, user: @user}
        )
      end
    end

    # Handle creation error
    def on_create_error(error)
      raise error
    end
  end

  class RecordInvalid < StandardError
  end

  class ParentNotFound < StandardError
  end
end
