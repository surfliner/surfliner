class DeletedBinary < Valkyrie::Resource
  attribute :file_identifier, Valkyrie::Types::ID
end
