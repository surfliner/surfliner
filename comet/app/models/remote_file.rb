# frozen_string_literal: true

# Wrap up url for source file download
class RemoteFile
  attr_reader :url, :original_filename

  def initialize(original_filename:, url:)
    @original_filename = original_filename
    @url = url
  end

  def file
    @file ||= Down.open(url)
  end

  def content_type
    file.data[:headers]["Content-Type"]
  end

  def content_length
    file.size
  end

  def close
    file.close
  end
end
