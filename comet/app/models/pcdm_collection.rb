##
# Base resource class for PCDM collections defined in the M3.
class PcdmCollection < Hyrax::PcdmCollection
  attribute :ark, Valkyrie::Types::ID
end
