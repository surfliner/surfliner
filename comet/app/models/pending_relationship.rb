class PendingRelationship < Valkyrie::Resource
  attribute :parent_id, Valkyrie::Types::ID
  attribute :child_id, Valkyrie::Types::ID
  attribute :importer_run_id, Valkyrie::Types::ID
end
