module ApplicationHelper
  # The label of the publish option
  # @param platform_name [String]
  # @return [String]
  def publish_option_label(platform_name)
    (platform_name == :all) ? I18n.t(:"hyrax.base.show_actions.publish.all_discovery_platforms") : platform_name
  end

  def iiif_viewer_display(*)
    return super if Comet.use_uv?
    render "Viewer Disabled"
  end
end
