# frozen_string_literal: true

module HyraxHelper
  include ::BlacklightHelper
  include Hyrax::BlacklightOverride
  include Hyrax::HyraxHelperBehavior

  # @override /dashboard/projects pagination search with page size option
  def search_action_for_dashboard
    case params[:controller]
    when "hyrax/dashboard/projects"
      main_app.dashboard_projects_path
    else
      super
    end
  end
end
