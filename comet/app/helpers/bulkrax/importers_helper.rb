module Bulkrax
  module ImportersHelper
    include WithAdminSetSelection

    def available_admin_sets
      super.select_options
    end

    # Available admin sets for select2
    def available_admin_sets_for_select2
      available_admin_sets.map { |entry| {text: entry[0], id: entry[1]} }
    end
  end
end
