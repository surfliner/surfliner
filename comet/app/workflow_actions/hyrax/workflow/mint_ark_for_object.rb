module Hyrax
  module Workflow
    ##
    # Mints an ARK for the target object
    # Avoid overriding existing ARK
    module MintARKForObject
      def self.call(target:, **)
        saved = ARK.mint_for(target.id)
        target.ark = saved.ark
      rescue ARK::ARKExistingError
        # this is a perfectly normal case
        Hyrax.logger.debug { "Skipped minting ARK #{target.ark} for object #{target.id} with existing ark #{target.ark}." }
      rescue NoMethodError => err
        # reraise unless undefined method is #ark
        raise(err) unless err.message.start_with?("undefined method `ark'")

        Hyrax.logger.warn { "Skipped minting ARK for object #{target.class} #{target.id} that does not have an #ark method." }
      end
    end
  end
end
