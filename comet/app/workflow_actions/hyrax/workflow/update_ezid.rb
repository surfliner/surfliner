module Hyrax
  module Workflow
    ##
    # updates the EZID registry with current metadata
    module UpdateEzid
      def self.call(target:, **)
        new_metadata = {}
        ARK.update_ark(target, metadata: new_metadata)
      end
    end
  end
end
