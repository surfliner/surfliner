# frozen_string_literal: true

##
# @Override Retrieve all WorkflowResponsibility at one time to improve performance
module WorkflowRolesPresenterOverride
  include WorkflowRolesBuilder

  # Retrieve Sipity::WorkflowResponsibility records for active workflows only
  def initialize(request = nil)
    @request = request
  end

  # @Override initiate once
  def users
    @users ||= ::User.registered
  end

  # @param user [User]
  # @return CometAgentPresenter
  def presenter_for(user)
    agent_key = user.is_a?(Hyrax::Group) ? user.name : user.id
    agent = agents_for_page.detect { |rec| rec.proxy_for_id == agent_key.to_s }
    return unless agent

    CometAgentPresenter.new(agent, build_responsibilities_for(agent: agent))
  end

  # @Override Retrieve minimum data needed for workflow role options
  # The select options for choosing a responsibility sorted by label
  def workflow_role_options
    sipity_workflow_roles.each_value.map { |wf_role| [Hyrax::Admin::WorkflowRolePresenter.new(wf_role).label, wf_role.id] }
      .sort_by(&:first)
  end

  # Build Sipity::WorflowRole objects for active workflows
  # @return [Hash<Integer, Sipity::WorflowRole>]
  def sipity_workflow_roles
    @sipity_workflow_roles ||= {}.tap do |pro|
      execute_query(query: workflow_roles_projects_filter_query).each do |tuple|
        pro[tuple["id"]] = build_workflow_role(tuple: tuple)
      end
    end
  end

  # Users of the current page
  def users_for_page
    @users_for_page ||= Kaminari.paginate_array(users, total_count: users.count).page(current_page).per(rows_per_page)
  end

  # @return [Integer]
  def current_page
    page = @request.params[:page] ? @request.params[:page].to_i : 1
    (page > total_pages) ? total_pages : page
  end

  # @return [Integer] total number of pages of users
  def total_pages
    @total_pages ||= (users.count / rows_per_page.to_f).ceil
  end

  # @return [Integer] rows per page
  def rows_per_page
    @request.params[:per_page] ? @request.params[:per_page].to_i : Hyrax.config.show_work_item_rows
  end

  private

  # All existing Hyrax::AdministrativeSet records
  def admin_set_ids
    @admin_set_ids ||= Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet).map(&:id)
  end

  # Workflow_roles query to filter existing projects
  def workflow_roles_projects_filter_query
    workflow_roles_join_builder
      .where(workflows[:active].eq(true).and(permission_templates[:source_id].in(admin_set_ids.map(&:to_s)))).to_sql
  end

  def build_responsibilities_for(agent:)
    [].tap do |pro|
      active_responsibilities_for(agent: agent)
        .each do |tuple|
          next unless sipity_workflow_roles[tuple[1]]

          workflow_responsibility = Sipity::WorkflowResponsibility.new(id: tuple[0])
          workflow_responsibility.agent = agent

          workflow_responsibility.workflow_role = sipity_workflow_roles[tuple[1]]

          pro << workflow_responsibility
        end
    end
  end

  # Related agents for the current page of users
  def agents_for_page
    agent_keys = users_for_page.map { |user| user.is_a?(Hyrax::Group) ? user.name : user.id.to_s }
    @agents ||= Sipity::Agent.where(proxy_for_id: agent_keys).to_a
  end

  # Workflow responsibilities with active workflows
  def active_responsiblilities
    @active_responsiblilities ||= Sipity::WorkflowResponsibility
      .where(workflow_role_id: Sipity::WorkflowRole.where(workflow_id: Sipity::Workflow.where(active: true, permission_template_id: Hyrax::PermissionTemplate.where(source_id: admin_set_ids.map(&:to_s)))))
  end

  # All workflow responsibilities for the current page of users
  def active_responsibilities_for(agent:)
    active_responsiblilities.where(agent_id: agent.id)
      .pluck(:id, :workflow_role_id, :agent_id)
  end

  ##
  # Workflow responsibilities for the agent
  class CometAgentPresenter
    def initialize(agent, workflow_responsibilities)
      @agent = agent
      @workflow_responsibilities = workflow_responsibilities
    end

    def responsibilities_present?
      @workflow_responsibilities&.present?
    end

    # Workflow responsibilities for the agent
    # @return [Array(ResponsibilityPresenter)]
    def responsibilities
      @workflow_responsibilities&.each do |responsibility|
        yield Hyrax::Admin::WorkflowRolesPresenter::ResponsibilityPresenter.new(responsibility)
      end
    end
  end
end

Hyrax::Admin::WorkflowRolesPresenter.class_eval do
  prepend WorkflowRolesPresenterOverride
end
