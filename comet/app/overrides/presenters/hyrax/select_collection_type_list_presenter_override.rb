# frozen_string_literal: true

module SelectCollectionTypeListPresenterOverride
  attr_accessor :collection_type

  # @param current_user [User]
  def initialize(current_user, collection_type = Hyrax::CollectionType::USER_COLLECTION_MACHINE_ID)
    @collection_type = collection_type
    super(current_user)
  end

  # Return an array of authorized collection types.
  def authorized_collection_types
    @authorized_collection_types ||=
      Hyrax::CollectionTypes::PermissionsService.can_create_collection_types(user: @current_user,
        collection_type: collection_type)
  end
end

Hyrax::SelectCollectionTypeListPresenter.class_eval do
  prepend SelectCollectionTypeListPresenterOverride
end
