# frozen_string_literal: true

Hyrax::FileSetPresenter.module_eval do
  ##
  # Retrieve file meatdata for all files in the fileset
  def files
    return [] unless solr_document["file_ids_ssim"]
    @files ||= Hyrax.custom_queries.find_files_by_file_set_id(file_set_id: id).sort_by { |fm| fm.id.to_s }
  end

  ##
  # Retrieve representative file of the file set
  # @return [Hyrax::FileMetadata]
  def representative_file
    @representative_file ||= find_representative_file
  end

  ##
  # Retrieve file use from files for the file set
  # @return [URI]
  def pcdm_use
    representative_file&.pcdm_use&.first
  end

  ##
  # Member presenters for file set
  # @return [Array(Hyrax::FileSetPresenter)]
  def file_set_members
    @file_set_members ||= parent.file_set_presenters.to_a
  end

  # Previous file set member to show
  def previous_file_set
    (current_index > 0) ? file_set_members[current_index - 1] : nil
  end

  # Next file set member to show
  def next_file_set
    (current_index < file_set_members.size - 1) ? file_set_members[current_index + 1] : nil
  end

  # Path of a file set
  def contextual_path(presenter, parent_presenter)
    ::Hyrax::ContextualPath.new(presenter, parent_presenter).show
  end

  private

  def find_representative_file
    use_original = Hyrax::FileMetadata::Use.uri_for(use: :original_file)
    use_preservation = Hyrax::FileMetadata::Use.uri_for(use: :preservation_file)
    original_file = files.find { |file| file.pcdm_use&.first == use_original }
    preservation_file = files.find { |file| file.pcdm_use&.first == use_preservation }

    original_file || preservation_file || files.first
  end

  ##
  # Index of the file set member currently shown.
  # @return [Integer]
  def current_index
    file_set_members.index(file_set_members.find { |m| m.id == id })
  end
end
