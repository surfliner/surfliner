Hyrax::Forms::WorkEmbargoForm.class_eval do
  ##
  # @return [String]
  def to_s
    model.title.first
  end
end
