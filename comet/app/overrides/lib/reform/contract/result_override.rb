##
# Overrides +hyrax/config/initializers/reform_rails_6_1_monkey_patch.rb+.
module ReformRails61MonkeyPatchOverride
  # The upstream code for this is (hopefully unnecessarily?) complicated;
  # this is much simpler and seems to work just as well?
  def filter_for(method, *)
    @results.collect { |result|
      result.public_send(method, *).to_h
    }.reduce({}) do |hah, err|
      hah.merge(err) { |_key, old_v, new_v| old_v.to_a | new_v }
    end
  end
end

Reform::Contract::Result.class_eval do
  prepend ReformRails61MonkeyPatchOverride
end
