# frozen_string_literal: true

# Copied from https://github.com/samvera/hyrax/blob/main/lib/hyrax/transactions/steps/file_metadata_delete.rb#L28
Hyrax::Transactions::Steps::FileMetadataDelete.class_eval do
  ##
  # @param [Hyrax::FileMetadata] FileMetadata resource
  # @param [::User] the user resposible for the delete action
  #
  # @return [Dry::Monads::Result]
  def call(resource)
    return Failure(:resource_not_persisted) unless resource.persisted?

    if resource.file_identifier.present?
      deleted_binary = DeletedBinary.new(file_identifier: resource.file_identifier)
      Comet::AppData.persister.save(resource: deleted_binary)
    end

    @persister.delete(resource: resource)
    @publisher.publish("file.metadata.deleted", metadata: resource)

    Success(resource)
  end
end
