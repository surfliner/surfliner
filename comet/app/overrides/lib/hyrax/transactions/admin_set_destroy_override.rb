# frozen_string_literal: true

require "dry/monads"

# TODO: Remove when we're on a Hyrax version that includes https://github.com/samvera/hyrax/pull/6917
module AdminSetDestroyOverride
  STEP_KEY = "delete_permission_template"

  def self.prepended(mod)
    return unless (existing = mod::DEFAULT_STEPS.find { |s| s.end_with?(STEP_KEY) })

    raise ArgumentError("Time to retire #{AdminSetDestroyOverride}: #{existing} already defined in #{mod}.DEFAULT_STEPS")
  end

  # Ideally we'd inject this in the initializer, but we can't override `initialize`
  # without losing the default arguments; thus this hack
  def container=(value)
    value.register(STEP_KEY, Hyrax::Transactions::Steps::DeletePermissionTemplate.new) unless value.key?(STEP_KEY)

    super
  end

  # Ideally we'd inject this in the initializer, but we can't override `initialize`
  # without losing the default arguments; thus this hack
  def steps=(value)
    super(value + [STEP_KEY])
  end
end

Hyrax::Transactions::AdminSetDestroy.class_eval do
  prepend AdminSetDestroyOverride
end

Hyrax::Transactions::CollectionDestroy.class_eval do
  prepend AdminSetDestroyOverride
end

# TODO: Remove when we're on a Hyrax version that includes https://github.com/samvera/hyrax/pull/6917
module Hyrax
  module Transactions
    module Steps
      class DeletePermissionTemplate
        include Dry::Monads[:result]

        def call(obj)
          permission_template = Hyrax::PermissionTemplate.find_by(source_id: obj.id)
          return Success(obj) if permission_template.nil?

          permission_template.destroy || (return Failure[:failed_to_delete_permission_template, permission_template])

          Success(obj)
        end
      end
    end
  end
end
