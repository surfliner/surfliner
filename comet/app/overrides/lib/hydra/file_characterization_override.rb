# frozen_string_literal: true

module FileCharacterizationOverride
  ##
  # @Override Process Tempfile directly or File like Files to avoid creating Tempfile's,
  # which avoids reading the whole file into memory in hydra FileCharaterization for TempFile creation.
  def run_characterizers(content, filename, tool_names, custom_paths)
    Hyrax.logger.debug("Running file characterization with #{tool_names} for #{content.try(:path) || "content string"} ...")
    return run_characterizers_on_file(content, tool_names, custom_paths) if content.try(:path) && File.exist?(content.path)

    super
  end
end

class << Hydra::FileCharacterization
  prepend FileCharacterizationOverride
end
