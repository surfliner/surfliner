# frozen_string_literal: true

##
# Provides collection representative image selection support.
Hyrax::PcdmCollection.class_eval do
  attribute :representative_id, Valkyrie::Types::ID.optional
  attribute :thumbnail_id, Valkyrie::Types::ID.optional
end
