module ImportBehaviorNoUserPermissionsOverride
  # User permissions are added by the transaction, so no action is necessary
  # here.
  def add_user_to_permission_templates!
  end
end

module ImportBehaviorFactoryClassOverrride
  # Override +factory_class+ to use the Valkyrie resource class resolver, and
  # also to give the Hyrax default collection type if the provided model is
  # “collection”.
  #
  # @note the latter part is temporary until we can auto‐generate collections of
  #   the appropriate name
  def factory_class
    if parsed_metadata&.[]("model").present?
      model_name = parsed_metadata["model"].is_a?(Array) ? parsed_metadata["model"].first : parsed_metadata["model"]
      return Hyrax.config.collection_class if model_name.downcase == "collection" # temporary; see CometCsvEntry spec
      begin
        return Valkyrie.config.resource_class_resolver.call(model_name)
      rescue NameError
      end
    end
    super
  end
end

Bulkrax::ImportBehavior.module_eval do
  prepend ImportBehaviorNoUserPermissionsOverride
  prepend ImportBehaviorFactoryClassOverrride
end
