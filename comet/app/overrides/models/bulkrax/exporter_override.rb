# frozen_string_literal: true

module ExporterOverride
  # @!attribute [rw] available export files from Minio/S3
  # @return [Array[String]]
  attr_accessor :available_export_files

  # Determine whether exclude empty fields from export or not
  def exclude_empty_fields?
    exclude_empty_fields
  end

  # Record status for the current exporter run
  def record_status
    exporter_run = Bulkrax::ExporterRun.find(current_run.id)
    if exporter_run.failed_records.positive?
      exporter_run.exporter.set_status_info("Complete (with failures)")
    else
      exporter_run.exporter.set_status_info("Complete")
    end
  end

  # @Override use schema properties
  def export_properties
    ["title"] + Bulkrax.curation_concerns.map { |klass|
      if klass.respond_to?(:reader)
        # The current class is a schema‐defined model.
        klass.reader.properties(availability: klass.availability).keys
      else
        []
      end
    }.flatten.uniq.sort
  end

  # @Override list exported zip files on S3/Minio
  def exporter_export_zip_files
    prefix_path = Bulkrax::CometCsvParser.export_zip_file_key(exporter_export_zip_path)
    # retrieve export zip files from cache if exists, otherwise retrieve from Minio/S3
    export_files = available_export_files.select { |file| file.start_with?("#{prefix_path}/") } if available_export_files.present?
    export_files ||= Bulkrax::ExportFilesService.available_export_files(prefix: prefix_path)
    export_files.map { |file| file&.split("/")&.last }
  end
end

Bulkrax::Exporter.class_eval do
  prepend ExporterOverride
end
