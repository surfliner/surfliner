# frozen_string_literal: true

Bulkrax::Importer.class_eval do
  # @Override update importer status for validate-only instead of preset it to 'Validated'.
  def status
    if validate_only
      "Validate #{super}"
    else
      super
    end
  end

  # @Override return filename from the s3 key to avoid connection timeout error.
  def self.safe_uri_filename(uri)
    return File.basename(uri.to_s) unless URI::DEFAULT_PARSER.make_regexp.match?(uri.to_s)

    super
  end
end
