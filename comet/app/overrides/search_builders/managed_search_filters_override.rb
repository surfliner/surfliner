# frozen_string_literal: true

##
# @Override ManagedSearchFilters to show dashboard objects for comet login users.
module ManagedSearchFiltersOverride
  # Override to include all groups from read access.
  def apply_group_permissions(permission_types, ability = current_ability)
    search_terms = add_managing_role_search_filter(ability: ability)
    groups = ability.user_groups
    return search_terms if groups.empty?

    permission_types.each do |type|
      field = solr_field_for(type, "group")
      # parens required to properly OR the clauses together:
      search_terms << "({!terms f=#{field}}#{groups.join(",")})"
    end
    search_terms
  end
end

Hyrax::Dashboard::ManagedSearchFilters.class_eval do
  prepend ManagedSearchFiltersOverride
end
