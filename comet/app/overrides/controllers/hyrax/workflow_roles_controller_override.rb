# frozen_string_literal: true

Hyrax::Admin::WorkflowRolesController.class_eval do
  # @override Include request for pagination
  def index
    add_breadcrumb t(:"hyrax.controls.home"), root_path
    add_breadcrumb t(:"hyrax.dashboard.breadcrumbs.admin"), hyrax.dashboard_path
    add_breadcrumb t(:"hyrax.admin.workflow_roles.header"), hyrax.admin_workflow_roles_path
    @presenter = Hyrax::Admin::WorkflowRolesPresenter.new(request)
  end
end
