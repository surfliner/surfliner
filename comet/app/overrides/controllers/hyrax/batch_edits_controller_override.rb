# frozen_string_literal: true

Hyrax::BatchEditsController.class_eval do
  # Publish objects in batch
  def publish
    batch.each do |id|
      resource = Hyrax.query_service.find_by(id: Valkyrie::ID.new(id))
      publish_resource(resource: resource)
    end
    after_update_redirect t("hyrax.works.batch_edits.publish.success")
  end

  # Unpublish objects in batch
  def unpublish
    batch.each do |id|
      resource = Hyrax.query_service.find_by(id: Valkyrie::ID.new(id))
      unpublish_resource(resource: resource)
    end
    after_update_redirect t("hyrax.works.batch_edits.unpublish.success")
  end

  # For details, see https://github.com/samvera/hyrax/blob/main/app/controllers/hyrax/batch_edits_controller.rb#L40
  def destroy_collection
    batch.each do |doc_id|
      resource = Hyrax.query_service.find_by(id: Valkyrie::ID.new(doc_id))
      if resource.is_a?(GenericObject)
        transactions["work_resource.destroy_with_recursive_delete_orphans"]
          .with_step_args("work_resource.delete" => {user: current_user},
            "work_resource.delete_all_file_sets" => {user: current_user},
            "work_resource.delete_orphan_member_objects" => {user: current_user})
          .call(resource).value!
      else
        transactions["collection_resource.destroy"]
          .with_step_args("collection_resource.delete" => {user: current_user},
            "collection_resource.remove_from_membership" => {user: current_user})
          .call(resource).value!
      end
    end
    flash[:notice] = "Batch delete complete"
    after_destroy_collection
  end

  private

  # Publish object to discovery platform(s) desinated
  # @param resource
  def publish_resource(resource:)
    published_platforms = DiscoveryPlatformService.published_platforms_for(resource).map(&:to_sym)

    platforms_to_publish = if params[:platform].to_sym == :all
      Rails.application.config.discovery_platforms
    else
      [params[:platform].to_sym]
    end

    (platforms_to_publish - published_platforms).each do |platform|
      DiscoveryPlatformPublisher.open_on(platform) do |publisher|
        publisher.publish(resource: resource)
      end
    end
  end

  # Unpublish object to discovery platform(s) desinated
  # @param resource
  def unpublish_resource(resource:)
    published_platforms = DiscoveryPlatformService.published_platforms_for(resource).map(&:to_sym)

    platforms_to_unpublish = if params[:platform].to_sym == :all
      Rails.application.config.discovery_platforms
    else
      [params[:platform].to_sym]
    end

    (platforms_to_unpublish & published_platforms).each do |platform|
      DiscoveryPlatformPublisher.open_on(platform) do |publisher|
        publisher.unpublish(resource: resource)
      end
    end
  end

  # Redirect to the return controller if present.
  # @param: notice[String]
  def after_update_redirect(notice)
    if params[:return_controller]
      redirect_to(hyrax.url_for(controller: params[:return_controller], only_path: true),
        notice: notice)
    else
      redirect_to(hyrax.dashboard_path,
        notice: notice)
    end
  end
end
