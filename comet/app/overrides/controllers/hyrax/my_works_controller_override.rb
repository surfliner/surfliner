# frozen_string_literal: true

module MyWorksControllerOverride
  # @override Include platform options for publish and unpublish
  def index
    super

    @publish_options = publish_options
    @unpublish_options = unpublish_options
  end

  # Discovery platform options that an object can unpublish from
  def unpublish_options
    platforms_to_unpublish = published_platforms.flatten.compact.uniq
    return [] unless platforms_to_unpublish.present?

    [:all] + platforms_to_unpublish
  end

  # Discovery platform options that an object can publish to
  def publish_options
    all_platforms = Rails.application.config.discovery_platforms
    platforms_to_publish = published_platforms
      .map { |platforms| all_platforms - platforms }
      .flatten.compact.uniq
    return [] unless platforms_to_publish.present?

    [:all] + platforms_to_publish
  end

  # Lookup publish platforms for all resource in the search result page
  def published_platforms
    @published_platforms ||= [].tap do |pro|
      @document_list.each do |doc|
        resource = Hyrax.query_service.find_by(id: doc.id)
        pro << DiscoveryPlatformService.published_platforms_for(resource).map(&:to_sym)
      end
    end
  end
end

Hyrax::My::WorksController.class_eval do
  prepend MyWorksControllerOverride
end
