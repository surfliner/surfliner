# frozen_string_literal: true

module FileSetsControllerOverride
  # @override Use inline ingest for carrierwave file handling
  def attempt_update_valkyrie
    return revert_valkyrie if wants_to_revert_valkyrie?
    if params.key?(:file_set)
      if params[:file_set].key?(:files)
        ValkyrieIngestJob.perform_now(uploaded_file_from_path)
      else
        update_metadata
      end
    elsif params.key?(:files_files) # version file already uploaded with ref id in :files_files array
      uploaded_files = Array(Hyrax::UploadedFile.find(params[:files_files]))
      uploaded_files.first.file_set_uri = file_set.id.to_s
      uploaded_files.first.save
      ValkyrieIngestJob.perform_now(uploaded_files.first)
      update_metadata
    end
  end
end

Hyrax::FileSetsController.class_eval do
  prepend FileSetsControllerOverride
end
