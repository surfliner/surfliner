# frozen_string_literal: true

Hyrax::Admin::AdminSetsController.class_eval do
  # @override my breadcrumb to dashborad projects
  def show
    add_breadcrumb I18n.t("hyrax.controls.home"), hyrax.root_path
    add_breadcrumb t(:"hyrax.dashboard.title"), hyrax.dashboard_path
    add_breadcrumb t(:"hyrax.admin.sidebar.projects"), main_app.dashboard_projects_path
    add_breadcrumb @admin_set.title.first
    super
  end

  # @override redirect admin sets to its own section as dashbord projects
  def index
    redirect_to main_app.dashboard_projects_path
  end

  # @override my breadcrumb to dashborad projects
  def setup_form
    add_breadcrumb t(:"hyrax.controls.home"), root_path
    add_breadcrumb t(:"hyrax.dashboard.breadcrumbs.admin"), hyrax.dashboard_path
    add_breadcrumb t(:"hyrax.admin.sidebar.projects"), main_app.dashboard_projects_path
    add_breadcrumb action_breadcrumb, request.path
    form
  end

  # @override redirect admin sets to its own section as dashbord projects
  def after_delete_success
    redirect_to main_app.dashboard_projects_path, notice: t(:"hyrax.admin.admin_sets.delete.notification")
  end
end
