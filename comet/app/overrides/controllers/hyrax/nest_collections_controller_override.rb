Hyrax::Dashboard::NestCollectionsController.class_eval do
  # @override redirect to dashboard_collections_path insted of my_collections_path from 'my' source.
  # @api public
  #
  # determine appropriate redirect location depending on specified source
  def redirect_path(item:)
    return dashboard_collections_path if params[:source] == "my"
    dashboard_collection_path(item)
  end
end
