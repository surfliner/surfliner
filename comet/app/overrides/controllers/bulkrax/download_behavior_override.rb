# frozen_string_literal: true

module DownloadBehaviorOverride
  # @Override adapt to S3/Minio support
  def file
    file_key = Bulkrax::CometCsvParser.export_zip_file_key(file_path)
    @file ||= Rails.application.config.staging_area_s3_connection
      .directories.get(S3Configurations::StagingArea.bucket)
      .files
      .get(file_key)
  end

  # @Override adapt to S3/Minio with Fog::AWS::Storage::File
  def send_file_contents
    self.status = 200
    prepare_file_headers
    send_data file.body, content_options
  end

  # @Override adapt to S3/Minio with Fog::AWS::Storage::File
  # render an HTTP HEAD response
  def content_head
    response.headers["Content-Length"] = file.try(:content_length) || file.size
    head :ok, content_type: download_content_type
  end

  # @Override adapt to S3/Minio with Fog::AWS::Storage::File support
  def prepare_file_headers
    send_file_headers! content_options
    response.headers["Content-Type"] = download_content_type
    response.headers["Content-Length"] ||= (file.try(:content_length) || file.size).to_s
    # Prevent Rack::ETag from calculating a digest over body
    response.headers["Last-Modified"] = (file.try(:last_modified) || File.mtime(file_path)).utc.strftime("%a, %d %b %Y %T GMT")
    self.content_type = download_content_type
  end
end

Bulkrax::DownloadBehavior.module_eval do
  prepend DownloadBehaviorOverride
end
