# frozen_string_literal: true

module ImportersControllerOverride
  # @override redirect to importers_path instead of importer_path with `Create and Validate` for with asynchronouse processing.
  def render_request(message, validate_only = false)
    if api_request?
      json_response("create", :created, message)
    else
      redirect_to importers_path, notice: message
    end
  end
end

Bulkrax::ImportersController.module_eval do
  prepend ImportersControllerOverride
end
