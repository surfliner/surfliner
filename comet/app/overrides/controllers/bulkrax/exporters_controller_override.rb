# frozen_string_literal: true

module ExportersControllerOverride
  # @Override cache export files for exporters
  # GET /exporters
  def index
    # NOTE: We're paginating this in the browser.
    @exporters = Bulkrax::Exporter.order(created_at: :desc).all
    if @exporters.present?
      prefix = Bulkrax::CometCsvParser.export_zip_file_key(@exporters.first.parser.base_path("export"))
      available_export_files = Bulkrax::ExportFilesService.available_export_files(prefix: prefix)
      @exporters.each { |exporter| exporter.available_export_files = available_export_files }
    end

    add_exporter_breadcrumbs if defined?(::Hyrax)
  end

  # @Override add param :exclude_empty_fields
  def exporter_params
    params[:exporter][:export_source] = params[:exporter][:"export_source_#{params[:exporter][:export_from]}"]
    if params[:exporter][:date_filter] == "1"
      params.fetch(:exporter).permit(:name, :user_id, :export_source, :export_from, :export_type, :generated_metadata,
        :include_thumbnails, :exclude_empty_fields, :parser_klass, :limit, :start_date, :finish_date, :work_visibility,
        :workflow_status, field_mapping: {})
    else
      params.fetch(:exporter).permit(:name, :user_id, :export_source, :export_from, :export_type, :generated_metadata,
        :include_thumbnails, :exclude_empty_fields, :parser_klass, :limit, :work_visibility, :workflow_status,
        field_mapping: {}).merge(start_date: nil, finish_date: nil)
    end
  end
end

Bulkrax::ExportersController.module_eval do
  prepend ExportersControllerOverride
end
