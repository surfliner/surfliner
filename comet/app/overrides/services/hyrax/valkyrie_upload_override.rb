# frozen_string_literal: true

Hyrax::ValkyrieUpload.class_eval do
  # @override apply file_metadata.file_identifier instead of file_metadata.id to keep file_ids consistenct in comet
  #
  # @param [Hyrax::FileSet] file_set the file set to add to
  # @param [Hyrax::FileMetadata] file_metadata the metadata object representing
  #   the file to add
  # @param [::User] user  the user performing the add
  #
  # @return [Hyrax::FileSet] updated file set
  def add_file_to_file_set(file_set:, file_metadata:, user:)
    file_set.file_ids |= [file_metadata.file_identifier]
    Hyrax.persister.save(resource: file_set)
    Hyrax.publisher.publish("object.membership.updated", object: file_set, user: user)
  end
end
