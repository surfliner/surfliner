# frozen_string_literal: true

module WorkflowFactoryOverride
  include Hyrax::Lockable

  # @Overrride Lock for concurrent update from bulkrax creating relationships job
  # @see CometCreateRelationshipsJob
  def create
    acquire_lock_for(work.id.to_s) do
      super
    end
  end
end

Hyrax::Workflow::WorkflowFactory.class_eval do
  prepend WorkflowFactoryOverride
end
