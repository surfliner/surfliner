# frozen_string_literal: true

Hyrax::Workflow::PermissionQuery.class_eval do
  ##
  # An ActiveRecord::Relation scope filtered by object IDs for a user
  # @param [User] user
  # @param [Array<String>] object_gids
  # @return [ActiveRecord::Relation<Sipity::Entity>]
  def self.filter_scope_entities_for_the_user(user:, object_gids: nil, workflow_state_filter: nil)
    entities = Sipity::Entity.arel_table
    workflow_state_actions = Sipity::WorkflowStateAction.arel_table
    workflow_states = Sipity::WorkflowState.arel_table
    workflow_state_action_permissions = Sipity::WorkflowStateActionPermission.arel_table

    user_agent_scope = scope_processing_agents_for(user: user)
    user_agent_contraints = user_agent_scope.arel_table.project(
      user_agent_scope.arel_table[:id]
    ).where(user_agent_scope.arel.constraints)

    join_builder = lambda do |responsibility|
      entities.project(
        entities[:id]
      ).join(workflow_state_actions).on(
        workflow_state_actions[:originating_workflow_state_id].eq(entities[:workflow_state_id])
      ).join(workflow_state_action_permissions).on(
        workflow_state_action_permissions[:workflow_state_action_id].eq(workflow_state_actions[:id])
      ).join(workflow_states).on(
        workflow_states[:id].eq(workflow_state_actions[:originating_workflow_state_id])
      ).join(responsibility).on(
        responsibility[:workflow_role_id].eq(workflow_state_action_permissions[:workflow_role_id])
      )
    end

    where_builder = ->(responsibility) { responsibility[:agent_id].in(user_agent_contraints) }

    entity_specific_joins = join_builder.call(entity_responsibilities)
    workflow_specific_joins = join_builder.call(workflow_responsibilities)

    entity_specific_where = where_builder.call(entity_responsibilities).and(
      entities[:id].eq(entity_responsibilities[:entity_id])
    )
    entity_specific_where = filter_by_workflow_state(entity_specific_where, workflow_states, workflow_state_filter) if workflow_state_filter
    workflow_specific_where = where_builder.call(workflow_responsibilities)
    workflow_specific_where = filter_by_workflow_state(workflow_specific_where, workflow_states, workflow_state_filter) if workflow_state_filter

    entities_filter = entities[:id].in(entity_specific_joins.where(entity_specific_where))
      .or(entities[:id].in(workflow_specific_joins.where(workflow_specific_where)))
    # filter by object gids if provided
    entities_filter = entities[:proxy_for_global_id].in(object_gids).and(entities_filter) if object_gids

    Sipity::Entity.where(
      entities_filter
    )
  end
end
