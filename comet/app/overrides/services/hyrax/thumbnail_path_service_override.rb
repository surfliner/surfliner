# frozen_string_literal: true

module ThumbnailPathServiceOverride
  # @override Handle thumbnail_file instead of file set for Valkyrie
  #
  # @param [#id] object - to get the thumbnail for
  # @return [String] a path to the thumbnail
  def call(object)
    return default_image if object.try(:thumbnail_id).blank?

    thumb = fetch_thumbnail(object)

    return default_image unless thumb

    # Override to support thumbnail_file in valkyrie
    return call(thumb) unless thumb.try(:file_set?) || thumb.try(:thumbnail_file?)
    return thumbnail_path(thumb) if thumb.try(:thumbnail_file?)

    if audio?(thumb)
      audio_image
    elsif thumbnail?(thumb)
      thumbnail_path(thumb)
    else
      default_image
    end
  end

  # @override Handle thumbnail path for Hyrax:FileMetadata
  #
  # @return the network path to the thumbnail
  # @param [FileSet] thumbnail the object that is the thumbnail
  def thumbnail_path(thumbnail)
    file_set_id = thumbnail.try(:file_set_id) || thumbnail.id

    Hyrax::Engine.routes.url_helpers.download_path(file_set_id,
      file: "thumbnail")
  end
end

class << Hyrax::ThumbnailPathService
  prepend ThumbnailPathServiceOverride
end
