# frozen_string_literal: true

module FindByDateRangeOverride
  ##
  # @Override Fix the issue with IN clause prepared statement for models through ANY clause with literal array,
  # This can be removed once it's fixed in upstream Hyrax.
  #
  # @param models [Array]
  # @param start_datetime [DateTime]
  # @param end_datetime [DateTime]
  def find_by_date_range(start_datetime:, end_datetime: nil, models: nil)
    end_datetime = 1.second.since(Time.zone.now).utc if end_datetime.blank?
    if models.present?
      models_val = "{#{models.join(",")}}"
      query_service.run_query(find_models_by_date_range_query, start_datetime.to_s, end_datetime.to_s, models_val)
    else
      query_service.run_query(find_by_date_range_query, start_datetime.to_s, end_datetime.to_s)
    end
  end

  def find_models_by_date_range_query
    <<-SQL
      SELECT * FROM orm_resources
      WHERE created_at >= ?
      AND created_at <= ?
      AND internal_resource = ANY(?);
    SQL
  end
end

Hyrax::CustomQueries::FindByDateRange.class_eval do
  prepend FindByDateRangeOverride
end
