# frozen_string_literal: true

Hyrax::CollectionTypes::PermissionsService.class_eval do
  # @override include :collection_type signature to distinguish project and collection
  #
  # @api public
  #
  # Ids of collection types that a user can create or manage
  #
  # @param roles [String] type of access, Hyrax::CollectionTypeParticipant::MANAGE_ACCESS and/or Hyrax::CollectionTypeParticipant::CREATE_ACCESS
  # @param user [User] user (required if ability is nil)
  # @param ability [Ability] the ability coming from cancan ability check (default: nil) (required if user is nil)
  # @return [Array<String>] ids for collection types for which a user has the specified role
  # @note Several checks get the user's groups from the user's ability.  The same values can be retrieved directly from a passed in ability.
  #   If calling from Abilities, pass the ability.  If you try to get the ability from the user, you end up in an infinit loop.
  def self.collection_type_ids_for_user_override(roles:, user: nil, ability: nil, collection_type: Hyrax::CollectionType::USER_COLLECTION_MACHINE_ID)
    return false unless user.present? || ability.present?
    permission_type_ids = Hyrax::CollectionType.where(hyrax_collection_types: {machine_id: collection_type}).select(:id).distinct.pluck(:id)
    return permission_type_ids if user_admin?(user, ability)
    Hyrax::CollectionTypeParticipant.where(agent_type: Hyrax::CollectionTypeParticipant::USER_TYPE,
      agent_id: user_id(user, ability),
      access: roles)
      .or(
        Hyrax::CollectionTypeParticipant.where(agent_type: Hyrax::CollectionTypeParticipant::GROUP_TYPE,
          agent_id: user_groups(user, ability),
          access: roles)
      )
      .select(:hyrax_collection_type_id)
      .distinct
      .pluck(:hyrax_collection_type_id) & permission_type_ids
  end

  # @override include :collection_type signature to distinguish project and collection
  #
  # @api public
  #
  # Instances of collection types that a user can create or manage
  #
  # @param roles [String] type of access, Hyrax::CollectionTypeParticipant::MANAGE_ACCESS and/or Hyrax::CollectionTypeParticipant::CREATE_ACCESS
  # @param user [User] user (required if ability is nil)
  # @param ability [Ability] the ability coming from cancan ability check (default: nil) (required if user is nil)
  # @return [Array<Hyrax::CollectionType>] instances of collection types for which a user has the specified role
  # @note Several checks get the user's groups from the user's ability.  The same values can be retrieved directly from a passed in ability.
  #   If calling from Abilities, pass the ability.  If you try to get the ability from the user, you end up in an infinit loop.
  def self.collection_types_for_user_override(roles:, user: nil, ability: nil, collection_type: Hyrax::CollectionType::USER_COLLECTION_MACHINE_ID)
    return false unless user.present? || ability.present?
    return Hyrax::CollectionType.where(hyrax_collection_types: {machine_id: collection_type}) if user_admin?(user, ability)
    Hyrax::CollectionType.where(id: collection_type_ids_for_user(user: user, roles: roles, ability: ability, collection_type: collection_type))
  end

  # @override include :collection_type signature to distinguish project and collection
  #
  # @api public
  #
  # Get a list of collection types that a user can create
  #
  # @param user [User] user (required if ability is nil)
  # @param ability [Ability] the ability coming from cancan ability check (default: nil) (required if user is nil)
  # @return [Array<Hyrax::CollectionType>] array of collection types the user can create
  # @note Several checks get the user's groups from the user's ability.  The same values can be retrieved directly from a passed in ability.
  #   If calling from Abilities, pass the ability.  If you try to get the ability from the user, you end up in an infinit loop.
  def self.can_create_collection_types_override(user: nil, ability: nil, collection_type: Hyrax::CollectionType::USER_COLLECTION_MACHINE_ID)
    collection_types_for_user_override(user: user, ability: ability, roles: [Hyrax::CollectionTypeParticipant::MANAGE_ACCESS, Hyrax::CollectionTypeParticipant::CREATE_ACCESS], collection_type: collection_type)
  end

  define_singleton_method(:collection_type_ids_for_user, singleton_method(:collection_type_ids_for_user_override))
  define_singleton_method(:collection_types_for_user, singleton_method(:collection_types_for_user_override))
  define_singleton_method(:can_create_collection_types, singleton_method(:can_create_collection_types_override))
end
