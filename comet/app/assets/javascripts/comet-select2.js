/** Build Select2 widget dynamically with class 'select2_wrapper' and data attribute 'select2-options' */
const callback = (mutationList, observer) => {
  for (const mutation of mutationList) {
    const select2Wrappers = $(mutation.target).find('.select2_wrapper');
    if (select2Wrappers.length > 0) {
      $(select2Wrappers).each(function(index, e) {
        $(e).select2({
          data: $(e).data('select2-options') || [],
          theme: "bootstrap"
        });
      });
      // Disconnect observer until next changes.
      observer.disconnect();
    }
  }
};
const observer = new MutationObserver(callback);
observer.observe(document.documentElement, { childList: true, subtree: true });
