/** Show representative image once selected from autocomplete. */
function showRepresentativeImage() {
  var data = $("input[data-autocomplete]").select2("data");
  $("#representative-icon").attr("src", `/downloads/${data.id}?inline=true`);
}