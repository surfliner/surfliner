/** Perform request for workflows filtering */
$(document).on('change', '.workflow_filters_select2_wrapper', function(e) {
  e.preventDefault();

  const filter = $(this).data('filter');
  const urlBase = $(this).data('url-base');
  const isFiltered = $(this).data('select2-selected') != '';

  $(("#filter_workflow_objects")).attr('href', isFiltered ? urlBase : `${urlBase}&${filter}=${e.target.value}`);
  document.getElementById("filter_workflow_objects").click();
});

/** Build selects options with link to remove filter constraint for filtered workflows */
function applyRemoveFilterConstraint(select2Elem) {
  let selected2Options = $(select2Elem).data('select2-options');
  const selected = $(select2Elem).data('select2-selected');
  if (selected2Options != null && selected != '') {
    // Add remove link to release the filter
    selected2Options = selected2Options.filter((option) => option.id == selected).map(function(x) {
      return { "text": buildRemoveLink(x, $(select2Elem).data('total-count')), "id": "-1"}
    });
  }
  return selected2Options;
}

function buildRemoveLink(option, count) {
  return `<a href="#" class="select2-remove"><span class="selected">${option.text}</span><span class="remove-icon" aria-hidden="true">✖</span></a> ${count}`;
}
