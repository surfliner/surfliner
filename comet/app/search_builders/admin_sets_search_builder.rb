# frozen_string_literal: true

class AdminSetsSearchBuilder < Hyrax::Dashboard::CollectionsSearchBuilder
  # This overrides the models in FilterByType
  def models
    Hyrax::ModelRegistry.admin_set_classes
  end
end
