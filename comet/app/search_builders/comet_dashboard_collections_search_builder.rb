# frozen_string_literal: true

# This search builder drops show_only_managed_collections_for_non_admins filter to allow non-admin users to access authorized collections.
class CometDashboardCollectionsSearchBuilder < Hyrax::Dashboard::CollectionsSearchBuilder
  # This override dashboard collections to access collections for non-admin users
  self.default_processor_chain -= [:show_only_managed_collections_for_non_admins]

  # This overrides the models in FilterByType
  def models
    Hyrax::ModelRegistry.collection_classes
  end
end
