# frozen_string_literal: true

# This search builder to find collection representative image
class FindCollectionRepresentativeSearchBuilder < ::Hyrax::WorksSearchBuilder
  include Hyrax::FilterByType

  self.default_processor_chain -= [:only_active_works]
  self.default_processor_chain += [:filter_on_title, :filter_on_collection_members, :filter_on_related_image, :show_without_representative]

  def initialize(context)
    super

    @id = context.params[:id] || raise("missing required parameter: id")
    @q = context.params[:q]
  end

  # Search filter on object title, original filename and file set title
  def filter_on_title(solr_parameters)
    q = @q.gsub(/[+\-"\/]/) { |ch| "\\#{ch}" }
    fs_ids = Hyrax::SolrService.query("({!type=dismax qf='title_tesim^20 original_filename_tesim^10'}#{q} AND has_model_ssim:*FileSet)", df: "title_tesim", rows: 100).map { |x| x.id }
    solr_parameters[:qt] = "standard"
    solr_parameters[:df] = "title_tesim"
    solr_parameters[:q] = fs_ids.present? ? "({!type=dismax qf=title_tesim}#{q} OR member_ids_ssim: (#{fs_ids.join(" OR ")}))" : "{!type=dismax qf=title_tesim}#{q}"
  end

  def filter_on_related_image(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] += ["hasRelatedImage_ssim:[* TO *]"]
  end

  def filter_on_collection_members(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] += [Hyrax::SolrQueryBuilderService.construct_query(member_of_collection_ids_ssim: @id)]
  end

  def show_without_representative(solr_parameters)
    ids = Hyrax::SolrService.query("{!field f=id}#{@id}", fl: "hasRelatedImage_ssim").flat_map { |x| x.fetch("hasRelatedImage_ssim", []) }.reject(&:blank?)
    return if ids.blank?

    solr_parameters[:fq] ||= []
    solr_parameters[:fq] += ["-hasRelatedImage_ssim:(#{ids.join(" OR ")})"]
  end
end
