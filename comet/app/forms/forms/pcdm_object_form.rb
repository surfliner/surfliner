module Forms
  ##
  # Dynamically creates a new +PcdmObjectForm+ class for the provided
  # (+PcdmObject+) model class.
  #
  # The class created through this method is not intended to be used for
  # collections or file·sets, which ought to have their own (hardcoded) forms.
  #
  # Compare +Hyrax::Forms::ResourceForm+.
  def self.PcdmObjectForm(model_class)
    Class.new(::Forms::PcdmObjectForm) do
      self.model_class = model_class

      include SurflinerSchema::FormFields(model_class)

      class << self
        prepend FormClassSchemaContractBehavior

        def inspect
          return "Forms::PcdmObjectForm(#{model_class})" if name.blank?
          super
        end
      end

      ##
      # Identifies the form as supporting +SurflinerSchema::FormFields+ methods.
      def schema_derived?
        true
      end
    end
  end

  ##
  # @see https://github.com/samvera/valkyrie/wiki/ChangeSets-and-Dirty-Tracking
  class PcdmObjectForm < Hyrax::Forms::PcdmObjectForm
    include FormPrimaryAndSecondaryTermsBehavior

    property :alternate_ids

    property :embargo, form: EmbargoForm, populator: :embargo_populator
    property :lease, form: LeaseForm, populator: :lease_populator

    property :visibility, default: Hyrax::VisibilityIntention::PRIVATE, populator: :visibility_populator

    def embargo_populator(fragment:, **)
      self.embargo = Hyrax::EmbargoManager.embargo_for(resource: model)
    end

    def lease_populator(fragment:, **)
      self.lease = Hyrax::LeaseManager.lease_for(resource: model)
    end

    def visibility_populator(fragment:, doc:, **)
      case fragment
      when "embargo"
        self.visibility = doc["visibility_during_embargo"]

        doc["embargo"] = doc.slice("visibility_after_embargo",
          "visibility_during_embargo",
          "embargo_release_date")
      when "lease"
        self.visibility = doc["visibility_during_lease"]
        doc["lease"] = doc.slice("visibility_after_lease",
          "visibility_during_lease",
          "lease_expiration_date")
      else
        self.visibility = fragment
      end
    end

    def discovery_links
      @discovery_links = DiscoveryPlatformService.call(id)
    end

    # Override make :title field to be single value form field
    def multiple?(key)
      return false if key.to_sym == :title
      super
    end
  end
end
