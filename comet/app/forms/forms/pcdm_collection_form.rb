module Forms
  class PcdmCollectionForm < Hyrax::Forms::PcdmCollectionForm
    self.model_class = Hyrax.config.collection_class

    include SurflinerSchema::FormFields(Hyrax.config.collection_class)

    class << self
      prepend FormClassSchemaContractBehavior
    end

    include FormPrimaryAndSecondaryTermsBehavior

    ##
    # Populate the representative image path for the collection
    RepresentativeImagePathPrepopulator = lambda do |**_options|
      return unless id.present?
      self.representative_image_path ||= Hyrax::CollectionThumbnailPathService.call(self)
    end

    ##
    # Populate the existing representative image for the collection
    RepresentativeImagePrepopulator = lambda do |**_options|
      return unless representative_id.present?
      self.representative_image ||= Hyrax.query_service.find_by(id: representative_id)
    rescue Valkyrie::Persistence::ObjectNotFoundError
      Hyrax.logger.error("Representative iamge #{representative_id} for collection #{title} is not found.")
    end

    property :representative_id, default: "", type: Valkyrie::Types::ID.optional
    property :thumbnail_id, default: "", type: Valkyrie::Types::ID.optional

    property :representative_image_path, virtual: true, prepopulator: RepresentativeImagePathPrepopulator
    property :representative_image, virtual: true, prepopulator: RepresentativeImagePrepopulator
    property :find_representative_image, virtual: true

    ##
    # Identifies the form as supporting +SurflinerSchema::FormFields+ methods.
    def schema_derived?
      true
    end

    # Override Hyrax :title field to be single value form field
    def multiple?(key)
      return false if key.to_sym == :title
      super
    end
  end
end
