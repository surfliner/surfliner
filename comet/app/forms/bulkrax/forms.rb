module Bulkrax
  module Forms
    # Create the appropriate Bulkrax form for the provided +model_class+,
    # assuming that +model_class+ is a class of PCDM Object.
    #
    # Don’t use this directly; instead, just call
    # +Bulkrax::Forms::ResourceForm+.
    def self.PcdmObjectForm(model_class)
      Class.new(Bulkrax::Forms::PcdmObjectForm) do
        self.model_class = model_class

        include SurflinerSchema::FormFields(model_class)

        class << self
          prepend Bulkrax::FormClassSchemaContractBehavior

          def inspect
            return "Bulkrax::Forms::PcdmObjectForm(#{model_class})" if name.blank?
            super
          end
        end

        ##
        # Identifies the form as supporting +SurflinerSchema::FormFields+ methods.
        def schema_derived?
          true
        end
      end
    end

    # Create the appropraite Bulkrax form from the provided +model_class+.
    def self.ResourceForm(model_class)
      @resource_forms ||= {}.compare_by_identity
      @resource_forms[model_class] ||=
        # +#respond_to?+ needs to be used here, not +#try+, because Dry::Types
        # overrides the latter??
        if model_class.respond_to?(:pcdm_collection?) && model_class.pcdm_collection?
          Bulkrax::Forms::PcdmCollectionForm
        elsif model_class.respond_to?(:pcdm_object?) && model_class.pcdm_object?
          if model_class.respond_to?(:file_set?) && model_class.file_set?
            Bulkrax::Forms::FileSetForm
          else
            Bulkrax::Forms::PcdmObjectForm(model_class)
          end
        else
          Hyrax::Forms::ResourceForm
        end
    end
  end
end
