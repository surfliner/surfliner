module Bulkrax
  module Forms
    # +Hyrax::Forms::FileSetForm+ requires +#creator+ to be set, which causes
    # problems for us in Bulkrax, so we just inherit more simply from
    # +Hyrax::Forms::ResourceForm+ for now. Ideally, we would resolve these
    # issues and inherit from the upstream version?
    class FileSetForm < Hyrax::Forms::ResourceForm
      property :title
      property :depositor
    end
  end
end
