module Bulkrax
  module Forms
    class ResourceForm < Hyrax::Forms::ResourceForm
      def self.for(resource: nil)
        Bulkrax::Forms::ResourceForm(resource.class).new(resource: resource)
      end
    end
  end
end
