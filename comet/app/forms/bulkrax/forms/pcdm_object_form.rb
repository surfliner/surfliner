module Bulkrax
  module Forms
    class PcdmObjectForm < Hyrax::Forms::PcdmObjectForm
      include FormPrimaryAndSecondaryTermsBehavior

      property :alternate_ids
      property :ark

      property :embargo, form: EmbargoForm, populator: :embargo_populator
      property :lease, form: LeaseForm, populator: :lease_populator

      property :visibility, default: Hyrax::VisibilityIntention::PRIVATE, populator: :visibility_populator

      def embargo_populator(fragment:, **)
        self.embargo = Hyrax::EmbargoManager.embargo_for(resource: model)
      end

      def lease_populator(fragment:, **)
        self.lease = Hyrax::LeaseManager.lease_for(resource: model)
      end

      def visibility_populator(fragment:, doc:, **)
        case fragment
        when "embargo"
          self.visibility = doc["visibility_during_embargo"]

          doc["embargo"] = doc.slice("visibility_after_embargo",
            "visibility_during_embargo",
            "embargo_release_date")
        when "lease"
          self.visibility = doc["visibility_during_lease"]
          doc["lease"] = doc.slice("visibility_after_lease",
            "visibility_during_lease",
            "lease_expiration_date")
        else
          self.visibility = fragment
        end
      end
    end
  end
end
