module Bulkrax
  module Forms
    class PcdmCollectionForm < Hyrax::Forms::PcdmCollectionForm
      self.model_class = Hyrax.config.collection_class

      include SurflinerSchema::FormFields(Hyrax.config.collection_class)

      class << self
        prepend Bulkrax::FormClassSchemaContractBehavior
      end

      include FormPrimaryAndSecondaryTermsBehavior

      property :ark

      ##
      # Identifies the form as supporting +SurflinerSchema::FormFields+ methods.
      def schema_derived?
        true
      end
    end
  end
end
