module FormPrimaryAndSecondaryTermsBehavior
  def primary_terms
    self.class.definitions.select { |key, form_options|
      schema_definition = self.class.form_definition(key.to_sym)
      if schema_definition
        # Use the schema definition if possible.
        schema_definition.primary?
      else
        # Hyrax‐defined primary terms have a :primary form option.
        form_options[:primary]
      end
    }.keys.map(&:to_sym)
  end

  def secondary_terms
    self.class.definitions.select { |key, form_options|
      schema_definition = self.class.form_definition(key.to_sym)
      if schema_definition
        # Display all schema‐defined nonprimary terms.
        !schema_definition.primary?
      else
        # Hyrax‐defined secondary terms have a :display form option but a
        # falsey :primary.
        form_options[:display] && !form_options[:primary]
      end
    }.keys.map(&:to_sym)
  end
end
