module Bulkrax
  module FormClassSchemaContractBehavior
    ##
    # Returns the +Dry::Validation::Contract+ for the form’s schema.
    #
    # This overrides the contract provided by +SurflinerSchema::FormFields+
    # to add additional validations.
    #
    # Compare the version of this module which _isn’t_ in the +Bulkrax+
    # namespace; we’re maintaining this one separately with the understanding
    # that bulk ingests may require a different schema contract than other
    # contexts in Comet.
    def schema_contract
      return @schema_contract unless @schema_contract.nil?
      availability = schema_availability
      @schema_contract = Class.new(super) do
        params do
          # Define additional params for things not in the schema.
          required(:title).value(Valkyrie::Types::Set.of(Valkyrie::Types::String),
            min_size?: 1, max_size?: 1)
          required(:ark).value(Valkyrie::Types::ID)
        end

        params.key_map.each do |schema_key|
          # Define additional validations for each param.
          property_name = schema_key.name.to_sym

          # Validate ARK value
          if property_name == :ark
            rule(property_name) do
              key(property_name).failure("ARK '#{value}' invalid") if value.present? && !value.to_s.match(Rails.application.config.ark_pattern)
            end
            next
          end

          # Define rules for controlled values.
          begin
            authority = Qa::Authorities::Schema.property_authority_for(name: property_name, availability: availability)
            valid_uris = Set.new(authority.all.map { |term| term[:uri].to_s })
            rule(property_name).each do
              # Rules are applied after schema coercions, so everything
              # should be a supported URI at this point. If it isn’t, that’s
              # an error.
              key(property_name).failure("value '#{value}' not in list of controlled values") unless valid_uris.include?(value.to_s)
            end
          rescue Qa::InvalidSubAuthority
            # +property_name+ is not a controlled value.
          end
        end
      end
    end
  end
end
