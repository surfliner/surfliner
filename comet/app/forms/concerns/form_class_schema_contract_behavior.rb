module FormClassSchemaContractBehavior
  ##
  # Returns the +Dry::Validation::Contract+ for the form’s schema.
  #
  # This overrides the contract provided by +SurflinerSchema::FormFields+
  # to add +:title+ and controlled value validations.
  def schema_contract
    return @schema_contract unless @schema_contract.nil?
    availability = schema_availability
    @schema_contract = Class.new(super) do
      params do
        # Define additional params for things not in the schema.
        required(:title).value(Valkyrie::Types::Set.of(Valkyrie::Types::String),
          min_size?: 1, max_size?: 1)
      end

      params.key_map.each do |schema_key|
        # Define additional validations for each param.
        property_name = schema_key.name.to_sym

        # Define rules for controlled values.
        begin
          authority = Qa::Authorities::Schema.property_authority_for(name: property_name, availability: availability)
          valid_uris = Set.new(authority.all.map { |term| term[:uri].to_s })
          rule(property_name).each do
            # Rules are applied after schema coercions, so everything
            # should be a supported URI at this point. If it isn’t, that’s
            # an error.
            key(property_name).failure("value '#{value}' not in list of controlled values") unless valid_uris.include?(value.to_s)
          end
        rescue Qa::InvalidSubAuthority
          # +property_name+ is not a controlled value.
        end
      end
    end
  end
end
