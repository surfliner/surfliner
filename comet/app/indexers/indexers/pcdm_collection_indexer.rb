module Indexers
  class PcdmCollectionIndexer < Hyrax::Indexers::PcdmCollectionIndexer
    prepend ::RdfLiteralIndexerBehavior
    prepend ::RdfUriIndexerBehavior
    include Hyrax::Indexer(Hyrax.config.collection_class.availability,
      index_loader: ::SchemaLoader.new)

    def to_solr
      super.merge({
        # Add any solr extensions here.
        hasRelatedMediaFragment_ssim: resource.try(:representative_id)&.to_s,
        hasRelatedImage_ssim: resource.try(:thumbnail_id)&.to_s
      })
    end
  end
end
