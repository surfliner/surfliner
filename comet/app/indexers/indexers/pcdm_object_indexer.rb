module Indexers
  ##
  # Dynamically creates a new +PcdmObjectIndexer+ class for the provided
  # (+Resource+) model class.
  def self.PcdmObjectIndexer(model_class)
    Class.new(::Indexers::PcdmObjectIndexer) do
      @model_class = model_class

      prepend ::RdfLiteralIndexerBehavior
      prepend ::RdfUriIndexerBehavior
      include Hyrax::Indexer(model_class.availability, index_loader: ::SchemaLoader.new)

      class << self
        attr_reader :model_class

        ##
        # @return [String]
        def inspect
          return "Indexers::PcdmObjectIndexer(#{model_class})" if name.blank?
          super
        end
      end
    end
  end

  class PcdmObjectIndexer < Hyrax::Indexers::PcdmObjectIndexer
    def to_solr
      super.merge(
        rendering_ids_ssim: resource.rendering_ids.map(&:to_s),
        source_tesim: resource.alternate_ids.map(&:to_s),
        workflow_state_name_ssim: (sipity_entity&.workflow_state ? sipity_entity.workflow_state.name : [])
      )
    end

    # Retrieve Sipity::Entity for workflow state
    def sipity_entity
      @sipity_entity ||= Sipity::Entity(resource)
    rescue Sipity::ConversionError => e
      Hyrax.logger.error { "Error retrieving Sipity::Entity for resource with id #{resource.id}: #{e}" }
      nil
    end
  end
end
