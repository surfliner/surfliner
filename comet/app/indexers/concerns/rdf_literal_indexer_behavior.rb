##
# This module should be _prepended_ to ensure that its +to_solr+ implementation
# is the final transform.
module RdfLiteralIndexerBehavior
  def to_solr
    super.transform_values do |value|
      cast_literals(value)
    end
  end

  def cast_literals(value)
    case value
    when Enumerable
      value.flat_map { |v| cast_literals(v) }
    when RDF::Literal
      # For unrecognized +RDF::Literal+s or anything which makes use of the
      # default implementation, +#object+ defaults to whatever is passed in as
      # the first argument. Use the lexical value in these cases instead. The
      # +#object+ value is only desirable for datatypes which explicitly cast
      # it, for example dates.
      #
      # Also use the lexical value for any invalid literal, as the object is
      # not meaningful in this case.
      if value.instance_of?(RDF::Literal) || !value.valid? # not a valid subclass
        value.value
      else
        value.object
      end
    else
      if value.respond_to?(:terms)
        # Nested resources may wrap multiple values simultaneously.
        #
        # This will raise an exception if the value is a nested resource which
        # cannot be coerced into an array of terms (by default, nested resources
        # are coerced to their +rdf:value+).
        terms = value.terms
        if terms.include?(value)
          # It’s not expected that this will ever be true, but just to be safe,
          # check to ensure that the +:terms+ of the value doesn’t contain the
          # value itself (which would cause an infinite loop).
          #
          # (This _is_ true for +RDF::Literal+s, but they are already handled by
          # the above case.)
          value
        else
          cast_literals(terms)
        end
      else
        value
      end
    end
  end
end
