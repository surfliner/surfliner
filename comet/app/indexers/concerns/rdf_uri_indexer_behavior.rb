##
# This module should be _prepended_ to ensure that its +to_solr+ implementation
# is the final transform.
module RdfUriIndexerBehavior
  def to_solr
    super.transform_values do |value|
      serialize_uris(value)
    end
  end

  def serialize_uris(value)
    case value
    when Enumerable
      value.map { |v| serialize_uris(v) }
    when RDF::URI
      value.canonicalize.to_s
    else
      value
    end
  end
end
