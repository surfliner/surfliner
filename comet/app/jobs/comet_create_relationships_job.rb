##
# Replaces Bulkrax's built in relationship behavior with a setup intended to use
# Sidekiq's retry behavior.
#
# This job is intended to be enqueued as soon as a pending relationship is
# added to the database (normally as part of another Bulkrax background job).
# Like the job it replaces, the scope of action is the "parent" and the
# +Bulkrax::ImporterRun+.
#
# A +Bulkrax::ImporterRun+ will process some number of +Bulkrax::Entry+ objects,
# which create a +Bulkrax::PendingRelationship+ record for each relationship in
# the parser data. It will also enqueue at least one job for each unique
# "parent" within the data. We then expect these jobs to be tried any number of
# times, and processed in any order, allowing for the possibility that the
# member objects have not yet been imported.
#
# The following outcomes are possible:
#
# - The relationships are all successfully processed, and the job succeeds.
#
# - The relationships are not successfully processed, but the errors are
#   “expected”. Expected errors include an resource not existing yet or a
#   failure to obtain a lock. The importer is left in a pending state, and the
#   job fails (to allow it to retry later).
#
# - The relationships are not successfully processed, and the errors are
#   unexpected. Unexpected errors include incorrect ACLs for the requested
#   object or miscellaneous code errors. The importer fails, while the job
#   succeeds (it will not be retried, and the import must be run again).
#
# When a job runs it goes through the following process:
#
# - Gets a lock for the importer run/parent id pair, or crashes immediately for
#   retry; it holds this lock through +#perform+ guaranteeing that only one job
#   is working with a set of pending relationships at a time.
#
# - Finds the "parent" object, or crashes immediately for retry; normally the
#   parent should have been created by the process that enqueues the job.
#
# - Succeeds immediately if no +Bulkrax::PendingRelationship+ exist; they have
#   likely been destroyed by another job with the same run/id pair or by a past
#   try of this same job. The job's goal is to handle any that exist at runtime.
#
# - Authorizes the user to edit the "parent", or fails the import (and succeeds
#   the job); a user may choose to remediate by changing the parent's ACLs and
#   then rerunning the importer.
#
# - Processes the +Bulkrax::PendingRelationship+s.
#
# - Fails the import and succeeds the job if there were unexpected errors, and
#   otherwise succeeds the job only if there were no errors.
#
# When the "parent" is not a +#collection?+, the relationships are processed by:
#
# - Aquiring a lock on parent, guaranteeing only one process is changing a
#   parent's members at a given time.
#
# - Handling each +Bulkrax::PendingRelationship+ by:
#
#   - Finding and authorizing the member;
#   - Adding an error for any members we can't find or authorize; and
#   - Collecting the member id.
#
# - Adding collected members to the parent's +#member_ids+ iff is not already in
#   the set.
#
# - Saving the parent and publishing the membership change.
#
# - Destroying +Bulkrax::PendingRelationship+s for each successfully processed
#   +Bulkrax::PendingRelationship+.
#
# When the "parent" is a +#collection?+, the relationships are processed by:
#
# - Handling each +Bulkrax::PendingRelationship+ by:
#
#   - Finding and authorizing the member object;
#   - Acquiring a lock on the member object;
#   - Collecting errors for objects which can't be found, authorized, or
#     locked;
#   - Adding the parent id to the member's +#member_of_collection_ids+ iff
#     it is not already in the set;
#   - Saving the member; and
#   - Destroying the +Bulkrax::PendingRelationship+.
#
# - Publishing the collection membership change if any
#   +Bulkrax::PendingRelationship+s were successfully handled.
#
# In either case, if the job did not succeed, we expect it to retry over the
# remaining +Bulkrax::PendingRelationship+. It's possible those relationships
# will reflect membership already handled (because the job failed before the
# record could be destroyed); in this case we expect the relationship to be
# harmlessly processed again.
#
# Eventually, all the jobs for the run/id pair will succeed when no more
# +Bulkrax::PendingRelationship+ entries exist.
#
# Believe it or not, this all should be more reliable than the older mechanism
# and should handle relationships without the need for a long delay or a
# separate +ScheduleRelationshipsJob+. We'll want to watch sidekiq's +import+
# queue for possible improvements.
#
# @see Hyrax::LockManager
# @see Bulkrax::CometCsvEntryBehavior
class CometCreateRelationshipsJob < ApplicationJob
  include Hyrax::Lockable
  include Bulkrax::ValkyrieRecordLookupBehavior

  queue_as :import

  def perform(importer_run_id:, parent_identifier:)
    acquire_lock_for("bulkrax#{importer_run_id}#{parent_identifier}") do
      importer_run = Bulkrax::ImporterRun.find(importer_run_id)

      parent_entry, parent_record = find_entry_and_record(importer_run: importer_run,
        identifier: parent_identifier)

      pending = pending_relationships(importer_run_id: importer_run_id,
        parent_identifier: parent_identifier)
      if pending.none?
        Hyrax.logger.info { "No pending relationships found for #{parent_identifier} on #{importer_run_id}; succeding relationship job." }
        return
      end

      begin
        ability = Ability.new(importer_run.user)
        ability.authorize!(:edit, parent_record) # authorize the parent up front, just once
      rescue => err
        importer_run.increment!(:failed_relationships, pending.count)
        parent_entry&.set_status_info(err, importer_run)
        return # succeed the job, but the importer failed
      end

      if parent_record.collection?
        successes, failures, errors = process_member_of_collection_relationships(pending: pending, record: parent_record, run: importer_run, ability: ability)
      else
        successes, failures, errors = process_member_relationships(pending: pending, record: parent_record, run: importer_run, ability: ability)
      end

      importer_run.increment!(:processed_relationships, successes.count)
      importer_run.increment!(:failed_relationships, failures.count)

      unexpected = errors.select do |e|
        !e.is_a?(Bulkrax::RecordNotFound) && !e.is_a?(Hyrax::LockManager::UnableToAcquireLockError)
      end
      if unexpected.any?
        # There was an unexpected error; fail the import, but succeed the job.
        unexpected.each do |e|
          parent_entry&.set_status_info(e, importer_run)
        end
      elsif errors.any?
        raise errors.last
      end
      # The only errors were allowable ones; fail the job if necessary to
      # retry it later.
    end
  end

  private

  ##
  # Returns an array of pending relationships for the provided importer run ID
  # and parent identifier.
  #
  # This collects both old‐style (ActiveRecord) and new‐style (Valkyrie)
  # relationships together.
  def pending_relationships(importer_run_id:, parent_identifier:)
    Bulkrax::PendingRelationship.where(importer_run_id: importer_run_id,
      parent_id: parent_identifier).to_a +
      Comet::AppData.query_service
        .custom_queries
        .find_pending_relationships(importer_run_id: importer_run_id,
          parent_id: parent_identifier)
        .to_a
  end

  # Lock manager with custom retries count and delay to acquire lock for relationships creation.
  def lock_manager
    @lock_manager ||= Hyrax::LockManager.new(
      Rails.application.config.lock_time_to_live_relationships,
      Rails.application.config.lock_retry_count_relationships,
      Rails.application.config.lock_retry_delay_relationships
    )
  end

  private

  ##
  # Returns an array of arrays of successes, failures, and errors. Resources
  # which fail for “expected” reasons will not be considered successes _or_
  # failures.
  #
  # @param ability [Ability]
  # @param pending [Array<Bulkrax::PendingRelationship, ::PendingRelationship>]
  # @param record [Valkyrie::Resource]
  # @param run [Bulkrax::ImporterRun]
  # @return [(Array, Array, Array)]
  def process_member_of_collection_relationships(ability:, pending:, record:, run:)
    errors = []
    failures = []
    successes = []

    pending.each do |relationship|
      member_record = find_and_authorize_record(relationship.child_id, importer_run: run, ability: ability)

      acquire_lock_for(member_record.id.to_s) do
        member_record = Hyrax.query_service.find_by(id: member_record.id)
        member_record.member_of_collection_ids |= [record.id]
        saved_record = Hyrax.persister.save(resource: member_record)

        Hyrax.publisher.publish("object.metadata.updated", object: saved_record, user: ability.current_user)
      end

      successes << relationship.child_id
      destroy_relationship(relationship)
    rescue => err
      failures << relationship.child_id unless err.is_a?(Bulkrax::RecordNotFound) || err.is_a?(Hyrax::LockManager::UnableToAcquireLockError)
      errors << err
    end

    [successes, failures, errors]
  ensure
    Hyrax.publisher.publish("collection.membership.updated", collection_id: record.id, user: ability.current_user) if successes&.any?
  end

  ##
  # Returns an array of arrays of successes, failures, and errors. Resources
  # which fail for “expected” reasons will not be considered successes _or_
  # failures.
  #
  # @param ability [Ability]
  # @param pending [Array<Bulkrax::PendingRelationship, ::PendingRelationship>]
  # @param record [Valkyrie::Resource]
  # @param run [Bulkrax::ImporterRun]
  # @return [(Array, Array, Array)]
  def process_member_relationships(ability:, pending:, record:, run:)
    errors = []
    failures = []
    successes = []

    begin
      acquire_lock_for(record.id.to_s) do
        authorized = []

        pending.each do |relationship|
          # Process each pending relationship for this record and try to
          # authorize the member object.
          member_record = find_and_authorize_record(relationship.child_id, importer_run: run, ability: ability)
          authorized << {relationship: relationship, member_record: member_record}
        rescue => err
          failures << relationship.child_id unless err.is_a?(Bulkrax::RecordNotFound)
          errors << err
        end

        if authorized.any?
          # There were sucessfully authorized members; handle them
          # appropriately.
          begin
            record = Hyrax.query_service.find_by(id: record.id)
            member_records = authorized.map { |r| r[:member_record] }
            saved_record = attach_members_to_work(record, member_records)

            initiate_file_set_permissions(saved_record, member_records)

            authorized.each do |r|
              # Attempt to destroy the relationship record now that the
              # membership has been applied.
              destroy_relationship(r[:relationship])
              successes << r[:relationship].child_id
            rescue => err
              failures << r[:relationship].child_id
              errors << err
            end
          rescue => err
            authorized.each do |r|
              failures << r[:relationship].child_id
            end
            errors << err
          ensure
            Hyrax.publisher.publish("object.membership.updated", object: saved_record, user: ability.current_user) if successes&.any?
          end
        end
      end
    rescue Hyrax::LockManager::UnableToAcquireLockError => err
      errors << err
    end

    [successes, failures, errors]
  end

  # Destroys the provided relationship; this is a convienence method for
  # handling both the ActiveRecord and Valkyrie case.
  def destroy_relationship(relationship)
    case relationship
    when Bulkrax::PendingRelationship
      relationship.destroy
    when ::PendingRelationship
      Comet::AppData.persister.delete(resource: relationship)
    end
  end

  def find_and_authorize_record(id, ability:, importer_run: nil)
    member_record = find_valkyrie_record(id, importer_run: importer_run)
    ability.authorize!(:edit, member_record)
    member_record
  end

  # Attach members to work and assign representative media
  # @work [Hyrax::Resource]
  # @members [Array(Valkyrie::ID)]
  def attach_members_to_work(work, members)
    work.member_ids = work.member_ids | members.map(&:id)

    file_sets = members.select { |m| m.is_a?(Hyrax::FileSet) }
    if file_sets.present?
      # find the first FileSet with the smallest id in order as the representative media
      file_set_ids = file_sets.map(&:id).sort_by(&:to_s)
      work.representative_id = file_set_ids.first if work.respond_to?(:representative_id) && work.representative_id.blank?
      work.thumbnail_id = file_set_ids.first if work.respond_to?(:thumbnail_id) && work.thumbnail_id.blank?
      work.rendering_ids = work.rendering_ids | file_set_ids
    end

    Hyrax.persister.save(resource: work)
  end

  # Setup permissions for file set and files
  # @work [Hyrax::Resource]
  # @members [Array(Valkyrie::ID)]
  def initiate_file_set_permissions(work, members)
    file_sets = members.select { |m| m.is_a?(Hyrax::FileSet) }
    return unless file_sets.present?

    permissions = Hyrax::AccessControlList.new(resource: work)
    file_sets.each do |fs|
      Hyrax::AccessControlList.copy_permissions(source: permissions, target: fs)

      files = Hyrax.custom_queries.find_files(file_set: fs)
      files.each { |fm| Hyrax::AccessControlList.copy_permissions(source: permissions, target: fm) }
    end
  end
end
