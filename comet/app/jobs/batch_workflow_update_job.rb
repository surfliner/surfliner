# frozen_string_literal: true

##
# a +ActiveJob+ job to update batch of workflow actions for objects.
class BatchWorkflowUpdateJob < Hyrax::ApplicationJob
  queue_as Hyrax.config.ingest_queue_name

  # Update batch of workflow object actions
  # @param [User] user
  # @param [Array<String>] object_ids identifiers of the worflow objects
  # @param [Hash] attributes the workflow attributes to update
  def perform(user:, object_ids:, attributes:)
    current_ability = Ability.new(user)
    object_ids.each do |id|
      object = Hyrax.query_service.find_by(id: id)
      workflow_action = Hyrax::Forms::WorkflowActionForm.new(current_ability: current_ability,
        work: object, attributes: attributes)
      workflow_action.save
    end
  end
end
