# frozen_string_literal: true

##
# @Override Hyrax::Workflow::ActionableObjects to show active workflow objects with ACL read access for users
class CometActionableObjects < Hyrax::Workflow::ActionableObjects
  PAGE_SIZE = 500

  ##
  # @!attribute [rw] ids and state ids of all user scope actionable workflow objects
  # @return [Array[string, string]]
  attr_accessor :ids_and_state_ids

  ##
  # @param [::User] user the user whose
  # @param [String] optional filter by workstate name
  def initialize(user:, workflow_state_filter: nil, bulkrax_importer_filter: nil, project_filter: nil)
    super(user: user, workflow_state_filter: workflow_state_filter)

    gids_and_state_ids = self.class.gid_and_state_id_pairs_for(user: user, object_ids: nil, workflow_state_filter: workflow_state_filter)

    ids_and_state_ids = gids_and_state_ids.map { |gid, state_id| [GlobalID.new(gid).model_id, state_id] }
    unless bulkrax_importer_filter.blank?
      object_ids = imported_object_ids(bulkrax_importer_filter)
      ids_and_state_ids = ids_and_state_ids.select { |id_and_state| object_ids.include?(id_and_state[0]) }
    end

    unless project_filter.blank?
      object_ids = project_object_ids(project_filter)
      ids_and_state_ids = ids_and_state_ids.select { |id_and_state| object_ids.include?(id_and_state[0]) }
    end

    @ids_and_state_ids = ids_and_state_ids
  end

  ##
  # @Override Iterate through active workflow objects with workflow state attached.
  # @return [Hyrax::Workflow::ObjectInWorkflowDecorator]
  def each
    return enum_for(:each) unless block_given?

    docs = build_solr_query&.solr_documents(page: @page, rows: @per_page, sort: "system_create_dtsi ASC") || []
    docs.each do |solr_doc|
      object = Hyrax::Workflow::ObjectInWorkflowDecorator.new(solr_doc)
      _, state_id = ids_and_state_ids.find { |id, _| id == object.id }

      object.workflow_state = all_states.find { |state| state.id == state_id }

      yield object
    end
  end

  ##
  # Active workflow states for all workflow objects
  # @return [Array[Hash<string, int>]]
  def workflow_states
    active_object_ids = documents_for_filters&.map { |doc| doc["id"] }&.compact
    state_ids = ids_and_state_ids.select { |(id, state_id)| active_object_ids&.include?(id) }.map(&:last).compact.uniq
    return [] if state_ids.none?

    Sipity::WorkflowState.find(state_ids)
  end

  ##
  # The iulkrax importers related to these actionable objects
  # @return [Array<Bulkrax::Importer>]
  def bulkrax_importers
    source_identifiers = documents_for_filters&.map { |doc| doc["source_tesim"]&.first }&.compact

    importer_ids = Bulkrax::Entry.where("importerexporter_type = ? AND identifier IN (?)", "Bulkrax::Importer", source_identifiers)
      .pluck(:importerexporter_id)
      .uniq
    Bulkrax::Importer.where("id IN (?)", importer_ids)
  end

  ##
  # List the available projects
  # @return Hyrax::AdminSetSelectionPresenter
  def available_projects
    admin_set_ids = documents_for_filters.present? ? documents_for_filters&.map { |doc| doc["admin_set_id_ssim"]&.first }&.compact&.uniq : []
    admin_sets = Hyrax.query_service.find_many_by_ids(ids: admin_set_ids)
    Hyrax::AdminSetSelectionPresenter.new(admin_sets: admin_sets)
  end

  ##
  # @Override
  # @return [Integer] total number of entities selected
  def total_count
    build_solr_query&.count || 0
  end

  ##
  # Lookup workflow state for objects in batch that a user can take actions.
  # @return [Array[String, Sipity::WorkflowState]]
  def self.id_and_state_pairs_for(user:, object_ids: nil)
    gids_and_states = gid_and_state_id_pairs_for(user: user, object_ids: object_ids)

    return [] if gids_and_states.none?

    # Lookup workflow state records for the workflow state IDs
    all_states = Sipity::WorkflowState.find(gids_and_states.map(&:last).uniq)

    gids_and_states.map do |gid, state_id|
      [GlobalID.new(gid).model_id,
        all_states.find { |state| state.id == state_id }]
    end
  end

  ##
  # Lookup workflow state ids for objects.
  # @return [Array[String, string]]
  def self.gid_and_state_id_pairs_for(user:, object_ids: nil, workflow_state_filter: nil)
    object_gids = object_ids.map { |id| Hyrax::GlobalID(Hyrax.query_service.find_by(id: id)).to_s } if object_ids

    Hyrax::Workflow::PermissionQuery
      .filter_scope_entities_for_the_user(user: user, object_gids: object_gids, workflow_state_filter: workflow_state_filter)
      .pluck(:proxy_for_global_id, :workflow_state_id)
  end

  private

  ##
  # Build solr query with ACL read access for the user that can take workflow actions
  def build_solr_query(ids: ids_and_state_ids.map(&:first))
    return if ids.none?

    Hyrax::SolrQueryService.new.accessible_by(ability: Ability.new(user), action: :read)
      .with_ids(ids: ids)
  end

  ##
  # Retrieve solr documents with specific data fields for actionable objects filter handling
  def documents_for_filters
    @documents_for_filters = if ids_and_state_ids.empty?
      []
    else
      docs = []
      ids_and_state_ids.map(&:first).each_slice(CometActionableObjects::PAGE_SIZE) do |ids|
        docs += build_solr_query(ids: ids)&.solr_documents(rows: ids.size, fl: "id,source_tesim,admin_set_id_ssim")
      end
    end
    docs
  end

  ##
  # Workflow state records of all active workflow objects
  def all_states
    @all_states ||= Sipity::WorkflowState.find(ids_and_state_ids.map(&:last).uniq)
  end

  ##
  # Retrieve objects imported by the importer
  # @param importer [Integer] - the bulkrax importer id
  def imported_object_ids(importer)
    return [] unless importer.to_i.to_s == importer

    entry_ids = Bulkrax::Importer.find(importer).entries.pluck(:id)
    complete_statuses = Bulkrax::Status.latest_by_statusable
      .includes(:statusable)
      .where("bulkrax_statuses.statusable_id IN (?) AND bulkrax_statuses.statusable_type = ? AND bulkrax_statuses.status_message = ?", entry_ids, "Bulkrax::Entry", "Complete")

    complete_statuses.map { |latest_statusable| latest_statusable.statusable&.identifier }
      .map { |id| Hyrax.query_service.find_by_alternate_identifier(alternate_identifier: id)&.id.to_s }
      .compact
  rescue
    []
  end

  ##
  # Retrieve objects by project
  # @param project [Integer] - project id
  def project_object_ids(project)
    return [] unless project.to_s == project
    objects = Hyrax::SolrQueryService.new(query: ["admin_set_id_ssim:#{project}"]).solr_documents
    objects.map { |obj| obj.id.to_s }
  rescue
    []
  end
end
