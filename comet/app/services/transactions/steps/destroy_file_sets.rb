# frozen_string_literal: true

require "dry/monads"

module Transactions
  module Steps
    ##
    # Query file sets of object and trigger a destroy transaction for them.
    class DestroyFileSets
      include Dry::Monads[:result]

      ##
      # @param [Hyrax::Work] obj
      # @param [User] user
      # @param [Boolean] force_destroy
      #
      # @return [Dry::Monads::Result]
      def call(obj, user: nil, force_destroy: false)
        if force_destroy
          work_members = Hyrax.custom_queries.find_child_work_ids(resource: obj)

          result = Hyrax::Transactions::Steps::DeleteAllFileSets.new.call(obj, user: user)
          return result unless result.success

          obj.member_ids = work_members
          obj.rendering_ids = []
          obj.representative_id = nil
          obj.thumbnail_id = nil
        end

        Success(obj)
      end
    end
  end
end
