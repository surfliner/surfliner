# frozen_string_literal: true

require "dry/monads"

module Transactions
  module Steps
    class AddBulkraxFiles
      include Dry::Monads[:result]

      ##
      # @param [Hyrax::Work | Hyrax::FileSet] obj
      # @param [Hash<Symbol, Array<RemoteFile>>] files
      # @param [User] user
      #
      # @return [Dry::Monads::Result]
      def call(obj, files: {}, user: nil)
        if files&.any?
          user ||= if obj.try(:depositor)
            User.find_by_user_key(obj.depositor)
          else
            User.system_user
          end

          # The current assumption, until a more comprehensive approach is agreed upon, is to
          # create a NEW FileSet for each bulkrax ingest, assigning ALL files for a given object/CSVEntry to that
          # FileSet.
          # see: https://gitlab.com/surfliner/surfliner/-/issues/1317
          file_set_timestamp = Hyrax::TimeService.time_in_utc

          file_set = if obj.file_set?
            obj
          else
            FileIngest.make_fileset_for(
              filename: filename(files),
              last_modified: file_set_timestamp,
              permissions: Hyrax::AccessControlList.new(resource: obj),
              user: user,
              work: obj
            )
          end

          begin
            files.each do |use, file|
              Array.wrap(file).each do |f|
                FileIngest.upload(
                  content_type: f.content_type,
                  file_body: f.file,
                  filename: f.original_filename,
                  last_modified: Hyrax::TimeService.time_in_utc,
                  permissions: Hyrax::AccessControlList.new(resource: obj),
                  size: f.content_length,
                  user: user,
                  work: (obj.file_set? ? nil : obj), # we don't need/want a work here when attaching files to a fileset.
                  use: use,
                  file_set: file_set
                )
              ensure
                f.close
              end
            end
          rescue => e
            Hyrax.logger.error("#{e.message}\n#{e.backtrace}")
            return Failure[:failed_to_attach_file_sets, files]
          end
        end

        Success(obj)
      end

      private

      def filename(files)
        file_for_name = Array(files[:preservation_file] ||
                              files[:intermediate_file] ||
                              files[:original_file] ||
                              files.values.flatten).first

        file_for_name.original_filename
      rescue TypeError
        Hyrax::TimeService.time_in_utc
      end
    end
  end
end
