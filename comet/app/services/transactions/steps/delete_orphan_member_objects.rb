# frozen_string_literal: true

require "dry/monads"

module Transactions
  module Steps
    ##
    # Identifies member objects that would be orphaned by the deletion of obj and triggers a destroy transaction for
    # them.
    # see: https://gitlab.com/surfliner/surfliner/-/issues/1451
    class DeleteOrphanMemberObjects
      include Dry::Monads[:result]

      ##
      # @param [Hyrax::Work] obj
      # @param [User] user
      #
      # @return [Dry::Monads::Result]
      def call(obj, user: nil)
        Hyrax.custom_queries.find_child_works(resource: obj).each do |child|
          if child_will_be_orphaned?(child: child, parent: obj)
            Rails.logger.info "Deleting member object #{child} that will be orphaned when #{obj} is deleted"
            Hyrax::Transactions::Container["work_resource.destroy_with_recursive_delete_orphans"]
              .with_step_args("work_resource.delete" => {user: user},
                "work_resource.delete_all_file_sets" => {user: user},
                "work_resource.delete_orphan_member_objects" => {user: user})
              .call(child)
              .value!
          else
            Rails.logger.info "Member object #{child} exists in relationship with other objects. It will not be deleted"
          end
        end
        Success(obj)
      end

      private

      def child_will_be_orphaned?(child:, parent:)
        member_of_collections = Hyrax.custom_queries.find_collections_for(resource: child)
        return false if member_of_collections.size > 0

        parents = Hyrax.query_service.find_parents(resource: child)
        parents.size == 1 && parents.first.id == parent.id
      end
    end
  end
end
