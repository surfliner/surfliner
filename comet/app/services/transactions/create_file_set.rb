module Transactions
  class CreateFileSet < Hyrax::Transactions::Transaction
    # file_set.save_acl is introduced after Hyrax 5 rc1, and is identical
    # (for now); fix this to use `file_set.save_acl` after the 5.0.0 release
    DEFAULT_STEPS = ["change_set.set_user_as_depositor",
      "change_set.apply",
      "file_set.upload_files",
      "work_resource.save_acl"].freeze

    ##
    # @see Hyrax::Transactions::Transaction
    def initialize(container: Hyrax::Transactions::Container, steps: DEFAULT_STEPS)
      super
    end
  end
end
