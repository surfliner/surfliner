# frozen_string_literal: true

module Hyrax
  class CollectionThumbnailPathService < Hyrax::ThumbnailPathService
    class << self
      ##
      # @Override find representative image uploaded by user from local storage
      # @param [#id] object - to get the thumbnail for
      # @return [String] a path to the thumbnail
      def call(object)
        local_thumbnail_path = thumbnail_filepath(object)
        return local_thumbnail_path(object) if File.exist?(local_thumbnail_path)

        super
      end

      def default_image
        ActionController::Base.helpers.image_path "collection.png"
      end

      private

      ##
      # Build thumbnail path for downloading from local storage
      # @return the network path to the thumbnail in local storage
      # @param [FileSet] thumbnail the object that is the thumbnail
      def local_thumbnail_path(thumb)
        Hyrax::Engine.routes.url_helpers.download_path(thumb.id,
          file: "thumbnail")
      end

      ##
      # @Override Build thumbnail path for comet download url pattern
      # @return the network path to the thumbnail
      # @param [FileSet] thumbnail the object that is the thumbnail
      def thumbnail_path(thumb)
        Hyrax::Engine.routes.url_helpers.download_path(thumb.id,
          inline: "true")
      end

      ##
      # @Override detect the existance of the image file in the fileset inorder
      def file_in_storage?(thumb)
        find_original_file thumb
      end

      ##
      # @Override detect the existance of the image file in the fileset inorder
      def find_original_file(thumb)
        @original_file ||= Hyrax.custom_queries.find_original_file(file_set: thumb)
      rescue Valkyrie::StorageAdapter::FileNotFound, Valkyrie::Persistence::ObjectNotFoundError
        Hyrax.logger.error("No files found in file set #{thumbnail.title} (id: #{thumbnail.id}).")
        nil
      end
    end
  end
end
