module S3Configurations
  class Default
    def self.access_key
      ENV.fetch("REPOSITORY_S3_ACCESS_KEY", "")
    end

    def self.secret_key
      ENV.fetch("REPOSITORY_S3_SECRET_KEY", "")
    end

    def self.bucket
      ENV.fetch("REPOSITORY_S3_BUCKET") { "comet#{Rails.env}" }
    end

    def self.region
      ENV.fetch("REPOSITORY_S3_REGION", "us-east-1")
    end

    def self.force_path_style
      ActiveModel::Type::Boolean.new.cast(
        ENV.fetch("REPOSITORY_S3_FORCE_PATH_STYLE", "false")
      )
    end

    def self.endpoint
      "#{ENV.fetch("REPOSITORY_S3_PROTOCOL", "http")}://#{ENV["REPOSITORY_S3_HOST"]}:#{ENV.fetch("REPOSITORY_S3_PORT", 9000)}"
    end

    def self.present?
      access_key.present? && secret_key.present?
    end
  end
end
