module S3Configurations
  class StagingArea
    def self.access_key
      ENV.fetch("STAGING_AREA_S3_ACCESS_KEY", "")
    end

    def self.secret_key
      ENV.fetch("STAGING_AREA_S3_SECRET_KEY", "")
    end

    def self.bucket
      ENV.fetch("STAGING_AREA_S3_BUCKET") { "comet-staging-area-#{Rails.env}" }
    end

    def self.region
      ENV.fetch("STAGING_AREA_S3_REGION", "us-east-1")
    end

    def self.force_path_style
      ActiveModel::Type::Boolean.new.cast(
        ENV.fetch("STAGING_AREA_S3_FORCE_PATH_STYLE", "false")
      )
    end

    def self.endpoint
      "#{ENV.fetch("STAGING_AREA_S3_PROTOCOL", "http")}://#{ENV["STAGING_AREA_S3_HOST"]}:#{ENV.fetch("STAGING_AREA_S3_PORT", 9000)}"
    end

    def self.present?
      access_key.present? && secret_key.present?
    end
  end
end
