##
# Defines a map from a visibility string value to appropriate permissions
# representing that visibility.
class VisibilityMap < Hyrax::VisibilityMap
  DEFAULT_MAP = {
    Hydra::AccessControls::AccessRight::VISIBILITY_TEXT_VALUE_PUBLIC => {
      permission: Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC,
      additions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC].freeze,
      deletions: [Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY].freeze
    }.freeze,
    Hydra::AccessControls::AccessRight::VISIBILITY_TEXT_VALUE_AUTHENTICATED => {
      permission: Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_AUTHENTICATED,
      additions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_AUTHENTICATED].freeze,
      deletions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC,
        Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY].freeze
    }.freeze,
    Comet::VISIBILITY_TEXT_VALUE_COMET => {
      permission: Comet::PERMISSION_TEXT_VALUE_COMET,
      additions: [Comet::PERMISSION_TEXT_VALUE_COMET].freeze,
      deletions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC,
        Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_AUTHENTICATED,
        Comet::PERMISSION_TEXT_VALUE_CAMPUS,
        Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY].freeze
    }.freeze,
    Comet::VISIBILITY_TEXT_VALUE_METADATA_ONLY => {
      permission: Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY,
      additions: [Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY,
        Comet::PERMISSION_TEXT_VALUE_COMET,
        Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC].freeze,
      deletions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_AUTHENTICATED,
        Comet::PERMISSION_TEXT_VALUE_CAMPUS].freeze
    }.freeze,
    Comet::VISIBILITY_TEXT_VALUE_CAMPUS => {
      permission: Comet::PERMISSION_TEXT_VALUE_CAMPUS,
      additions: [Comet::PERMISSION_TEXT_VALUE_CAMPUS,
        Comet::PERMISSION_TEXT_VALUE_COMET].freeze,
      deletions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC,
        Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_AUTHENTICATED,
        Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY].freeze
    }.freeze,
    Hydra::AccessControls::AccessRight::VISIBILITY_TEXT_VALUE_PRIVATE => {
      permission: :PRIVATE,
      additions: [].freeze,
      deletions: [Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_PUBLIC,
        Hydra::AccessControls::AccessRight::PERMISSION_TEXT_VALUE_AUTHENTICATED,
        Comet::PERMISSION_TEXT_VALUE_COMET,
        Comet::PERMISSION_TEXT_VALUE_METADATA_ONLY,
        Comet::VISIBILITY_TEXT_VALUE_CAMPUS].freeze
    }.freeze
  }.freeze

  def initialize(map: ::VisibilityMap::DEFAULT_MAP)
    super
  end
end
