# frozen_string_literal: true

# Methods for minting and editing ARKs
module ARK
  # @param [#to_s] id
  # @return [Hyrax::Work]
  def self.mint_for(id, client: Ezid::Identifier)
    obj = Hyrax.query_service.find_by(id: id.to_s)
    return obj unless obj.respond_to?(:ark)
    raise ARKExistingError unless obj.ark.nil? || obj.ark.id.blank?

    # don't rescue, we want errors to halt everything
    obj.ark = client.mint
    saved = Hyrax.persister.save(resource: obj)

    Hyrax.publisher.publish("object.metadata.updated", object: saved, user: User.system_user)
    saved
  end

  # @param [Hyrax::Work] work
  # @param [Ezid::Identifier] client
  # @param [String] target
  # @param [Hash] metadata
  #
  # @return [Hyrax::Work]
  def self.update_ark(work, client: Ezid::Identifier, metadata: {}, target: nil)
    return work unless work.respond_to?(:ark)
    id = client.find(work.ark.to_s)

    id.target = target if target.present?
    metadata.each { |key, value| id[key] = value }

    id.save

    work
  end

  # Raised when attempting to mint ARK for object with an existing ARK.
  class ARKExistingError < StandardError
  end
end
