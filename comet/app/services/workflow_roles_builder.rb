# frozen_string_literal: true

# Build Sipity::WorkflowRole with mininum data for admin workflow roles
module WorkflowRolesBuilder
  # Build workflow_roles query
  def workflow_roles_join_builder
    roles = Sipity::Role.arel_table

    workflow_roles.project(
      workflow_roles[:id],
      workflows[:id].as("workflow_id"),
      workflows[:name].as("workflow_name"),
      roles[:id].as("role_id"),
      roles[:name].as("role_name"),
      permission_templates[:id].as("permission_template_id"),
      permission_templates[:source_id]
    ).join(workflows).on(
      workflows[:id].eq(workflow_roles[:workflow_id])
    ).join(roles).on(
      roles[:id].eq(workflow_roles[:role_id])
    ).join(permission_templates).on(
      permission_templates[:id].eq(workflows[:permission_template_id])
    )
  end

  def workflows
    @workflows ||= Sipity::Workflow.arel_table
  end

  def workflow_roles
    @workflow_roles ||= Sipity::WorkflowRole.arel_table
  end

  def permission_templates
    @permission_templates ||= Hyrax::PermissionTemplate.arel_table
  end

  # Query results
  # @query[String]
  # @return [Array<Hash>]
  def execute_query(query:)
    ActiveRecord::Base.connection.exec_query(query)
  end

  # Build Sipity::WorkflowRole
  # @param tuple[Array<Hash>]
  # @return Sipity::WorkflowRole
  def build_workflow_role(tuple:)
    workflow_role = Sipity::WorkflowRole.new(id: tuple["id"])
    workflow_role.role = build_role(tuple: tuple)
    workflow_role.workflow = build_workflow(tuple: tuple)
    workflow_role
  end

  # Build Sipity::Role with minimum data for UI display
  # @param tuple[Array<Hash>]
  # @return Sipity::Role
  def build_role(tuple:)
    Sipity::Role.new(id: tuple["role_id"], name: tuple["role_name"])
  end

  # Build Sipity::Workflow with minimum data for UI display
  # @param tuple[Array<Hash>]
  # @return Sipity::Workflow
  def build_workflow(tuple:)
    workflow = Sipity::Workflow.new(id: tuple["workflow_id"], name: tuple["workflow_name"])
    workflow.permission_template = build_permission_template(tuple: tuple)
    workflow
  end

  # Build Hyrax::PermissionTemplate with minimum data for UI display
  # @param tuple[Array]
  # @return Hyrax::PermissionTemplate
  def build_permission_template(tuple:)
    Hyrax::PermissionTemplate.new(id: tuple["permission_template_id"], source_id: tuple["source_id"])
  end
end
