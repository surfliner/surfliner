# frozen_string_literal: true

# This class overrides the standard schema loader to read schema names from the
# configuration.
class SchemaLoader < SurflinerSchema::Loader
  def self.config_location
    Rails.application.config.metadata_config_location
  end

  def self.default_schemas
    Rails.application.config.metadata_config_schemas
  end

  ##
  # Provide a transformation function to coerce controlled values to their URI’s
  # within the schema & form.
  def self.property_transform_for(_schema_name)
    ->(value, property:, availability:) {
      begin
        # This will raise a +Qa::InvalidSubAuthority+ error if the property is not
        # a controlled value.
        authority = Qa::Authorities::Schema.property_authority_for(name: property.name, availability: availability)
        query_result = authority.find(value) # returns empty hash on failure
        query_result[:uri] || value
      rescue Qa::InvalidSubAuthority
        # Just return the value unmodified.
        value
      end
    }
  end

  def self.valkyrie_resource_class_for(_schema_name)
    ->(resource_class) do
      case resource_class.iri.to_s
      when "http://pcdm.org/models#Collection"
        ::PcdmCollection
      else
        resource_class.nested ? SurflinerSchema::Resource : ::PcdmObject
      end
    end
  end

  ##
  # Override the Valkyrie resource class resolver to cast any resources with a
  # name of +"Hyrax::PcdmCollection"+ to the PCDM collection class.
  #
  # This provides backwards‐compatibility for resources which were created prior
  # to configurable collection metadata.
  def resource_class_resolver
    default_resolver = super
    lambda do |class_name|
      return default_resolver.call("http://pcdm.org/models#Collection") if class_name.to_s == "Hyrax::PcdmCollection"
      default_resolver.call(class_name)
    end
  end
end
