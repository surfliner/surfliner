module Bulkrax
  ##
  # A custom CsvEntry class for Bulkrax
  class CometCsvEntry < CsvEntry
    include Bulkrax::ImportBehavior
    include CometCsvEntryBehavior

    delegate :exclude_empty_fields, to: :importerexporter

    def self.parent_field(parser)
      parser.related_parents_parsed_mapping
    end

    # @Override Read the CSV file from S3/Minio
    # there's a risk that this reads the whole file into memory and could cause a memory leak
    def self.read_data(path)
      raise "CSV path empty" if path.blank?

      options = {
        headers: true,
        header_converters: ->(h) { h.to_sym },
        encoding: "utf-8"
      }.merge(csv_read_data_options)

      csv_file = csv_file_for(path)
      raise "no file found at path #{path}" unless csv_file

      begin
        csv_fie_io = StringIO.new(csv_file.body)
        csv_fie_io.set_encoding_by_bom
        csv_wrapper_class.new(CSV.parse(csv_fie_io, **options))
      ensure
        csv_fie_io.close
      end
    end

    def self.csv_file_for(path)
      bucket_name = S3Configurations::StagingArea.bucket
      bucket = Rails.application.config.staging_area_s3_connection.directories.get(bucket_name)
      raise("Could not find a bucket for #{bucket_name}") unless bucket.present?

      bucket.files.get(path)
    end

    def multiple?(*fields)
      # Visibility need to be single value. Otherwise Dry::Monads::UnwrapError will be thrown by hyrax transactions
      return false if ["id", "visibility"].include?(fields.first)
      true
    end

    # Override to add M3 properties
    def build_export_metadata
      self.parsed_metadata = {}

      build_system_metadata
      build_files_metadata unless hyrax_record.collection?

      # A hack to initiate mapping for related_parents_parsed_mapping to avoid error.
      # see Bulkrax::CsvEntry#build_relationship_metadata
      mapping[related_parents_parsed_mapping] = mapping[related_parents_parsed_mapping] || {}
      build_relationship_metadata

      build_mapping_metadata
      build_visibility

      save!

      parsed_metadata
    end

    def build_files_metadata
      return unless hyrax_record.is_a?(Bulkrax.file_model_class)

      # prefix use like use:OriginalFile for filename column
      map_files(hyrax_record).each do |pair|
        handle_join_on_export(pair.first, [pair.last], true)
      end
    end

    ##
    # Flag to exclude field value from export
    def exclude_field_value?(value)
      exclude_empty_fields && value.blank?
    end

    ##
    # Override to remove the properties that have custom mappings from parsed_metadata, which are extracted from M3 mappings.
    def build_value(key, value)
      data = hyrax_record.send(key.to_s)

      return if exclude_field_value?(data)

      if data.is_a?(ActiveTriples::Relation)
        if value["join"]
          parsed_metadata[key_for_export(key)] = data.map { |d| prepare_export_data(d) }.join(Bulkrax.multi_value_element_join_on).to_s
        else
          data.each_with_index do |d, i|
            parsed_metadata["#{key_for_export(key)}_#{i + 1}"] = prepare_export_data(d)
          end
        end
      else
        # Now delete the mapping from m3 since custom mapping is defined for the property
        parsed_metadata.delete(key) if parsed_metadata.key? key

        parsed_metadata[key_for_export(key)] = prepare_export_data(data)
      end
    end

    def add_collections
      parent_key = related_parents_raw_mapping || "parents_column"
      add_metadata(related_parents_parsed_mapping, record[parent_key])

      children_key = related_children_raw_mapping || "children_column"
      add_metadata(related_children_parsed_mapping, record[children_key])
    end

    ##
    # Override to join value array as multi-value string
    def prepare_export_data(datum)
      if datum.is_a?(ActiveTriples::Resource)
        datum.to_uri.to_s
      elsif datum.is_a?(Array)
        datum.present? ? datum.join(Bulkrax.multi_value_element_join_on) : nil
      else
        datum
      end
    end

    # The record's visibility
    def build_visibility
      visibility = Hyrax::VisibilityReader.new(resource: hyrax_record).read
      handle_join_on_export("visibility", [visibility], true)
    end
  end
end
