module Bulkrax
  ##
  # Shared CSV Behavior for CSV entry classes in Comet/using Valkyrie
  module CometCsvEntryBehavior
    def hyrax_record
      @hyrax_record ||= Hyrax.query_service.find_by(id: identifier)
    end

    # Metadata required by Bulkrax for round-tripping
    def build_system_metadata
      model_class = hyrax_record.class
      parsed_metadata["id"] = hyrax_record.id.to_s
      parsed_metadata[source_identifier] = hyrax_record.alternate_ids.first.to_s
      parsed_metadata[key_for_export("model")] = if model_class.respond_to?(:availability)
        model_class.availability
      elsif model_class.collection?
        "Collection"
      else
        model_class.name.demodulize
      end
    end

    # @Override update logic for parents column mapping.
    def build_relationship_metadata
      parent_ids = Hyrax.query_service
        .find_parents(resource: hyrax_record)
        .map { |parent| (parent.respond_to?(:alternate_ids) && parent.alternate_ids&.present?) ? parent.alternate_ids.first : parent.id.to_s }
      return unless parent_ids.present?

      handle_join_on_export(related_parents_parsed_mapping, parent_ids, mapping[related_parents_parsed_mapping] & ["join"].present? || true)
    end

    def validate_metadata
      build_metadata
      factory.form.validate(parsed_metadata)
      factory.form.errors
    end

    # this method is called by bulkrax's `#build_metadata` so it doesn't/can't
    # validate the whole metadata. we're replacing it with #validate!
    # :noop
    def validate_record
    end

    # @Override raise Bulkrax::ParentNotFound for job retry
    def set_status_info(e = nil, current_run = nil)
      raise e if e.is_a?(ParentNotFound)
      super
    end

    private

    ##
    # @note enqueue relationship jobs immediately when adding pending relationships.
    def child_jobs
      parsed_metadata[related_children_parsed_mapping].each do |child_id|
        next if child_id.blank?

        pending = ::PendingRelationship.new(parent_id: Valkyrie::ID.new(identifier),
          child_id: Valkyrie::ID.new(child_id),
          importer_run_id: Valkyrie::ID.new(importerexporter.last_run.id))
        Comet::AppData.persister.save(resource: pending)
      end

      relationship_job.perform_later(parent_identifier: identifier,
        importer_run_id: importerexporter.last_run.id)
    end

    def map_files(file_set)
      Hyrax.custom_queries.find_files(file_set: file_set).sort_by { |fm| fm.id.to_s }
        .map { |file| file.pcdm_use&.present? ? ["use:#{file.pcdm_use.first&.to_s&.split("#")&.last}", file.original_filename] : nil }
        .compact
    end

    ##
    # @note enqueue relationship jobs immediately when adding pending relationships.
    def parent_jobs
      parsed_metadata[related_parents_parsed_mapping].each do |parent_id|
        next if parent_id.blank?

        pending = ::PendingRelationship.new(parent_id: Valkyrie::ID.new(parent_id),
          child_id: Valkyrie::ID.new(identifier),
          importer_run_id: Valkyrie::ID.new(importerexporter.last_run.id))
        Comet::AppData.persister.save(resource: pending)

        relationship_job.perform_later(parent_identifier: parent_id,
          importer_run_id: importerexporter.last_run.id)
      end
    end

    def relationship_job
      Bulkrax.relationship_job_class.constantize
    end
  end
end
