# frozen_string_literal: true

module Bulkrax
  class CometCsvCollectionEntry < CometCsvEntry
    # Use identifier set by CsvParser#unique_collection_identifier, which falls back
    # on the Collection's first title if record[source_identifier] is not present
    def add_identifier
      parsed_metadata[work_identifier] = [identifier].flatten
    end

    def add_collection_type_gid
      return if parsed_metadata["collection_type_gid"].present?

      parsed_metadata["collection_type_gid"] = ::Hyrax::CollectionType.find_or_create_default_collection_type.to_global_id.to_s
    end
  end
end
