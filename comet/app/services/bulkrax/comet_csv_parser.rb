# frozen_string_literal: true

module Bulkrax
  ##
  # A custom CsvParser for Bulkrax
  class CometCsvParser < CsvParser
    def entry_class
      CometCsvEntry
    end
    alias_method :work_entry_class, :entry_class

    def file_set_entry_class
      CometCsvFileSetEntry
    end

    def collection_entry_class
      CometCsvCollectionEntry
    end

    # @override make bulkrax Create and Validate option to be performed asynchronously
    # @return [String]
    def perform_method
      "perform_later"
    end

    class Record
      extend Forwardable
      attr_reader :csv_data, :parser

      def_delegators :@data, :[], :[]=, :keys, :merge, :to_a, :to_h, :to_hash

      def initialize(csv_data, parser)
        @csv_data = csv_data
        @parser = parser
        @data = data
      end

      def data
        entry_class.data_for_entry(csv_data, nil, parser)
      end

      def entry
        @entry ||= find_or_create_entry
      end

      def find_or_create_entry
        parser.find_or_create_entry(entry_class, self[:source_identifier], "Bulkrax::Importer", data.to_h)
      end

      def model
        model_field = csv_data.headers.intersection(parser.model_field_mappings.map(&:to_sym)).first
        model_name = csv_data[model_field]
        return unless model_name.present?
        if model_name.casecmp("collection")&.zero?
          Hyrax.config.collection_class
        else
          begin
            Valkyrie.config.resource_class_resolver.call(model_name)
          rescue NameError
          end
        end
      end

      def collection?
        model&.collection?
      end

      def file_set?
        model&.file_set?
      end

      def work?
        !collection? && !file_set?
      end

      def entry_class
        if collection?
          parser.collection_entry_class
        elsif file_set?
          parser.file_set_entry_class
        else
          parser.entry_class
        end
      end
    end

    ##
    # @note inline this from upstream to skip the `relationship' handling.
    def create_objects(types_array = nil)
      index = 0
      (types_array || %w[collection work file_set]).each do |type|
        next if type.eql?("relationship")

        send(type.pluralize).each do |current_record|
          next unless record_has_source_identifier(current_record, index)
          break if limit_reached?(limit, index)

          Bulkrax::ImporterRun.increment_counter(:enqueued_records, current_run.id)
          seen[current_record[source_identifier]] = true
          create_entry_and_job(current_record, type)
          index += 1
        end
        importer.record_status
      end
      true
    rescue => e
      set_status_info(e)
    end

    def records(_opts = {})
      return @records if @records.present?

      ## this block is from upstream Bulkrax::CSVParser; why is it necessary to save the importer when populating records?!
      file_for_import = only_updates ? parser_fields["partial_import_file_path"] : import_file_path
      # data for entry does not need source_identifier for csv, because csvs are read sequentially and mapped after raw data is read.
      csv_data = entry_class.read_data(file_for_import)
      importer.parser_fields["total"] = csv_data.count
      importer.save
      ## end block

      @records ||= csv_data.map { |r| Record.new(r, self) }
    end

    ##
    # @Override ensure exporter status is recorded
    def create_new_entries
      super
    ensure
      importerexporter.record_status
    end
    alias_method :create_from_collection, :create_new_entries
    alias_method :create_from_importer, :create_new_entries
    alias_method :create_from_worktype, :create_new_entries
    alias_method :create_from_all, :create_new_entries

    def current_records_for_export
      @current_records_for_export ||= CometParserExportRecordSet.for(
        parser: self,
        export_from: importerexporter.export_from
      )
    end

    def build_records
      @collections = records.select(&:collection?)
      @file_sets = records.select(&:file_set?)
      @works = records.select(&:work?)

      true
    end

    # @Override with additional validations
    def valid_import?
      compressed_record = records.flat_map(&:to_a).partition { |_, v| !v }.flatten(1).to_h
      error_alert = "Missing at least one required element, missing element(s) are: #{missing_elements(compressed_record).join(", ")}. "
      invalid_models = validate_model
      model_alert = "Invalid model(s): #{invalid_models.join(", ")}."
      error_alert = "#{required_elements?(compressed_record) ? "" : error_alert}#{invalid_models.present? ? model_alert : ""}"
      raise StandardError, error_alert unless invalid_models.blank? && required_elements?(compressed_record)

      perform_form_validations!

      arks_rejected = arks_assigned
      raise StandardError, "ARK(s) rejected: #{arks_rejected.join(", ")}" if arks_rejected.present?

      files_missing = missing_files
      error_alert = "File(s) missing: #{files_missing.join(", ")}"
      raise StandardError, error_alert if files_missing.present?

      file_paths.is_a?(Array)
    rescue => e
      set_status_info(e)
      false
    end

    # @Override Write CSV file to S3/Minio
    # @param file [#path, #original_filename] the file object that with the relevant data for the
    #        import.
    def write_import_file(file)
      path = File.join(path_for_import, file.original_filename)

      Rails.application.config.staging_area_s3_connection
        .directories.get(S3Configurations::StagingArea.bucket).files
        .create(key: path, body: File.open(file.path))
      path
    end

    def perform_form_validations!
      errors = []

      records.each_with_index do |record, i|
        # Fill in source identifier if it's not presented to avoid extra entries with empty source identifier created during form validation.
        # Use :id if it presents, which will be used for looking up existing resource.
        if record[:source_identifier].blank? && record[:id].present?
          record[:source_identifier] = record[:id]
        elsif record[:source_identifier].blank?
          record_has_source_identifier(record, i)
        end

        entry = record.find_or_create_entry

        next unless entry.respond_to?(:validate_metadata)

        validation_errors = entry.validate_metadata

        errors << {row: i + 2, source_identifier: record[:source_identifier], messages: validation_errors.messages} if
          validation_errors.any?

        GC.start if i % 100 == 0 # forcing garbage collection to release memories
      end

      raise StandardError, format_errors(errors) if errors.present?
    end

    # Set the following instance variables: @work_ids, @collection_ids, @file_set_ids
    # @see #current_record_ids
    def set_ids_for_exporting_from_importer
      entry_ids = Bulkrax::Importer.find(importerexporter.export_source).entries.pluck(:id)
      complete_statuses = Bulkrax::Status.latest_by_statusable
        .includes(:statusable)
        .where("bulkrax_statuses.statusable_id IN (?) AND bulkrax_statuses.statusable_type = ? AND status_message = ?", entry_ids, "Bulkrax::Entry", "Complete")
      complete_entry_identifiers = complete_statuses.map { |s| s.statusable&.identifier&.tr(":", ":") }

      all_results = Hyrax.custom_queries
        .find_many_by_alternate_ids(alternate_ids: complete_entry_identifiers)
        .each_with_object({works: [], collections: [], file_sets: []}) do |r, h|
          if r.collection?
            h[:collections] << r.id.to_s
          elsif r.file_set?
            h[:file_sets] << r.id.to_s
          elsif r.work?
            h[:works] << r.id.to_s
          end
        end
      @work_ids = all_results[:works]
      @collection_ids = all_results[:collections]
      @file_set_ids = all_results[:file_sets]
    end

    ##
    # Override to reomve the have exclamation mark from`Array.uniq` to avoid error "undefined method `delete" for nil:NilClass"
    def object_names
      return @object_names if @object_names

      @object_names = mapping.values.map { |value| value["object"] }
      @object_names.uniq.delete(nil)

      @object_names
    end

    # find the related file set ids so entries can be made for export
    def find_child_file_sets(work_ids)
      work_ids.each do |id|
        work = Hyrax.query_service.find_by(id: id)
        Hyrax.custom_queries.find_child_file_set_ids(resource: work).each { |fs_id| @file_set_ids << fs_id.to_s }
      end
    end

    def store_files(identifier, folder_count)
      record = Hyrax.query_service.find_by(id: identifier)
      return unless record

      file_sets = record.file_set? ? Array.wrap(record) : Hyrax.custom_queries.find_child_file_sets(resource: record)
      file_sets << record.thumbnail if exporter.include_thumbnails && record.thumbnail.present? && record.work?
      file_sets.each do |fs|
        path = File.join(exporter_export_path, folder_count, "files")
        FileUtils.mkdir_p(path) unless File.exist? path
        # file = filename(fs)
        # next if file.blank? || fs.original_file.blank?
        file_metadata = Hyrax::FileSetFileService.new(file_set: fs).original_file
        file = Hyrax.storage_adapter.find_by(id: file_metadata.file_identifier)

        file.rewind
        File.open(File.join(path, file_metadata.original_filename), "wb") do |f|
          f.write(file.read)
          f.close
        end
      end
    end

    # @Override upload exported zip file to S3/miniod
    def zip
      FileUtils.mkdir_p(exporter_export_zip_path)

      Dir["#{exporter_export_path}/**"].each do |folder|
        zip_path = "#{exporter_export_zip_path.split("/").last}_#{folder.split("/").last}.zip"
        FileUtils.rm_rf("#{exporter_export_zip_path}/#{zip_path}")

        zip_file_path = File.join(exporter_export_zip_path, zip_path)
        Zip::File.open(zip_file_path, create: true) do |zip_file|
          Dir["#{folder}/**/**"].each do |file|
            zip_file.add(file.sub("#{folder}/", ""), file)
          end
        end

        # Send a copy to S3/Minio for download to avoid local storage/PVC dependency
        self.class.staging_area_s3_bucket.files
          .create(key: self.class.export_zip_file_key(zip_file_path), body: File.open(zip_file_path))
      end
    end

    # The key in S3/Minio for the zip file.
    # Remove the leading slash / to avoid the files listing issue with prefix key in S3/Minio.
    def self.export_zip_file_key(file_path)
      file_path&.sub(/^(\/)+/, "")
    end

    # Retrive the s3_bucket from configuration
    def self.staging_area_s3_bucket
      s3_bucket_name = S3Configurations::StagingArea.bucket
      Rails.application.config.staging_area_s3_connection
        .directories.get(s3_bucket_name)
    end

    private

    def format_errors(errors)
      errors.each_with_object([]) { |hsh, txts|
        txts << "Row #{hsh[:row]}"
        txts << " (#{hsh[:source_identifier]})" unless hsh[:source_identifier].nil?
        txts << ":\n"
        hsh[:messages].each do |(att, msgs)|
          txts << "  #{att}:\n"
          msgs.each { |msg| txts << "    #{msg}.\n" }
        end
      }.join("")
    end

    # Validate model
    # @return Array[String]
    def validate_model
      records.each_with_index.map { |record, index|
        source_identifier = record[:source_identifier]
        model_name = record[:model]
        if model_name.blank?
          "Row #{index + 2}#{source_identifier.present? ? " (" + source_identifier + ")" : ""} => model missing"
        elsif model_name.downcase == "collection" # temporary; see specs
        else
          begin
            Valkyrie.config.resource_class_resolver.call(model_name)
            nil
          rescue NameError
            "Row #{index + 2}#{source_identifier.present? ? " (" + source_identifier + ")" : ""} => unknown model `#{model_name}`"
          end
        end
      }.compact
    end

    # Detect files that are not exists on staging area
    # @result remote_files [Array]
    def missing_files
      s3_bucket = self.class.staging_area_s3_bucket

      records.each_with_index.map do |record, index|
        next if record[:model] == "Collection"

        remote_files = []

        uses = record.keys.select { |key| key&.starts_with?("use:") }
        file_keys = uses + ["remote_files"]

        file_keys.each do |key|
          file = record[key.to_sym]
          next if file.blank?

          remote_files << file if s3_bucket.files.head(file).nil?
        end

        next unless remote_files.compact.present?

        source_identifier = record[:source_identifier]
        "Row #{index + 2}#{source_identifier.present? ? " (" + source_identifier + ")" : ""} => #{remote_files.join(" | ")}"
      end
        .compact
    end

    # Check existing ARKs minted for each record.
    def arks_assigned
      arks_minted = []
      records.each_with_index.map do |record, index|
        ark_minted = find_ark(record)
        arks_minted << "Row #{index + 2} #{record[:ark]} (#{ark_minted} assigned)" if ark_minted&.to_s&.present? && record[:ark]&.present? && ark_minted&.to_s != record[:ark]
      end

      arks_minted
    end

    # Find existing ARK minted for the record
    # @param record [CometCsvParser::Record]
    def find_ark(record)
      object = Bulkrax.object_factory.new(attributes: record, source_identifier_value: record[:source_identifier])
        .find

      return unless object&.respond_to?(:ark)
      object.ark
    rescue Valkyrie::Persistence::ObjectNotFoundError
    end
  end
end
