# frozen_string_literal: true

class Bulkrax::ExportFilesService
  class << self
    # retrieve all zip files from Minio/S3
    # @param base_path [String] - the prefix path to search
    def available_export_files(prefix:)
      Rails.application.config.staging_area_s3_connection
        .directories.get(S3Configurations::StagingArea.bucket, prefix: prefix)
        .files
        .map { |fm| fm.key }
    end
  end
end
