module Bulkrax
  module CometParserExportRecordSet
    # @api public
    # @see Bulkrax::ParserExportRecordSet
    def self.for(parser:, export_from:)
      "Bulkrax::CometParserExportRecordSet::#{export_from.classify}".constantize.new(parser: parser)
    end

    module BaseExtension
      # @override use member_ids_ssim instead
      def query_kwargs
        {fl: "id,member_ids_ssim,member_of_collection_ids_ssim", method: :post, rows: row_limit}
      end
      alias_method :works_query_kwargs, :query_kwargs
      alias_method :collections_query_kwargs, :query_kwargs

      # @Override retrieve filesets from member_ids_ssim instead
      def candidate_file_set_ids
        @candidate_file_set_ids ||= works.flat_map { |work| work.fetch("member_ids_ssim", []) }
      end

      # Retrieve memeber collection IDs from works
      def member_of_collection_ids
        @member_of_collection_ids ||= works.flat_map { |work| work.fetch("member_of_collection_ids_ssim", []) }.uniq
      end

      ##
      # @Override retrieve all results from solr batch by batch for more scalable support.
      def works
        @works ||= query_results(query: works_query, query_kwargs: works_query_kwargs)
      end

      ##
      # @Override retrieve all results from solr batch by batch for more scalable support.
      def collections
        @collections ||= query_results(query: collections_query, query_kwargs: collections_query_kwargs)
      end

      ##
      # Retrieve all results from solr batch by batch for more scalable support.
      # @param query [String]
      # @param query_kwargs [Hash]
      def query_results(query:, query_kwargs: {})
        return [] unless query.present?

        results = []
        page_size = ParserExportRecordSet::SOLR_QUERY_PAGE_SIZE
        size_expected = query_kwargs[:rows]

        return solr_documents(query, query_kwargs) if size_expected.blank? || size_expected <= page_size

        pages = (size_expected.to_f / page_size.to_f).ceil
        (1..pages).each do |page|
          start = (page - 1) * page_size
          remaining_size = size_expected - start
          rows = (remaining_size > page_size) ? page_size : remaining_size

          query_kwargs = query_kwargs.merge(start: start, rows: rows)

          page_results = solr_documents(query, query_kwargs)
          results += Array.wrap(page_results)
          break if page_results.size < page_size
        end
        results
      end

      ##
      # Query solr results
      # @param query [String]
      # @param query_kwargs [Hash]
      # @return [[]]
      def solr_documents(query, query_kwargs = {})
        ActiveFedora::SolrService.query(
          query,
          **query_kwargs
        )
      end

      # @override escape mdel name Hyrax::Fileset to avoid solr query error
      def file_sets
        @file_sets ||= if candidate_file_set_ids.empty?
          []
        else
          results = []
          candidate_file_set_ids.each_slice(ParserExportRecordSet::SOLR_QUERY_PAGE_SIZE) do |ids|
            fsq = "has_model_ssim:\"Hyrax::FileSet\" AND id:(\"" + ids.join('" OR "') + "\")"
            fsq += extra_filters if extra_filters.present?
            results += Hyrax::SolrService.query_result(
              fsq, fl: "id", method: :post, rows: ids.size
            )["response"]["docs"]
          end
          results
        end
      end
    end

    class All < ParserExportRecordSet::All
      include BaseExtension

      ##
      # @Override use model Hyrax::PcdmCollection instead of Collection
      def collections_query
        "generic_type_sim:Collection #{extra_filters}"
      end
    end

    class Collection < ParserExportRecordSet::Collection
      include BaseExtension

      ##
      # @Override use model Hyrax::PcdmCollection instead of Collection
      def collections_query
        "(id:#{importerexporter.export_source} #{extra_filters}) OR " \
        "(generic_type_sim:Collection AND member_of_collection_ids_ssim:#{importerexporter.export_source})"
      end
    end

    class Worktype < ParserExportRecordSet::Worktype
      include BaseExtension
    end

    class Importer < ParserExportRecordSet::Importer
      include BaseExtension

      ##
      # @Override Include member_ids for file sets searching, and apply size for rows to avoid solr maxSize error.
      def works
        @works ||= ParserExportRecordSet.in_batches(complete_entry_identifiers) do |ids|
          ActiveFedora::SolrService.query(
            extra_filters.to_s,
            **query_kwargs.merge(
              fq: [
                %(#{solr_name(work_identifier)}:("#{ids.join('" OR "')}")),
                "has_model_ssim:(#{Bulkrax.curation_concerns.join(" OR ")})"
              ],
              rows: ids.size
            )
          )
        end
      end

      ##
      # @Override use model Hyrax::PcdmCollection instead of Collection, and apply size for rows to avoid solr maxSize error.
      def collections
        @collections ||= ParserExportRecordSet.in_batches(complete_entry_identifiers) do |ids|
          ActiveFedora::SolrService.query(
            "has_model_ssim:\"Hyrax::PcdmCollection\" #{extra_filters}",
            **query_kwargs.merge(
              fq: [
                %(#{solr_name(work_identifier)}:("#{ids.join('" OR "')}")),
                "generic_type_sim:Collection"
              ],
              fl: "id",
              rows: ids.size
            )
          )
        end
      end

      # @override search file sets with member_ids from works, and apply size for rows to avoid solr maxSize error.
      # This is an exception; we don't know how many candidate file sets there might be.  So we will instead
      # make the query (assuming that there are {#complete_entry_identifiers}).
      #
      # @see Bulkrax::ParserExportRecordSet::Base#file_sets
      def file_sets
        @file_sets ||= (search_file_sets(complete_entry_identifiers, solr_name(work_identifier)) + super).uniq { |doc| doc["id"] }
      end

      def search_file_sets(ids, solr_field)
        ParserExportRecordSet.in_batches(complete_entry_identifiers) do |ids|
          ActiveFedora::SolrService.query(
            extra_filters,
            query_kwargs.merge(
              fq: [
                %(#{solr_field}:("#{ids.join('" OR "')}")),
                "has_model_ssim:\"#{Bulkrax.file_model_class}\""
              ],
              fl: "id",
              rows: ids.size
            )
          )
        end
      end
    end
  end
end
