# frozen_string_literal: true

module Bulkrax
  module ValkyrieRecordLookupBehavior
    ##
    # Search valkyrie resources for the given identifier.
    #
    # The bulk ingest may supply either an alternate identifier (source id) or an
    # ordinary identifier for an existing resource.
    #
    # If a +Bulkrax::ImporterRun+ is provided, its information will be added to
    # any resulting error.
    #
    # @param identifier [String]
    # @param importer_run [Bulkrax::ImporterRun, nil]
    # @return [Valkyrie::Resource]
    def find_valkyrie_record(identifier, importer_run: nil)
      begin
        return Hyrax.query_service.find_by_alternate_identifier(alternate_identifier: identifier)
      rescue Valkyrie::Persistence::ObjectNotFoundError
        # The record could not be found by alternate identifier; try finding by
        # ordinary identifier.
      end
      begin
        Hyrax.query_service.find_by(id: identifier)
      rescue Valkyrie::Persistence::ObjectNotFoundError
        # The record couldn’t be found by ordinary identifier either; this is an
        # error.
        err_msg = "Could not find object with identifier:#{identifier}"
        err_msg += " for ImporterRun:#{importer_run.id} on #{importer_run.importer_id}" if importer_run
        raise RecordNotFound, err_msg
      end
    end

    ##
    # Returns an array whose first element is the +Bulkrax::Entry+ for the parent
    # of this relationship job, and whose second element is the corresponing
    # Valkyrie resource.
    #
    # The entry may be +nil+, but it is an error if no record can be found.
    def find_entry_and_record(identifier:, importer_run:)
      entry = Bulkrax::Entry.find_by(identifier: identifier, importerexporter_id: importer_run&.importer_id)
      if entry
        record = entry.factory.find
        raise RecordNotFound, "#{entry.class}:#{entry.id} exists with status #{entry.status}, but couldn't find a related Resource" unless record
        [entry, record]
      else
        [nil, find_valkyrie_record(identifier, importer_run: importer_run)]
      end
    end
  end

  class RecordNotFound < RuntimeError; end
end
