module Bulkrax
  ##
  # A custom CsvFileSetEntry class for Bulkrax
  class CometCsvFileSetEntry < CsvFileSetEntry
    include CometCsvEntryBehavior

    delegate :metadata_only?, to: :importerexporter

    def self.parent_field(parser)
      parser.related_parents_parsed_mapping
    end

    def factory
      @factory ||=
        ValkyrieFileSetFactory.new(attributes: parsed_metadata,
          source_identifier_value: identifier,
          work_identifier: parser.work_identifier,
          related_parents_parsed_mapping: parser.related_parents_parsed_mapping,
          replace_files: replace_files,
          user: user,
          klass: factory_class,
          importer_run_id: importerexporter.last_run.id,
          update_files: update_files)
    end

    def file_reference
      parsed_metadata.keys.find { |key| key.starts_with?("use:") && parsed_metadata[key].any? } ||
        super
    end

    ##
    # @Override metadata overlay with records exported from bulkrax that includes id,
    # skip checking files since we don't need to ingest files.
    def validate_presence_of_filename!
      return if parsed_metadata&.[](file_reference)&.map(&:present?)&.any? || parsed_metadata.key?("id")

      raise StandardError, "File set must have a filename"
    end

    def build_files_metadata
      # Don't export files with metadata_only export type for metadata overlay (re-import).
      return if metadata_only?

      # prefix use like use:OriginalFile for filename column
      map_files(hyrax_record).each do |pair|
        handle_join_on_export(pair.first, [pair.last], true)
      end
    end

    # @Override raise Bulkrax::RecordConflict for job retry
    def set_status_info(e = nil, current_run = nil)
      raise e if e.is_a?(RecordConflict)
      super
    end

    class ValkyrieFileSetFactory < ValkyrieObjectFactory
      def transform_attributes(**opts)
        super.tap do |attrs|
          attrs["creator"] = @user.user_key
        end
      end

      def update
        raise "Object doesn't exist" unless @object

        # Raise RecordConflict error that allow the Bulkrax::ImportFileSetJob to retry.
        handle_conflict_file_set if @replace_files && @object.is_a?(Hyrax::FileSet)

        attrs = transform_attributes(update: true)

        update_record(attrs)
      end

      # Delete the orphan FileSet and raise RecordConflict to allow job retry.
      def handle_conflict_file_set
        destroy_file_set(file_set: @object) unless find_parent(@object)

        raise(RecordConflict, @object)
      end

      # Find parent by child resource, FileSet etc.
      # @param resource [Hyrax::Resource]
      def find_parent(resource)
        Hyrax.custom_queries.find_parent_work(resource: resource)
      end

      # Destroy FileSet skeleton on error.
      def on_create_error(error)
        file_set = search_by_identifier
        destroy_file_set(file_set: file_set) if file_set

        super
      end

      # Destroy FileSet
      def destroy_file_set(file_set:)
        Hyrax::Transactions::Container["file_set.destroy"]
          .with_step_args("file_set.remove_from_work" => {user: @user},
            "file_set.delete" => {user: @user})
          .call(file_set)
          .value_or { |failure| raise failure.first.to_s }
      end

      def create_transaction_for(_klass)
        Hyrax::Transactions::Container["file_set.create"]
          .with_step_args("file_set.upload_files" => {files: get_s3_files, user: @user},
            "change_set.set_user_as_depositor" => {user: @user})
      end

      def update_transaction_for(_klass, replace_files = false)
        s3_files = get_s3_files
        if s3_files.present?
          Hyrax::Transactions::Container["file_set.create"]
            .with_step_args("file_set.upload_files" => {files: s3_files, user: @user},
              "change_set.set_user_as_depositor" => {user: @user})
        else
          Hyrax::Transactions::Container["change_set.update_file_set"]
        end
      end

      def new_remote_files(file_attribute)
        return super unless @object.try(:persisted?)

        @_existing_file_metadata_names ||= Hyrax.custom_queries.find_files(file_set: @object).map(&:original_filename)
        super.reject { |f| @_existing_file_metadata_names.include?(f[:file_name]) }
      end
    end
  end

  class RecordConflict < StandardError
  end
end
