if ENV["COVERAGE"]
  require "simplecov-cobertura"
  require "simplecov-html"

  SimpleCov.formatters = [
    SimpleCov::Formatter::HTMLFormatter,
    SimpleCov::Formatter::CoberturaFormatter
  ]

  SimpleCov.start do
    coverage_dir ENV.fetch("COVERAGE_DIR", "artifacts/coverage")
    puts "SimpleCov: Writing coverage information to #{coverage_dir}"

    enable_coverage :branch
    # TODO: set minimum_coverage to 100 / 100 once we've achieved that
    minimum_coverage line: 0, branch: 0
  end
else
  puts "SimpleCov: COVERAGE not set; skipping coverage checks"
end
