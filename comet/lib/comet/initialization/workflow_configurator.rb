# frozen_string_literal: true

module Comet
  module Initialization
    # Configures custom settings for Comet workflow loading / initialization
    class WorkflowConfigurator
      class << self
        # Configures workflow loading / initialization from environment
        def configure!
          new(
            workflows_subpath: ENV["COMET_WORKFLOWS_SUBPATH"],
            default_active_workflow_name: ENV["COMET_DEFAULT_ACTIVE_WORKFLOW_NAME"]
          ).configure!
        end
      end

      attr_reader :wf_path_new, :wf_name_new

      def initialize(workflows_subpath:, default_active_workflow_name:)
        @wf_path_new = construct_workflow_files_path(workflows_subpath) if workflows_subpath.present?
        @wf_name_new = default_active_workflow_name if default_active_workflow_name.present?

        validate!
      end

      def configure!
        Hyrax::Workflow::WorkflowImporter.path_to_workflow_files = wf_path_new if wf_path_new
        Hyrax.config.default_active_workflow_name = wf_name_new if wf_name_new
      end

      private

      # Injects `subpath` just above the last path element (e.g. `*.json`) in the
      # currently configured `path_to_workflow_files`
      def construct_workflow_files_path(subpath)
        current_path = Hyrax::Workflow::WorkflowImporter.path_to_workflow_files
        pn_current = Pathname.new(current_path)
        pn_current.parent
          .join(subpath)
          .join(pn_current.basename) # probably *.json but let's not assume
      end

      def validate!
        wf_path = wf_path_new || Hyrax::Workflow::WorkflowImporter.path_to_workflow_files
        wf_files = Dir.glob(wf_path)
        raise ArgumentError, "Invalid workflow path #{wf_path}>: no workflows found" if wf_files.empty?

        wf_name = wf_name_new || Hyrax.config.default_active_workflow_name
        raise ArgumentError, "Default workflow #{wf_name.inspect} not found in #{wf_path}" unless workflow_exists?(
          wf_name, wf_files
        )
      end

      def workflow_exists?(wf_name, wf_files)
        wf_files.any? do |wf_file|
          data = JSON.load_file(wf_file).with_indifferent_access
          Array.wrap(data[:workflows]).any? { |wf_config| wf_config[:name] == wf_name }
        end
      end
    end
  end
end
