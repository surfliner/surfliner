# frozen_string_literal: true

module Comet
  module Initialization
    class PermissionTemplateCleaner
      class << self
        # Deletes any PermissionTemplates (and associated records) that are
        # not associated with any AdminSet or Collection.
        def clean!
          valid_source_ids = find_valid_source_ids

          orphaned_templates = Hyrax::PermissionTemplate.where.not(source_id: valid_source_ids)
          Rails.logger.warn("Destroying #{orphaned_templates.count} PermissionTemplates with invalid source_ids")

          # destroy_all loads all templates into memory at once, let's not do that
          orphaned_templates.find_each { |template| template.destroy }
        rescue ActiveRecord::ConnectionNotEstablished, ActiveRecord::StatementInvalid, Sequel::DatabaseError
          # TODO: find a more elegant way not to run this as part of db:migrate, assets:precompile etc.
          Rails.logger.debug("Skipping #{self}; database not ready")
        end

        private

        def valid_model_classes
          [
            Hyrax.config.admin_set_class,
            Hyrax.config.collection_class,
            # TODO: We have a handful of old PcdmCollection resources -- but should we?
            Hyrax::PcdmCollection
          ]
        end

        # This is pretty inefficient if the number of valid sources is large, but
        # (1) it shouldn't be, and (2) the Valkyrie::Persistence::Memory::QueryService
        # used in specs doesn't give us any more efficient options.
        def find_valid_source_ids
          valid_model_classes.each_with_object([]) do |model_class, source_ids|
            resources = Hyrax.query_service.find_all_of_model(model: model_class)
            resources.each do |resource|
              source_ids << resource.id.to_s
            end
          end
        end
      end
    end
  end
end
