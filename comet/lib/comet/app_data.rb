module Comet
  module AppData
    def self.query_service
      Valkyrie::MetadataAdapter.find(:app_data).query_service
    end

    def self.persister
      Valkyrie::MetadataAdapter.find(:app_data).persister
    end
  end
end
