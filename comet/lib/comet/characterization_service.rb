# frozen_string_literal: true

module Comet
  class CharacterizationService < Hyrax::Characterization::ValkyrieCharacterizationService
    # Create temporary file from source for characterization.
    # This will bypass the OOM error while reading the whole file content for
    # file characterization on upstream Hyrax.
    def content
      Hyrax.logger.debug "Creating temporary file from source (id: #{source.id}) for characterization."

      file_io = source.io.try(:file) || source.io
      Tempfile.new([File.basename(file_name), File.extname(file_name)]).tap do |f|
        IO.copy_stream(file_io, f)
        f.rewind
      end
    end
  end
end
