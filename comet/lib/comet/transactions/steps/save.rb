module Comet
  module Transactions
    module Steps
      class Save < Hyrax::Transactions::Steps::Save
        private

        # Override for datetime validation with time zone
        def valid_future_date?(item, attribute)
          return true if item.fields[attribute].blank?
          raise StandardError, "#{item.model} must use a future date" if item.fields[attribute]&.in_time_zone(Time.zone)&.before?(Time.zone.now)
        end
      end
    end
  end
end
