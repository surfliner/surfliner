# frozen_string_literal: true

require "comet/app_data"
require "comet/characterization_service"
require "comet/publisher_otel_behavior"
require "comet/transactions/steps/save"

module Comet
  # visibility and permission values for comet
  PERMISSION_TEXT_VALUE_COMET = "comet"
  PERMISSION_TEXT_VALUE_METADATA_ONLY = "metadata_only"
  PERMISSION_TEXT_VALUE_CAMPUS = "campus"
  VISIBILITY_TEXT_VALUE_COMET = "comet"
  VISIBILITY_TEXT_VALUE_METADATA_ONLY = "metadata_only"
  VISIBILITY_TEXT_VALUE_CAMPUS = "campus"

  ##
  # @return an OpenTelemetry tracer
  # @see https://opentelemetry.io/docs/instrumentation/ruby/manual/
  def tracer
    Rails.application.config.tracer
  end
  module_function :tracer

  ##
  # @return [Boolean]
  def use_uv?
    !ActiveModel::Type::Boolean.new.cast(ENV["COMET_DISABLE_UNIVERSAL_VIEWER"])
  end
end
