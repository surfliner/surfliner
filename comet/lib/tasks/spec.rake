# Note that RSpec::Core::RakeTask isn't available in production build
begin
  require "rspec/core/rake_task"

  namespace :spec do
    non_unit_tags = %w[type:system rabbitmq integration]

    desc "Run all tests not tagged #{non_unit_tags.join("/")}"
    RSpec::Core::RakeTask.new(unit: ["spec:prepare"]) do |task|
      task.rspec_opts = non_unit_tags.map { |t| "--tag ~#{t}" }.join(" ")
    end

    desc "Run all tasks tagged #{non_unit_tags.join("/")}"
    RSpec::Core::RakeTask.new(non_unit: ["spec:prepare", "comet:rabbitmq:wait"]) do |task|
      task.rspec_opts = non_unit_tags.map { |t| "--tag #{t}" }.join(" ")
    end
  end

  namespace :coverage do
    desc "Collates all test coverage results"
    task :report do
      require "simplecov"
      coverage_root = ENV.fetch("COVERAGE_DIR", "artifacts/coverage")
      coverage_data = Dir.glob("#{coverage_root}/**/.resultset.json")
      puts "SimpleCov: collating coverage data from #{coverage_data.join(":")}"
      SimpleCov.collate coverage_data
    end
  end

  if ENV["CI_GENERATE_REPORTS"]
    ENV["CI_REPORTS"] ||= "artifacts/rspec"
    require "ci/reporter/rake/rspec"

    puts "CI::Reporter: writing reports to #{ENV["CI_REPORTS"]}"
    Rake::Task["spec:prepare"].enhance(["ci:setup:rspec"])
  else
    puts "CI::Reporter: CI_GENERATE_REPORTS not set, skipping reports"
  end
rescue LoadError => e
  warn "Skipping RSpec rake tasks due to LoadError: #{e.message} (RAILS_ENV=#{Rails.env})"
end
