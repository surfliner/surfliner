# frozen_string_literal: true

namespace :comet do
  namespace :index do
    desc "Reindex all resources in the Comet metadata database"
    task reindex: :environment do
      [
        Hyrax::FileSet,
        PcdmCollection.subclasses,
        PcdmObject.subclasses
      ].flatten.each do |klass|
        Hyrax.logger.info("Reindexing all #{klass} objects")

        Hyrax.query_service.find_all_of_model(model: klass).each do |resource|
          Hyrax.logger.debug("Reindexing #{klass} with ID #{resource.id}")
          Hyrax.index_adapter.save(resource: resource)
        end
      end
    end

    desc "Wipe the comet solr index"
    task wipe: :environment do
      Hyrax.index_adapter.wipe!
    end
  end
end
