# frozen_string_literal: true

require "csv"

namespace :comet do
  namespace :bulkrax do
    # This will create CSV for Bulkrax Importer. CSV files can be fond in folder spec/fixtures/bulkrax.
    # @param importer_name - the importer name to use for Bulkrax import, which is the CSV filename.
    # @param rows - data rows
    # @param model - The object model
    desc "Create example CSV for bulkrax load test. Command to run: `rake comet:bulkrax:create_csv importer_name=load-test-100 rows=100 model=GenericObject`"
    task :create_csv do
      rows = (ENV["rows"] || "100").to_i
      importer_name = ENV["importer_name"] || "load-test-#{rows}"
      model_name = ENV["model"] || "GenericObject"
      csv_path = Rails.root.join("spec", "fixtures", "bulkrax", "#{importer_name}.csv")

      puts "-- Creating #{rows} rows of CSV for bulkrax importer ..."

      create_csv(model_name, csv_path, rows)

      puts "-- CSV created. Location: #{csv_path}"
    end

    # This will import CSV(s) through Bulkrax Importer, then check for importer status and time needed for the imperter run to finish.
    # @param csv_files - the CSV files, which will be loaded from directory spec/fixtures/bulkrax. Multiple CSV files can be delimited by comma (,). And the file name will be the importer name.
    # @param validate_only - true: Create and Validate; false: Create and Import
    desc "Import CSVs. Command to run: `rake comet:bulkrax:import_csv csv_files=load-test-100.csv validate_only=false`"
    task import_csv: [:environment] do
      validate_only = ActiveModel::Type::Boolean.new.cast((ENV["validate_only"] || "false"))
      csv_files = (ENV["csv_files"] || "load-test.csv").split(",")
      user = User.find_or_create_by(email: "comet-admin@library.ucsb.edu")
      admin_set = Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet).first

      importer_names = []
      ingest_threads = []
      csv_files.each do |csv_file|
        puts "-- Importing CSV #{csv_file} ..."

        importer_name = File.basename(csv_file, ".csv")
        importer_names << importer_name

        csv_path = Rails.root.join("spec", "fixtures", "bulkrax", csv_file)
        upload_file = OpenStruct.new(original_filename: csv_file, path: csv_path)

        importer_params = {
          name: importer_name,
          admin_set_id: admin_set.id,
          user_id: user.id,
          parser_klass: "Bulkrax::CometCsvParser",
          parser_fields: {}
        }

        ingest_thread = Thread.new { process_import(upload_file, importer_params, validate_only) }
        ingest_threads << ingest_thread

        sleep 1.seconds
      end

      ingest_threads.each { |thread| thread.join }

      sleep 10.seconds
      # Check import status
      import_status importer_names
    end

    # This will check for importer status and time needed for the imperter run to finish.
    # @param importer_names - the the name of the importers. Multiple importer names can be delimited by comma (,).
    desc "Check Status of importers. Command to run: `rake comet:bulkrax:import_status importer_names=load-test-100`"
    task import_status: [:environment] do
      importer_names = ENV["importer_names"] || "load-test"
      import_status importer_names.split(",").map(&:strip)
    end
  end
end

##
# Process CSV validation/import
# @param upload_csv_file [OpenStruct] - the upload CSV file
# @param importer_params [Hash] - parameters for the importer
def process_import(upload_csv_file, importer_params, validate_only = false)
  puts "-- Processing csv #{upload_csv_file.original_filename} for importer #{importer_params[:name]} ..."

  importer = Bulkrax::Importer.new(importer_params)
  importer.validate_only = true if validate_only
  importer.parser_fields["update_files"] = true if !validate_only
  importer.save
  importer[:parser_fields]["import_file_path"] = importer.parser.write_import_file(upload_csv_file)
  importer.save

  Bulkrax::ImporterJob.send(importer.parser.perform_method, importer.id)
end

##
# Checkfor importer status and the time it need to finish validation/import
# @param importer_names [Array<String>]
def import_status(importer_names)
  importers = importer_names.map { |importer_name| Bulkrax::Importer.where(name: importer_name).order(created_at: :desc).first }.compact

  puts "No imorter with name #{importer_names} found!" unless importers.size.positive?

  loop do
    importer_statuses = []
    entries_sizes = []
    importers.each_with_index do |importer, index|
      entries = Bulkrax::Entry.where(importerexporter_id: importer.id)
      entry_statuses = Bulkrax::Status.where("statusable_id IN (?) AND statusable_type='Bulkrax::Entry'", entries.map(&:id)).order(created_at: :desc)
      entry_statuses_size = entry_statuses.size
      entries_size = entries.size
      entries_sizes << entries_size
      if entries_size.positive? && entry_statuses_size.positive? && (entry_statuses_size % entries_size) == 0
        sleep 1.seconds

        importer_status = Bulkrax::Status.latest_by_statusable
          .includes(:statusable)
          .where("bulkrax_statuses.statusable_id=? AND bulkrax_statuses.statusable_type='Bulkrax::Importer'", importer.id)

        next unless importer_status.present?

        import_seconds = if entries_size == entry_statuses_size
          (entry_statuses.first.created_at.to_f - importer.created_at.to_f).to_i
        else
          # In case of "Create and Validate" first, then Continue import:
          # calculate time for validation and ingest specifically
          validation_time = (entry_statuses[entries_size].created_at.to_f - importer.created_at.to_f).to_i
          ingest_time = (entry_statuses.first.created_at.to_f - importer.updated_at.to_f).to_i
          puts "#{entry_statuses[entries_size].inspect}\nCreate and Validate: #{validation_time} seconds\nContinue Ingest: #{ingest_time} seconds"
          (validation_time + ingest_time)
        end

        puts "\n### #{importer.name} (#{importer_status.first.status_message}): #{import_seconds} seconds"

        importer_statuses << import_seconds
      end
    end

    break if importer_statuses.size == importers.size

    puts "Waiting on bulkrax importer status. #{importer_statuses.size} of #{importers.size} finished."

    # Scale up waiting time for status querying basing on size to minimize overhead with status check
    wait_scale = (entries_sizes.max / 1000.0).to_f.ceil
    sleep wait_scale * 15.seconds
  end
end

##
# Create CSV for Bulkrax import
# @param model_name [String]
# @param csv_file [String] - the path of the CSV
# @param rows [Integer] - member of rows
def create_csv(model_name, csv_file, rows)
  headers = ["model", "source_identifier", "title", "visibility"]

  CSV.open(csv_file, "wb") do |csv|
    csv << headers

    (1..rows).each do |i|
      csv << [model_name, nil, "Object title: #{rows}-#{i.to_s.rjust(4, "0")}", "open"]
    end
  end
end
