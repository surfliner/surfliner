abort("Expected RAILS_ENV=test but was #{ENV["RAILS_ENV"]}") unless ENV["RAILS_ENV"] == "test"

require_relative "../config/application"
Rails.application.load_tasks

require "equivalent-xml"
require "equivalent-xml/rspec_matchers"
require "factory_bot_rails"
require "spec_helper"
require "sidekiq/testing"

ENV["SOLR_URL"] = ENV["SOLR_TEST_URL"] if ENV["SOLR_TEST_URL"]

begin
  require File.expand_path("../config/environment", __dir__)
rescue => e
  abort(e.full_message)
end

require "rspec/rails"

# TODO: consider requiring these as needed rather than all of them for every test
Dir[Rails.root.join("spec", "support", "**", "*.rb")].sort.each { |f| require f }

require "hyrax/specs/shared_specs/factories/strategies/valkyrie_resource"
FactoryBot.register_strategy(:valkyrie_create, ValkyrieCreateStrategy)

ActiveJob::Base.queue_adapter = :test
Sidekiq::Testing.inline!

Rake::Task["db:prepare"].invoke

# register a test adapter for unit tests
Valkyrie::MetadataAdapter
  .register(Valkyrie::Persistence::Memory::MetadataAdapter.new,
    :test_adapter)

query_registration_target =
  Valkyrie::MetadataAdapter.find(:test_adapter).query_service.custom_queries
[Hyrax::CustomQueries::Navigators::CollectionMembers,
  Hyrax::CustomQueries::Navigators::ChildFileSetsNavigator,
  Hyrax::CustomQueries::Navigators::ChildWorksNavigator,
  Hyrax::CustomQueries::Navigators::ParentWorkNavigator,
  Hyrax::CustomQueries::FindAccessControl,
  Hyrax::CustomQueries::FindCollectionsByType,
  Hyrax::CustomQueries::FindManyByAlternateIds,
  Hyrax::CustomQueries::FindIdsByModel,
  Hyrax::CustomQueries::Navigators::FindFiles,
  TestQueries::FindFileMetadata].each do |handler|
  query_registration_target.register_query_handler(handler)
end

# register/use the memory storage adapter for tests
Valkyrie::StorageAdapter
  .register(Valkyrie::Storage::Memory.new,
    :memory)

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true

  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  DatabaseCleaner.allow_remote_database_url = true
  config.before :suite do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before do
    Hyrax.persister.wipe!
    Hyrax.index_adapter.wipe! rescue RSolr::Error::Http # rubocop:disable Style/RescueModifier
  end

  config.before(:suite) do
    Fog.mock!
    Fog::Mock.reset

    Rails.application.config.staging_area_s3_connection =
      Fog::Storage.new(provider: "AWS", aws_access_key_id: "minio-access-key",
        aws_secret_access_key: "minio-secret-key", region: "us-east-1",
        endpoint: "http://minio:9000", path_style: true)
    Rails.application.config.staging_area_s3_connection
      .directories.create(key: S3Configurations::StagingArea.bucket)

    Valkyrie.config.metadata_adapter = :test_adapter
    Valkyrie.config.storage_adapter = :memory
  end

  config.after(:suite) do
    Valkyrie.config.metadata_adapter = :comet_metadata_store
    Valkyrie.config.storage_adapter = :repository_s3
  end

  config.around(:example, :integration) do |example|
    Fog.unmock!
    old_s3_conn = Rails.application.config.staging_area_s3_connection
    Rails.application.config.staging_area_s3_connection =
      Fog::Storage.new(provider: "AWS",
        aws_access_key_id: S3Configurations::StagingArea.access_key,
        aws_secret_access_key: S3Configurations::StagingArea.secret_key,
        endpoint: S3Configurations::StagingArea.endpoint,
        path_style: S3Configurations::StagingArea.force_path_style,
        region: S3Configurations::StagingArea.region)

    Valkyrie.config.metadata_adapter = :comet_metadata_store
    Valkyrie.config.storage_adapter = :repository_s3
    example.run
    Hyrax.persister.wipe!
    Valkyrie.config.metadata_adapter = :test_adapter
    Valkyrie.config.storage_adapter = :memory
    Rails.application.config.staging_area_s3_connection = old_s3_conn
    Fog.mock!
  end

  config.around(:example, :metadata_adapter) do |example|
    Valkyrie.config.metadata_adapter = example.metadata[:metadata_adapter]
    example.run
    Hyrax.persister.wipe! # cleanup after ourselves when using a custom adapter
    Valkyrie.config.metadata_adapter = :test_adapter
  end
  config.around(:example, :storage_adapter) do |example|
    Valkyrie.config.storage_adapter = example.metadata[:storage_adapter]
    example.run
    Valkyrie.config.storage_adapter = :memory
  end

  config.around(:example, :perform_enqueued) do |example|
    ActiveJob::Base.queue_adapter.filter =
      example.metadata[:perform_enqueued].try(:to_a)
    ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
    ActiveJob::Base.queue_adapter.perform_enqueued_at_jobs = true

    example.run

    ActiveJob::Base.queue_adapter.filter = nil
    ActiveJob::Base.queue_adapter.perform_enqueued_jobs = false
    ActiveJob::Base.queue_adapter.perform_enqueued_at_jobs = false
  end

  config.around :example, :tz do |example|
    Time.use_zone(example.metadata[:tz]) { example.run }
  end

  config.include Capybara::RSpecMatchers, type: :input
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include Devise::Test::IntegrationHelpers, type: :system
  config.include CometCapybaraHelpers
  config.include_context "with an admin set", type: :system
  config.include_context "with an admin set", with_admin_set: true
  config.include_context "with an admin set", with_project: true

  config.before(:each, type: :system) do
    Capybara.app_host = "http://#{Capybara.server_host}:#{Capybara.server_port}"

    if ENV["SKIP_SELENIUM"].present?
      driven_by(:rack_test)
    else
      driven_by(:selenium_chrome_comet)
    end

    Hyrax.persister.wipe!
  end
  config.around(:each, type: :system) do |example|
    Fog.unmock!
    old_s3_conn = Rails.application.config.staging_area_s3_connection
    Rails.application.config.staging_area_s3_connection =
      Fog::Storage.new(provider: "AWS",
        aws_access_key_id: S3Configurations::StagingArea.access_key,
        aws_secret_access_key: S3Configurations::StagingArea.secret_key,
        endpoint: S3Configurations::StagingArea.endpoint,
        path_style: S3Configurations::StagingArea.force_path_style,
        region: S3Configurations::StagingArea.region)

    example.run

    Rails.application.config.staging_area_s3_connection = old_s3_conn
    Fog.mock!
  end
  config.after(:each, type: :system) do
    Capybara.reset_sessions!
    page.driver.reset!
    Hyrax.persister.wipe!
  end

  config.after do
    DatabaseCleaner.clean
    # Ensuring we have a clear queue between each spec.
    ActiveJob::Base.queue_adapter.enqueued_jobs = []
    ActiveJob::Base.queue_adapter.performed_jobs = []
  end
end
