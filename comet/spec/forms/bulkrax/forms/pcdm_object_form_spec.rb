# frozen_string_literal: true

require "rails_helper"

RSpec.describe Bulkrax::Forms::PcdmObjectForm do
  let(:generic_object) { GenericObject.new }
  let(:form) { Bulkrax::Forms::ResourceForm.for(resource: generic_object) }

  it "is a Bulkrax::Forms::PcdmObjectForm" do
    expect(form).to be_a Bulkrax::Forms::PcdmObjectForm
  end

  describe ".definitions" do
    it "includes fields from the loaded metadata schema as keys" do
      expect(form.class.definitions.keys.map(&:to_sym)).to include :title_alternative
    end
  end

  describe ".schema_contract" do
    let(:contract) { form.class.schema_contract.new }

    it "exists" do
      expect(form.class).to respond_to(:schema_contract)
      expect(contract).to be_a(Dry::Validation::Contract)
    end

    it "does not error when given a single title" do
      result = contract.call({title: ["My title"]})
      expect(result.errors.to_h).not_to include(:title)
    end

    it "validates title presence" do
      result = contract.call({})
      expect(result).to be_failure
      expect(result.errors.to_h).to include(:title)
      expect(result.errors.to_h[:title]).to contain_exactly("size cannot be less than 1")
    end

    it "validates title as single‐valued" do
      result = contract.call({title: ["a thing", "b thing"]})
      expect(result).to be_failure
      expect(result.errors.to_h).to include(:title)
      expect(result.errors.to_h[:title]).to contain_exactly("size cannot be greater than 1")
    end
  end

  describe "#sync" do
    context "when setting an embargo" do
      let(:params) do
        {title: ["Object Under Embargo"],
         embargo_release_date: Date.tomorrow.to_s,
         visibility: "embargo",
         visibility_after_embargo: "open",
         visibility_during_embargo: "restricted"}
      end

      it "builds an embargo" do
        form.validate(params)

        expect { form.sync }
          .to change { generic_object.embargo }
          .to have_attributes(embargo_release_date: Date.tomorrow.to_s,
            visibility_after_embargo: "open",
            visibility_during_embargo: "restricted")
      end

      it 'sets visibility to "during" value' do
        form.validate(params)

        expect(form.visibility).to eq "restricted"
      end
    end

    context "when setting a lease" do
      let(:params) do
        {title: ["Object Under Lease"],
         lease_expiration_date: Date.tomorrow.to_s,
         visibility: "lease",
         visibility_after_lease: "restricted",
         visibility_during_lease: "open"}
      end

      it "builds an embargo" do
        form.validate(params)

        expect { form.sync }
          .to change { generic_object.lease }
          .to have_attributes(lease_expiration_date: Date.tomorrow.to_s)
      end

      it 'sets visibility to "during" value' do
        form.validate(params)
        expect(form.visibility).to eq "open"
      end
    end
  end
end
