FactoryBot.define do
  factory :generic_object, class: "GenericObject", aliases: [:hyrax_object, :pcdm_object] do
    sequence(:title) { |n| "Complete Moomin Comics; vol #{n}" }

    transient do
      visibility { nil }
      edit_users { [] }
      edit_groups { [] }
      read_users { [] }
      read_groups { [] }
    end

    transient do
      members { [] }
      member_of_collections { [] }
    end

    after(:build) do |object, evaluator|
      object.member_ids = evaluator.members.map(&:id) if evaluator.members.present?
      object.member_of_collection_ids = evaluator.member_of_collections.map(&:id) if evaluator.member_of_collections.present?
    end

    after(:create) do |object, evaluator|
      if evaluator.visibility
        Hyrax::VisibilityWriter
          .new(resource: object)
          .assign_access_for(visibility: evaluator.visibility)
      end

      object.permission_manager.edit_groups = evaluator.edit_groups
      object.permission_manager.edit_users = evaluator.edit_users
      object.permission_manager.read_users = evaluator.read_users
      object.permission_manager.read_users = evaluator.read_groups
      object.permission_manager.acl.save
    end

    trait :with_index do
      after(:create) do |object|
        Hyrax.index_adapter.save(resource: object)
      end
    end
  end

  factory :geospatial_object, class: "GeospatialObject", aliases: [:geo_object] do
    sequence(:title) { |n| "Maps of Africa; vol #{n}" }

    trait :with_index do
      after(:create) do |object|
        Hyrax.index_adapter.save(resource: object)
      end
    end
  end
end
