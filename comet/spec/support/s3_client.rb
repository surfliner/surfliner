# frozen_string_literal: true

require "aws-sdk-s3"

class S3Client
  def s3_cache
    @s3_cache ||= {}
  end

  def create_bucket
    lambda { |context|
      name = context.params[:bucket]
      return "BucketAlreadyExists" if s3_cache[name]
      s3_cache[name] = {}
    }
  end

  def client
    Aws::S3::Client.new(
      stub_responses: {
        create_bucket: create_bucket
      }
    )
  end
end
