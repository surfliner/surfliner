class FadeDiscoveryPlatformPublisher
  attr_reader :published
  attr_reader :unpublished

  def initialize
    @published = []
    @unpublished = []
  end

  def open_on(*)
    yield self
  end

  def append_access_control_to(**)
    :no_op
  end

  def publish(resource:)
    @published << resource
  end

  def unpublish(resource:)
    @unpublished << resource
  end
end
