class PublisherSpy
  Hyrax::Publisher.events.each_value do |registered_event|
    listener_method = registered_event.listener_method
    attr_name = registered_event.listener_method.to_s.sub(/^on_/, "")
    attr_reader attr_name
    define_method listener_method do |published_event|
      instance_variable_set(:"@#{attr_name}", published_event)
    end
  end
end
