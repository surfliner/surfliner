##
# setup an Importer/Parser pair with the given parser class
#
# @example
#   include_context "with a Bulkrax importer", Bulkrax::CometCsvParser
RSpec.shared_context "with a Bulkrax importer" do |parser_class|
  let(:parser) { importer.parser }
  let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }
  let(:project) { FactoryBot.valkyrie_create(:project) }
  let(:user) { FactoryBot.create(:user) }

  let(:fog_connection) { Rails.application.config.staging_area_s3_connection }
  let(:bucket) { S3Configurations::StagingArea.bucket }

  let(:importer) do
    Bulkrax::Importer.create(name: "test importer #{SecureRandom.uuid}",
      parser_klass: parser_class,
      user_id: user.id,
      admin_set_id: project.id,
      parser_fields: {})
  end

  let(:uploaded) do
    FactoryBot.create(:uploaded_file, file: File.open(csv_path))
  end

  before do
    # Ensure minio/S3 bucket exists for importer to write CSV uploaded.
    create_bucket(fog_connection: fog_connection, bucket: bucket)

    # Bulkrax controllers write this data after save and then resave the importer.
    # trying to do this on Importer#create runs into a circular dependency: we need
    # the parser instance to `write_import_file` and we need the importer to create
    # the parser instance.
    importer[:parser_fields]["import_file_path"] =
      parser.write_import_file(uploaded.file.file)

    importer.current_run
  end
end
