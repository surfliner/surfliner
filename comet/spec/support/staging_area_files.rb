##
# setup a mock Fog connection to act with a mock fog connection to act as a
# test staging area when we don't want to connect to the real minio.
#
# @example
#   include_context "with a mock fog connection for bulk file staging"
RSpec.shared_context "with a mock fog connection for bulk file staging" do |parser_class|
  let(:fog_connection) { mock_fog_connection }

  before do
    allow(Rails.application.config)
      .to receive(:staging_area_s3_connection)
      .and_return(fog_connection)
  end
end

##
# Mock fog/aws connection.
# @param [source_file] the destination file
# @param [s3_key] the key for the file in S3/Minio
def mock_fog_connection
  Fog.mock!
  Fog::Mock.reset
  fog_connection_options = {
    aws_access_key_id: "minio-access-key",
    aws_secret_access_key: "minio-secret-key",
    region: "us-east-1",
    endpoint: "http://minio:9000",
    path_style: true
  }
  Fog::Storage.new(provider: "AWS", **fog_connection_options)
end

##
# Upload file to S3/Minio with fog/aws.
# @param [fog_connection] Fog::Storage
# @param [source_file] the destination file
# @param [s3_key] the key for the file in S3/Minio
def staging_area_upload(fog_connection:, bucket:, s3_key:, source_file:)
  create_bucket(fog_connection: fog_connection, bucket: bucket)
  fog_connection.directories.get(bucket).files
    .create(key: s3_key, body: File.open(source_file))
end

# Create the bucket if it doesn't exist
def create_bucket(fog_connection:, bucket:)
  # Create the bucket if it doesn't exist
  fog_connection.head_bucket(bucket)
rescue Excon::Error::NotFound
  puts "-- Creating bucket #{bucket}"
  fog_connection.directories.create(key: bucket)
end
