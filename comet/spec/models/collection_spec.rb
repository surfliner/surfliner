require "rails_helper"

RSpec.describe "::Collection" do
  it "is a ::PcdmCollection" do
    expect("Collection".safe_constantize).to be < ::PcdmCollection
  end

  it "is generated from a Hyrax::PcdmCollection stored in Valkyrie", integration: true do
    collection = Hyrax.metadata_adapter.resource_factory.to_resource(object: {
      internal_resource: "Hyrax::PcdmCollection",
      metadata: {title: "etaoin shrdlu"}
    })
    expect(collection.class).to be ::Collection
  end
end
