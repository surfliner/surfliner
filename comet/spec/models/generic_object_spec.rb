# frozen_string_literal: true

# Generated via
#  `rails generate hyrax:work_resource GenericObject`
require "rails_helper"
require "hyrax/specs/shared_specs/hydra_works"

RSpec.describe GenericObject do
  subject(:work) { described_class.new }

  it_behaves_like "a Hyrax::Work"

  it "round trips the work" do
    work.title = "Comet in Moominland"
    id = Hyrax.persister.save(resource: work).id

    expect(Hyrax.query_service.find_by(id: id))
      .to have_attributes(title: contain_exactly("Comet in Moominland"))
  end

  # This test makes some assumptions about the format of the metadata config
  # file in order to avoid having to actually process the M3.
  describe "loading metadata attributes from the provided file" do
    let(:m3_metadata_config) do
      YAML.safe_load(File.open(File.join(
        Rails.root,
        Rails.application.config.metadata_config_location,
        Rails.application.config.metadata_config_schemas.first.to_s + ".yaml"
      )), [], [], true)
    end

    it "defines appropriate property setters" do
      m3_metadata_config["properties"].each do |property|
        # Dynamically get the list of expected properties from the M3 YAML.
        next unless property["available_on"].to_a.include?("generic_object")
        property_name = property["name"]

        # Ensure each property can be changed and casts to an RDF literal.
        expect { work.public_send(:"#{property_name}=", ["etaoin"]) }
          .to change { work.public_send(property_name.to_sym).flat_map(&:terms) }
          .to contain_exactly RDF::Literal("etaoin", datatype: property["data_type"])
      end
    end

    it "can save and index each property" do
      m3_metadata_config["properties"].each do |property|
        # Dynamically get the list of expected properties from the M3 YAML.
        next unless property["available_on"].to_a.include?("generic_object")
        property_name = property["name"]
        work.public_send(:"#{property_name}=", ["etaoin"])
        expect { Hyrax.persister.save(resource: work) }.not_to raise_error
        expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      end
    end
  end

  describe "controlled value handling" do
    it "allows saving controlled value uris" do
      work.controlled_test = "about:surfliner_schema/controlled_values/controlled_test/value1"
      expect(work.controlled_test).to contain_exactly RDF::Literal("about:surfliner_schema/controlled_values/controlled_test/value1", datatype: RDF::XSD.anyURI)
    end

    it "coerces controlled values to their uris" do
      work.controlled_test = "value1"
      expect(work.controlled_test).to contain_exactly RDF::Literal("about:surfliner_schema/controlled_values/controlled_test/value1", datatype: RDF::XSD.anyURI)
    end

    # This tests storing and retrieving values from the database and so will not
    # work properly with the memory adapter:
    it "does not error when accessing invalid values", metadata_adapter: :comet_metadata_store do
      invalid_generic_object_class = Class.new(::PcdmObject) do
        attribute :controlled_test, Valkyrie::Types::String
      end
      invalid_generic_object = invalid_generic_object_class.new
      invalid_generic_object.internal_resource = "GenericObject"
      invalid_generic_object.controlled_test = "not_a_value"
      persisted = Hyrax.persister.save(resource: invalid_generic_object)
      expect(persisted).to be_a ::GenericObject
      expect(persisted.controlled_test).to contain_exactly RDF::Literal.new("not_a_value", datatype: RDF::XSD.anyURI)
    end
  end

  describe "date handling" do
    it "can save and index simple dates" do
      work.date_created = "1972-12-31"
      expect(work.date_created.first.object.iso8601).to eq "1972-12-31"
      expect(work.date_created.first.object.precision).to eq :day
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "1972-12"
      expect(work.date_created.first.object.iso8601).to eq "1972-12-01"
      expect(work.date_created.first.object.precision).to eq :month
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "1972"
      expect(work.date_created.first.object.iso8601).to eq "1972-01-01"
      expect(work.date_created.first.object.precision).to eq :year
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index simple intervals" do
      work.date_created = "1970/1972"
      expect(work.date_created.first.object.from.iso8601).to eq "1970-01-01"
      expect(work.date_created.first.object.from.precision).to eq :year
      expect(work.date_created.first.object.to.iso8601).to eq "1972-01-01"
      expect(work.date_created.first.object.to.precision).to eq :year
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index uncertain and approximate dates" do
      work.date_created = "1972-12-31?"
      expect(work.date_created.first.object.iso8601).to eq "1972-12-31"
      expect(work.date_created.first.object.uncertain?).to eq true
      expect(work.date_created.first.object.approximate?).to eq false
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "1972-12-31~"
      expect(work.date_created.first.object.iso8601).to eq "1972-12-31"
      expect(work.date_created.first.object.uncertain?).to eq false
      expect(work.date_created.first.object.approximate?).to eq true
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "1972-12-31%"
      expect(work.date_created.first.object.iso8601).to eq "1972-12-31"
      expect(work.date_created.first.object.uncertain?).to eq true
      expect(work.date_created.first.object.approximate?).to eq true
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index unspecified digits from the right" do
      work.date_created = "1972-12-XX"
      expect(work.date_created.first.object.iso8601).to eq "1972-12-01"
      expect(work.date_created.first.object.precision).to eq :day
      expect(work.date_created.first.object.unspecified?).to eq true
      expect(work.date_created.first.object.unspecified?(:day)).to eq true
      expect(work.date_created.first.object.unspecified?(:month)).to eq false
      expect(work.date_created.first.object.unspecified?(:year)).to eq false
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "1972-XX-XX"
      expect(work.date_created.first.object.iso8601).to eq "1972-01-01"
      expect(work.date_created.first.object.precision).to eq :day
      expect(work.date_created.first.object.unspecified?).to eq true
      expect(work.date_created.first.object.unspecified?(:day)).to eq true
      expect(work.date_created.first.object.unspecified?(:month)).to eq true
      expect(work.date_created.first.object.unspecified?(:year)).to eq false
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "197X-XX-XX"
      expect(work.date_created.first.object.iso8601).to eq "1970-01-01"
      expect(work.date_created.first.object.precision).to eq :day
      expect(work.date_created.first.object.unspecified?).to eq true
      expect(work.date_created.first.object.unspecified?(:day)).to eq true
      expect(work.date_created.first.object.unspecified?(:month)).to eq true
      expect(work.date_created.first.object.unspecified?(:year)).to eq true
      expect(work.date_created.first.object.unspecified.year).to eq [nil, nil, false, true]
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "19XX-XX-XX"
      expect(work.date_created.first.object.iso8601).to eq "1900-01-01"
      expect(work.date_created.first.object.precision).to eq :day
      expect(work.date_created.first.object.unspecified?).to eq true
      expect(work.date_created.first.object.unspecified?(:day)).to eq true
      expect(work.date_created.first.object.unspecified?(:month)).to eq true
      expect(work.date_created.first.object.unspecified?(:year)).to eq true
      expect(work.date_created.first.object.unspecified.year).to eq [nil, nil, true, true]
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "19XX"
      expect(work.date_created.first.object.iso8601).to eq "1900-01-01"
      expect(work.date_created.first.object.precision).to eq :year
      expect(work.date_created.first.object.unspecified?).to eq true
      expect(work.date_created.first.object.unspecified?(:day)).to eq false
      expect(work.date_created.first.object.unspecified?(:month)).to eq false
      expect(work.date_created.first.object.unspecified?(:year)).to eq true
      expect(work.date_created.first.object.unspecified.year).to eq [nil, nil, true, true]
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    # The rdf-edtf gem doesn’t support this syntax yet.
    xit "can save and index open‐ended intervals" do
      work.date_created = "1970-01-01/"
      expect(work.date_created.first.object.open?).to eq true
      expect(work.date_created.first.object.cover?(Date.today)).to eq true
      expect(work.date_created.first.object.from.iso8601).to eq "1970-01-01"
      expect(work.date_created.first.object.from.precision).to eq :day
      expect(work.date_created.first.object.to).to eq :open
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index uncertain and approximate intervals" do
      work.date_created = "1970?/1972~"
      expect(work.date_created.first.object.from.iso8601).to eq "1970-01-01"
      expect(work.date_created.first.object.from.uncertain?).to eq true
      expect(work.date_created.first.object.from.approximate?).to eq false
      expect(work.date_created.first.object.to.iso8601).to eq "1972-01-01"
      expect(work.date_created.first.object.to.uncertain?).to eq false
      expect(work.date_created.first.object.to.approximate?).to eq true
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
      work.date_created = "1970-01-01%/1972-12-31"
      expect(work.date_created.first.object.from.iso8601).to eq "1970-01-01"
      expect(work.date_created.first.object.from.uncertain?).to eq true
      expect(work.date_created.first.object.from.approximate?).to eq true
      expect(work.date_created.first.object.to.iso8601).to eq "1972-12-31"
      expect(work.date_created.first.object.to.uncertain?).to eq false
      expect(work.date_created.first.object.to.approximate?).to eq false
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index mixed precision intervals" do
      work.date_created = "1970/1972-12-31"
      expect(work.date_created.first.object.from.iso8601).to eq "1970-01-01"
      expect(work.date_created.first.object.from.precision).to eq :year
      expect(work.date_created.first.object.to.iso8601).to eq "1972-12-31"
      expect(work.date_created.first.object.to.precision).to eq :day
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index negative years" do
      work.date_created = "-0001"
      expect(work.date_created.first.object.iso8601).to eq "-0001-01-01"
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    it "can save and index very negative years" do
      work.date_created = "Y-10000"
      expect(work.date_created.first.object.iso8601).to eq "-10000-01-01"
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end

    # The rdf-edtf gem doesn’t support this syntax for days yet, only years.
    xit "can save and index one of a closed range of dates" do
      work.date_created = "[1970-01-01..1970-01-11]"
      expect(work.date_created.first.object.choice?).to eq true
      expect(work.date_created.first.object.to_a).to eq [Date.new(1970, 1, 1),
        Date.new(1970, 1, 2), Date.new(1970, 1, 3), Date.new(1970, 1, 4),
        Date.new(1970, 1, 5), Date.new(1970, 1, 6), Date.new(1970, 1, 7),
        Date.new(1970, 1, 8), Date.new(1970, 1, 9), Date.new(1970, 1, 10),
        Date.new(1970, 1, 11)]
      expect { Hyrax.persister.save(resource: work) }.not_to raise_error
      expect { Hyrax.index_adapter.save(resource: work) }.not_to raise_error
    end
  end
end
