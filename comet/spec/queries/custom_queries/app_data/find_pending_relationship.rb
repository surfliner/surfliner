require "rails_helper"

RSpec.describe CustomQueries::AppData::FindPendingRelationship do
  subject(:query_handler) { described_class.new(query_service: Comet::AppData.query_service) }

  let(:parent_id) { Valkyrie::ID.new("my parent entry") }
  let(:child_ids) {
    [
      Valkyrie::ID.new("my child entry or object"),
      Valkyrie::ID.new("my other child entry or object")
    ]
  }
  let(:importer_run_id) { Valkyrie::ID.new("my importer run") }

  before do
    child_ids.each do |child_id|
      Hyrax.persister.save(resource: ::PendingRelationship.new(parent_id: parent_id,
        child_id: child_id,
        importer_run_id: importer_run_id))
    end
  end

  describe "#find_pending_relationships" do
    it "finds the pending relationships with a given parent and importer run id" do
      expect(query_handler.find_pending_relationships(parent_id: parent_id, importer_run_id: importer_run_id))
        .to match_array(child_ids)
    end
  end
end
