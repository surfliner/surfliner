# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::ThumbnailPathService do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: "image.jpg", edit_users: [user]) }
  let(:origina_file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
  let(:original_file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: origina_file, original_filename: "image.jpg") }
  let(:original_file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_identifier: original_file_upload.id,
      original_filename: "image.jpg")
  end

  let(:thumbnail_path) { Hyrax::DerivativePath.derivative_path_for_reference(file_set.id.to_s, "thumbnail") }
  let(:thumbnail_file) { File.open(thumbnail_path).tap { |f| f.write("fade thumbnail file") } }
  let(:thumbnail_file_identifier) { "disk://#{thumbnail_path}" }
  let!(:thumbnail_file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_identifier: thumbnail_file_identifier,
      original_filename: Pathname.new(thumbnail_path).basename.to_s,
      use: :thumbnail_file)
  end

  let!(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      members: [file_set],
      title: ["Object with thumbnail"],
      edit_users: [user])
  end

  before do
    file_set.file_ids = [original_file_upload.id]
    Hyrax.persister.save(resource: file_set)
    Hyrax.persister.save(resource: original_file_metadata)
    Hyrax.index_adapter.save(resource: file_set)
  end

  describe "#call" do
    subject(:service) { described_class }

    context "when it's a work" do
      it "returns the default image path" do
        expect(service.call(object)).to include("/assets/default", ".png")
      end

      context "with a thumbnail_id" do
        before do
          thumbnail_file_metadata.file_set_id = file_set.id
          Hyrax.persister.save(resource: thumbnail_file_metadata)

          file_set.file_ids = [original_file_upload.id, thumbnail_file_identifier]
          Hyrax.persister.save(resource: file_set)

          object.thumbnail_id = [file_set.id]
          Hyrax.persister.save(resource: object)
        end

        it "returns the thumbnail path for the file_set" do
          expect(service.call(object)).to include("/downloads/#{file_set.id}?file=thumbnail")
        end
      end
    end

    context "when it's a FileSet" do
      it "returns the default image path" do
        expect(service.call(file_set)).to include("/assets/default", ".png")
      end

      context "with thumbnail file" do
        before do
          thumbnail_file_metadata.file_set_id = file_set.id
          Hyrax.persister.save(resource: thumbnail_file_metadata)

          file_set.file_ids = [original_file_upload.id, thumbnail_file_identifier]
          Hyrax.persister.save(resource: file_set)
        end

        it "returns the thumbnail file path" do
          expect(service.call(file_set)).to include("/downloads/#{file_set.id}?file=thumbnail")
        end
      end
    end
  end
end
