# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::CustomQueries::FindByDateRange do
  subject(:query_handler) { described_class.new(query_service: Hyrax.query_service) }

  after { Hyrax.persister.wipe! }
  before { Hyrax.persister.wipe! }

  describe "#find_by_date_range", :integration do
    let(:start_time) { DateTime.now - 1.day }

    it "gives an empty result" do
      expect(query_handler.find_by_date_range(start_datetime: start_time))
        .to be_empty
    end

    context "with objects" do
      let!(:object) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          title: ["Test Statistic Object"])
      end

      it "finds the object" do
        expect(query_handler.find_by_date_range(start_datetime: start_time))
          .to contain_exactly(object)
      end
    end
  end
end
