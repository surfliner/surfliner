require "rails_helper"

RSpec.describe Bulkrax::CometParserExportRecordSet do
  describe ".for" do
    let(:parser) { Bulkrax::CometCsvParser.new(nil) }
    subject { described_class.for(parser: parser, export_from: export_from) }

    context "export_from: 'all'" do
      let(:export_from) { "all" }

      it { is_expected.to be_a described_class::All }
    end

    context "export_from: 'worktype'" do
      let(:export_from) { "worktype" }

      it { is_expected.to be_a described_class::Worktype }
    end

    context "export_from: 'collection'" do
      let(:export_from) { "collection" }

      it { is_expected.to be_a described_class::Collection }
    end

    context "export_from: 'importer'" do
      let(:export_from) { "importer" }

      it { is_expected.to be_a described_class::Importer }
    end
  end

  describe "Record sets" do
    let(:collections) { [SolrDocument.new(id: 3, title: "A Test Collection")] }
    let(:works) { [SolrDocument.new(id: 4, title: "An Test Object", member_of_collection_ids: [3], member_ids: [8])] }
    let(:file_sets) { [SolrDocument.new(id: 8, title: "A Test File Set")] }
    let(:exporter) do
      Bulkrax::Exporter.new(name: "Exporter",
        export_type: "metadata",
        parser_klass: "Bulkrax::CometCsvParser",
        limit: 0,
        generated_metadata: false)
    end
    let(:parser) { exporter.parser }
    let(:record_set) { described_class.new(parser: parser) }

    context "retrieve records" do
      before do
        allow(record_set).to receive(:works).and_return(works)
        allow(record_set).to receive(:collections).and_return(collections)
        allow(record_set).to receive(:file_sets).and_return(file_sets)
      end

      [Bulkrax::CometParserExportRecordSet::All, Bulkrax::CometParserExportRecordSet::Worktype, Bulkrax::CometParserExportRecordSet::Collection, Bulkrax::CometParserExportRecordSet::Importer].each do |klass|
        let(:described_class) { klass }

        describe "#count" do
          subject { record_set.count }

          it "returns the count" do
            expect(subject).to eq(3)
          end
        end

        describe "#each" do
          it "yield the items" do
            expect do |block|
              record_set.each(&block)
            end.to yield_successive_args(
              [works[0].id, parser.entry_class],
              [collections[0].id, parser.collection_entry_class],
              [file_sets[0].id, parser.file_set_entry_class]
            )
          end
        end
      end
    end

    context "retrieve records in batch" do
      let(:solr_page_size) { Bulkrax::ParserExportRecordSet::SOLR_QUERY_PAGE_SIZE }

      let(:collection) do
        FactoryBot.valkyrie_create(
          :collection,
          :with_index,
          title: "Test Collection"
        )
      end

      before do
        Bulkrax::ParserExportRecordSet.const_set(:SOLR_QUERY_PAGE_SIZE, 2)

        collection
        ["Object for Bulkrax export #1", "Object for Bulkrax export #2", "Object for Bulkrax export #3"].each do |title|
          FactoryBot.valkyrie_create(:generic_object,
            :with_index,
            title: [title],
            member_of_collections: [collection])
        end
      end

      after { Bulkrax::ParserExportRecordSet.const_set(:SOLR_QUERY_PAGE_SIZE, solr_page_size) }

      describe Bulkrax::CometParserExportRecordSet::Worktype do
        let(:record_set) { described_class.new(parser: parser) }

        before { exporter.export_source = "GenericObject" }

        describe "#count" do
          subject { record_set.count }

          it "returns the count" do
            expect(subject).to be >= 3
          end
        end
      end

      describe Bulkrax::CometParserExportRecordSet::Collection do
        let(:record_set) { described_class.new(parser: parser) }

        before { exporter.export_source = collection.id.to_s }

        describe "#count" do
          subject { record_set.count }

          it "returns the count" do
            expect(subject).to eq(4)
          end
        end
      end

      describe Bulkrax::CometParserExportRecordSet::All do
        let(:record_set) { described_class.new(parser: parser) }

        describe "#count" do
          subject { record_set.count }

          it "returns the count" do
            expect(subject).to be >= 4
          end
        end
      end
    end
  end
end
