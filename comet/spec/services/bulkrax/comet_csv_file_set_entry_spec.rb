require "rails_helper"

RSpec.describe Bulkrax::CometCsvFileSetEntry do
  describe "Import metadata", :integration do
    include_context "with a Bulkrax importer", Bulkrax::CometCsvParser

    let(:staging_file) { Tempfile.new("image.jpg").tap { |f| f.write("A fake image!") } }

    before do
      ["demo_files/demo.jpg", "demo_files/image.jpg", "demo_files/document.pdf"].each do |key|
        staging_area_upload(fog_connection: fog_connection,
          bucket: bucket, s3_key: key, source_file: staging_file)
      end
    end

    let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_file_sets.csv") }

    subject(:entry) do
      described_class.create(importerexporter: importer,
        identifier: "spec-work-and-fs-fs2",
        parsed_metadata: metadata,
        raw_metadata: metadata)
    end

    let(:metadata) do
      {"model" => "FileSet",
       "source_identifier" => "spec-work-and-fs-fs2",
       "title" => "FileSet 2",
       "parents" => "spec-work-and-fs-work1",
       "use:ServiceFile" => nil,
       "use:OriginalFile" => "demo_files/document.pdf",
       "use:PreservationFile" => nil}
    end

    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: members,
        title: ["My Test Object"],
        alternate_ids: ["spec-work-and-fs-work1"],
        edit_users: [importer.last_run.user])
    end
    let(:members) { [] }

    describe "#build" do
      before { object }

      it "creates a FileSet with the metadata" do
        expect(entry.build)
          .to have_attributes(title: contain_exactly("FileSet 2"),
            alternate_ids: contain_exactly("spec-work-and-fs-fs2"),
            depositor: user.user_key)
      end

      it "uploads and attaches the target file", perform_enqueued: [Bulkrax::ImportFileSetJob, Bulkrax::ImportWorkJob] do
        file_set = entry.build

        expect(Hyrax.custom_queries.find_files(file_set: file_set))
          .to contain_exactly(have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#OriginalFile")),
            original_filename: "document.pdf"))
      end

      context "with multiple file types" do
        let(:metadata) do
          {"model" => "FileSet",
           "source_identifier" => "spec-work-and-fs-fs2",
           "title" => "FileSet 2",
           "parents" => "spec-work-and-fs-work1",
           "use:ServiceFile" => "demo_files/image.jpg",
           "use:OriginalFile" => "demo_files/document.pdf",
           "use:PreservationFile" => "demo_files/demo.jpg"}
        end

        it "uploads and attaches all target files", perform_enqueued: [Bulkrax::ImportFileSetJob, Bulkrax::ImportWorkJob] do
          file_set = entry.build

          expect(Hyrax.custom_queries.find_files(file_set: file_set))
            .to contain_exactly(have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#OriginalFile")),
              original_filename: "document.pdf"),
              have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#ServiceFile")),
                original_filename: "image.jpg"),
              have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#PreservationFile")),
                original_filename: "demo.jpg"))
        end
      end

      context "update a FileSet" do
        let(:parser_field) { {} }
        let(:importer) do
          Bulkrax::Importer.create(name: "test importer #{SecureRandom.uuid}",
            parser_klass: Bulkrax::CometCsvParser,
            user_id: user.id,
            admin_set_id: project.id,
            parser_fields: parser_field)
        end

        let(:file) { Tempfile.new("document.pdf").tap { |f| f.write("fade image file") } }
        let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "document.pdf") }
        let(:file_metadata) do
          FactoryBot.valkyrie_create(:file_metadata,
            file_identifier: file_upload.id,
            original_filename: "document.pdf")
        end

        let(:file_set) do
          FactoryBot.valkyrie_create(:file_set,
            :with_index,
            title: "document.pdf",
            alternate_ids: ["spec-work-and-fs-fs2"],
            edit_users: [importer.last_run.user])
        end

        let(:members) { [file_set] }

        before do
          file_set.file_ids = [file_upload.id]
          file_set_saved = Hyrax.persister.save(resource: file_set)
          Hyrax.persister.save(resource: file_metadata)
          Hyrax.index_adapter.save(resource: file_set_saved)

          expect(file_set_saved.title).to contain_exactly("document.pdf")
        end

        it "update metadata" do
          file_set = entry.build

          expect(file_set.title).not_to include("document.pdf")
          expect(file_set.title).to contain_exactly("FileSet 2")
        end

        context "update metadata & files" do
          let(:parser_field) { {update_files: true} }
          let(:metadata) do
            {"model" => "FileSet",
             "source_identifier" => "spec-work-and-fs-fs2",
             "title" => "FileSet 2",
             "parents" => "spec-work-and-fs-work1",
             "use:ServiceFile" => nil,
             "use:OriginalFile" => "demo_files/document.pdf",
             "use:PreservationFile" => "demo_files/demo.jpg"}
          end

          it "update FileSet metadata and the files" do
            file_set = entry.build

            expect(file_set.title).to contain_exactly("FileSet 2")
            expect(Hyrax.custom_queries.find_files(file_set: file_set))
              .to contain_exactly(have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#OriginalFile")),
                original_filename: "document.pdf"),
                have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#PreservationFile")),
                  original_filename: "demo.jpg"))
          end
        end

        context "update & replace files" do
          let(:parser_field) { {replace_files: true} }

          let(:metadata) do
            {"model" => "FileSet",
             "source_identifier" => "spec-work-and-fs-fs2",
             "title" => "FileSet 2",
             "parents" => "spec-work-and-fs-work1",
             "use:ServiceFile" => nil,
             "use:OriginalFile" => "demo_files/image.jpg",
             "use:PreservationFile" => "demo_files/demo.jpg"}
          end

          it "raised error" do
            expect { entry.build }.to raise_error(Bulkrax::RecordConflict)
          end

          context "with files destroyed" do
            before { Hyrax::Transactions::Steps::DeleteAllFileSets.new.call(object, user: user) }

            it "builds successful" do
              expect { entry.build }.not_to raise_error
            end

            it "update FileSet metadata and replace the files" do
              file_set = entry.build

              expect(file_set.title).to contain_exactly("FileSet 2")
              expect(Hyrax.custom_queries.find_files(file_set: file_set))
                .to contain_exactly(have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#OriginalFile")),
                  original_filename: "image.jpg"),
                  have_attributes(pcdm_use: contain_exactly(RDF::URI("http://pcdm.org/use#PreservationFile")),
                    original_filename: "demo.jpg"))
            end
          end
        end
      end
    end

    describe "#build_metadata" do
      it "does not raise a validation error" do
        expect { entry.build_metadata }.not_to raise_error
      end
    end
  end

  xdescribe "Export metadata" do
    subject(:csv_file_set_entry) do
      described_class.new(importerexporter: exporter, identifier: file_set.id)
    end

    let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

    context "with object fields and no prefix" do
      let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: "image.jpg") }
      let(:file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
      let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "image.jpg") }
      let(:file_metadata) do
        FactoryBot.valkyrie_create(:file_metadata,
          file_identifier: file_upload.id,
          original_filename: "image.jpg")
      end

      let(:exporter) do
        Bulkrax::Exporter.new(name: "Export from Worktype",
          user: user,
          export_type: "metadata",
          export_from: "worktype",
          export_source: "GenericObject",
          parser_klass: "Bulkrax::CometCsvParser",
          limit: 0,
          generated_metadata: false)
      end

      before do
        file_set.file_ids = [file_upload.id]
        Hyrax.persister.save(resource: file_set)
        Hyrax.persister.save(resource: file_metadata)
        Hyrax.index_adapter.save(resource: file_set)

        allow(subject).to receive(:hyrax_record).and_return(file_set)
      end

      describe "#build_export_metadata" do
        it "populates metadata" do
          expect(csv_file_set_entry.build_export_metadata)
            .to include("title" => "image.jpg",
              "use:OriginalFile" => "image.jpg",
              "visibility" => "restricted")
        end
      end
    end
  end
end
