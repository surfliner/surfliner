require "rails_helper"

RSpec.describe Bulkrax::CometCsvCollectionEntry do
  subject(:csv_collection_entry) do
    described_class.new(importerexporter: exporter, identifier: collection.id)
  end

  describe "Export metadata" do
    let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

    context "with collection title" do
      let(:collection) do
        FactoryBot.valkyrie_create(
          :collection,
          :with_index,
          :with_permission_template,
          edit_users: [user.user_key],
          read_users: [user.user_key],
          title: "Test Collection",
          alternate_ids: ["col_1"],
          user: user
        )
      end

      let(:exporter) do
        Bulkrax::Exporter.new(name: "Export from Worktype",
          user: user,
          export_type: "metadata",
          export_from: "worktype",
          export_source: "GenericObject",
          parser_klass: "Bulkrax::CometCsvParser",
          limit: 0,
          generated_metadata: false)
      end

      before do
        allow(subject).to receive(:hyrax_record).and_return(collection)
      end

      describe "#build_export_metadata" do
        it "populates metadata" do
          expect(csv_collection_entry.build_export_metadata)
            .to include("title" => "Test Collection",
              "visibility" => "restricted")
        end
      end
    end
  end
end
