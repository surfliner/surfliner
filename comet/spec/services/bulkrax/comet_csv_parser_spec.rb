# frozen_string_literal: true

require "rails_helper"

RSpec.describe Bulkrax::CometCsvParser do
  subject(:parser) { described_class.new(importer) }
  let(:importer) { nil }

  describe "#create_objects", :integration do
    include_context "with a Bulkrax importer", described_class

    context "with works with inline files", perform_enqueued: [Bulkrax::ImportFileSetJob, Bulkrax::ImportWorkJob] do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_with_file_uses.csv") }

      it "imports works with file sets attached" do
        parser.create_objects

        expect(Hyrax.query_service.find_all_of_model(model: Hyrax::FileSet).count).to eq 2
        expect(Hyrax.query_service.find_all_of_model(model: GenericObject).count).to eq 2
      end
    end

    context "with works and file sets" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_file_sets.csv") }

      context "total counts for importer run" do
        subject(:importer_run) { Bulkrax::ImporterRun.find(parser.importer.current_run.id) }

        let(:staging_file) { Tempfile.new("image.jpg").tap { |f| f.write("A fake image!") } }

        before do
          ["demo_files/demo.jpg", "demo_files/image.jpg", "demo_files/document.pdf"].each do |key|
            staging_area_upload(fog_connection: fog_connection,
              bucket: S3Configurations::StagingArea.bucket,
              s3_key: key,
              source_file: staging_file)
          end
        end

        it "initiates counts for importer run" do
          parser.create_objects

          expect(importer_run.enqueued_records).to eq 4
          expect(importer_run.processed_records).to eq 0
          expect(importer_run.total_work_entries).to eq 2
          expect(importer_run.total_file_set_entries).to eq 2
        end

        it "updates counts", perform_enqueued: [Bulkrax::ImportFileSetJob, Bulkrax::ImportWorkJob] do
          parser.create_objects

          expect(importer_run.enqueued_records).to eq 0
          expect(importer_run.processed_records).to eq 4
          expect(importer_run.total_work_entries).to eq 2
          expect(importer_run.total_file_set_entries).to eq 2
        end
      end

      context "with stubbed connection" do
        include_context "with a mock fog connection for bulk file staging" do
          let(:staging_file) { Tempfile.new("image.jpg").tap { |f| f.write("A fake image!") } }

          before do
            ["demo_files/demo.jpg", "demo_files/image.jpg", "demo_files/document.pdf"].each do |key|
              staging_area_upload(fog_connection: fog_connection,
                bucket: S3Configurations::StagingArea.bucket,
                s3_key: key,
                source_file: staging_file)
            end
          end
        end

        it "enqueues jobs for works, file sets" do
          expect { parser.create_objects }
            .to have_enqueued_job(Bulkrax::ImportFileSetJob).exactly(:twice)
            .and have_enqueued_job(Bulkrax::ImportWorkJob).exactly(:twice)
        end
      end

      it "imports works and file sets, enqueues relationship jobs", perform_enqueued: [Bulkrax::ImportFileSetJob, Bulkrax::ImportWorkJob] do
        # enqueuues twice, once for each child, but should be a no-op the second run in most cases.
        expect { parser.create_objects }
          .to have_enqueued_job(CometCreateRelationshipsJob).exactly(:twice)
          .with(parent_identifier: "spec-work-and-fs-work1", importer_run_id: importer.current_run.id)

        expect(Hyrax.query_service.find_all_of_model(model: Hyrax::FileSet).count).to eq 2
        expect(Hyrax.query_service.find_all_of_model(model: GenericObject).count).to eq 2
      end
    end

    context "with works and collections" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_collections.csv") }
      let(:ark_pattern) { Rails.application.config.ark_pattern }

      before { Rails.application.config.ark_pattern = /^ark:.*/ }
      after { Rails.application.config.ark_pattern = ark_pattern }

      it "enqueues jobs for works and collections" do
        expect { parser.create_objects }
          .to have_enqueued_job(Bulkrax::ImportCollectionJob).exactly(:once)
          .and have_enqueued_job(Bulkrax::ImportWorkJob).exactly(:once)
      end

      it "creates pending relationships", perform_enqueued: [Bulkrax::ImportCollectionJob, Bulkrax::ImportWorkJob] do
        expect { parser.create_objects }
          .to change {
            Comet::AppData.query_service
              .custom_queries
              .find_pending_relationships(importer_run_id: importer.last_run.id,
                parent_id: "test-collection-1")
              .to_a
          }
          .from(be_empty)
          .to contain_exactly(have_attributes(child_id: "test-work-in-collection-1", parent_id: "test-collection-1"))
      end

      it "creates works and collections, enqueues jobs for relationships", perform_enqueued: [Bulkrax::ImportCollectionJob, Bulkrax::ImportWorkJob] do
        expect { parser.create_objects }
          .to have_enqueued_job(CometCreateRelationshipsJob).exactly(:once)
          .with(parent_identifier: "test-collection-1", importer_run_id: importer.current_run.id)

        expect(Hyrax.query_service.find_all_of_model(model: ::Collection).count).to eq 1
        expect(Hyrax.query_service.find_all_of_model(model: GenericObject).count).to eq 1
      end
    end
  end

  describe "#valid_import?", :integration do
    include_context "with a Bulkrax importer", described_class

    it { is_expected.to be_valid_import }

    context "with a csv with file set entries" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_file_sets.csv") }

      it { is_expected.to be_valid_import }
    end

    context "with a csv that has invalid cardinalities" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "cardinality-test.csv") }

      it { is_expected.not_to be_valid_import }

      it "populates errors" do
        parser.valid_import?
        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 2 (w1):
            cardinality_test:
              size cannot be greater than 1.
        EXPECTED
        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 4 (w3):
            cardinality_test:
              size cannot be greater than 1.
            title:
              size cannot be less than 1.
              can't be blank.
        EXPECTED
      end
    end

    context "regarding temporary patches to support 'collection'…" do
      # These pending tests note places in which `collection` is currently
      # hardcoded into the CSV parser. `collection` is the name used by
      # collections in our M3 schemas and also in our CSVs. It’s good that these
      # match, but we don’t actually have a `::Collection` class for them to
      # resolve to yet.
      #
      # Once collection models are generated from the M3 schemas, then a
      # `::Collection` class will be defined, and these hardcodings should be
      # removed.

      let(:collection_record) do
        {model: "collection"}
      end

      before do
        allow(parser).to receive(:records).and_return([collection_record])
      end

      xit "#build_records" do
        expect(Valkyrie.config.resource_class_resolver).to receive(:call).and_call_original
        parser.build_records
      end

      xit "#validate_model" do
        expect(Valkyrie.config.resource_class_resolver).to receive(:call).and_call_original
        parser.send(:validate_model)
      end
    end

    context "with an empty and an invalid model in csv" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "invalid-models.csv") }
      let(:error_message) { "Invalid model(s): Row 2 (w1) => unknown model `UnknownModel`, Row 3 (w2) => model missing." }

      it { is_expected.not_to be_valid_import }

      it "populates errors" do
        parser.valid_import?

        expect(importer.current_status)
          .to have_attributes(status_message: "Failed",
            error_message: include(error_message))
      end
    end

    context "with an invalid ARK" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_collections.csv") }
      let(:ark_pattern) { Rails.application.config.ark_pattern }

      before { Rails.application.config.ark_pattern = /^ark:\/[0-9]{5}\/[a-z0-9]{7}/ }
      after { Rails.application.config.ark_pattern = ark_pattern }

      it { is_expected.not_to be_valid_import }

      it "populates errors" do
        parser.valid_import?

        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 2 (test-work-in-collection-1):
            ark:
              ARK 'ark:12345/test1' invalid.
          Row 3 (test-collection-1):
            ark:
              ARK 'ark:12345/test2' invalid.
        EXPECTED
      end
    end

    context "with ARK for existing objects with ARK minted" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_collections.csv") }

      let(:object) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          title: ["Test Object Update"],
          alternate_ids: ["test-work-in-collection-1"],
          ark: "ark:/12345/testark")
      end

      let(:collection) do
        FactoryBot.valkyrie_create(:collection,
          :with_index,
          title: ["Test Collection Update"],
          alternate_ids: ["test-collection-1"],
          ark: "ark:/12345/test123")
      end

      let(:error_message) { "ARK(s) rejected: Row 2 ark:12345/test1 (ark:/12345/testark assigned), Row 3 ark:12345/test2 (ark:/12345/test123 assigned)" }

      let(:ark_pattern) { Rails.application.config.ark_pattern }

      before do
        Rails.application.config.ark_pattern = /^ark:.*/
        object
        collection
      end

      after { Rails.application.config.ark_pattern = ark_pattern }

      it { is_expected.not_to be_valid_import }

      it "populates errors" do
        parser.valid_import?

        expect(importer.current_status)
          .to have_attributes(status_message: "Failed",
            error_message: include(error_message))
      end

      context "same ARK for records with ARK minted" do
        let(:object) do
          FactoryBot.valkyrie_create(:generic_object,
            :with_index,
            title: ["Test Same ARK Object Update"],
            alternate_ids: ["test-work-in-collection-2"],
            ark: "ark:/12345/test1")
        end

        let(:collection) do
          FactoryBot.valkyrie_create(:collection,
            :with_index,
            title: ["Test Same ARK Collection Update"],
            alternate_ids: ["test-collection-2"],
            ark: "ark:/12345/test2")
        end

        it { is_expected.to be_valid_import }
      end
    end

    context "with a csv that has invalid controlled values" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "invalid_cv_entry.csv") }

      it { is_expected.not_to be_valid_import }

      it "populates errors" do
        parser.valid_import?
        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 3 (w2):
            controlled_test:
              value 'invalid value' not in list of controlled values.
        EXPECTED
        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 4 (w3):
            controlled_test:
              value 'invalid value' not in list of controlled values.
              value 'another invalid value' not in list of controlled values.
        EXPECTED
      end
    end

    context "with a very bad csv" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "very_bad.csv") }

      it { is_expected.not_to be_valid_import }

      it "populates errors" do
        parser.valid_import?

        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 3 (w2):
            cardinality_test:
              size cannot be greater than 1.
            controlled_test:
              value 'notvalue1' not in list of controlled values.
        EXPECTED
        expect(importer.current_status.error_message).to include <<~EXPECTED
          Row 4 (w3):
            cardinality_test:
              size cannot be greater than 1.
            title:
              size cannot be less than 1.
              can't be blank.
            controlled_test:
              value 'and' not in list of controlled values.
              value 'bad' not in list of controlled values.
              value 'controlled' not in list of controlled values.
        EXPECTED
      end
    end
  end

  describe "#records", :integration do
    include_context "with a Bulkrax importer", described_class

    it "has some records" do
      expect(parser.records).not_to be_empty
    end

    context "with works and file sets" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_file_sets.csv") }

      it "lists works and file_sets" do
        expect(parser.works)
          .to contain_exactly(include(source_identifier: "spec-work-and-fs-work1"),
            include(source_identifier: "spec-work-and-fs-work2"))
        expect(parser.file_sets)
          .to contain_exactly(include(source_identifier: "spec-work-and-fs-fs1"),
            include(source_identifier: "spec-work-and-fs-fs2"))
      end
    end

    context "with works with inline files" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_with_file_uses.csv") }

      it "includes the file columns" do
        expect(parser.works)
          .to contain_exactly(include(model: "GenericObject",
            source_identifier: "test-use-1",
            title: "Work with File Uses",
            parents: nil,
            "use:ServiceFile": nil,
            "use:OriginalFile": "demo_files/document.pdf",
            "use:PreservationFile": nil),
            include(model: "GenericObject",
              source_identifier: "test-use-2",
              title: "Work with Other Uses",
              parents: nil,
              "use:ServiceFile": "demo_files/image.jpg",
              "use:OriginalFile": nil,
              "use:PreservationFile": "demo_files/demo.jpg"))
      end
    end

    context "with works and collections" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_collections.csv") }
      let(:ark_pattern) { Rails.application.config.ark_pattern }

      before { Rails.application.config.ark_pattern = /^ark:.*/ }

      after { Rails.application.config.ark_pattern = ark_pattern }

      it "lists works and collections" do
        expect(parser.works).not_to be_empty
        expect(parser.collections).not_to be_empty

        expect(parser.records)
          .to contain_exactly(*(parser.works + parser.collections))
      end
    end

    context "with bom encoding CSV" do
      let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "works_and_collections_bom.csv") }
      let(:ark_pattern) { Rails.application.config.ark_pattern }

      it "lists works and collections" do
        expect(parser.records)
          .to contain_exactly(*(parser.works + parser.collections))

        expect(parser.works.first.to_h)
          .to include(source_identifier: "test-work-in-collection1-bom",
            model: "GenericObject",
            title: "Work in Collection 1 from BOM CSV",
            parents: "test-collection1-bom")

        expect(parser.collections.first.to_h)
          .to include(source_identifier: "test-collection1-bom",
            model: "Collection",
            title: "Collection 1 from BOM CSV")
      end
    end
  end

  describe "#entry_class" do
    it "is the custom CsvParser" do
      expect(parser.entry_class).to eq Bulkrax::CometCsvEntry
    end
  end

  describe "#work_entry_class" do
    it "is the custom CsvParser" do
      expect(parser.work_entry_class).to eq Bulkrax::CometCsvEntry
    end
  end

  describe "#file_set_entry_class" do
    it "is the custom CsvFileSetEntry" do
      expect(parser.file_set_entry_class).to eq Bulkrax::CometCsvFileSetEntry
    end
  end

  describe "#collection_entry_class" do
    it "is the custom CsvCollectionEntry" do
      expect(parser.collection_entry_class).to eq Bulkrax::CometCsvCollectionEntry
    end
  end

  describe "#current_records_for_export" do
    subject(:parser) { described_class.new(exporter) }

    let(:user) { FactoryBot.create(:user) }
    let(:exporter) do
      Bulkrax::Exporter.new(name: "Export by worktype",
        user: user,
        export_type: "metadata",
        export_from: "worktype",
        export_source: "GenericObject",
        parser_klass: "Bulkrax::CometCsvParser",
        limit: 0,
        generated_metadata: false)
    end
    let(:parser_export_record_set) { Bulkrax::CometParserExportRecordSet::Worktype }

    it "is the custom ParserExportRecordSet" do
      expect(parser.current_records_for_export.is_a?(parser_export_record_set)).to eq true
    end
  end

  describe "#create_new_entries", perform_enqueued: [Bulkrax::ImportFileSetJob, Bulkrax::ImportWorkJob] do
    subject(:export_parser) { described_class.new(exporter) }

    context "total counts for exporter run" do
      include_context "with a Bulkrax importer", described_class

      let(:exporter) do
        Bulkrax::Exporter.create(name: "Export by importer",
          user: user,
          export_type: "metadata",
          export_from: "importer",
          export_source: importer.id.to_s,
          parser_klass: "Bulkrax::CometCsvParser",
          generated_metadata: false)
      end

      before do
        Bulkrax::ImporterJob.send(:perform_now, importer.id)
        exporter.current_run
      end

      it "initiates counts" do
        expect(exporter.last_run.enqueued_records).to eq 2
        expect(exporter.last_run.total_work_entries).to eq 2
      end

      it "updates counts" do
        export_parser.create_new_entries

        expect(exporter.last_run.enqueued_records).to eq 0
        expect(exporter.last_run.processed_records).to eq 2
        expect(exporter.last_run.total_work_entries).to eq 2
      end
    end
  end
end
