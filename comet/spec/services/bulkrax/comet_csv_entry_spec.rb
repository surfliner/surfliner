require "rails_helper"

RSpec.describe Bulkrax::CometCsvEntry do
  subject(:csv_entry) do
    described_class.new(importerexporter: nil)
  end

  context "regarding temporary patches to support 'collection'…" do
    # These pending tests note places in which `collection` is currently
    # hardcoded into the CSV parser. `collection` is the name used by
    # collections in our M3 schemas and also in our CSVs. It’s good that these
    # match, but we don’t actually have a `::Collection` class for them to
    # resolve to yet.
    #
    # Once collection models are generated from the M3 schemas, then a
    # `::Collection` class will be defined, and these hardcodings should be
    # removed.
    let(:collection_record) do
      {"model" => "collection"}
    end

    before do
      allow(csv_entry).to receive(:parsed_metadata).and_return(collection_record)
    end

    xit "#factory_class" do
      expect(Valkyrie.config.resource_class_resolver).to receive(:call).and_call_original
      csv_entry.factory_class
    end
  end

  describe "Export metadata" do
    let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

    context "with object fields and no prefix" do
      subject(:csv_entry) do
        described_class.new(importerexporter: exporter, identifier: work.id)
      end
      let(:work) do
        w = GenericObject.new(title: "Test Object Title",
          title_alternative: "An Alternative Title",
          creator: "Tester",
          alternate_ids: ["w_1"])
        Hyrax.persister.save(resource: w)
      end

      let(:exporter) do
        Bulkrax::Exporter.new(name: "Export from Worktype",
          user: user,
          export_type: "metadata",
          export_from: "worktype",
          export_source: "GenericObject",
          parser_klass: "Bulkrax::CometCsvParser",
          limit: 0,
          generated_metadata: false)
      end

      before do
        allow_any_instance_of(Bulkrax::ValkyrieObjectFactory).to receive(:run!)
        allow(subject).to receive(:hyrax_record).and_return(work)
        allow(work).to receive(:id).and_return("w_1")
        allow(work).to receive(:member_of_work_ids).and_return([])
        allow(work).to receive(:in_work_ids).and_return([])
        allow(work).to receive(:member_work_ids).and_return([])
      end

      describe "#build_export_metadata" do
        it "populates metadata" do
          expect(csv_entry.build_export_metadata)
            .to include "title" => "Test Object Title",
              "title_alternative" => "An Alternative Title",
              "creator" => "Tester"
        end
      end
    end
  end

  RSpec.shared_context "create files in S3" do
    include_context "with a mock fog connection for bulk file staging"
    let(:s3_bucket) { S3Configurations::StagingArea.bucket }
    let(:staging_file) {
      Tempfile.new("validcsv.csv").tap { |f|
        f.write("data1,data2,data3")
        f.rewind
      }
    }
    let(:valid_csv_path) { "demo_files/validcsv.csv" }
    let(:empty_csv_path) { "" }
    let(:dummy_csv_path) { "demo_files/invalidcsv.csv" }

    before do
      staging_area_upload(fog_connection: fog_connection,
        bucket: s3_bucket, s3_key: valid_csv_path, source_file: staging_file)
    end
  end

  describe "#read_data" do
    context "read csv file" do
      include_context "create files in S3"
      it "read data from csv file" do
        contents = Bulkrax::CometCsvEntry.read_data(valid_csv_path)
        csv_table = contents.instance_variable_get(:@original)
        extracted_data = csv_table.to_csv
        expected_data = "data1,data2,data3\n"
        expect(extracted_data).to include(expected_data)
      end

      it "empty csv path" do
        expect {
          Bulkrax::CometCsvEntry.read_data(empty_csv_path)
        }.to raise_error(RuntimeError, "CSV path empty")
      end

      it "invalid csv path" do
        expect {
          Bulkrax::CometCsvEntry.read_data(dummy_csv_path)
        }.to raise_error(RuntimeError, "no file found at path #{dummy_csv_path}")
      end
    end
  end

  describe "fetch file from S3" do
    context "fetch files from s3" do
      include_context "create files in S3"
      it "file present in S3 bucket" do
        expect(Bulkrax::CometCsvEntry.csv_file_for(valid_csv_path).key).to eq(valid_csv_path)
      end
      it "file not present in S3 bucket" do
        expect(Bulkrax::CometCsvEntry.csv_file_for(dummy_csv_path)).to be_nil
      end
    end
  end

  describe "#prepare_export_data" do
    let(:datum) { ActiveTriples::Resource.new("about:surfliner_schema/controlled_values/controlled_test/value1") }
    it "converts active triples to uri" do
      expected_value = "about:surfliner_schema/controlled_values/controlled_test/value1"
      expect(csv_entry.prepare_export_data(datum)).to eq(expected_value)
    end
    let(:datum_array) {
      %w[data1 data2 data3]
    }
    it "joins the array contents" do
      expected_value = "data1 | data2 | data3"
      expect(csv_entry.prepare_export_data(datum_array)).to eq(expected_value)
    end
    let(:datum_simple_string) { "simple string" }
    it "returns all other data as is" do
      expected_value = "simple string"
      expect(csv_entry.prepare_export_data(datum_simple_string)).to eq(expected_value)
      # returns nil for empty array
      expect(csv_entry.prepare_export_data(%w[])).to be_nil
    end
  end
end
