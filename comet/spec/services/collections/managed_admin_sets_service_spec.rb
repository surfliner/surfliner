# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::Collections::ManagedAdminSetsService do
  let(:scope) { FakeSearchBuilderScope.new(current_ability: current_ability) }
  let(:current_ability) { instance_double(Ability, admin?: true) }
  let(:admin_set) { Hyrax.persister.save(resource: Hyrax::AdministrativeSet.new(title: "Test Admin Set")) }

  describe ".managed_collections_count" do
    it "returns number of AdminSets that can be managed" do
      expect { Hyrax.index_adapter.save(resource: admin_set) }
        .to change { described_class.managed_collections_count(scope: scope) }
        .by 1
    end
  end
end
