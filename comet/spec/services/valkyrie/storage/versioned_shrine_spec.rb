# frozen_string_literal: true

require "rails_helper"
require "valkyrie"

RSpec.describe Valkyrie::Storage::VersionedShrine do
  let(:s3_adapter) { Shrine::Storage::S3.new(bucket: "my-bucket", client: client, identifier_prefix: "1234") }
  let(:storage_adapter) { described_class.new(s3_adapter, verifier) }
  let(:file) { Rack::Test::UploadedFile.new(File.open(Rails.root.join("spec", "fixtures", "image.jpg"))) }
  let(:client) { S3Client.new.client }

  before do
    client.create_bucket(bucket: "my-bucket")
  end

  describe "#upload_version" do
    let(:verifier) { NullVerifier }
    let(:version_file) { Rack::Test::UploadedFile.new(File.open(Rails.root.join("spec", "fixtures", "staging_area", "demo_files", "demo.jpg"))) }
    let(:uploaded_file) { storage_adapter.upload(file: file, original_filename: "image.jpg", resource: ExampleResource.new(id: "fake_id"), fake_upload_argument: true) }

    it "upload version file" do
      allow(s3_adapter).to receive(:open).and_call_original

      uploaded_version = storage_adapter.upload_version(id: uploaded_file.id, file: version_file)

      uploaded_version.read
      expect(s3_adapter).to have_received(:open)
    end
  end
end

class ExampleResource < Valkyrie::Resource
end

class NullVerifier
  def self.verify_checksum(_io, _result)
    true
  end
end
