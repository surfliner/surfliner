require "rails_helper"

RSpec.describe Hyrax::Transactions::Steps::Save do
  subject(:step) { described_class.new }

  describe "#valid_future_date?" do
    subject { step.send(:valid_future_date?, item, "embargo_release_date") }

    let(:item) { FakeEmbargoForm.new(fields: fields) }
    let(:fields) { {title: ["Test embargo"], embargo_release_date: tomorrow, visibility_after_embargo: "open"} }

    context "with time zone Etc/GMT-14", tz: "Etc/GMT-14" do
      let(:tomorrow) { Date.tomorrow.to_s }

      it "validates date string" do
        expect(subject).to eq true
      end

      context "with a data" do
        let(:tomorrow) { Date.tomorrow }

        it "validates date" do
          expect(subject).to eq true
        end
      end
    end

    context "with time zone with timezone Etc/GMT+12", tz: "Etc/GMT+12" do
      let(:tomorrow) { Date.tomorrow.to_s }

      it "validates date string" do
        expect(subject).to eq true
      end

      context "with date" do
        let(:tomorrow) { Date.tomorrow }

        it "validates date" do
          expect(subject).to eq true
        end
      end
    end
  end
end

class FakeEmbargoForm
  attr_accessor :fields

  def initialize(fields:)
    @fields = fields
  end
end
