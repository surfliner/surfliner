# frozen_string_literal: true

require "rails_helper"

RSpec.describe Transactions::CreateFileSet do
  subject(:tx) { described_class.new }

  let(:file_set) { Hyrax::FileSet.new }
  let(:change_set) { Bulkrax::Forms::ResourceForm.for(resource: file_set) }

  describe "#call" do
    it "is successful" do
      expect(tx.call(change_set)).to be_success
    end

    it "sets attributes" do
      change_set.title = "file title"

      expect(tx.call(change_set).value!)
        .to have_attributes(title: contain_exactly("file title"))
    end

    context "when providing user as depositor" do
      let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

      it "sets depositor" do
        tx.with_step_args("change_set.set_user_as_depositor" => {user: user})

        expect(tx.call(change_set).value!)
          .to have_attributes depositor: user.user_key
      end
    end
  end
end
