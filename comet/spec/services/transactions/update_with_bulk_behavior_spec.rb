require "rails_helper"

RSpec.describe "work_resource.update_with_bulk_behavior", :integration do
  subject(:tx_result) do
    Hyrax::Transactions::Container["work_resource.update_with_bulk_behavior"].with_step_args(
      "work_resource.destroy_file_sets" => {user: user, force_destroy: replace_files},
      "work_resource.add_bulkrax_files" => {files: s3_files, user: user}
    ).call(form)
  end

  let!(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
  let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "image.jpg") }
  let(:file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_identifier: file_upload.id,
      original_filename: "image.jpg")
  end

  let(:file_set) do
    FactoryBot.valkyrie_create(:file_set,
      :with_index,
      title: "image.jpg",
      edit_users: [user])
  end

  let!(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      members: [file_set],
      representative_id: file_set.id,
      title: "Test Object - Replace files",
      depositor: user.user_key,
      edit_users: [user])
  end

  let(:s3_key) { "demo_files/image.jpg" }
  let(:staging_file) { Tempfile.new("image.jpg").tap { |f| f.write("A fake image!") } }
  let(:s3_files) { {original_file: [s3_file]} }
  let(:s3_file) do
    bucket = Rails.application.config.staging_area_s3_connection
      .directories.get(S3Configurations::StagingArea.bucket)

    url = bucket.files.get_url(s3_key, Time.now + 60)
    RemoteFile.new(url: url, original_filename: "image.jpg")
  end

  let(:form) { Bulkrax::Forms::ResourceForm.for(resource: object) }

  before do
    file_set.file_ids = [file_upload.id]
    Hyrax.persister.save(resource: file_set)
    Hyrax.persister.save(resource: file_metadata)
    Hyrax.index_adapter.save(resource: file_set)

    staging_area_upload(fog_connection: Rails.application.config.staging_area_s3_connection,
      bucket: S3Configurations::StagingArea.bucket,
      s3_key: s3_key,
      source_file: staging_file)
  end

  context "with replace_files set to false" do
    let(:replace_files) { false }
    let(:updated) { tx_result.value! }

    it "is successful" do
      expect(tx_result).to be_success
    end

    it "does not replace the file set and the representative media" do
      expect(updated.member_ids).to include(file_set.id)
      expect(updated.representative_id).to eq file_set.id
    end
  end

  context "with replace_files set to true" do
    let(:replace_files) { true }
    let(:updated) { tx_result.value! }

    it "is successful" do
      expect(tx_result).to be_success
    end

    it "replace the file sets in object and reset the representative media" do
      expect(updated.member_ids.size).to eq 1
      expect(updated.member_ids).not_to include(file_set.id)
      expect(updated.representative_id).not_to eq be_nil
      expect(updated.representative_id).not_to eq file_set.id
    end
  end
end
