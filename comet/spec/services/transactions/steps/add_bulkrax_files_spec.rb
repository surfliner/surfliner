require "rails_helper"

RSpec.describe Transactions::Steps::AddBulkraxFiles do
  subject(:step) { described_class.new }

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:object) { FactoryBot.valkyrie_create(:generic_object, depositor: user.user_key) }

  it "gives success" do # steps should pass without args
    expect(step.call(object)).to be_success
  end

  it "doesn't create any FileSets" do # steps should pass without args
    expect { step.call(object) }
      .not_to change { Hyrax.query_service.find_all_of_model(model: Hyrax::FileSet).count }
  end

  context "with files", :integration do
    let(:files) { {original_file: file} }

    let(:bucket) do
      Rails.application.config.staging_area_s3_connection
        .directories
        .get(S3Configurations::StagingArea.bucket)
    end

    let(:file) do
      path = Rails.root.join("spec", "fixtures", "upload.txt")
      key = path.basename.to_s
      bucket.files.create(key: key, body: File.read(path))

      url = bucket.files.get_url(key, Time.now + 60)
      RemoteFile.new(url: url, original_filename: "upload.txt")
    end

    let(:use) { Hyrax::FileMetadata::Use.uri_for(use: :original_file) }

    it "is a success" do
      expect(step.call(object, files: files)).to be_success
    end

    it "creates a FileSet" do
      result = step.call(object, files: files).value!
      members = Hyrax.query_service.find_members(resource: result)

      expect(members)
        .to contain_exactly(an_instance_of(Hyrax::FileSet))

      expect(Hyrax.custom_queries.find_files(file_set: members.first).first)
        .to have_attributes(
          label: [file.original_filename],
          mime_type: "text/plain",
          pcdm_use: [use],
          original_filename: file.original_filename
        )
    end

    context "attach files to file set" do
      let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: ["upload.txt"], depositor: user.user_key) }

      it "is a success" do
        expect(step.call(file_set, files: files)).to be_success
      end

      it "uploads files" do
        result = step.call(file_set, files: files).value!

        expect(Hyrax.custom_queries.find_files_by_file_set_id(file_set_id: result.id).first)
          .to have_attributes(
            label: [file.original_filename],
            mime_type: "text/plain",
            pcdm_use: [use],
            original_filename: file.original_filename
          )
      end
    end
  end
end
