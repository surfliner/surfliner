require "rails_helper"

RSpec.describe Transactions::Steps::DeleteOrphanMemberObjects do
  let(:transaction) do
    steps = Hyrax::Transactions::WorkDestroy::DEFAULT_STEPS.dup
    steps.prepend("work_resource.delete_orphan_member_objects")
    Hyrax::Transactions::WorkDestroy.new(steps: steps)
  end

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:grandchild_member) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      visibility: "open",
      depositor: user.user_key,
      title: "Recursive test: grandchild member object with one parent")
  end
  let(:orphaned_member) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      visibility: "open",
      members: [grandchild_member],
      depositor: user.user_key,
      title: "Member object with one parent")
  end

  context "with an member object that would be orphaned" do
    it "deletes the parent and member objects" do
      expect {
        transaction.with_step_args("work_resource.delete" => {user: user},
          "work_resource.delete_all_file_sets" => {user: user},
          "work_resource.delete_orphan_member_objects" => {user: user})
          .call(orphaned_member)
      }
        .to change { Hyrax.query_service.find_all_of_model(model: Hyrax::Work) }
        .from(contain_exactly(orphaned_member, grandchild_member))
        .to(be_empty)
    end
  end

  context "with a member object that is part of a collection" do
    let(:member_of_multiple_works) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        visibility: "open",
        member_of_collection_ids: [collection_with_member_work.id],
        depositor: user.user_key,
        title: "Member object with 2 parents")
    end
    let!(:parent_with_one_member) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: [member_of_multiple_works],
        visibility: "open",
        depositor: user.user_key,
        title: "Parent with 1 member objects")
    end
    let(:collection_with_member_work) do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        title: "Collection with member work")
    end

    it "does not delete the member object that is a member of another collection" do
      expect {
        transaction.with_step_args("work_resource.delete" => {user: user},
          "work_resource.delete_all_file_sets" => {user: user},
          "work_resource.delete_orphan_member_objects" => {user: user})
          .call(parent_with_one_member)
      }
        .to change { Hyrax.query_service.find_all_of_model(model: Hyrax::Work) }
        .from(contain_exactly(parent_with_one_member, member_of_multiple_works))
        .to contain_exactly(member_of_multiple_works)
    end
  end

  context "with a member object that would not be orphaned" do
    let(:member_of_multiple_works) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        visibility: "open",
        depositor: user.user_key,
        title: "Member object with 2 parents")
    end
    let(:parent_with_multiple_members) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: [orphaned_member, member_of_multiple_works],
        visibility: "open",
        depositor: user.user_key,
        title: "Parent with 2 member objects")
    end
    let!(:parent_with_one_member) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: [member_of_multiple_works],
        visibility: "open",
        depositor: user.user_key,
        title: "Parent with 1 member objects")
    end

    it "does not delete the member object that is a member of another object" do
      expect {
        transaction.with_step_args("work_resource.delete" => {user: user},
          "work_resource.delete_all_file_sets" => {user: user},
          "work_resource.delete_orphan_member_objects" => {user: user})
          .call(parent_with_multiple_members)
      }
        .to change { Hyrax.query_service.find_all_of_model(model: Hyrax::Work) }
        .from(contain_exactly(parent_with_one_member, parent_with_multiple_members, member_of_multiple_works, orphaned_member, grandchild_member))
        .to contain_exactly(member_of_multiple_works, parent_with_one_member)
    end
  end
end
