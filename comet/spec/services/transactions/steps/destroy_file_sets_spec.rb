require "rails_helper"

RSpec.describe Transactions::Steps::DestroyFileSets do
  subject(:tx_result) { described_class.new.call(object, user: user, force_destroy: replace_files) }

  let(:replace_files) { false }
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let!(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: "Test Object",
      edit_users: [user])
  end

  it "is successful" do
    expect(tx_result).to be_success
  end

  context "with file sets" do
    let(:file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
    let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "image.jpg") }
    let(:file_metadata) do
      FactoryBot.valkyrie_create(:file_metadata,
        file_identifier: file_upload.id,
        original_filename: "image.jpg")
    end

    let(:file_set) do
      FactoryBot.valkyrie_create(:file_set,
        :with_index,
        title: "image.jpg",
        edit_users: [user])
    end

    let!(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: [file_set],
        representative_id: file_set.id,
        title: "Test Object - Replace files",
        edit_users: [user])
    end

    before do
      file_set.file_ids = [file_upload.id]
      Hyrax.persister.save(resource: file_set)
      Hyrax.persister.save(resource: file_metadata)
      Hyrax.index_adapter.save(resource: file_set)
    end

    it "does not destroy the file set memebers" do
      expect(tx_result.value!.member_ids).to contain_exactly(file_set.id)
    end

    context "with force destroy" do
      let(:replace_files) { true }
      let(:updated) { tx_result.value! }

      it "destroy the file set" do
        tx_result.value!

        expect { Hyrax.query_service.find_by(id: file_set.id) }
          .to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
      end

      it "reset the file set members and representative media" do
        expect(updated.member_ids).to eq []
        expect(updated.representative_id).to be_nil
      end
    end
  end
end
