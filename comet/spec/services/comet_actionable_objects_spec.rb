require "rails_helper"

RSpec.describe CometActionableObjects, :perform_enqueued do
  subject(:actionable_objects) do
    described_class.new(user: user,
      workflow_state_filter: workflow_state_filter,
      bulkrax_importer_filter: bulkrax_importer_filter,
      project_filter: project_filter)
  end

  let(:workflow_state_filter) { nil }
  let(:bulkrax_importer_filter) { nil }
  let(:project_filter) { nil }
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:admin_set) do
    Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
      .find { |p| p.title.include?("Test Project") }
  end

  let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: ["in_review.jpg"]) }
  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Workflow Object"],
      members: [file_set],
      admin_set_id: admin_set.id,
      ark: "ark:/fade#in_review")
  end

  let(:file_set_completed) { FactoryBot.valkyrie_create(:file_set, title: ["completed.jpg"]) }
  let(:object_completed) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Workflow Object - completed"],
      members: [file_set_completed],
      admin_set_id: admin_set.id,
      ark: "ark:/fade#completed")
  end

  before {
    setup_workflow_for(user)
    Hyrax::Workflow::WorkflowFactory.create(object, {}, user)
    Hyrax::Workflow::WorkflowFactory.create(object_completed, {}, user)

    workflow_action_completed = Hyrax::Forms::WorkflowActionForm.new(current_ability: ::Ability.new(user),
      work: object_completed, attributes: {name: "approve", comment: "Approved"})
    workflow_action_completed.save
  }

  describe "#total_count" do
    it "return the count for the workflows" do
      expect(subject.total_count).to eq 2
    end

    context "filters workflow state in_revie" do
      let(:workflow_state_filter) { "in_review" }

      it "return the count for workflows in_review only" do
        expect(subject.total_count).to eq 1
      end
    end

    context "filters workflow state completed" do
      let(:workflow_state_filter) { "completed" }

      it "return the count for workflow state completed only" do
        expect(subject.total_count).to eq 1
      end
    end
  end

  describe "#workflow_states" do
    it "returns scope workflow states" do
      expect(subject.workflow_states.map(&:name)).to include("in_review", "completed")
    end
  end

  describe "#id_and_state_pairs_for" do
    subject(:id_and_state_pairs) do
      described_class.id_and_state_pairs_for(user: user, object_ids: object_ids)
    end

    let(:object_ids) { nil }

    it "return object id and state pairs for all objects" do
      expect(subject.map { |p| p.last&.name }).to eq ["in_review", "completed"]
    end

    context "with object_ids filter for one object" do
      let(:object_ids) { [object.id] }

      it "return the object id and state pair" do
        expect(subject.map { |p| p.last&.name }).to eq ["in_review"]
      end
    end

    context "with object_ids filter for multiple objects" do
      let(:object_ids) { [object.id, object_completed.id] }

      it "return object id and state pairs" do
        expect(subject.map { |p| p.last&.name }).to eq ["in_review", "completed"]
      end
    end
  end

  describe "bulkrax importer filter" do
    include_context "with a mock fog connection for bulk file staging"

    let(:s3_bucket) { S3Configurations::StagingArea.bucket }
    let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }
    let(:importer) do
      Bulkrax::Importer.create(name: "test importer #{SecureRandom.uuid}",
        parser_klass: "Bulkrax::CometCsvParser",
        user_id: user.id,
        admin_set_id: admin_set.id,
        parser_fields: {})
    end
    let(:uploaded) do
      FactoryBot.create(:uploaded_file, file: File.open(csv_path))
    end

    let(:bulkrax_importer_filter) { importer.id.to_s }

    before do
      create_bucket(fog_connection: fog_connection, bucket: s3_bucket)

      importer[:parser_fields]["import_file_path"] =
        importer.parser.write_import_file(uploaded.file.file)
      importer.save

      Bulkrax::ImporterJob.send(:perform_now, importer.id)
    end

    describe "#total_count" do
      it "return the count for the workflows" do
        expect(subject.total_count).to eq 2
      end
    end

    describe "#bulkrax_importers" do
      it "returns bulkrax importers" do
        expect(subject.bulkrax_importers.size).to eq 1
        expect(subject.bulkrax_importers.first.name).to eq importer.name
      end
    end
  end

  describe "project filter" do
    let(:project_filter) { admin_set.id.to_s }

    describe "#total_count" do
      it "return the count for the workflows" do
        expect(subject.total_count).to eq 2
      end
    end
  end
end
