require "rails_helper"

RSpec.describe ARK do
  subject(:minter) { described_class }
  let(:ark) { "ark:/99999/real/true" }

  context "when the object doesn't support #ark" do
    let(:resource) { FactoryBot.valkyrie_create(:file_set) }
    let(:ezid) { double }

    it ".mint_for is a no-op" do
      expect(ezid).not_to receive(:mint)

      minter.mint_for(resource.id, client: ezid)
    end

    it ".update_ark is a no-op" do
      expect(ezid).not_to receive(:find)

      minter.update_ark(resource, client: ezid)
    end
  end

  context "minting a new ARK" do
    let(:work) { Hyrax.persister.save(resource: GenericObject.new) }
    let(:id) { work.id }

    before do
      allow(Ezid::Identifier).to receive(:mint).and_return(ark)
    end

    it "returns an ARK" do
      expect(minter.mint_for(id).ark).to eq ark
    end

    context "minting ARK for object with an existing ARK" do
      before { work.ark = ark }

      it "raise an exception" do
        expect { minter.mint_for(id) }.to raise_error(ARK::ARKExistingError)
      end
    end
  end

  context "updating an existing ARK" do
    let(:work) { FactoryBot.valkyrie_create(:generic_object, ark: ark) }

    let(:identifier) { instance_of(Ezid::Identifier) }
    let(:ezid) { double }

    it "calls the ezid-client and returns the object" do
      expect(ezid).to receive(:find).with(ark).and_return(identifier)
      expect(identifier).to receive(:target=).with("https://example.com")
      expect(identifier).to receive(:save)

      expect(minter.update_ark(work, target: "https://example.com", client: ezid).ark)
        .to eq ark
    end
  end
end
