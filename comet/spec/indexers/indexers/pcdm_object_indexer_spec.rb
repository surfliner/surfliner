# frozen_string_literal: true

require "rails_helper"
require "hyrax/specs/shared_specs/indexers"

RSpec.describe Indexers::PcdmObjectIndexer do
  let(:current_date) { Date.today }
  let(:indexer_class) { Indexers::PcdmObjectIndexer(GenericObject) }
  let(:resource) { GenericObject.new }
  let(:resource_indexer) { indexer_class.new(resource: resource) }

  it_behaves_like "a Hyrax::Resource indexer"

  context "with RDF URI values" do
    before do
      resource.controlled_test =
        RDF::URI("about:surfliner_schema/controlled_values/controlled_test/value1")
    end

    it "coerces to literal when range is rdf-schema#Literal" do
      expect(resource_indexer.to_solr[:controlled_test_tim])
        .to eql ["about:surfliner_schema/controlled_values/controlled_test/value1"]
    end
  end

  context "with RDF literal values" do
    before do
      resource.creator = [RDF::Literal("Tove")]
      resource.date_created = [RDF::Literal(current_date, datatype: "http://id.loc.gov/datatypes/edtf/EDTF")]
      resource.date = [current_date] # will cast to a value wrapper object
      resource.language = [RDF::Literal("epo", datatype: "http://id.loc.gov/vocabulary/languageschemes/iso6392b")]
    end

    it "gives appropriate values for string literals" do
      solr_doc = resource_indexer.to_solr

      expect(solr_doc[:creator_tsim]).to eql ["Tove"]
    end

    it "gives appropriate values for date literals" do
      solr_doc = resource_indexer.to_solr

      expect(solr_doc[:date_created_tsim]).to eql [current_date]
    end

    it "gives appropriate values for invalid literals" do
      resource.date_created = [RDF::Literal("🆖", datatype: RDF::XSD.date)]
      solr_doc = resource_indexer.to_solr

      expect(solr_doc[:date_created_tsim]).to eql ["🆖"]
    end

    it "gives appropriate values for unrecognized literals" do
      solr_doc = resource_indexer.to_solr

      expect(solr_doc[:language_tsim]).to eql ["epo"]
    end

    it "gives appropriate values for value wrapper objects" do
      solr_doc = resource_indexer.to_solr

      expect(solr_doc[:date_tsim]).to eql [current_date]
    end
  end
end
