# frozen_string_literal: true

require "rails_helper"

RSpec.describe Bulkrax::ImportersController, storage_adapter: :memory, metadata_adapter: :test_adapter, type: :controller do
  routes { Bulkrax::Engine.routes }

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:valid_attributes) do
    {
      name: "Test Importer",
      admin_set_id: "admin_set/default",
      user_id: user.id,
      parser_klass: "Bulkrax::CometCsvParser",
      parser_fields: {some_attribute: "some_fields"}
    }
  end

  before { sign_in user }

  describe "#create" do
    context "with Create and Validate" do
      it "uses perform_later" do
        expect(Bulkrax::ImporterJob).to receive(:perform_later).exactly(1).times
        post :create, params: {importer: valid_attributes, commit: "Create and Validate"}

        expect(response).to redirect_to(importers_path(locale: "en"))
      end
    end
  end
end
