# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::Admin::WorkflowRolesController, :perform_enqueued, storage_adapter: :memory, metadata_adapter: :test_adapter do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  before { setup_workflow_for(user) }

  describe "#index" do
    routes { Hyrax::Engine.routes }

    it "redirects to sign in" do
      get :index

      expect(response).to redirect_to("/users/sign_in?locale=en")
    end

    context "with a logged in user" do
      before { sign_in(user) }

      it "gives a succesful response" do
        get :index

        expect(response).to be_successful
      end

      context "with pagination" do
        subject(:presenter) { controller.view_assigns["presenter"] }

        let(:another_user) { User.find_or_create_by(email: "comet-admin@library.ucsd.edu") }

        before { setup_workflow_for(another_user) }

        it "responds to pagination" do
          get :index

          expect(presenter.users_for_page).not_to be_nil
          expect(presenter.users_for_page.total_pages).to be_positive
        end

        it "respects :page and :per_page" do
          get :index, params: {page: "2", per_page: "1"}

          expect(presenter.current_page).to be(2)
          expect(presenter.rows_per_page).to be(1)
          expect(presenter.users_for_page.count).to eq(1)
        end
      end
    end
  end
end
