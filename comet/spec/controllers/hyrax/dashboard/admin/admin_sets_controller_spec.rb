# frozen_string_literal: true

require "rails_helper"
require "hyrax/specs/shared_specs/factories/administrative_sets"

RSpec.describe Hyrax::Admin::AdminSetsController, storage_adapter: :memory, metadata_adapter: :test_adapter, type: :controller do
  include_context "with an admin set"

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  before { sign_in user }

  describe "#index" do
    routes { Hyrax::Engine.routes }

    it "redirects to /dashboard/projects#index" do
      get :index
      expect(response).to redirect_to(main_app.dashboard_projects_path(locale: "en"))
    end
  end

  describe "#destroy" do
    routes { Hyrax::Engine.routes }

    let(:admin_set) do
      FactoryBot.valkyrie_create(:hyrax_admin_set,
        edit_users: [user])
    end

    it "deletes the admin set and redirect to dashboard projects" do
      delete :destroy, params: {id: admin_set}

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(main_app.dashboard_projects_path(locale: "en"))
      expect(flash[:notice]).to eq "Project deleted."
    end
  end
end
