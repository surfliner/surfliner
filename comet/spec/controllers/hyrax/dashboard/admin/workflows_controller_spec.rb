# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::Admin::WorkflowsController, :perform_enqueued, storage_adapter: :memory, metadata_adapter: :test_adapter do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  before do
    Hyrax.metadata_adapter.persister.wipe!
    setup_workflow_for(user)
    sign_in user
  end

  describe "batch worflow actions" do
    let(:admin_set) do
      Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
        .find { |p| p.title.include?("Test Project") }
    end

    let(:file_set_1) { FactoryBot.valkyrie_create(:file_set, title: ["image_1.jpg"]) }
    let(:object_1) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Workflow Object 1"],
        members: [file_set_1],
        admin_set_id: admin_set.id,
        ark: "ark:/fade#obj_1")
    end

    let(:file_set_2) { FactoryBot.valkyrie_create(:file_set, title: ["image_2.jpg"]) }
    let(:object_2) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Workflow Object 2"],
        members: [file_set_2],
        admin_set_id: admin_set.id,
        ark: "ark:/fade#obj_2")
    end

    before {
      setup_workflow_for(user)
      Hyrax::Workflow::WorkflowFactory.create(object_1, {}, user)
      Hyrax::Workflow::WorkflowFactory.create(object_2, {}, user)
    }

    describe "#workflow_actions" do
      let(:batch_document_ids) { [object_1.id, object_2.id] }

      it "gives a succesful response" do
        get :workflow_actions, params: {batch_document_ids: batch_document_ids.map(&:to_s)}

        expect(response).to be_successful
      end
    end

    describe "#batch_update" do
      let(:batch_document_ids) { [object_1.id, object_2.id] }
      let(:params) do
        {batch_document_ids: batch_document_ids.map(&:to_s).to_json,
         workflow_action: {name: "approve", comment: "Approved"}}
      end

      it "gives a succesful response" do
        put :batch_update, params: params

        expect(response).to redirect_to("/admin/workflows?locale=en")

        id_and_states = CometActionableObjects.id_and_state_pairs_for(user: user, object_ids: batch_document_ids)
        expect(id_and_states.map { |p| p&.last&.name }).to eq ["completed", "completed"]
      end
    end
  end
end
