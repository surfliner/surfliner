# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::Dashboard::ProjectsController, storage_adapter: :memory, metadata_adapter: :test_adapter, type: :controller do
  let(:user) { User.create(email: "moomin@example.com") }

  describe "#index" do
    it "redirects to sign in" do
      get :index

      expect(response).to redirect_to("/users/sign_in")
    end

    context "with a logged in user" do
      before { sign_in(user) }

      it "gives a succesful response" do
        get :index

        expect(response).to be_successful
      end
    end
  end

  describe "#search_action_url" do
    routes { Hyrax::Engine.routes }

    it do
      expect(controller.send(:search_action_url, q: "my"))
        .to include("/dashboard/projects?locale=en&q=my")
    end
  end

  describe "#search_facet_path" do
    routes { Hyrax::Engine.routes }

    it do
      expect(controller.send(:search_facet_path, id: "keyword_sim"))
        .to eq "/dashboard/projects/facet/keyword_sim?locale=en"
    end
  end
end
