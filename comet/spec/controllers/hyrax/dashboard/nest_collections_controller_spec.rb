# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::Dashboard::NestCollectionsController, storage_adapter: :memory, metadata_adapter: :test_adapter, type: :controller do
  let(:user) { User.create(email: "moomin@example.com") }

  describe "#create_relationship_within" do
    routes { Hyrax::Engine.routes }

    let(:collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [user.user_key],
        read_users: [user.user_key],
        title: "Test Collection",
        user: user
      )
    end

    let(:child_collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [user.user_key],
        read_users: [user.user_key],
        title: "Test Parent Collection",
        user: user
      )
    end

    let(:parameters) do
      {child_id: child_collection.id, parent_id: collection.id, source: "my"}
    end

    before { sign_in(user) }

    it "expect(response).to redirects to /dashboard/collectioins#index" do
      post :create_relationship_within, params: parameters

      expect(flash[:notice]).to include("has been added")
      expect(response).to redirect_to(dashboard_collections_path(locale: "en"))
    end
  end
end
