# frozen_string_literal: true

require "rails_helper"
require "hyrax/specs/spy_listener"

RSpec.describe Hyrax::Dashboard::CollectionsController, storage_adapter: :memory, metadata_adapter: :test_adapter, type: :controller do
  let(:user) { User.create(email: "moomin@example.com") }

  describe "#new" do
    routes { Hyrax::Engine.routes }

    it "redirects to sign in" do
      get :new

      expect(response).to redirect_to("/users/sign_in")
    end

    context "with a logged in user" do
      before { sign_in(user) }

      let(:collection_type) { Hyrax::CollectionType.create(title: "Spec Type") }

      it "gives a succesful response" do
        get :new
        expect(response).to be_successful
      end
    end
  end

  describe "#create" do
    routes { Hyrax::Engine.routes }

    it "redirects to sign in" do
      post :create, params: {collection: {title: ["My Title"]}}

      expect(response).to redirect_to("/users/sign_in")
    end

    context "with a logged in user" do
      before { sign_in(user) }

      let(:collection_type) { Hyrax::CollectionType.create(title: "Spec Type") }

      it "gives a succesful response" do
        expect do
          post :create, params: {collection: {title: "My Title"},
                                 collection_type_gid: collection_type.to_global_id}
        end
          .to change { Hyrax.query_service.count_all_of_model(model: ::Collection) }
          .by 1
      end

      it "redirects to /dashboard/collectioins#index" do
        post :create, params: {collection: {title: "My Title"},
                               collection_type_gid: collection_type.to_global_id}

        expect(response).to redirect_to(dashboard_collections_path(locale: "en"))
      end

      context "with multiple values field" do
        let(:alternative_title) { ["Alternative title 1", "Alternative title 2"] }

        it "save succesfully" do
          post :create, params: {collection: {title: ["My Collection with Multiple Values Field"],
                                              title_alternative: alternative_title},
                                 collection_type_gid: collection_type.to_global_id}

          expect(controller.view_assigns["collection"]&.title_alternative&.map(&:to_s))
            .to contain_exactly(*alternative_title)

          expect(flash[:notice]).to eq(I18n.t("hyrax.dashboard.my.action.collection_create_success"))
        end
      end
    end
  end

  describe "#show" do
    routes { Hyrax::Engine.routes }

    let(:collection_type) { Hyrax::CollectionType.create(title: "Spec Type") }
    let(:collection_type_gid) { collection_type.to_global_id.to_s }

    let(:collection) do
      c = Hyrax::PcdmCollection.new(title: ["My Collection"], collection_type_gid: collection_type_gid)
      c = Hyrax.persister.save(resource: c)
      Hyrax.index_adapter.save(resource: c)
      c
    end

    it "redirects to login" do
      get :show, params: {id: collection.id}

      expect(response).to redirect_to("/users/sign_in?locale=en")
    end

    context "with a logged in user" do
      before do
        sign_in(user)

        acl = Hyrax::AccessControlList(collection)
        acl.grant(:read).to(user)
        acl.save
      end

      it "gives a succesful response" do
        get :show, params: {id: collection.id}
        expect(response).to be_successful
      end
    end

    describe "#search_action_url" do
      it do
        expect(controller.send(:search_action_url, q: "my"))
          .to include("/dashboard/collections?locale=en&q=my")
      end
    end

    describe "#search_facet_path" do
      it do
        expect(controller.send(:search_facet_path, id: "keyword_sim"))
          .to eq "/dashboard/collections/facet/keyword_sim?locale=en"
      end
    end
  end

  describe "#destroy" do
    routes { Hyrax::Engine.routes }

    let(:collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [user.user_key],
        read_users: [user.user_key],
        title: "Test Collection",
        user: user
      )
    end

    it "redirects to sign in" do
      put :destroy, params: {id: collection.id}

      expect(response).to redirect_to("/users/sign_in")
    end

    context "with a logged in user" do
      before { sign_in(user) }

      it "gives a succesful response" do
        put :destroy, params: {id: collection.id}

        expect { Hyrax.query_service.find_by(id: collection.id) }
          .to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
      end

      it "redirects to /dashboard/collectioins#index" do
        put :destroy, params: {id: collection.id}

        expect(response).to redirect_to(dashboard_collections_path(locale: "en"))
      end
    end
  end
end
