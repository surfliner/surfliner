# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::BatchEditsController, storage_adapter: :memory, metadata_adapter: :test_adapter, type: :controller do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:object1) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Object One"])
  end

  let(:object2) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Object Two"])
  end

  before {
    object1
    object2
    sign_in(user)
  }

  describe "#publish" do
    it "publish objects in batch" do
      post :publish, params: {batch_document_ids: [object1.id, object2.id],
                              platform: "all",
                              return_controller: "hyrax/dashboard/works"}

      expect(controller.batch).to include(object1.id.to_s, object2.id.to_s)
      expect(response).to redirect_to("/dashboard/works?locale=en")
      expect(flash[:notice]).to eq "The batch objects publish request is submitted successfully."
    end
  end

  describe "#upublish" do
    it "unpublish objects in batch" do
      post :unpublish, params: {batch_document_ids: [object1.id, object2.id],
                                platform: "all",
                                return_controller: "hyrax/dashboard/works"}

      expect(controller.batch).to include(object1.id.to_s, object2.id.to_s)
      expect(response).to redirect_to("/dashboard/works?locale=en")
      expect(flash[:notice]).to eq "The batch objects unpublish request is submitted successfully."
    end
  end
end
