# frozen_string_literal: true

require "rails_helper"

RSpec.describe Hyrax::Admin::WorkflowRolesPresenter do
  subject(:presenter) { described_class.new(request) }

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:request) { double(host: "example.com", base_url: "http://example.com", params: {}) }

  before { setup_workflow_for(user) }

  describe "#presenter_for" do
    it "gives a CometAgentPresenter" do
      expect(presenter.presenter_for(user))
        .to be_a Hyrax::Admin::WorkflowRolesPresenter::CometAgentPresenter
    end
  end

  describe "#CometAgentPresenter" do
    subject(:agent_presenter) { presenter.presenter_for(user) }

    it "has responsibilities" do
      expect(agent_presenter.responsibilities_present?)
        .to be true
    end

    it "returns workflow responsibilities for the agent" do
      agent_presenter.responsibilities do |responsibility|
        expect(responsibility)
          .to be_a Hyrax::Admin::WorkflowRolesPresenter::ResponsibilityPresenter
      end
    end
  end
end
