# frozen_string_literal: true

require "rails_helper"

RSpec.describe Presenters::CometObjectPresenter do
  subject(:presenter) { described_class.new(solr_doc, ability) }
  let(:solr_doc) { SolrDocument.new(attributes) }
  let(:ability) { Ability.new(user) }
  let(:user) { FactoryBot.create(:user) }
  let(:read_permission) { true }
  let(:file_set_presenter) { Hyrax::FileSetPresenter.new(fs_solr_doc, ability) }
  let(:fs_solr_doc) { SolrDocument.new(fs_attributes) }
  let(:fs_attributes) { {} }
  let(:fs_id) { "777777" }
  let(:attributes) do
    {"id" => "888888",
     :title_tesim => ["Lovely Title Remote File"],
     :has_model_ssim => ["GenericObject"],
     :human_readable_type_tesim => ["Generic object"],
     :read_access_group_ssim => ["public"],
     :read_access_person_ssim => [user.user_key],
     :visibility_ssi => "open",
     "depositor_tesim" => user.user_key,
     :member_ids_ssim => [fs_id],
     :hasRelatedImage_ssim => [fs_id],
     :rendering_ids_ssim => [fs_id],
     "hasRelatedMediaFragment_ssim" => fs_id}
  end
  let(:member_presenter_factory) { instance_double(Hyrax::MemberPresenterFactory) }
  let(:file_set_presenters) { [file_set_presenter] }
  let(:upload_id) { Valkyrie::ID.new("fake://1") }

  describe "#iiif_viewer?" do
    before do
      allow(ability).to receive(:can?).with(:read, solr_doc.id).and_return(read_permission)
      allow(ability).to receive(:can?).with(:read, fs_solr_doc.id).and_return(read_permission)
    end

    it "is false by default" do
      expect(subject.iiif_viewer?).to eq false
    end

    context "with a viewable original file" do
      let(:fs_attributes) do
        {"id" => fs_id,
         :has_model_ssim => ["Hyrax::FileSet"],
         :human_readable_type_tesim => ["File Set"],
         :read_access_group_ssim => ["public"],
         :original_filename_tesi => "image.jpg",
         :original_filename_ssi => "image.jpg",
         :mime_type_tesi => "image/jpeg",
         :mime_type_ssi => "image/jpeg",
         :file_format_tesim => ["jpeg (JPEG File Interchange Format)"],
         :type_tesim => ["http://pcdm.org/use#OriginalFile"],
         :format_label_tesim => ["JPEG File Interchange Format"],
         :size_tesim => ["3223"]}
      end

      it "is true" do
        presenter.member_presenter_factory = member_presenter_factory
        allow(member_presenter_factory)
          .to receive(:member_presenters)
          .with([fs_id])
          .and_return([file_set_presenter])

        allow(member_presenter_factory)
          .to receive(:file_set_presenters)
          .and_return(file_set_presenters)

        expect(subject.iiif_viewer?).to eq true
      end
    end

    context "with a viewable service file" do
      let(:fs_attributes) do
        {"id" => fs_id,
         :has_model_ssim => ["Hyrax::FileSet"],
         :human_readable_type_tesim => ["File Set"],
         :read_access_group_ssim => ["public"],
         :original_filename_tesi => "image.jpg",
         :original_filename_ssi => "image.jpg",
         :mime_type_tesi => "image/jpeg",
         :mime_type_ssi => "image/jpeg",
         :file_format_tesim => ["jpeg (JPEG File Interchange Format)"],
         :type_tesim => ["http://pcdm.org/use#ServiceFile"]}
      end

      it "is true" do
        presenter.member_presenter_factory = member_presenter_factory
        allow(member_presenter_factory)
          .to receive(:member_presenters)
          .with([fs_id])
          .and_return([file_set_presenter])

        allow(member_presenter_factory)
          .to receive(:file_set_presenters)
          .and_return(file_set_presenters)

        expect(subject.iiif_viewer?).to eq true
      end
    end
  end

  describe "#representative_presenter" do
    it "nil by default" do
      expect(presenter.representative_presenter).to be nil
    end

    context "with a viewable original file" do
      let(:file_set) { FactoryBot.valkyrie_create(:file_set, id: fs_id, title: ["image.jpg"], file_ids: [upload_id]) }

      let(:file_metadata) do
        Hyrax::FileMetadata.new(mime_type: "image/jpeg",
          file_set_id: file_set.id,
          file_identifier: upload_id,
          type: [::Valkyrie::Vocab::PCDMUse.OriginalFile])
      end

      let(:obj) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          members: [file_set],
          representative_id: file_set.id,
          rendering_ids: [file_set.id],
          title: ["Comet Object"])
      end
      let(:solr_doc) { SolrDocument.new(Hyrax::ValkyrieIndexer.for(resource: obj).to_solr) }

      before do
        Hyrax.persister.save(resource: file_metadata)
        acl = Hyrax::AccessControlList.new(resource: file_set)
        acl.grant(:read).to(user)
        acl.save

        Hyrax.index_adapter.save(resource: file_set)
      end

      it "is presenter for original file" do
        expect(presenter.representative_presenter).not_to be nil
        expect(presenter.representative_presenter.solr_document.id).to eq(obj.member_ids.first.to_s)
        expect(presenter.representative_presenter.solr_document["type_tesim"].first).to eq("http://pcdm.org/use#OriginalFile")
      end
    end

    context "with a viewable service file" do
      let(:file_set_service) { FactoryBot.valkyrie_create(:file_set, title: ["image_service.jpg"], file_ids: [upload_id]) }

      let(:file_metadata_service) do
        Hyrax::FileMetadata.new(mime_type: "image/jpeg",
          file_set_id: file_set_service.id,
          file_identifier: upload_id,
          type: [::Valkyrie::Vocab::PCDMUse.ServiceFile],
          pcdm_use: [::Valkyrie::Vocab::PCDMUse.ServiceFile])
      end

      let(:obj_service) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          members: [file_set_service],
          representative_id: file_set_service.id,
          rendering_ids: [file_set_service.id],
          title: ["Comet Object service"])
      end

      let(:solr_doc) { SolrDocument.new(Hyrax::ValkyrieIndexer.for(resource: obj_service).to_solr) }

      before do
        Hyrax.persister.save(resource: file_metadata_service)
        acl = Hyrax::AccessControlList.new(resource: file_set_service)
        acl.grant(:read).to(user)
        acl.save

        Hyrax.index_adapter.save(resource: file_set_service)
      end

      it "is presenter for service file" do
        expect(presenter.representative_presenter).not_to be nil
        expect(presenter.representative_presenter.solr_document.id).to eq(obj_service.member_ids.first.to_s)
        expect(presenter.representative_presenter.solr_document["type_tesim"].first).to eq("http://pcdm.org/use#ServiceFile")
      end
    end
  end

  describe "#member_of_authorized_parent_collections" do
    context "with a parent in a collection" do
      let(:collection) do
        FactoryBot.valkyrie_create(
          :collection,
          :with_index,
          :with_permission_template,
          edit_users: [user.user_key],
          read_users: [user.user_key],
          title: "Test Collection",
          user: user
        )
      end

      let(:component) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          title: ["Test Comet Component"],
          edit_users: [user.user_key],
          read_users: [user.user_key],
          depositor: user.id)
      end

      let(:obj) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          title: ["Comet Object"],
          member_ids: [component.id],
          member_of_collection_ids: [collection.id],
          edit_users: [user.user_key],
          read_users: [user.user_key],
          depositor: user.id)
      end

      let(:solr_doc) { SolrDocument.new(Hyrax::ValkyrieIndexer.for(resource: component).to_solr) }

      before { obj }

      it "is a member of parent's collection'" do
        expect(presenter.member_of_authorized_parent_collections).to eq([collection.id])
      end
    end
  end
end
