require "rails_helper"
require "hyrax/transactions"
require "hyrax/specs/shared_specs/factories/administrative_sets"

# This is duplicative of specs in https://github.com/samvera/hyrax/pull/6917,
# but might be worth keeping even after we've incorporated that
RSpec.shared_examples "it includes a delete_permission_template step" do
  describe "#call" do
    it "is a success" do
      expect(tx.call(resource_with_pt)).to be_success
    end

    it "will destroy the associated permission template" do
      # NOTE: We don't just check the PermissionTemplate count, because there are too many
      # possible PermissionTemplate-creating side effects to depend the "before" value
      tx.call(resource_with_pt)
      expect(Hyrax::PermissionTemplate.where(source_id: resource_with_pt.id)).not_to exist
    end

    it "succeeds if the associated permission template has already been destroyed" do
      permission_template = Hyrax::PermissionTemplate.find_by!(source_id: resource_with_pt.id)
      permission_template.destroy!
      expect(tx.call(resource_with_pt)).to be_success
    end
  end
end

RSpec.describe Hyrax::Transactions::Steps::DeletePermissionTemplate do
  context "Hyrax::Transactions::AdminSetDestroy" do
    let(:resource_with_pt) do
      # If there's no default admin set, the first one we create will be it,
      # and we won't be able to delete it.
      FactoryBot.valkyrie_create(:hyrax_admin_set) # throwaway default
      FactoryBot.valkyrie_create(:hyrax_admin_set, :with_permission_template)
    end
    subject(:tx) { Hyrax::Transactions::AdminSetDestroy.new }

    it_behaves_like "it includes a delete_permission_template step"
  end

  context "Hyrax::Transactions::CollectionDestroy" do
    let(:user) { FactoryBot.create(:user) }
    let(:resource_with_pt) { FactoryBot.valkyrie_create(:hyrax_collection, :with_permission_template) }

    subject(:tx) do
      Hyrax::Transactions::CollectionDestroy.new
        .with_step_args(
          "collection_resource.delete" => {user: user},
          "collection_resource.remove_from_membership" => {user: user}
        )
    end

    it_behaves_like "it includes a delete_permission_template step"
  end
end
