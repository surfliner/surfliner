require "rails_helper"

RSpec.describe Hyrax::Workflow::MintARKForObject do
  let(:admin_set) do
    Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
      .find { |p| p.title.include?("Test Project") }
  end
  let(:work) do
    Hyrax.persister.save(resource: GenericObject.new(title: "friend", admin_set_id: admin_set.id))
  end

  let(:spy_listener) { PublisherSpy.new }
  before { Hyrax.publisher.subscribe(spy_listener) }
  after { Hyrax.publisher.unsubscribe(spy_listener) }

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  subject(:workflow_method) { described_class }

  before do
    setup_workflow_for(user)

    allow(Ezid::Identifier).to receive(:mint).and_return("ark:/99999/fk4test")
  end

  describe ".call" do
    it "sets the ARK property" do
      expect { workflow_method.call(target: work) }
        .to change { Hyrax.query_service.find_by(id: work.id).ark }
        .from(be_blank)
        .to("ark:/99999/fk4test")
    end

    it "publishes object.metadata.updated on object" do
      expect { workflow_method.call(target: work) }
        .to change { Hash(spy_listener.object_metadata_updated&.payload)[:object] }
        .from(nil)
        .to eq work
    end

    context "when the object doesn't support #ark" do
      let(:work) { Hyrax.persister.save(resource: Hyrax::Work.new) }

      it "is a silent no-op" do # it shouldn't interrupt the workflow
        expect { workflow_method.call(target: work) }
          .not_to raise_error
      end

      it "does not publish object.metadata.updated" do
        expect { workflow_method.call(target: work) }
          .not_to change { spy_listener.object_metadata_updated }
          .from(nil)
      end
    end
  end
end
