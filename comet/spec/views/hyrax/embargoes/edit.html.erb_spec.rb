require "rails_helper"

RSpec.describe "hyrax/embargoes/edit.html.erb", type: :view do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Embargo Object"],
      visibility: "open")
  end
  let(:ability) { double }
  let(:controller_class) { Hyrax::EmbargoesController }
  let(:form) { Hyrax::Forms::WorkEmbargoForm.new(object) }

  before do
    new_emb = Hyrax::Embargo.new(visibility_during_embargo: "restricted",
      visibility_after_embargo:  "open",
      embargo_release_date: Date.tomorrow.to_s)
    new_emb = Hyrax.persister.save(resource: new_emb)
    object.embargo = new_emb
    acl = Hyrax::AccessControlList.new(resource: object)
    acl.grant(:read).to(user)
    acl.save
    Hyrax.index_adapter.save(resource: object)

    allow(view).to receive(:curation_concern).and_return(form)
    allow(view).to receive(:embargoes_path).and_return("")
    allow(controller).to receive(:current_user).and_return(user)
    assign(:form, form)

    view.controller = controller_class.new
    view.controller.request = ActionController::TestRequest.create(env: {})
    view.controller.action_name = "edit"
    stub_template "hyrax/base/form_permission_embargo" => "rendered form_permission_embargo"
    stub_template "hyrax/embargoes/_embargo_history" => "rendered embargo_history"
  end

  it "displays embargo object title" do
    expect(view).to receive(:provide).with(:page_header).and_yield
    render
    expect(rendered).to have_content "Manage Embargoes for #{form} (#{form.human_readable_type})"
  end
end
