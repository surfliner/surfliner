# frozen_string_literal: true

require "rails_helper"

RSpec.describe "hyrax/dashboard/collections/_default_group.html.erb", type: :view do
  let(:ability) { double }

  before do
    allow(view).to receive(:docs).and_return([])
    allow(view).to receive(:current_ability).and_return(ability)
    allow(ability).to receive(:admin?).and_return(false)
    allow(view).to receive(:render_check_all).and_return("")

    stub_template "hyrax/dashboard/collections/_list_collections.html.erb" => "rendered list_collections"
    render
  end

  it "the line item displays columns for object" do
    expect(rendered).to have_css("th", text: "Last Modified", count: 1)
    expect(rendered).to have_css("th", text: "Items", count: 1)
    expect(rendered).not_to have_css("th", text: "Type")
    expect(rendered).to have_css("th", text: "Visibility", count: 1)
    expect(rendered).to have_css("th", text: "Publication Status", count: 1)
    expect(rendered).to have_css("th", text: "Actions", count: 1)
  end
end
