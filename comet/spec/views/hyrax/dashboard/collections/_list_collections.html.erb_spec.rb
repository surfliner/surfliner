# frozen_string_literal: true

require "rails_helper"

RSpec.describe "hyrax/dashboard/collections/_list_collections.html.erb", type: :view do
  let(:ability) { double }

  let(:collection) do
    FactoryBot.valkyrie_create(:collection,
      :with_index,
      title: ["Test Collection"])
  end

  let(:doc) { SolrDocument.new(SolrDocument.new(Hyrax::ValkyrieIndexer.for(resource: collection).to_solr)) }
  let(:presenter) { Hyrax::CollectionPresenter.new(doc, ability) }

  before do
    allow(Hyrax.query_service).to receive(:find_by).and_return(collection)
    allow(view).to receive(:current_ability).and_return(ability)
    allow(ability).to receive(:admin?).and_return(true)
    allow(ability).to receive(:can?).with(:edit, doc).and_return(false)

    allow(view).to receive(:is_admin_set).and_return(false)
    allow(doc).to receive(:collection?).and_return(true)
    allow(view).to receive(:document_presenter).with(doc).and_return(presenter)
    allow(presenter).to receive(:thumbnail_path).and_return(nil)
    allow(presenter).to receive(:available_parent_collections).and_return([])
    allow(presenter).to receive(:total_viewable_items).and_return(0)
    allow(presenter).to receive(:modified_date).and_return(collection.created_at)
    allow(presenter).to receive(:permission_badge).and_return("<span>Private</span>".html_safe)
    allow(presenter).to receive(:managed_access).and_return("Manage Access")

    allow(view)
      .to receive(:available_parent_collections_data)
      .with(collection: presenter)
      .and_return([])

    view.lookup_context.prefixes << "hyrax/my"
    stub_template "hyrax/my/_collection_action_menu.html.erb" => "actions"
    render "hyrax/dashboard/collections/list_collections", document: doc, collection_presenter: presenter
  end

  it "the line item displays the collection and its actions" do
    expect(rendered).to have_link "Test Collection"
    expect(rendered).to have_css("td span", text: "Private")
    expect(rendered).to have_css("td span", text: "Unpublished")
    expect(rendered).to have_content "actions"
  end
end
