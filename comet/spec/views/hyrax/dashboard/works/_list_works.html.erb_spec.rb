# frozen_string_literal: true

require "rails_helper"

RSpec.describe "hyrax/dashboard/works/_list_works.html.erb", type: :view do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Object"])
  end

  let(:doc) { SolrDocument.new(SolrDocument.new(Hyrax::ValkyrieIndexer.for(resource: object).to_solr)) }
  let(:presenter) { Hyrax::WorkShowPresenter.new(doc, nil) }
  let(:thumbnail) { FileSet.new }

  before do
    allow(Hyrax.query_service).to receive(:find_by).and_return(object)
    allow(doc).to receive(:collection?).and_return(false)
    allow(doc).to receive(:date_modified).and_return(object.created_at)
    allow(doc).to receive(:to_model).and_return(object)
    allow(presenter).to receive(:thumbnail).and_return(thumbnail)
    allow(thumbnail).to receive(:thumbnail_tag).and_return("thumbnail")

    allow(view).to receive(:current_user).and_return(user)
    allow(view).to receive(:document_presenter).with(doc).and_return(presenter)
    allow(view).to receive(:render_thumbnail_tag).and_return("")
    allow(view).to receive(:render_collection_links).with(doc).and_return("")
    allow(view).to receive(:render_visibility_link).with(doc).and_return("<a class=\"visibility-link\">Private</a>".html_safe)

    view.lookup_context.prefixes << "hyrax/my"
    stub_template "hyrax/my/_work_action_menu.html.erb" => "actions"
    render "hyrax/dashboard/works/list_works", document: doc, presenter: presenter
  end

  it "the line item displays the work and its actions" do
    expect(rendered).to have_link "Test Object"
    expect(rendered).to have_css("td span", text: "Unpublished")
    expect(rendered).to have_content "actions"
    expect(rendered).to have_css "a.visibility-link", text: "Private"
  end
end
