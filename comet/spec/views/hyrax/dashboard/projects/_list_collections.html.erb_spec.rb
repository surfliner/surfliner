# frozen_string_literal: true

require "rails_helper"
require "hyrax/specs/shared_specs/factories/administrative_sets"

RSpec.describe "hyrax/dashboard/projects/_list_collections.html.erb", type: :view do
  let(:ability) { double }

  let(:project) do
    FactoryBot.valkyrie_create(:hyrax_admin_set,
      title: ["Test Project"])
  end

  let(:doc) { SolrDocument.new(SolrDocument.new(Hyrax::ValkyrieIndexer.for(resource: project).to_solr)) }
  let(:presenter) { Hyrax::AdminSetPresenter.new(doc, ability) }

  before do
    allow(Hyrax.query_service).to receive(:find_by).and_return(project)
    allow(view).to receive(:current_ability).and_return(ability)
    allow(ability).to receive(:admin?).and_return(true)
    allow(ability).to receive(:can?).with(:edit, doc).and_return(false)

    allow(view).to receive(:is_admin_set).and_return(true)
    allow(view).to receive(:document_presenter).with(doc).and_return(presenter)
    allow(presenter).to receive(:thumbnail_path).and_return(nil)
    allow(presenter).to receive(:total_viewable_items).and_return(0)
    allow(presenter).to receive(:modified_date).and_return(project.created_at)
    allow(presenter).to receive(:permission_badge).and_return("<span>Private</span>".html_safe)
    allow(presenter).to receive(:managed_access).and_return("Manage Access")

    view.lookup_context.prefixes << "hyrax/my"
    stub_template "hyrax/my/_admin_set_action_menu.html.erb" => "actions"
    render "hyrax/dashboard/projects/list_collections", document: doc, collection_presenter: presenter
  end

  it "the line item displays the project and its actions" do
    expect(rendered).to have_link "Test Project"
    expect(rendered).to have_css("td span", text: "Private")
    expect(rendered).to have_content "actions"
  end
end
