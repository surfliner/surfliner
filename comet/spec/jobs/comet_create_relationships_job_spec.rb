# frozen_string_literal: true

require "rails_helper"

RSpec.describe CometCreateRelationshipsJob, :integration, type: :job do
  subject(:job) { described_class.new }
  let(:importer_run_id) { importer&.last_run&.id.to_s }
  let(:object_source_id) { "123" }

  include_context "with a Bulkrax importer", Bulkrax::CometCsvParser

  describe "#perform" do
    context "with a missing importer" do
      it "raises NotFound" do
        expect { job.perform(importer_run_id: 32341, parent_identifier: object_id) }
          .to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "when the parent identifier does not exist" do
      it "raises an internally namespaced error" do
        expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
          .to raise_error(Bulkrax::RecordNotFound)
      end
    end

    context "when the parent is an Entry id" do
      let!(:object_entry) do
        Bulkrax::CometCsvEntry.create(importerexporter: importer, identifier: object_source_id)
      end

      context "and there is not yet a Parent resource" do
        it "raises an internally namespaced error" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
            .to raise_error(Bulkrax::RecordNotFound)
        end
      end

      context "and there are no pending relationships" do
        let!(:object) { FactoryBot.valkyrie_create(:generic_object, :with_index, alternate_ids: [object_source_id]) }

        it "succeeds without incrementing relationships" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
            .not_to change { Bulkrax::ImporterRun.find(importer_run_id).processed_relationships }
            .from(0)
        end
      end

      context "and pending members aren't yet created" do
        before do
          relationships = [
            ::PendingRelationship.new(parent_id: Valkyrie::ID.new(object_source_id),
              child_id: Valkyrie::ID.new("moomin"),
              importer_run_id: Valkyrie::ID.new(importer_run_id)),
            ::PendingRelationship.new(parent_id: Valkyrie::ID.new(object_source_id),
              child_id: Valkyrie::ID.new("snork"),
              importer_run_id: Valkyrie::ID.new(importer_run_id))
          ]
          relationships.each { |r| Comet::AppData.persister.save(resource: r) }
        end

        # we normally expect that missing member entries/objects are simply due to them not yet being created
        it "does not change status on parent" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
            .to raise_error(Bulkrax::RecordNotFound)
          expect(object_entry.reload.status).to eq "Pending"
        end
      end

      context "and there are pending relationships to add" do
        let!(:object) { FactoryBot.valkyrie_create(:generic_object, :with_index, alternate_ids: [object_source_id]) }

        let(:component_entry) do
          Bulkrax::CometCsvEntry.create(importerexporter: importer, identifier: "component_source_id")
        end

        let(:component) do
          FactoryBot.valkyrie_create(:generic_object, :with_index, alternate_ids: ["component_source_id"], edit_users: [importer.last_run.user])
        end

        let!(:component_with_no_entry) do
          FactoryBot.valkyrie_create(:generic_object, :with_index)
        end

        before do
          relationships = [
            ::PendingRelationship.new(parent_id: Valkyrie::ID.new(object_source_id),
              child_id: Valkyrie::ID.new(component_entry.identifier.to_s),
              importer_run_id: Valkyrie::ID.new(importer_run_id)),
            ::PendingRelationship.new(parent_id: Valkyrie::ID.new(object_source_id),
              child_id: Valkyrie::ID.new(component_with_no_entry.id.to_s),
              importer_run_id: Valkyrie::ID.new(importer_run_id))
          ]
          relationships.each { |r| Comet::AppData.persister.save(resource: r) }
        end

        it "counts failed_relationships on run" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
            .to change { Bulkrax::ImporterRun.find(importer_run_id).failed_relationships }
            .from(0).to(2)
        end

        it "populates status on parent entry" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
            .to change { object_entry.reload.status }
            .from("Pending")
            .to("Failed")
        end

        context "and the user is authorized on the parent" do
          let!(:object) { FactoryBot.valkyrie_create(:generic_object, :with_index, alternate_ids: [object_source_id], edit_users: [importer.last_run.user]) }

          it "counts failed_relationships on run" do
            expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
              .to change { Bulkrax::ImporterRun.find(importer_run_id).failed_relationships }
              .from(0).to(1)
          end

          it "updates status on object entry" do
            expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
              .to change { object_entry.reload.status }
              .from("Pending")
              .to("Failed")
          end

          context "and the user is authorized on the members" do
            let!(:component_with_no_entry) do
              FactoryBot.valkyrie_create(:generic_object, :with_index, edit_users: [importer.last_run.user])
            end

            it "adds existing components to members" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)
                .and change { Hyrax.query_service.find_by(id: object.id).member_ids }
                .from(be_empty)
                .to contain_exactly(component_with_no_entry.id)
            end

            it "adds newly created components to members on retry" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)

              component # create the component

              job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id)
              expect(Hyrax.query_service.find_by(id: object.id).member_ids)
                .to contain_exactly(component.id, component_with_no_entry.id)
            end

            it "counts processed_relationships on run" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)
                .and change { Bulkrax::ImporterRun.find(importer_run_id).processed_relationships }
                .from(0).to(1)
            end

            it "does not count failed_relationships on run" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)
                .and avoid_changing { Bulkrax::ImporterRun.find(importer_run_id).failed_relationships }
            end

            it "leaves status on object entry" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)
              expect(object_entry.reload.status).to eq "Pending"
            end

            it "destroys relationships" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)
                .and change { Comet::AppData.query_service.custom_queries.find_pending_relationships(importer_run_id: importer_run_id, parent_id: object_source_id) }
                .to contain_exactly(have_attributes(child_id: component_entry.identifier.to_s))
            end

            context "and all the members exist" do
              before { component } # create the component

              it "counts processed_relationships on run" do
                expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                  .to change { Bulkrax::ImporterRun.find(importer_run_id).processed_relationships }
                  .from(0).to(2)
              end

              it "counts failed_relationships on run" do
                expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                  .not_to change { Bulkrax::ImporterRun.find(importer_run_id).failed_relationships }
                  .from(0)
              end

              it "destroys relationships" do
                job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id)
                expect(
                  Comet::AppData.query_service
                    .custom_queries
                    .find_pending_relationships(importer_run_id: importer_run_id,
                      parent_id: object_source_id)
                    .to_a
                ).to be_empty
              end

              xit "updates status on object entry to Complete" do # should this happen? how ???
                expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id) }
                  .to change { object_entry.reload.status }
                  .from("Pending")
                  .to("Complete")
              end
            end
          end
        end

        context "with FileSet members" do
          subject(:object_persisted) { Hyrax.query_service.find_by(id: object.id) }

          let!(:object) do
            FactoryBot.valkyrie_create(:generic_object,
              :with_index,
              alternate_ids: [object_source_id],
              visibility: object_visibility,
              edit_users: [importer.last_run.user])
          end

          let(:object_visibility) { "open" }
          let(:file_set_source_id) { "fs-123" }

          let(:original_file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
          let(:original_file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: original_file, original_filename: "image.jpg") }
          let(:original_file_metadata) do
            FactoryBot.valkyrie_create(:file_metadata,
              file_identifier: original_file_upload.id,
              original_filename: "image.jpg")
          end

          let(:file_set) do
            FactoryBot.valkyrie_create(:file_set,
              :with_index,
              title: "image.jpg",
              alternate_ids: [file_set_source_id],
              edit_users: [importer.last_run.user])
          end
          let(:file_set_entry) do
            Bulkrax::CometCsvFileSetEntry.create(importerexporter: importer, identifier: file_set_source_id)
          end

          before do
            file_set.file_ids = [original_file_upload.id]
            Hyrax.persister.save(resource: file_set)
            Hyrax.persister.save(resource: original_file_metadata)
            Hyrax.index_adapter.save(resource: file_set)

            relationships = [
              ::PendingRelationship.new(parent_id: Valkyrie::ID.new(object_source_id),
                child_id: Valkyrie::ID.new(file_set_source_id),
                importer_run_id: Valkyrie::ID.new(importer_run_id))
            ]
            relationships.each { |r| Comet::AppData.persister.save(resource: r) }

            job.perform(importer_run_id: importer_run_id, parent_identifier: object_source_id)
          end

          it "adds fileset members" do
            expect(object_persisted.member_ids).to contain_exactly(file_set.id)
          end

          it "set the fileset as representative media" do
            expect(object_persisted.representative_id).to eq file_set.id
            expect(object_persisted.thumbnail_id).to eq file_set.id
            expect(object_persisted.rendering_ids).to contain_exactly(file_set.id)
          end

          context "FileSet visibility" do
            subject(:fileset_visibility_reader) { Hyrax::VisibilityReader.new(resource: file_set_persisted) }

            let(:file_set_persisted) { Hyrax.query_service.find_by(id: file_set.id) }

            it "set visibility open" do
              expect(fileset_visibility_reader.read).to eq "open"
            end

            context "with comet object visibility" do
              let(:object_visibility) { "comet" }

              it "set visibility comet" do
                expect(fileset_visibility_reader.read).to eq "comet"
              end
            end

            context "with visibility metadata_only object visibility" do
              let(:object_visibility) { "metadata_only" }

              it "set visibility comet" do
                expect(fileset_visibility_reader.read).to eq "comet"
              end
            end
          end

          context "File visibility" do
            subject(:file_visibility_reader) { Hyrax::VisibilityReader.new(resource: original_file_metadata) }

            it "set visibility open" do
              expect(file_visibility_reader.read).to eq "open"
            end

            context "with comet object visibility" do
              let(:object_visibility) { "comet" }

              it "set visibility comet" do
                expect(file_visibility_reader.read).to eq "comet"
              end
            end

            context "with visibility metadata_only object visibility" do
              let(:object_visibility) { "metadata_only" }

              it "set visibility comet" do
                expect(file_visibility_reader.read).to eq "comet"
              end
            end
          end
        end
      end
    end

    context "when the parent is a Valkyrie::ID" do
      let!(:object) { FactoryBot.valkyrie_create(:generic_object, :with_index, edit_users: [importer.last_run.user]) }

      it "raises an internally namespaced error for missing object" do
        expect { job.perform(importer_run_id: importer_run_id, parent_identifier: Valkyrie::ID.new("abc").to_s) }
          .to raise_error(Bulkrax::RecordNotFound)
      end

      context "and there are no pending relationships" do
        it "succeeds without incrementing relationships" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object.id.to_s) }
            .not_to change { Bulkrax::ImporterRun.find(importer_run_id).processed_relationships }
            .from(0)
        end
      end

      context "and there are relationships to add" do
        let!(:component_with_no_entry) do
          FactoryBot.valkyrie_create(:generic_object, :with_index, edit_users: [importer.last_run.user])
        end

        before do
          importer.current_run.pending_relationships.create(parent_id: object.id.to_s, child_id: component_with_no_entry.id.to_s)
        end

        it "adds new members" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: object.id) }
            .to change { Hyrax.query_service.find_by(id: object.id).member_ids }
            .from(be_empty)
            .to contain_exactly(component_with_no_entry.id)
        end
      end
    end

    context "when the parent is a Collection" do
      let!(:collection) { FactoryBot.valkyrie_create(:collection, :with_index, alternate_ids: [collection_source_id]) }
      let(:collection_source_id) { "bulk_collection_create_rel_jobs_spec" }

      let!(:collection_entry) do
        Bulkrax::CometCsvEntry.create(importerexporter: importer, identifier: collection_source_id)
      end

      context "and there are no pending relationships" do
        it "succeeds without incrementing relationships" do
          expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
            .not_to change { Bulkrax::ImporterRun.find(importer_run_id).processed_relationships }
            .from(0)
        end
      end

      context "with pending relationships" do
        context "and relationship ids are spurious" do
          before do
            importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: "moomin")
            importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: "snork")
          end

          it "counts failed_relationships on run" do
            expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
              .to change { Bulkrax::ImporterRun.find(importer_run_id).failed_relationships }
              .from(0).to(2)
          end

          it "populates status on parent entry" do
            expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
              .to change { collection_entry.reload.status }
              .from("Pending")
              .to("Failed")
          end
        end

        context "and user has edit on Collection" do
          let!(:collection) { FactoryBot.valkyrie_create(:collection, :with_index, alternate_ids: [collection_source_id], edit_users: [importer.last_run.user]) }

          context "and pending members don't exist yet" do
            before do
              importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: "moomin")
              importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: "snork")
            end

            # we normally expect that missing member entries/objects are simply due to them not yet being created
            it "does not change status on parent" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
                .to raise_error(Bulkrax::RecordNotFound)
              expect(collection_entry.reload.status).to eq "Pending"
            end
          end

          context "when pending members exist without permissions" do
            let!(:object_entry) do
              Bulkrax::CometCsvEntry.create(importerexporter: importer, identifier: object_source_id)
            end

            let!(:object) { FactoryBot.valkyrie_create(:generic_object, :with_index, alternate_ids: [object_source_id]) }

            before do
              importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: object_source_id)
            end

            it "counts failed relationships" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
                .to change { Bulkrax::ImporterRun.find(importer_run_id).failed_relationships }
                .from(0).to(1)
            end
          end

          context "when pending members exist with permissions" do
            let!(:object_entry) do
              Bulkrax::CometCsvEntry.create(importerexporter: importer, identifier: object_source_id)
            end

            let!(:object) { FactoryBot.valkyrie_create(:generic_object, :with_index, alternate_ids: [object_source_id], edit_users: [importer.last_run.user]) }

            let!(:object_with_no_entry) do
              FactoryBot.valkyrie_create(:generic_object, :with_index, edit_users: [importer.last_run.user])
            end

            let!(:other_collection) do
              FactoryBot.valkyrie_create(:collection, :with_index, edit_users: [importer.last_run.user])
            end

            before do
              importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: object_source_id)
              importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: object_with_no_entry.id.to_s)
              importer.current_run.pending_relationships.create(parent_id: collection_source_id, child_id: other_collection.id.to_s)
            end

            it "counts processed relationships" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
                .to change { Bulkrax::ImporterRun.find(importer_run_id).processed_relationships }
                .from(0).to(3)
            end

            it "adds collection members" do
              expect { job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id) }
                .to change { Hyrax.custom_queries.find_members_of(collection: collection) }
                .from(be_empty)
                .to contain_exactly(have_attributes(id: other_collection.id),
                  have_attributes(id: object.id),
                  have_attributes(id: object_with_no_entry.id))
            end

            it "destroys pending relationships" do
              job.perform(importer_run_id: importer_run_id, parent_identifier: collection_source_id)
              expect(importer.current_run.reload.pending_relationships.to_a)
                .to be_empty
            end
          end
        end
      end
    end
  end
end
