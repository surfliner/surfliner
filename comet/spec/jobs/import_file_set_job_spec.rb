# frozen_string_literal: true

require "rails_helper"

RSpec.describe Bulkrax::ImportFileSetJob, type: :job do
  include_context "with a Bulkrax importer", Bulkrax::CometCsvParser

  let(:staging_file) { Tempfile.new("image.jpg").tap { |f| f.write("A fake image!") } }
  let(:s3_key) { "demo_files/document.pdf" }

  before do
    staging_area_upload(fog_connection: fog_connection,
      bucket: bucket, s3_key: s3_key, source_file: staging_file)
  end

  describe "#perform", :integration do
    subject(:perform) { described_class.new.perform(file_set_entry_id, importer_run_id) }

    let(:parent_identifier) { object_source_id }
    let(:object_source_id) { "123" }
    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Object with FileSet"],
        member_ids: member_ids,
        alternate_ids: [object_source_id],
        edit_users: [importer.last_run.user])
    end
    let(:member_ids) { [] }

    let(:file_set_source_id) { "fs-123" }
    let(:file_set_entry_id) { file_set_entry.id }
    let(:file_set_entry) do
      Bulkrax::CometCsvFileSetEntry.create(importerexporter: importer,
        identifier: file_set_source_id,
        parsed_metadata: metadata,
        raw_metadata: metadata)
    end

    let(:metadata) do
      {"model" => "FileSet",
       "source_identifier" => file_set_source_id,
       "title" => "My FileSet",
       "parents" => parent_identifier,
       "use:OriginalFile" => s3_key}
    end

    let(:importer_run_id) { importer.current_run.id.to_s }

    context "without parent object created" do
      it "raises Bulkrax::ParentNotFound error" do
        expect { perform }
          .to raise_error(Bulkrax::ParentNotFound)
          .and avoid_changing { Bulkrax::ImporterRun.find(importer_run_id).enqueued_records }
      end
    end

    context "with parent object created" do
      before { object }

      it "creates successfully" do
        expect(perform.status_message).to eq("Complete")
      end
    end

    context "with existing FileSet" do
      let(:importer) do
        Bulkrax::Importer.create(name: "test importer #{SecureRandom.uuid}",
          parser_klass: Bulkrax::CometCsvParser,
          user_id: user.id,
          admin_set_id: project.id,
          parser_fields: parser_field)
      end

      let(:original_file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
      let(:original_file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: original_file, original_filename: "image.jpg") }
      let(:original_file_metadata) do
        FactoryBot.valkyrie_create(:file_metadata,
          file_identifier: original_file_upload.id,
          original_filename: "image.jpg")
      end

      let(:file_set) do
        FactoryBot.valkyrie_create(:file_set,
          :with_index,
          title: "image.jpg",
          alternate_ids: [file_set_source_id],
          edit_users: [importer.last_run.user])
      end

      before do
        file_set.file_ids = [original_file_upload.id]
        Hyrax.persister.save(resource: file_set)
        Hyrax.persister.save(resource: original_file_metadata)
        Hyrax.index_adapter.save(resource: file_set)

        object
      end

      context "update FileSet" do
        let(:parser_field) { {update_files: true} }

        it "is successful" do
          expect(perform.status_message).to eq("Complete")
        end
      end

      context "replace FileSet" do
        let(:parser_field) { {replace_files: true} }

        it "delete orphans and raised error" do
          expect { perform }.to raise_error(Bulkrax::RecordConflict)
          expect { Hyrax.query_service.find_by(id: file_set.id) }
            .to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
        end

        context "with parent object" do
          let(:member_ids) { [file_set.id] }

          it "raised error" do
            expect { perform }.to raise_error(Bulkrax::RecordConflict)
          end
        end
      end
    end
  end
end
