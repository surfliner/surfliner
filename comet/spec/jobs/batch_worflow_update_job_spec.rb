# frozen_string_literal: true

require "rails_helper"

RSpec.describe BatchWorkflowUpdateJob, type: :job do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:admin_set) do
    Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
      .find { |p| p.title.include?("Test Project") }
  end
  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Workflow Object"],
      depositor: user.user_key,
      admin_set_id: admin_set.id,
      ark: "ark:/fade")
  end
  let(:attributes) { {name: "approve", comment: "Approved"} }

  before do
    Hyrax.metadata_adapter.persister.wipe!
    setup_workflow_for(user)
    Hyrax::Workflow::WorkflowFactory.create(object, {}, user)
  end

  describe "#perform" do
    subject(:perform) do
      described_class.new.perform(user: user,
        object_ids: [object.id&.to_s],
        attributes: attributes)
    end

    before do
      allow(Hyrax::Forms::WorkflowActionForm).to receive(:authorized_for_processing)
        .and_return(true)
    end

    it "calls out to workflow service" do
      expect(Hyrax::Workflow::WorkflowActionService).to receive(:run).once
      perform
    end
  end
end
