# frozen_string_literal: true

require "rails_helper"

RSpec.xdescribe "dashboard objects filters", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:admin_set) do
    Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
      .find { |p| p.title.include?("Test Shoreline Project") }
  end
  let(:object_in_review) do
    FactoryBot.valkyrie_create(:geospatial_object,
      :with_index,
      depositor: user.user_key,
      admin_set_id: admin_set.id,
      title: ["Test Shoreline Mediated Object - In review"],
      ark: "ark:/fade#in_review")
  end
  let(:object_completed) do
    FactoryBot.valkyrie_create(:geospatial_object,
      :with_index,
      depositor: user.user_key,
      admin_set_id: admin_set.id,
      title: ["Test Shoreline Mediated Object - Completed"],
      ark: "ark:/fade#completed")
  end

  before do
    sign_in(user)
    setup_shoreline_mediated_workflow_for(user)

    object_in_review
    object_completed

    Hyrax::Workflow::WorkflowFactory.create(object_in_review, {}, user)
    Hyrax::Workflow::WorkflowFactory.create(object_completed, {}, user)

    workflow_action_completed = Hyrax::Forms::WorkflowActionForm.new(current_ability: ::Ability.new(user),
      work: object_completed, attributes: {name: "approve", comment: "Approved"})
    workflow_action_completed.save
  end

  context "workflow state filter" do
    context "Your Objects tab" do
      it "shows the objects" do
        visit "/dashboard"
        click_on "Objects"

        expect(page).to have_link("Test Shoreline Mediated Object - In review")
        expect(page).to have_link("Test Shoreline Mediated Object - Completed")
      end

      it "shows the object under review only" do
        visit "/dashboard"
        click_on "Objects"

        find("#suppressed_bsi").click

        within "#suppressed_bsi-dropdown-options" do
          click_on "Under Review"
        end

        expect(page).to have_link("Test Shoreline Mediated Object - In review")
        expect(page).not_to have_link("Test Shoreline Mediated Object - Completed")
      end

      it "shows the object with completed status only" do
        visit "/dashboard"
        click_on "Objects"

        find("#suppressed_bsi").click

        within "#suppressed_bsi-dropdown-options" do
          click_on "Completed"
        end

        expect(page).not_to have_link("Test Shoreline Mediated Object - In review")
        expect(page).to have_link("Test Shoreline Mediated Object - Completed")
      end
    end

    context "All Objects tab" do
      it "shows the objects" do
        visit "/dashboard"
        click_on "Objects"

        expect(page).to have_link("All Objects")
        click_on "All Objects"

        expect(page).to have_content("the repository")
        find("#suppressed_bsi").click

        expect(page).to have_link("Test Shoreline Mediated Object - In review")
        expect(page).to have_link("Test Shoreline Mediated Object - Completed")
      end

      it "shows the object under review only" do
        visit "/dashboard"

        click_on "Objects"

        expect(page).to have_link("All Objects")
        click_on "All Objects"

        expect(page).to have_content("the repository")
        find("#suppressed_bsi").click

        within "#suppressed_bsi-dropdown-options" do
          click_on "Under Review"
        end

        expect(page).to have_link("Test Shoreline Mediated Object - In review")
        expect(page).not_to have_link("Test Shoreline Mediated Object - Completed")
      end

      it "shows the object with completed status only" do
        visit "/dashboard"

        click_on "Objects"

        expect(page).to have_link("All Objects")
        click_on "All Objects"

        expect(page).to have_content("the repository")
        find("#suppressed_bsi").click

        within "#suppressed_bsi-dropdown-options" do
          click_on "Completed"
        end

        expect(page).not_to have_link("Test Shoreline Mediated Object - In review")
        expect(page).to have_link("Test Shoreline Mediated Object - Completed")
      end
    end
  end
end
