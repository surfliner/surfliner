# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Admin Workflows", :perform_enqueued, type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:admin_set) do
    Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
      .find { |p| p.title.include?("Test Project") }
  end

  let(:file_set_in_review) { FactoryBot.valkyrie_create(:file_set, title: ["in_review.jpg"]) }
  let(:object_in_review) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Workflow - in_review"],
      members: [file_set_in_review],
      admin_set_id: admin_set.id,
      ark: "ark:/fade#in_review")
  end
  let(:file_set_changes_required) { FactoryBot.valkyrie_create(:file_set, title: ["changes_required.jpg"]) }
  let(:object_changes_required) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Workflow - changes_required"],
      members: [file_set_changes_required],
      admin_set_id: admin_set.id,
      ark: "ark:/fade#changes_required")
  end
  let(:file_set_completed) { FactoryBot.valkyrie_create(:file_set, title: ["completed.jpg"]) }
  let(:object_completed) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Workflow - completed"],
      members: [file_set_completed],
      admin_set_id: admin_set.id,
      ark: "ark:/fade#completed")
  end

  before {
    sign_in user
    setup_workflow_for(user)
    Hyrax::Workflow::WorkflowFactory.create(object_in_review, {}, user)
    Hyrax::Workflow::WorkflowFactory.create(object_changes_required, {}, user)
    Hyrax::Workflow::WorkflowFactory.create(object_completed, {}, user)

    workflow_action_changes_required = Hyrax::Forms::WorkflowActionForm.new(current_ability: ::Ability.new(user),
      work: object_changes_required, attributes: {name: "request_changes", comment: "Approved"})
    workflow_action_changes_required.save

    workflow_action_completed = Hyrax::Forms::WorkflowActionForm.new(current_ability: ::Ability.new(user),
      work: object_completed, attributes: {name: "approve", comment: "Approved"})
    workflow_action_completed.save
  }

  it "shows all objects with active workflows" do
    visit "/admin/workflows"

    expect(page).to have_content("3 objects")
    expect(page).to have_link("Test Workflow - in_review")
    expect(page).to have_link("Test Workflow - changes_required")
    expect(page).to have_link("Test Workflow - completed")
  end

  it "has workflow status filter" do
    visit "/admin/workflows"

    find("#s2id_workflow_state_dropdown_options").click

    within(".select2-results") do
      expect(page).to have_content("In review")
      expect(page).to have_content("Changes required")
      expect(page).to have_content("Completed")
    end
  end

  it "filters by workflow status 'in_review'" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_workflow_state_dropdown_options", "In review")

    expect(page).to have_content("1 objects in_review")
    expect(page).to have_link("Test Workflow - in_review")
    expect(page).not_to have_link("Test Workflow - changes_required")
    expect(page).not_to have_link("Test Workflow - completed")
  end

  it "filters by workflow status 'changes_required'" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_workflow_state_dropdown_options", "Changes required")

    expect(page).to have_content("1 objects changes_required")
    expect(page).not_to have_link("Test Workflow - in_review")
    expect(page).to have_link("Test Workflow - changes_required")
    expect(page).not_to have_link("Test Workflow - completed")
  end

  it "filters by workflow status 'completed'" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_workflow_state_dropdown_options", "Completed")

    expect(page).to have_content("1 objects completed")
    expect(page).not_to have_link("Test Workflow - in_review")
    expect(page).not_to have_link("Test Workflow - changes_required")
    expect(page).to have_link("Test Workflow - completed")
  end

  xit "clears filtered by 'in_review' results" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_workflow_state_dropdown_options", "In review")

    # Results for filtered by in_review
    expect(page).to have_content("1 objects in_review")

    find("#s2id_workflow_state_dropdown_options").click
    within(".select2-results") do
      find(".remove-icon").click
    end

    # Filter cleared with results for all active workflow objects
    expect(page).to have_content("3 objects")
    expect(page).to have_link("Test Workflow - in_review")
    expect(page).to have_link("Test Workflow - changes_required")
    expect(page).to have_link("Test Workflow - completed")
  end

  it "filters workflow objects by project" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_project_dropdown_options", admin_set.title.first)

    expect(page).to have_content("3 objects active")
    expect(page).to have_link("Test Workflow - in_review")
    expect(page).to have_link("Test Workflow - changes_required")
    expect(page).to have_link("Test Workflow - completed")
  end

  it "filters workflow objects by workflow status and project" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_workflow_state_dropdown_options", "In review")

    expect(page).to have_content("1 objects in_review")

    select_workflow_filter("#s2id_project_dropdown_options", admin_set.title.first)

    expect(page).to have_content("1 objects in_review")
    expect(page).to have_link("Test Workflow - in_review")
    expect(page).to_not have_link("Test Workflow - changes_required")
    expect(page).to_not have_link("Test Workflow - completed")
  end

  it "clears project filter" do
    visit "/admin/workflows"

    select_workflow_filter("#s2id_project_dropdown_options", admin_set.title.first)

    expect(page).to have_content("3 objects active")

    within("#appliedParams") do
      expect(page).to have_content(admin_set.title.first)
      find(".remove").click
    end

    expect(page).to have_content("3 objects active")
  end

  context "project filter for other user" do
    let(:other_user) { User.find_or_create_by(email: "other_user@library.ucsb.edu") }
    before do
      setup_workflow_for(other_user)
      sign_in other_user
    end

    after { sign_out other_user }

    it "won't show project list with no workflow role" do
      visit "/admin/workflows"

      find("#s2id_project_dropdown_options").click
      expect(page).not_to have_content(admin_set.title.first)
    end

    context "with :read access ACL" do
      before do
        acl = Hyrax::AccessControlList(object_in_review)
        acl.grant(:read).to(other_user)
        acl.save

        Hyrax.index_adapter.save(resource: object_in_review)
      end

      xit "shows project list" do
        visit "/admin/workflows"

        find("#s2id_project_dropdown_options").click
        expect(page).to have_content(admin_set.title.first)
        find(:css, "ul.select2-results").find(:xpath, "li[1]").click

        expect(page).to have_link("Test Workflow - in_review")
      end
    end
  end

  context "bulkrax importer filter" do
    let(:csv_path) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }
    let(:importer) do
      Bulkrax::Importer.create(name: "test importer #{SecureRandom.uuid}",
        parser_klass: "Bulkrax::CometCsvParser",
        user_id: user.id,
        admin_set_id: admin_set.id,
        parser_fields: {})
    end

    let(:uploaded) do
      FactoryBot.create(:uploaded_file, file: File.open(csv_path))
    end

    before do
      importer[:parser_fields]["import_file_path"] =
        importer.parser.write_import_file(uploaded.file.file)
      importer.save

      Bulkrax::ImporterJob.send(:perform_now, importer.id)
    end

    it "filters workflow objects by importer" do
      visit "/admin/workflows"

      expect(page).to have_content("5 objects")

      select_workflow_filter("#s2id_importer_dropdown_options", importer.name)

      expect(page).to have_content("2 objects active")
      expect(page).to have_link("Lovely Title")
      expect(page).to have_link("Another Title")
    end

    it "remove the constrain filter for the importer" do
      visit "/admin/workflows"

      expect(page).to have_content("5 objects")

      select_workflow_filter("#s2id_importer_dropdown_options", importer.name)

      expect(page).to have_content("2 objects active")

      within("#appliedParams") do
        expect(page).to have_content(importer.name)
        find(".remove").click
      end

      expect(page).to have_content("5 objects")
    end
  end

  context "update object workflows in batch" do
    it "can update workflows successfully for objects with the same workflow state" do
      visit "/admin/workflows"

      expect(page).to have_content("in_review")

      # Filtered by workflow state in_review
      select_workflow_filter("#s2id_workflow_state_dropdown_options", "In review")
      expect(page).to have_content("1 objects in_review")

      # Check all objects with workflow state in_review and update workflows
      find("#check_all").set(true)
      click_button("Update Workflows")

      # Choose workflow action 'approve' to update
      expect(page).to have_content("Workflow Actions")
      choose("workflow_action_name_approve")
      fill_in("Review Comment:", with: "Review Comment")

      click_button("Submit")

      expect(current_path).to eq "/admin/workflows"
      expect(page).to have_content("Successfully enqueued workflows for objects")

      # Objects with in_review workflow state should be changed to Completed
      sleep(1.seconds)
      visit "/admin/workflows"
      find("#s2id_workflow_state_dropdown_options").click
      within(".select2-results") do
        expect(page).not_to have_content("In review")
        expect(page).to have_content("Completed")
      end
    end

    it "can not update workflows for objects with different workflow states" do
      visit "/admin/workflows"

      expect(page).to have_content("3 objects")

      # Check all objects that have different workflow states
      find("#check_all").set(true)
      click_button("Update Workflows")

      expect(page).to have_content("Can't update workflow objects in batch with different workflow states")
    end
  end

  context "user with workflow role" do
    let(:comet_user) { User.find_or_create_by(email: "comet_user@library.ucsb.edu") }
    let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: ["in_review.jpg"]) }
    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Workflow for Comet User - in_review"],
        members: [file_set],
        admin_set_id: admin_set.id,
        ark: "ark:/comet_user#in_review")
    end

    before do
      setup_workflow_for(comet_user)
      Hyrax::Workflow::WorkflowFactory.create(object, {}, comet_user)

      sign_in comet_user
    end

    after { sign_out comet_user }

    it "won't show objects for user with no :read access ACL" do
      visit "/admin/workflows"

      expect(page).not_to have_link("Test Workflow for Comet User - in_review")
    end

    context "with :read access ACL" do
      before do
        acl = Hyrax::AccessControlList(object)
        acl.grant(:read).to(comet_user)
        acl.save

        Hyrax.index_adapter.save(resource: object)
      end

      it "shows the object with active workflow" do
        visit "/admin/workflows"

        expect(page).to have_content("1 objects")
        expect(page).to have_link("Test Workflow for Comet User - in_review")
      end
    end
  end
end
