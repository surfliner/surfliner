# frozen_string_literal: true

require "rails_helper"

RSpec.xdescribe "Objects", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  before do
    sign_in user
    setup_workflow_for(user)
  end

  it "can create and edit object" do
    visit "/concern/generic_objects/new"

    expect(page).to have_content("Add Object")
    expect(page).not_to have_content("Add another Title")

    fill_in("Title", with: "Test Object")
    choose("generic_object_visibility_open")

    click_on "Save"
    expect(page).to have_content("Test Object")

    click_on "Edit"
    expect(page).to have_field("Title", with: "Test Object")
    expect(page).not_to have_content("Add another Title")

    fill_in("Title", with: "My Test Object")

    click_on "Save"
    expect(page).to have_content("successfully updated.")
    expect(page).to have_content("My Test Object")
  end

  it "can list components in a separate tab with counts" do
    visit "/concern/generic_objects/new"

    fill_in("Title", with: "Test Object")
    choose("generic_object_visibility_open")

    # ensure that the form fields are fully populated
    sleep(1.seconds)
    click_on("Save")
    id = page.current_path.split("/").last

    click_button("Add Component")
    click_on("Attach Generic object")

    fill_in("Title", with: "Test Component")
    choose("generic_object_visibility_open")
    sleep(1.seconds)
    click_on("Save")

    visit "/concern/generic_objects/#{id}"
    expect(page).to have_content("All (1)")
    expect(page).to have_content("Components (1)")
    expect(page).to have_content("File Sets (0)")

    click_on("Components (1)")
    expect(page).to have_link("Test Component")
  end

  context "load page from paginator" do
    let(:show_work_item_rows) { Rails.application.config.show_work_item_rows }
    let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: ["image.jpg"]) }
    let(:component_1) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Component"])
    end
    let(:component_2) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Another Test Component"])
    end
    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: [file_set, component_1, component_2],
        title: ["Test Object"])
    end

    before { Rails.application.config.show_work_item_rows = 1 }

    after { Rails.application.config.show_work_item_rows = show_work_item_rows }

    it "has paginator and can load page" do
      visit "/concern/generic_objects/#{object.id}"
      expect(page).to have_content("All (3)")
      expect(page).to have_content("Components (2)")
      expect(page).to have_content("File Sets (1)")

      # Pagination for all members
      expect(page).to have_link("image.jpg")

      click_on("2")
      expect(page).to have_link("Test Component")

      click_on("3")
      expect(page).to have_link("Another Test Component")

      # Pagination for all components
      visit "/concern/generic_objects/#{object.id}#components"
      click_on("Components (2)")
      expect(page).to have_link("Test Component")

      click_on("2")
      expect(page).to have_link("Another Test Component")
    end
  end

  context "shows relavant collections for components" do
    let(:collection) do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        title: "Test Collection")
    end

    let(:component) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["My Test Component"])
    end

    let(:obj) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Comet Object"],
        member_ids: [component.id],
        member_of_collection_ids: [collection.id])
    end

    before { obj }

    it "shows the collection on component" do
      visit "/concern/generic_objects/#{component.id}"

      expect(page).to have_link("My Test Component")
    end
  end
end
