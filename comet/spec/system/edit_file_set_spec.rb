# frozen_string_literal: true

require "rails_helper"

RSpec.xdescribe "FileSet Edit", :integration, type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:file) { Rails.root.join("spec", "fixtures", "staging_area", "demo_files", "test.txt") }
  let(:versioned_file) { Rails.root.join("spec", "fixtures", "upload.txt") }

  before do
    setup_workflow_for(user)
    sign_in user
  end

  it "can upload a new version" do
    visit "/concern/generic_objects/new"
    click_link "Files"

    within("div#add-files") do
      attach_file("files[]", file, visible: false)
    end

    click_link "Descriptions"
    fill_in("Title", with: "Test Object")
    choose("generic_object_visibility_open")
    sleep(1.seconds)
    click_on("Save")

    expect(page).to have_xpath("//a[normalize-space(.)='test.txt']")
    find(:xpath, "//a[normalize-space(.)='test.txt']").click

    expect(page).to have_content("test.txt")

    click_on "Edit This File Set"
    expect(page).to have_content("Edit Hyrax::FileSet")

    click_on "Versions"
    expect(page).to have_content("Upload New Version")

    find(".fileinput-button").click
    attach_file("files[]", versioned_file, visible: false)

    sleep 1.seconds
    expect(page).to have_css("span.controls-remove-text", text: "Remove")

    click_on "Upload New Version"

    expect(page).to have_css("div.alert-success", text: /.*The file.*has been updated.*/)
  end
end
