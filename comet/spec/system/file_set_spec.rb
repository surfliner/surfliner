# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Objects", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: "image.jpg") }
  let(:origina_file) { Tempfile.new("image.jpg").tap { |f| f.write("fade image file") } }
  let(:original_file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: origina_file, original_filename: "image.jpg") }
  let(:original_file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_identifier: original_file_upload.id,
      original_filename: "image.jpg")
  end

  let(:service_file) { Tempfile.new("service.jpg").tap { |f| f.write("fade service file") } }
  let(:service_file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: service_file, original_filename: "service.jpg") }
  let(:service_file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_identifier: service_file_upload.id,
      use: :service_file)
  end

  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      members: [file_set],
      title: ["Show Files Use Object"])
  end

  before do
    file_set.file_ids = [original_file_upload.id, service_file_upload.id]
    Hyrax.persister.save(resource: file_set)
    Hyrax.persister.save(resource: original_file_metadata)
    Hyrax.persister.save(resource: service_file_metadata)
    Hyrax.index_adapter.save(resource: file_set)

    setup_workflow_for(user)
    sign_in user
  end

  it "displays the file use" do
    visit "/concern/parent/#{object.id}/file_sets/#{file_set.id}"

    within ".file-show-details" do
      expect(page).to have_link("http://pcdm.org/use#OriginalFile")
    end

    within ".file-show-files" do
      expect(first(".original_file")[:href]).to match(/downloads\/.*\?locale=en&file=original_file/m)
      expect(first(".service_file")[:href]).to match(/downloads\/.*\?locale=en&file=service_file/m)
    end
  end

  it "does not display the redundant Download image option" do
    visit "/concern/parent/#{object.id}/file_sets/#{file_set.id}"

    expect(page).not_to have_content("Download image")
  end

  context "displays file use for file set with preservation file and service file" do
    let(:original_file_metadata) do
      FactoryBot.valkyrie_create(:file_metadata,
        file_identifier: original_file_upload.id,
        original_filename: "image.jpg",
        use: :preservation_file)
    end

    it "displays the file use" do
      visit "/concern/parent/#{object.id}/file_sets/#{file_set.id}"

      within ".file-show-details" do
        expect(page).to have_link("http://pcdm.org/use#PreservationFile")
      end

      within ".file-show-files" do
        expect(first(".preservation_file")[:href]).to match(/downloads\/.*\?locale=en&file=preservation_file/m)
        expect(first(".service_file")[:href]).to match(/downloads\/.*\?locale=en&file=service_file/m)
      end
    end

    it "won't show the previous/next button for file set members navigation" do
      expect(page).not_to have_link("Previous")
      expect(page).not_to have_link("Next")
    end
  end

  context "object with multiple file set members" do
    let(:file_set_1) { FactoryBot.valkyrie_create(:file_set, title: "image_1.jpg") }
    let(:file_1) { Tempfile.new("image_1.jpg").tap { |f| f.write("fade image file") } }
    let(:file_1_upload) { Hyrax.storage_adapter.upload(resource: file_set_1, file: file_1, original_filename: "image_1.jpg") }
    let(:file_metadata_1) do
      FactoryBot.valkyrie_create(:file_metadata,
        file_identifier: file_1_upload.id,
        original_filename: "image_1.jpg")
    end

    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        members: [file_set, file_set_1],
        title: ["File Set Members Navigation"])
    end

    it "can navigate file set members" do
      visit "/concern/parent/#{object.id}/file_sets/#{file_set.id}"

      expect(page).to have_content "image.jpg"
      expect(page).not_to have_content "image_1.jpg"

      within ".members-navigation" do
        expect(page).to have_link("Next")
        click_on "Next"
      end

      expect(page).to have_content "image_1.jpg"
      expect(page).not_to have_content "image.jpg"

      within ".members-navigation" do
        expect(page).to have_link("Previous")
        click_on "Previous"
      end

      expect(page).to have_content "image.jpg"
      expect(page).not_to have_content "image_1.jpg"
    end
  end

  # context "displays option to download all Fileset files for Object with preservation file and service file" do
  #   let(:original_file_metadata) do
  #     FactoryBot.valkyrie_create(:file_metadata,
  #       file_identifier: original_file_upload.id,
  #       original_filename: "image.jpg",
  #       use: :preservation_file)
  #   end

  #   it "displays download file links" do
  #     visit "/concern/generic_objects/#{object.id}"

  #     expect(page).to have_content("File Sets (1)")
  #     expect(page).to have_content("image.jpg")
  #     click_on "Select an action"
  #     expect(page).to have_link("Download PreservationFile")
  #     expect(page).to have_link("Download ServiceFile")
  #   end
  # end
end
