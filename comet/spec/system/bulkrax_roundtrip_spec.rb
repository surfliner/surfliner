# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Bulkrax Import", :integration, :perform_enqueued, type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:collection_source_id) { "fade-collection-source-id" }
  let(:collection) do
    FactoryBot.valkyrie_create(:collection,
      :with_permission_template,
      alternate_ids: [collection_source_id],
      title: ["Test Collection"],
      edit_users: [user],
      read_users: [user],
      user: user)
  end
  let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: "image.jpg", depositor: user.user_key, edit_users: [user], read_users: [user]) }
  let(:file) { Rack::Test::UploadedFile.new(File.open(Rails.root.join("spec", "fixtures", "image.jpg"))) }
  let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "image.jpg") }
  let(:file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_identifier: file_upload.id,
      original_filename: "image.jpg")
  end

  let(:object_title) { "Test Representative Image Object" }
  let(:object_source_id) { "fade-source-id" }
  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      members: [file_set],
      member_of_collections: [collection],
      alternate_ids: [object_source_id],
      visibility: "open",
      depositor: user.user_key,
      title: [object_title])
  end

  let(:exporter) do
    Bulkrax::Exporter.new(name: "Export from collection",
      user: user,
      export_type: "metadata",
      export_from: "collection",
      export_source: collection.id.to_s,
      parser_klass: "Bulkrax::CometCsvParser",
      limit: 0,
      generated_metadata: false)
  end

  let(:export_path) { Dir.glob(exporter.exporter_export_path)[0] }
  let(:export_csv_file) { "#{export_path}/1/export_#{collection.id}_from_collection_1.csv" }

  before do
    setup_workflow_for(user)

    file_set.file_ids = [file_upload.id]
    Hyrax.persister.save(resource: file_set)
    Hyrax.persister.save(resource: file_metadata)
    Hyrax.index_adapter.save(resource: file_set)
    object

    # Run exporter
    exporter.save
    Bulkrax::ExporterJob.perform_now(exporter.id)

    sign_in user
  end

  it "can Create and Validate" do
    visit "/importers/new"

    fill_in("Name", with: "importer_validate")

    select_admin_set

    choose("importer_parser_fields_file_style_upload_a_file")
    attach_file "File", export_csv_file

    click_button "Create and Validate"

    expect(page).to have_link("importer_validate")
    click_on "importer_validate"

    expect(page).to have_content("Importer validation completed. Please review and choose to either Continue with or Discard the import.")
    expect(page).to have_content(object_source_id)

    click_on object_source_id
    expect(page).to have_content("Raw Metadata:")

    within("#raw-metadata-heading") do
      find(".accordion-title").click
    end

    expect(page).to have_content(object_title)
  end

  it "can create and import" do
    visit "/exporters/#{exporter.id}"
    expect(page).to have_content("Export from collection")
    expect(page).to have_content("Complete")
    expect(page).to have_button("Download")
    expect(find("#exporter_exporter_export_zip_files")&.value).to match(/export_.*\.zip/m)

    visit "/importers/new"

    fill_in("Name", with: "importer_metadata_overlay")

    select_admin_set

    choose("importer_parser_fields_file_style_upload_a_file")
    attach_file "File", export_csv_file

    click_button "Create and Import"

    expect(page).to have_content("Importer was successfully created and import has been queued.")

    expect(page).to have_css("td", text: "Complete")

    click_on "importer_metadata_overlay"
    expect(page).to have_link(object_source_id)
    expect(page).to have_css("td", text: "Complete")

    click_on "Collection Entries"
    expect(page).to have_link(collection_source_id)
    expect(page).to have_css("td", text: "Complete")

    click_on "File Set Entries"
    expect(page).to have_css("td", text: "Complete")

    click_on "Object Entries"
    click_on object_source_id
    expect(page).to have_link("GenericObject")

    click_on "GenericObject"

    expect(page).to have_content(object_title)
    expect(page).to have_link("image.jpg")
    within(".title-with-badges") do
      expect(page).to have_content(object_title)
    end
  end
end
