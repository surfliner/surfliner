# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Dashboard Collections", type: :system, js: true do
  include_context "with an admin set"

  context "admin users" do
    let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

    let(:collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [user.user_key],
        read_users: [user.user_key],
        title: "Test Collection",
        user: user
      )
    end

    before do
      collection
      sign_in(user)
    end

    it "shows the collection but no projects" do
      visit "/dashboard/collections"

      expect(page).to have_link("Test Collection")
      expect(page).not_to have_link("Default Project")
    end

    context "add to collection" do
      let(:other_user) { User.find_or_create_by(email: "comet-user@library.ucsb.edu") }

      let(:other_collection) do
        FactoryBot.valkyrie_create(
          :collection,
          :with_index,
          :with_permission_template,
          edit_users: [other_user.user_key],
          read_users: [other_user.user_key],
          title: "Other's Collection",
          user: other_user
        )
      end

      before { other_collection }

      it "add child collection from actions dropdown" do
        visit "/dashboard/collections"
        expect(page).to have_link("Other's Collection")

        find(:xpath, "(//div[@id='documents']//tr/td/div/button)[1]").click
        first("a.add-to-collection").click

        expect(page).to have_content("Select collection")
        find(:xpath, "(//select[@name='parent_id']/option)[2]").select_option

        click_button "Add to collection"

        expect(page).to have_content("has been added")
        expect(current_path).to include("/dashboard/collections")
        expect(page).to have_link("Test Collection")
        expect(page).to have_link("Other's Collection")
      end
    end
  end

  context "non-admin users" do
    let(:other_user) { User.find_or_create_by(email: "comet-user@library.ucsb.edu") }

    let(:other_collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [other_user.user_key],
        read_users: [other_user.user_key],
        title: "Collection created by non-admin",
        user: other_user
      )
    end

    before do
      other_collection
      sign_in(other_user)
    end

    it "display the collection" do
      visit "/dashboard/collections"

      expect(page).to have_link("Collection created by non-admin")
      expect(page).not_to have_link("Collections", href: hyrax.my_collections_path(locale: "en"))
    end
  end
end
