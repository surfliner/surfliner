# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Collections", :integration, type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:collection) do
    FactoryBot.valkyrie_create(:collection,
      :with_permission_template,
      title: ["Test Collection"],
      edit_users: [user],
      read_users: [user],
      user: user)
  end

  before { sign_in user }

  xit "can create a new collection and add object" do
    Hyrax::CollectionType.create(title: "Spec Type")

    visit "/admin/collection_types"
    click_on "Create new collection type"
    fill_in("Type name", with: "Curated Collection")
    click_on "Save"

    visit "/dashboard"
    click_on "Collections"
    find("#add-new-collection-button").click

    fill_in("Title", with: "System Spec Collection")

    expect { click_on("Save") }
      .to change { Hyrax.query_service.count_all_of_model(model: ::Collection) }
      .by 1

    expect(page).to have_content("Collection was successfully created.")
    expect(page).to have_content("System Spec Collection")

    # TODO: teach collection controller to use a new non-Solr presenter
    click_on("Display all details of System Spec Collection")
    expect(page).to have_content("System Spec Collection")
  end

  context "new collection form" do
    before do
      Hyrax::CollectionType.find_or_create_default_collection_type
    end

    it "can create a new collection" do
      visit "/dashboard/collections"

      find("#add-new-collection-button").click

      expect(page).to have_content("New User Collection")
      expect(page).not_to have_content("Add another Title")

      fill_in("Title", with: "My New Collection")

      click_on "Save"

      expect(page).to have_content("Collection was successfully created.")
      expect(page).to have_content("My New Collection")
    end
  end

  context "with edit access on an existing collection" do
    it "can destroy the collection" do
      visit "/dashboard/collections/#{collection.id}"
      accept_alert { click_on "Delete collection" }

      expect(page).to have_content(" successfully deleted")
      expect { Hyrax.query_service.find_by(id: collection.id) }
        .to raise_error Valkyrie::Persistence::ObjectNotFoundError
    end

    it "can edit a collection" do
      visit "/dashboard/collections/#{collection.id}"
      click_on "Edit collection"

      expect(page).to have_content("Test Collection")
      expect(page).not_to have_content("Add another Title")

      fill_in("Title", with: "Updated Collection")
      click_on "Save changes"

      reloaded = Hyrax.query_service.find_by(id: collection.id)
      expect(reloaded).to have_attributes title: contain_exactly("Updated Collection")
    end
  end

  context "nested collection" do
    let(:user) { User.create(email: "comet-admin@library.ucsd.edu") }

    let(:collection) do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        :with_permission_template,
        title: ["Test Collection"],
        edit_users: [user],
        member_of_collection_ids: [nested_collection.id, another_nested_collection.id],
        read_users: [user],
        user: user)
    end

    let(:nested_collection) do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        :with_permission_template,
        title: ["Nested Collection"],
        edit_users: [user],
        read_users: [user],
        user: user)
    end

    let(:another_nested_collection) do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        :with_permission_template,
        title: ["Another Nested Collection"],
        edit_users: [user],
        read_users: [user],
        user: user)
    end

    it "shows the nested collections" do
      visit "/dashboard/collections/#{collection.id}"

      within("section#parent-collections-wrapper") do
        expect(page).to have_content("Nested Collection")
        expect(page).to have_content("Another Nested Collection")
      end
    end
  end

  context "collection object members" do
    before do
      obj_a = ::GenericObject.new(
        title: ["Test Member Object A"],
        member_of_collection_ids: [collection.id]
      )
      Hyrax.index_adapter.save(
        resource: Hyrax.persister.save(resource: obj_a)
      )

      obj_b = ::GenericObject.new(
        title: ["Test Member Object B"],
        member_of_collection_ids: [collection.id]
      )
      Hyrax.index_adapter.save(
        resource: Hyrax.persister.save(resource: obj_b)
      )
    end

    it "display object members to authorized user" do
      visit "/dashboard/collections/#{collection.id}"

      expect(page).to have_content("Test Member Object A")
      expect(page).to have_content("Test Member Object B")
    end
  end

  context "collections created by other users" do
    let(:other_user) { User.find_or_create_by(email: "comet-user@library.ucsb.edu") }

    before do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        :with_permission_template,
        title: ["Other User's Collection"],
        edit_users: [other_user, user],
        member_of_collection_ids: [],
        user: other_user)
    end

    it "shows other user's collections" do
      visit "/dashboard/collections"

      expect(page).to have_link("Other User's Collection")
    end
  end

  context "representative image" do
    subject { find("img.representative-media") }

    context "upload image" do
      it "upload a representative image successfully" do
        visit "/dashboard/collections/#{collection.id}/edit"

        expect(page).to have_link "Representative image"

        click_on "Representative image"

        within("span.fileinput-button") do
          attach_file("files[]", Rails.root.join("spec", "fixtures", "image.jpg"), visible: false)
        end

        sleep(1.seconds)
        expect(page).to have_content("image.jpg")
        expect(page).to have_button("Remove")

        click_on "Save changes"

        expect(page).to have_content("Collection was successfully updated.")
        expect(page).to have_content("Test Collection")

        expect(subject[:src]).to match(/downloads\/.*\?file=thumbnail/m)

        visit subject[:src]
        expect(page.body).to match(/<img .* src=".*downloads\/.*\?file=thumbnail">/m)
      end

      context "new collection form" do
        let(:collection_title) { "New Collection with Representative Image Upload" }

        before { Hyrax::CollectionType.find_or_create_default_collection_type }

        it "can create collection with representative image" do
          visit "/dashboard/collections?locale=en"

          find("#add-new-collection-button").click

          fill_in("Title", with: collection_title)

          click_on "Representative image"

          within("span.fileinput-button") do
            attach_file("files[]", Rails.root.join("spec", "fixtures", "image.jpg"), visible: false)
          end

          sleep(1.seconds)
          expect(page).to have_content("image.jpg")
          expect(page).to have_button("Remove")

          click_on "Save"

          expect(page).to have_content("Collection was successfully created.")
          expect(page).to have_content(collection_title)

          within ".thumbnail-wrapper" do
            expect(find("img")[:src]).to match(/downloads\/.*\?file=thumbnail/m)
          end

          visit find("img")[:src]
          expect(page.body).to match(/<img .* src=".*downloads\/.*\?file=thumbnail">/m)
        end
      end

      context "with existing image uploaded" do
        let(:comfirm_remove) { "Are you sure to you want to remove the uploaded representative image? The image will be deleted from the system once the form is saved." }

        it "can be displayed and removed" do
          visit "/dashboard/collections/#{collection.id}/edit"

          expect(page).to have_link "Representative image"

          click_on "Representative image"

          within("span.fileinput-button") do
            attach_file("files[]", Rails.root.join("spec", "fixtures", "image.jpg"), visible: false)
          end

          sleep(1.seconds)
          expect(page).to have_content("image.jpg")
          expect(page).to have_button("Remove")

          click_on "Save changes"

          expect(page).to have_content("Collection was successfully updated.")

          visit "/dashboard/collections/#{collection.id}/edit"

          expect(page).to have_link "Representative image"
          click_on "Representative image"

          within("#representative-image-uploaded") do
            expect(find(".icon-wrapper").find("img")[:src]).to match(/downloads\/.*\?file=thumbnail/m)
            find(".representative-image-remove").click
          end

          alert = page.driver.browser.switch_to.alert

          expect(alert.text).to have_content(comfirm_remove)
          alert.accept

          expect(page).not_to have_link("Remove")

          click_on "Save changes"

          expect(page).to have_content("Collection was successfully updated.")
          expect(subject[:src]).to include("/assets/")
        end
      end
    end

    context "select image from collection objects" do
      let(:collection) do
        FactoryBot.valkyrie_create(:collection,
          :with_permission_template,
          title: ["Test Collection"],
          edit_users: [user],
          read_users: [user],
          user: user)
      end
      let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: "image.jpg") }
      let(:file) { Rack::Test::UploadedFile.new(File.open(Rails.root.join("spec", "fixtures", "image.jpg"))) }
      let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "image.jpg") }
      let(:file_metadata) do
        FactoryBot.valkyrie_create(:file_metadata,
          file_identifier: file_upload.id,
          original_filename: "image.jpg")
      end

      let(:object_title) { "Test Representative Image Object" }
      let(:object) do
        FactoryBot.valkyrie_create(:generic_object,
          :with_index,
          members: [file_set],
          member_of_collections: [collection],
          representative_id: file_set.id,
          thumbnail_id: file_set.id,
          title: [object_title])
      end

      let(:select2_option) { "image.jpg" }

      before do
        file_set.file_ids = [file_upload.id]
        Hyrax.persister.save(resource: file_set)
        Hyrax.persister.save(resource: file_metadata)
        Hyrax.index_adapter.save(resource: file_set)
        object

        setup_workflow_for(user)
        sign_in user
      end

      it "select representative image with filename" do
        visit "/dashboard/collections/#{collection.id}/edit"

        expect(page).to have_link "Representative image"

        click_on "Representative image"

        select_representative_image(select2_option)

        expect(find("#representative-icon")[:src]).to include("/downloads/#{file_set.id}?inline=true")

        click_on "Save changes"

        expect(page).to have_content("Collection was successfully updated.")
        expect(page).to have_content("Test Collection")
        expect(subject[:src]).to include("/downloads/#{file_set.id}?inline=true")
      end

      it "select representative image with object title" do
        visit "/dashboard/collections/#{collection.id}/edit"

        expect(page).to have_link "Representative image"

        click_on "Representative image"

        select_representative_image(object_title)

        expect(find("#representative-icon")[:src]).to include("/downloads/#{file_set.id}?inline=true")

        click_on "Save changes"

        expect(page).to have_content("Collection was successfully updated.")
        expect(page).to have_content("Test Collection")
        expect(subject[:src]).to include("/downloads/#{file_set.id}?inline=true")
      end

      context "with existing image selected" do
        let(:comfirm_remove) { "Are you sure to you want to remove the selected representative image? This will take effect once the form is saved." }

        it "can be displayed and removed" do
          visit "/dashboard/collections/#{collection.id}/edit"

          expect(page).to have_link "Representative image"

          click_on "Representative image"

          select_representative_image(object_title)

          click_on "Save changes"
          expect(page).to have_content("Collection was successfully updated.")

          visit "/dashboard/collections/#{collection.id}/edit"

          expect(page).to have_link "Representative image"
          click_on "Representative image"

          within("#representative-image-selected") do
            expect(find(".icon-wrapper").find("img")[:src]).to include("/downloads/#{file_set.id}?inline=true")
            find(".representative-image-remove").click
          end

          alert = page.driver.browser.switch_to.alert

          expect(alert.text).to have_content(comfirm_remove)
          alert.accept

          expect(page).not_to have_content(file_set.title)
          expect(page).not_to have_link("Remove")

          click_on "Save changes"
          expect(page).to have_content("Collection was successfully updated.")
          expect(subject[:src]).to include("/assets/")
        end
      end
    end
  end
end
