# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Batch Publish Objects", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Batch Publish Object"])
  end

  before { sign_in(user) }

  context "with RabbitMQ", :rabbitmq do
    let(:queue_message) { [] }
    let(:connection) { Rails.application.config.rabbitmq_connection }
    let(:broker) { MessageBroker.new(connection: connection, topic: topic) }
    let(:tidewater_conf) { DiscoveryPlatform.new(:tidewater).message_route }
    let(:topic) { tidewater_conf.metadata_topic }
    let(:tidewater_routing_key) { tidewater_conf.metadata_routing_key }
    let(:url_base) { ENV.fetch("DISCOVER_PLATFORM_TIDEWATER_URL_BASE") { Rails.application.config.metadata_api_uri_base } }
    let(:object_url) { DiscoveryPlatformPublisher.api_uri_for(object) }

    before {
      object
      broker.channel.queue(topic).bind(broker.exchange, routing_key: tidewater_routing_key).subscribe do |delivery_info, metadata, payload|
        queue_message << payload
      end
    }

    after { broker.close }

    it "publish the object to tidewater" do
      visit "/dashboard/works?locale=en"

      find("#check_all").set(true)
      expect(page).to have_button("Publish")

      click_on "Publish"
      within("#publish-dropdown-options") do
        expect(page).to have_button("All")
        expect(page).to have_button("Tidewater")

        click_on("Tidewater")
      end

      alert = page.driver.browser.switch_to.alert
      alert.accept

      expect(page).to have_content("The batch objects publish request is submitted successfully.")

      publish_wait(queue_message, 0) {}

      visit "/concern/generic_objects/#{object.id}?locale=en"

      expect(page).to have_link("Tidewater", href: "#{url_base}#{ERB::Util.url_encode(object_url)}")
    end

    context "publish to all patforms" do
      let(:shoreline_url_base) { ENV.fetch("DISCOVER_PLATFORM_SHORELINE_URL_BASE") { Rails.application.config.metadata_api_uri_base } }

      it "publish the object to both tidewater and shoreline" do
        visit "/dashboard/works?locale=en"

        find("#check_all").set(true)
        expect(page).to have_button("Publish")

        click_on "Publish"
        within("#publish-dropdown-options") do
          expect(page).to have_button("All")
          expect(page).to have_button("Tidewater")

          click_on("All")
        end

        alert = page.driver.browser.switch_to.alert
        alert.accept

        expect(page).to have_content("The batch objects publish request is submitted successfully.")

        publish_wait(queue_message, 0) {}

        visit "/concern/generic_objects/#{object.id}?locale=en"

        expect(page).to have_link("Tidewater", href: "#{url_base}#{ERB::Util.url_encode(object_url)}")
        expect(page).to have_link("Shoreline", href: "#{shoreline_url_base}#{ERB::Util.url_encode(object_url)}")
      end
    end
  end
end
