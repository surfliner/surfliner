# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Dashboard Publication Status", type: :system, js: true, storage_adapter: :memory, metadata_adapter: :test_adapter do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:platforms) { {} }

  let(:collection) do
    FactoryBot.valkyrie_create(
      :collection,
      :with_index,
      :with_permission_template,
      edit_users: [user.user_key],
      read_users: [user.user_key],
      title: "System Spec Collection",
      user: user
    )
  end

  before do
    setup_workflow_for(user)
    collection

    sign_in(user)
    allow(DiscoveryPlatformService)
      .to receive(:call)
      .and_return(platforms)
  end

  context "with a published collection" do
    let(:platforms) { [["Tidewater", "http://tidewater/1234"]] }

    it "can show the publication status in the collection list" do
      visit "/dashboard/collections"
      expect(page).to have_content("Published")
    end
  end

  context "with an unpublished collection" do
    let(:platforms) { [] }

    it "can show the publication status in the collection list" do
      visit "/dashboard/collections"
      expect(page).to have_content("Unpublished")
    end
  end
end
