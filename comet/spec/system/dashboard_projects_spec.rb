# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Dashboard Projects", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  before { sign_in(user) }

  context "#index" do
    include_context "with an admin set"

    let(:collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [user.user_key],
        read_users: [user.user_key],
        title: "Test Collection",
        user: user
      )
    end

    it "shows the projects but no collections" do
      visit "/dashboard/projects"

      expect(page).not_to have_link("Test Collection")
      expect(page).to have_link("Default Project")
    end

    it "has breadcrumb for /dashboard/projects" do
      visit "/dashboard/projects"

      expect(page).to have_link("Projects", href: main_app.dashboard_projects_path(locale: "en"))
    end

    context "with pagination" do
      before do
        my_project = Hyrax.persister.save(resource: Hyrax::AdministrativeSet.new(title: "My Second Project"))
        Hyrax.index_adapter.save(resource: my_project)
      end

      it "shows the second page" do
        visit "/dashboard/projects/page/2?locale=en&per_page=1"

        expect(page).to have_link("« Previous")
      end

      it "stays in /dashboard/projects when page size changed" do
        visit "/dashboard/projects?locale=en"

        find(:xpath, "(//select[@name='per_page']/option)[2]").select_option

        expect(page).to have_link("My Second Project")
        expect(current_path).to include("/dashboard/projects")
      end
    end
  end
end
