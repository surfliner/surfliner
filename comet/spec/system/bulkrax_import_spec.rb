# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Bulkrax Import", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }

  before do
    setup_workflow_for(user)
    sign_in user
  end

  it "queues for create and validate with Pending status" do
    visit "/importers/new"

    fill_in("Name", with: "Importer: Validate-only")

    select_admin_set

    choose("importer_parser_fields_file_style_upload_a_file")
    attach_file "File", source_file

    click_button "Create and Validate"

    expect(page).to have_link("Importer: Validate-only")

    expect(page).to have_content("Validate Pending")
  end

  context "bulkrax import", :perform_enqueued do
    it "can successfully create and validate" do
      visit "/dashboard"
      click_on "Importers"
      click_on "New"

      fill_in("Name", with: "importer_validate")

      select_admin_set

      choose("importer_parser_fields_file_style_upload_a_file")
      attach_file "File", source_file

      click_button "Create and Validate"

      expect(page).to have_content("Importer validation completed. Please review and choose to either Continue with or Discard the import.")

      expect(page).to have_link("importer_validate")
      expect(page).to have_content("Validate Complete")

      click_on "importer_validate"

      expect(page).to have_content("w1")
      expect(page).to have_content("w2")

      click_on "w1"
      expect(page).to have_content("Raw Metadata:")

      within("#raw-metadata-heading") do
        find(".accordion-title").click
      end

      expect(page).to have_content("Lovely Title")
    end

    it "can successfully create and import" do
      visit "/dashboard"
      click_on "Importers"
      click_on "New"

      fill_in("Name", with: "importer_ingest")

      select_admin_set

      choose("importer_parser_fields_file_style_upload_a_file")
      attach_file "File", source_file

      click_button "Create and Import"

      expect(page).to have_content("Importer was successfully created and import has been queued.")

      expect(page).to have_css("td", text: "Complete")

      click_on "importer_ingest"
      expect(page).to have_link("w1")

      click_on "w1"
      expect(page).to have_link("GenericObject")

      click_on "GenericObject"
      expect(page).to have_content("Lovely Title")
      within(".title-with-badges") do
        expect(page).to have_content("Lovely Title")
        expect(page).to have_css(".badge-primary", text: "Comet")
      end
    end

    context "sort by last run" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }

      before do
        ["Importer-1", "Importer-2", "Importer-3"].each do |importer|
          visit "/dashboard"
          click_on "Importers"
          click_on "New"

          fill_in("Name", with: importer)

          select_admin_set

          choose("importer_parser_fields_file_style_upload_a_file")
          attach_file "File", source_file

          click_button "Create and Validate"

          expect(page).to have_content("Importer validation completed.")
        end
      end

      xit "listed importers in order while sorting" do
        visit "/dashboard"
        click_on "Importers"

        expect(page.body).to match(/Importer-3.*Importer-2.*Importer-1/m)

        find(:xpath, "//th[@aria-label='Last Run: activate to sort column ascending']").click
        expect(page.body).to match(/Importer-1.*Importer-2.*Importer-3/m)
      end
    end

    context "import records with no source identifier" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "missing_source_id.csv") }

      it "can successfully create and import" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_no_source_id")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Import"

        expect(page).to have_content("Importer was successfully created and import has been queued.")

        click_on "Importers"

        expect(page).to have_content("Complete")

        click_on "importer_no_source_id"

        expect(page).to have_content("Importer: importer_no_source_id")

        click_on "Object Entries"
        within("#work-entries") do
          expect(page).to have_css("a", count: 2)
          expect(page).to have_selector("td", text: "Complete", exact_text: true, count: 2)
          expect(page).not_to have_selector("td", text: "Pending")
        end
      end
    end

    context "import records with property using multiple csv headers" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "property_multiple_headers.csv") }
      it "can successfully create multiple records for a property using multiple headers" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_multivalue_columns")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Import"

        expect(page).to have_content("Importer was successfully created and import has been queued.")

        click_on "Importers"
        click_on "importer_multivalue_columns"

        expect(page).to have_content("w1")
        expect(page).to have_content("w2")

        # check the 1st record in the csv
        click_on "w1"
        expect(page).to have_content("Raw Metadata:")

        within("#parsed-metadata-heading") do
          find(".accordion-title").click
        end

        expect(page).to have_content("[\"Creator 1\", \"Creator 2\"]")

        within("div.main-content") do
          within("nav") do
            click_on "importer_multivalue_columns"
          end
        end

        click_on "w2"
        expect(page).to have_content("Raw Metadata:")

        within("#parsed-metadata-heading") do
          find(".accordion-title").click
        end

        expect(page).to have_content("[\"Creator 2\", \"Creator 3\"]")
      end
    end

    context "import records with delimited column values" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "multivalue_columns.csv") }

      it "can successfully split a delimited column value into multiple values" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_multivalue_columns")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Import"

        expect(page).to have_content("Importer was successfully created and import has been queued.")

        click_on "Importers"
        click_on "importer_multivalue_columns"

        expect(page).to have_content("w1")
        expect(page).to have_content("w2")

        # check the 1st record in the csv
        click_on "w1"
        expect(page).to have_content("Raw Metadata:")

        within("#parsed-metadata-heading") do
          find(".accordion-title").click
        end

        expect(page).to have_content("[\"Creator 1\", \"Creator 2\"]")

        within("div.main-content") do
          within("nav") do
            click_on "importer_multivalue_columns"
          end
        end

        click_on "w2"
        expect(page).to have_content("Raw Metadata:")

        within("#parsed-metadata-heading") do
          find(".accordion-title").click
        end

        expect(page).to have_content("[\"Creator 2\", \"Creator 3\"]")
      end
    end

    context "model validation" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "invalid-models.csv") }
      let(:error_message) do
        "StandardError - Invalid model(s): Row 2 (w1) => unknown model `UnknownModel`, Row 3 (w2) => model missing."
      end

      it "failed with validation" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_multivalue_columns")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Validate"

        expect(page).to have_content("Importer validation completed. Please review and choose to either Continue with or Discard the import.")

        expect(page).to have_link("importer_multivalue_columns")
        click_on "importer_multivalue_columns"

        within("#error-trace-heading") do
          find(:css, ".accordion-title").click
        end

        expect(page).to have_content(error_message)
      end
    end

    context "with missing files" do
      include_context "with a mock fog connection for bulk file staging"

      let(:s3_bucket) { S3Configurations::StagingArea.bucket }
      let(:file) { Tempfile.new("image.jpg").tap { |f| f.write("An image!") } }
      let(:s3_key) { "demo_files/image.jpg" }
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "missing-files.csv") }

      let(:error_message) { "StandardError - File(s) missing: Row 3 (w2) => demo_files/missing-image.jpg" }

      before do
        staging_area_upload(fog_connection: fog_connection,
          bucket: s3_bucket, s3_key: s3_key, source_file: file)
      end

      it "failed with validation" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_missing_files")

        select_admin_set
        find(:css, "#importer_parser_klass").find("option[value='Bulkrax::CometCsvParser']").select_option
        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Validate"

        expect(page).to have_content("Importer validation completed. Please review and choose to either Continue with or Discard the import.")

        expect(page).to have_link("importer_missing_files")
        click_on "importer_missing_files"

        within("#error-trace-heading") do
          find(:css, ".accordion-title").click
        end

        expect(page).to have_content(error_message)
      end
    end

    context "import records with url value in Note field" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }

      it "can successfully support full url value" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_urls_in_note")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Import"

        expect(page).to have_content("Importer was successfully created and import has been queued.")

        click_on "Importers"
        click_on "importer_urls_in_note"
        expect(page).to have_content("Total Objects: 2")

        visit "/dashboard/my/works?locale=en"

        click_link("Lovely Title", match: :first)
        expect(page).to have_content("https://www.sangis.org/legal_notice.htm")
      end
    end

    context "bulkrax import" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "generic_objects.csv") }

      it "can highlight active tab when going from one tab to another" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_active_tab")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Import"

        expect(page).to have_content("Importer was successfully created and import has been queued.")

        click_on "Importers"
        click_on "importer_active_tab"
        expect(page).to have_content("Total Objects: 2")
        expect(page).to have_content("w1")
        expect(page).to have_content("w2")

        expect(page).to have_xpath("//li[.//a[@href='#work-entries']][@class='nav-link active']")
        expect(page).not_to have_xpath("//li[.//a[@href='#collection-entries']][@class='nav-link active']")
        expect(page).not_to have_xpath("//li[.//a[@href='#file-set-entries']][@class='nav-link active']")

        click_on "Collection Entries"
        expect(page).not_to have_xpath("//li[.//a[@href='#work-entries']][@class='nav-link active']")
        expect(page).to have_xpath("//li[.//a[@href='#collection-entries']][@class='nav-link active']")
        expect(page).not_to have_xpath("//li[.//a[@href='#file-set-entries']][@class='nav-link active']")
      end
    end

    context "import records with different file use" do
      let(:source_file) { Rails.root.join("spec", "fixtures", "bulkrax", "different_file_use_objects.csv") }

      xit "can successfully display object preview" do
        visit "/dashboard"
        click_on "Importers"
        click_on "New"

        fill_in("Name", with: "importer_use_file")

        select_admin_set

        choose("importer_parser_fields_file_style_upload_a_file")
        attach_file "File", source_file

        click_button "Create and Import"

        expect(page).to have_content("Importer was successfully created and import has been queued.")

        click_on "Importers"
        click_on "importer_use_file"
        expect(page).to have_content("Total Objects: 1")
        sleep(10.seconds)

        visit "/dashboard/my/works?locale=en"
        click_link("Lovely Title for Service File", match: :first)

        expect(page).not_to have_content("No preview available")
      end
    end
  end
end
