# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Manage workflow roles", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:role_label) { "Test Project - depositing (surfliner_default)" }

  before do
    setup_workflow_for(user)
    sign_in(user)
  end

  it "shows the roles" do
    visit "/admin/workflow_roles"

    expect(page).to have_content(role_label)
    expect(page).to have_xpath("//select[@id='sipity_workflow_responsibility_workflow_role_id']/option[text()='#{role_label}']")
    within("table.table-striped") do
      expect(page).to have_content(user.email)
      expect(page).to have_content(role_label)
    end
  end

  context "with pagination" do
    let(:another_user) { User.find_or_create_by(email: "comet-admin@library.ucsd.edu") }
    let(:show_work_item_rows) { Rails.application.config.show_work_item_rows }

    before { setup_workflow_for(another_user) }

    it "shows the page with no paginators" do
      visit "/admin/workflow_roles?locale=en"

      expect(page).to have_content("Current Roles")
      expect(page).not_to have_css("ul.pagination")
    end

    it "shows the first page" do
      visit "/admin/workflow_roles?locale=en&per_page=1"

      expect(page).to have_xpath("//td/ul[@class='workflow-roles']", count: 1)
      expect(page).to have_css("ul.pagination")
      expect(page).to have_link("Next »")
    end

    it "shows the second page" do
      visit "/admin/workflow_roles?locale=en&page=2&per_page=1"

      expect(page).to have_xpath("//td/ul[@class='workflow-roles']", count: 1)
      expect(page).to have_css("ul.pagination")
      expect(page).to have_link("« Previous")
    end
  end
end
