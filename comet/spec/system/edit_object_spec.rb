# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Objects", type: :system, js: true do
  let(:collection) do
    FactoryBot.valkyrie_create(:collection, :with_permission_template, user: user)
  end

  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsd.edu") }

  before do
    collection # create the collection explictly
    sign_in user
  end

  context "from the object page" do
    let(:object) do
      obj = GenericObject.new(title: "Object Added to Collection by Spec",
        visibility: "open",
        edit_users: [user])
      obj = Hyrax.persister.save(resource: obj)
      Hyrax.index_adapter.save(resource: obj)
      obj
    end

    xit "can edit the object after assigning the collection to it" do
      visit main_app.hyrax_generic_object_path(id: object.id, locale: "en")

      click_button "Add to collection"
      select_member_of_collection(collection)
      click_button "Save changes"

      expect(object.member_of_collection_ids).to contain_exactly collection.id

      visit main_app.hyrax_generic_object_path(id: object.id)

      click_on "Edit"

      expect(page).to have_content(object.title.first)
    end
  end

  context "with pub status tab" do
    let(:collection) do
      FactoryBot.valkyrie_create(
        :collection,
        :with_index,
        :with_permission_template,
        edit_users: [user.user_key],
        read_users: [user.user_key],
        title: "Test Collection",
        user: user
      )
    end

    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Pub Status Object"],
        member_of_collections: [collection])
    end

    before do
      acl = Hyrax::AccessControlList.new(resource: object)
      acl.grant(:read).to(user)
      acl.save
      Hyrax.index_adapter.save(resource: object)
    end

    xit "has unpublished status" do
      visit main_app.hyrax_generic_object_path(id: object.id, locale: "en")

      expect(page).to have_content("Test Pub Status Object")

      click_on "Edit"

      expect(page).to have_link("Pub Status")
      click_on "Pub Status"

      expect(page).to have_content("Unpublished")
    end
  end

  context "with embargo object" do
    xit "can create and edit" do
      visit "/concern/generic_objects/new"

      fill_in("Title", with: "Test Embargo Object")
      choose("generic_object_visibility_embargo")

      click_on "Save"

      expect(page).to have_content("Test Embargo Object")

      click_on "Edit"

      expect(page).to have_content("This work is under embargo.")

      click_on "Save"
      expect(page).to have_content("successfully updated.")
    end
  end

  context "with lease object" do
    xit "can create and edit" do
      visit "/concern/generic_objects/new"

      fill_in("Title", with: "Test Lease Object")
      choose("generic_object_visibility_lease")

      click_on "Save"

      expect(page).to have_content("Test Lease Object")

      click_on "Edit"

      expect(page).to have_content("This work is under lease.")

      click_on "Save"
      expect(page).to have_content("successfully updated.")
    end
  end
end
