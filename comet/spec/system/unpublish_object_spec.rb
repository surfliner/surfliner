# frozen_string_literal: true

require "rails_helper"

RSpec.xdescribe "Unpublish Object", type: :system, js: true do
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

  let(:object) do
    FactoryBot.valkyrie_create(:generic_object,
      :with_index,
      title: ["Test Object"])
  end

  before {
    object
    sign_in(user)
  }

  it "has no unpublish button" do
    visit main_app.hyrax_generic_object_path(id: object.id)

    expect(page).not_to have_link("Unpublish")
  end

  context "with rabbitmq", :rabbitmq do
    let(:queue_message) { [] }
    let(:connection) { Rails.application.config.rabbitmq_connection }
    let(:broker) { MessageBroker.new(connection: connection, topic: topic) }
    let(:tidewater_conf) { DiscoveryPlatform.new(:tidewater).message_route }
    let(:topic) { tidewater_conf.metadata_topic }
    let(:tidewater_routing_key) { tidewater_conf.metadata_routing_key }
    let(:url_base) { ENV.fetch("DISCOVER_PLATFORM_TIDEWATER_URL_BASE") { Rails.application.config.metadata_api_uri_base } }
    let(:object_url) { DiscoveryPlatformPublisher.api_uri_for(object) }

    before {
      broker.channel.queue(topic).bind(broker.exchange, routing_key: tidewater_routing_key).subscribe do |delivery_info, metadata, payload|
        queue_message << payload
      end

      DiscoveryPlatformPublisher.open_on(:tidewater) do |publisher|
        publisher.publish(resource: object)
      end

      sleep(1.seconds)
    }

    after { broker.close }

    xit "has the unpublish button" do
      visit main_app.hyrax_generic_object_path(id: object.id)

      expect(page).to have_button("Unpublish")
    end

    it "can unpublish from :tidewater" do
      visit main_app.hyrax_generic_object_path(id: object.id)

      expect(page).to have_link("Tidewater", href: "#{url_base}#{ERB::Util.url_encode(object_url)}")
      expect(page).to have_button("Unpublish")

      click_on "Unpublish"
      within("#unpublish-dropdown-options") do
        expect(page).to have_link("All")
        expect(page).to have_link("Tidewater")
        click_on("Tidewater")
      end

      alert = page.driver.browser.switch_to.alert
      expect(alert.text).to have_content("Are you sure you want to unpublish the object from tidewater?")
      alert.accept

      expect(page).to have_content("The unpublish object request is submitted successfully.")

      publish_wait(queue_message, 1) {}

      visit main_app.hyrax_generic_object_path(id: object.id)
      expect(page).not_to have_button("Unpublish")
      expect(page).not_to have_link("Tidewater", href: "#{url_base}#{ERB::Util.url_encode(object_url)}")
    end

    it "unpublish from all patforms" do
      visit "/concern/generic_objects/#{object.id}?locale=en"

      expect(page).to have_button("Unpublish")
      expect(page).to have_link("Tidewater", href: "#{url_base}#{ERB::Util.url_encode(object_url)}")

      click_on "Unpublish"
      within("#unpublish-dropdown-options") do
        expect(page).to have_link("All")
        expect(page).to have_link("Tidewater")
        click_on("All")
      end

      alert = page.driver.browser.switch_to.alert
      expect(alert.text).to have_content("Are you sure you want to unpublish the object from all discovery platforms?")
      alert.accept

      publish_wait(queue_message, 1) {}

      visit "/concern/generic_objects/#{object.id}?locale=en"

      expect(page).not_to have_button("Unpublish")
      expect(page).not_to have_link("Tidewater", href: "#{url_base}#{ERB::Util.url_encode(object_url)}")
    end
  end
end
