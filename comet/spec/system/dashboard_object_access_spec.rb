# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Access objects created by other comet users", type: :system, js: true do
  let(:admin) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:user) { User.find_or_create_by(email: "comet-user@library.ucsb.edu") }

  context "with admin role" do
    before do
      obj = GenericObject.new(title: "Other Comet User's Object",
        depositor: user.user_key)

      obj = Hyrax.persister.save(resource: obj)
      Hyrax.index_adapter.save(resource: obj)

      sign_in admin
    end

    after { sign_out admin }

    it "shows the object" do
      visit "/dashboard/works"

      expect(page).to have_link("Other Comet User's Object")
    end
  end

  context "with login comet user role" do
    before { user.instance_variable_set(:@assigned_groups, Set["comet"]) }
    after { sign_out user }

    context "for edit access objects" do
      before do
        obj = GenericObject.new(title: "Other User's Edit Access Object",
          depositor: admin.user_key)
        obj = Hyrax.persister.save(resource: obj)
        acl = Hyrax::AccessControlList(obj)
        acl.grant(:read).to(user)
        acl.save

        Hyrax.index_adapter.save(resource: obj)

        sign_in user
      end

      it "shows the object" do
        visit "/dashboard/works"

        expect(page).to have_link("Other User's Edit Access Object")
      end
    end

    context "for read access objects" do
      before do
        obj = GenericObject.new(title: "Other User's Read Access Object",
          depositor: admin.user_key)
        obj = Hyrax.persister.save(resource: obj)
        acl = Hyrax::AccessControlList(obj)
        acl.grant(:read).to(user)
        acl.save

        Hyrax.index_adapter.save(resource: obj)

        sign_in user
      end

      it "shows the object" do
        visit "/dashboard/works"

        expect(page).to have_link("Other User's Read Access Object")
      end
    end

    context "for public visibility object" do
      before do
        sign_in admin

        visit "/concern/generic_objects/new"
        fill_in("Title", with: "Public Visibility Object")
        choose("generic_object_visibility_open")
        click_on "Save"

        sign_out admin
        sign_in user
      end

      it "shows the object" do
        visit "/dashboard/works"

        expect(page).to have_link("Public Visibility Object")
      end
    end

    context "for campus visibility object" do
      before do
        sign_in admin

        visit "/concern/generic_objects/new"
        fill_in("Title", with: "Campus Visibility Object")
        choose("generic_object_visibility_campus")
        click_on "Save"

        sign_out admin

        sign_in user
      end

      it "shows the object" do
        visit "/dashboard/works"

        expect(page).to have_content("Campus Visibility Object")
      end
    end

    context "for metadata-only visibility object" do
      before do
        sign_in admin

        visit "/concern/generic_objects/new"
        fill_in("Title", with: "Metadata-only Visibility Object")
        choose("generic_object_visibility_metadata_only")
        click_on "Save"

        sign_out admin

        sign_in user
      end

      it "shows the object" do
        visit "/dashboard/works"

        expect(page).to have_content("Metadata-only Visibility Object")
      end
    end

    context "for comet visibility object" do
      before do
        sign_in admin

        visit "/concern/generic_objects/new"
        fill_in("Title", with: "Comet Visibility Object")
        choose("generic_object_visibility_comet")
        click_on "Save"

        sign_out admin

        sign_in user
      end

      it "shows the object" do
        visit "/dashboard/works"

        expect(page).to have_content("Comet Visibility Object")
      end
    end
  end
end
