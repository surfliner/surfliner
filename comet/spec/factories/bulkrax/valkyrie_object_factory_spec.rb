require "rails_helper"

RSpec.describe Bulkrax::ValkyrieObjectFactory, :with_admin_set do
  subject(:object_factory) do
    described_class.new(attributes: attributes,
      source_identifier_value: source_identifier,
      user: user,
      klass: klass,
      update_files: update_files)
  end

  let(:klass) { ::GenericObject }
  let(:update_files) { false }
  let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
  let(:invalid_object_factory) do
    described_class.new(attributes: nil,
      source_identifier_value: nil,
      user: nil,
      klass: nil,
      update_files: nil)
  end

  RSpec.shared_context "create a collection" do
    let(:klass) { Bulkrax::Collection }

    let(:attributes) do
      {
        model: "Collection",
        title: "A Test Collection"
      }
    end
  end

  RSpec.shared_context "create files in s3 bucket" do
    let(:fog_connection) { Rails.application.config.staging_area_s3_connection }
    let(:s3_bucket) { S3Configurations::StagingArea.bucket }
    let(:file) { Tempfile.new("image.jpg").tap { |f| f.write("An image!") } }
    let(:s3_key) { "demo_files/demo.jpg" }
    let(:source_identifier) { "object_1" }
    let(:title) { "Test title" }
    let(:attributes) do
      {
        :title => title,
        "use:PreservationFile" => [{url: "demo_files/demo.jpg"}],
        "remote_files" => "demo_files/demo.jpg"
      }
    end

    before do
      staging_area_upload(fog_connection: fog_connection,
        bucket: s3_bucket, s3_key: s3_key, source_file: file)
    end
  end

  describe "#create" do
    context "with a visibility" do
      let(:attributes) do
        {title: "Test Import Title with Visibility",
         visibility: "open"}
      end

      let(:source_identifier) { "object_with_viz" }

      it "assigns the visibility" do
        object_factory.run!

        imported = Hyrax.query_service.find_all_of_model(model: GenericObject).first
        expect(imported)
          .to have_attributes(visibility: "open")
      end
    end

    context "when creating a collection" do
      include_context "create a collection"

      let(:source_identifier) { "spec_collection" }

      it "makes a collection" do
        object_factory.run!

        imported = Hyrax.query_service.find_all_of_model(model: ::Collection).first
        expect(imported)
          .to have_attributes(title: ["A Test Collection"])
      end
    end
  end

  context "when source_identifier_value does not match an existing object" do
    let(:source_identifier) { "object_1" }
    let(:title) { "Test Bulkrax Import Title" }
    let(:alternative_title) { "Test Alternative Title" }
    let(:attributes) { {title: title, title_alternative: [alternative_title]} }

    before do
      setup_workflow_for(user)
      attributes[:admin_set_id] = Hyrax.query_service.find_all_of_model(model: Hyrax::AdministrativeSet)
        .find { |p| p.title.include?("Test Project") }.id
    end

    it "create object with metadata" do
      object_imported = object_factory.run!

      expect(object_imported).to have_attributes(
        title: contain_exactly(title),
        alternate_ids: contain_exactly(source_identifier)
      )
    end

    it "create object in workflow" do
      object_imported = object_factory.run!

      workflow_object = Sipity::Entity(Hyrax.query_service.find_by(id: object_imported.id))

      expect(workflow_object.workflow_state).to have_attributes name: "in_review"
    end
  end

  context "when source_identifier_value matches an existing object" do
    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        title: ["Test Object"],
        title_alternative: ["Test Alternative Title"],
        alternate_ids: [source_identifier])
    end

    let(:source_identifier) { "object_2" }
    let(:title_updated) { "Test Bulkrax Import Title Update" }
    let(:alternative_title_updated) { "Test Alternative Title Added" }
    let(:attributes) do
      {
        title: title_updated,
        title_alternative: [alternative_title_updated],
        alternate_ids: [source_identifier]
      }
    end

    it "update object with metadata" do
      object_updated = object_factory.run!

      expect(object_updated)
        .to have_attributes(title: contain_exactly(title_updated),
          title_alternative: contain_exactly(alternative_title_updated))
    end
  end

  context "when update object with ARK" do
    let(:source_identifier) { "object_1" }
    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Object with ARK"],
        alternate_ids: [source_identifier],
        ark: "ark:/12345/test123")
    end

    let(:title_updated) { "Test Object with ARK - Update" }
    let(:attributes) do
      {
        title: title_updated,
        alternate_ids: [source_identifier]
      }
    end

    let(:ark_pattern) { Rails.application.config.ark_pattern }

    before do
      Rails.application.config.ark_pattern = /^ark:\/[0-9]{5}\/[a-z0-9]{7}/
      object
    end
    after { Rails.application.config.ark_pattern = ark_pattern }

    it "retains the ARK" do
      object_updated = object_factory.run!

      expect(object_updated)
        .to have_attributes(title: contain_exactly(title_updated), ark: object.ark)
    end
  end

  context "when update collection with ARK" do
    let(:source_identifier) { "collection_1" }
    let(:collection) do
      FactoryBot.valkyrie_create(:collection,
        :with_index,
        title: ["Test Collection with ARK"],
        alternate_ids: [source_identifier],
        ark: "ark:/12345/test123")
    end

    let(:title_updated) { "Test Collection with ARK - Update" }
    let(:attributes) do
      {
        model: "Collection",
        title: title_updated,
        alternate_ids: [source_identifier]
      }
    end

    let(:ark_pattern) { Rails.application.config.ark_pattern }

    before do
      Rails.application.config.ark_pattern = /^ark:\/[0-9]{5}\/[a-z0-9]{7}/
      collection
    end
    after { Rails.application.config.ark_pattern = ark_pattern }

    it "retains the ARK" do
      col_updated = object_factory.run!

      expect(col_updated)
        .to have_attributes(title: contain_exactly(title_updated), ark: collection.ark)
    end
  end

  context "attaching files", :integration do
    include_context "create files in s3 bucket"

    let(:missing_s3_key) { "not/inbucket.jpg" }
    let(:title) { "Test Bulkrax Import with missing file" }
    let(:attributes) do
      {
        title: title,
        "use:PreservationFile": [{url: s3_key}],
        "use:ServiceFile": [{url: missing_s3_key}]
      }
    end

    before do
      staging_area_upload(fog_connection: fog_connection,
        bucket: s3_bucket, s3_key: s3_key, source_file: file)
    end

    context "with multiple files with different uses" do
      let(:attributes) do
        {model: "GenericObject",
         source_identifier: "test-use-2",
         title: "Work with Other Uses",
         parents_column: nil,
         "use:ServiceFile": [{url: "demo_files/image.jpg"}],
         "use:OriginalFile": nil,
         "use:PreservationFile": [{url: "demo_files/demo.jpg"}]}
      end

      before do
        staging_area_upload(fog_connection: fog_connection,
          bucket: s3_bucket, s3_key: "demo_files/image.jpg", source_file: file)
      end

      after do
        file = fog_connection.directories.new(key: s3_bucket).files.new(key: "image.jpg")
        file.destroy
      end

      it "attaches the files to a single FileSet" do
        object = object_factory.run!
        file_sets = Hyrax.custom_queries.find_child_file_sets(resource: object)

        expect(file_sets.count).to eq 1

        expect(Hyrax.custom_queries.find_files(file_set: file_sets.first))
          .to contain_exactly(have_attributes(original_filename: "demo.jpg",
            pcdm_use: contain_exactly(Hyrax::FileMetadata::Use::PRESERVATION_FILE)),
            have_attributes(original_filename: "image.jpg",
              pcdm_use: contain_exactly(Hyrax::FileMetadata::Use::SERVICE_FILE)))
      end
    end

    it "raises an error when files exist in staging area s3 bucket" do
      expect { object_factory.run! }.to raise_error(/failed_to_attach_file_sets/)
    end
  end

  context "update_file", :integration do
    include_context "create files in s3 bucket"

    let(:object) do
      FactoryBot.valkyrie_create(:generic_object,
        :with_index,
        title: ["Test Object"],
        title_alternative: ["Test Alternative Title"],
        alternate_ids: [source_identifier])
    end

    let(:update_files) { true }
    let(:source_identifier) { "object_3" }
    let(:title_updated) { "Test Bulkrax Import Update and replace Files" }

    let(:attributes) do
      {
        title: title_updated,
        alternate_ids: [source_identifier],
        remote_files: [{url: "demo_files/demo.jpg"}]
      }
    end

    it "update object with metadata and files" do
      object_updated = object_factory.run!

      fs = Hyrax.custom_queries.find_child_file_sets(resource: object_updated).first
      expect(fs.title.first).to eq "demo.jpg"

      use = Hyrax.custom_queries.find_files(file_set: fs).first.pcdm_use.first

      expect(object_updated)
        .to have_attributes(title: contain_exactly(title_updated))
      expect(object_updated.member_ids.size).to eq 1
      expect(use).to eq Hyrax::FileMetadata::Use::ORIGINAL_FILE
    end

    context "with pcdm:use values" do
      let(:attributes) do
        {
          :title => title_updated,
          :alternate_ids => [source_identifier],
          "use:PreservationFile" => [{url: "demo_files/demo.jpg"}]
        }
      end

      it "updates object with metadata and files with pcdm:use values" do
        object_updated = object_factory.run!

        fs = Hyrax.custom_queries.find_child_file_sets(resource: object_updated).first
        expect(fs.title.first).to eq "demo.jpg"

        use = Hyrax.custom_queries.find_files(file_set: fs).first.pcdm_use.first

        expect(object_updated)
          .to have_attributes(title: contain_exactly(title_updated))
        expect(object_updated.member_ids.size).to eq 1
        expect(use).to eq Hyrax::FileMetadata::Use::PRESERVATION_FILE
      end
    end
  end

  describe "#schema_properties" do
    let(:generic_properties) { ::GenericObject.reader.properties(availability: ::GenericObject.availability).keys }
    let(:geospatial_properties) { ::GeospatialObject.reader.properties(availability: ::GeospatialObject.availability).keys }

    context "with GenericObject" do
      let(:generic_object) { GenericObject.new }

      it "being responded to GenericObject instant" do
        generic_properties.each do |pro|
          expect(generic_object.respond_to?(pro))
        end
      end
    end

    context "with GeospatialObject" do
      let(:geospatial_object) { GeospatialObject.new }

      it "being responded to GeospatialObject instant" do
        geospatial_properties.each do |pro|
          expect(geospatial_object.respond_to?(pro))
        end
      end
    end
  end

  describe "#find" do
    let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

    context "with source identifier present" do
      let(:attributes) do
        {title: "Test find method",
         visibility: "open"}
      end

      let(:source_identifier) { "source_id" }

      it "with source identifier and no attribute id" do
        object_factory.run!
        expect(object_factory.find).not_to be_nil
      end
    end

    context "with attribute id and source identifier as nil" do
      let(:attributes) do
        Hyrax.persister.save(resource: Hyrax::FileSet.new(file_ids: "fake://1"))
      end

      let(:source_identifier) { nil }

      it "find by attribute ID" do
        object_factory.run!
        expect(object_factory.find).not_to be_nil
      end
    end
  end

  describe "#search_by_identifier" do
    let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }

    context "with source identifier not present" do
      let(:attributes) do
        {title: "Test search_by_identifier method",
         visibility: "open"}
      end

      let(:source_identifier) { nil }

      it "source identifier being nil" do
        object_factory.run!
        expect(object_factory.search_by_identifier).to eq(false)
      end
    end
  end

  describe "#update" do
    context "raise object does not exist error" do
      it "raise error" do
        expect { invalid_object_factory.update }.to raise_error(RuntimeError, "Object doesn't exist")
      end
    end

    context "do not destroy existing files if replace_files is set to false", :integration do
      include_context "create files in s3 bucket"

      it "existing files not destroyed when replace_files is false" do
        object = object_factory.run!
        existing_files = Hyrax.custom_queries.find_child_file_sets(resource: object)
        expect(object_factory.update).to_not be_nil
        expect(existing_files.count).to eq 1
      end

      context "destroy existing files if replace_files is set to true" do
        include_context "create files in s3 bucket"
        let(:klass) { Bulkrax::Collection }

        let(:replace_files) { true }

        it "destroy existing files" do
          object = object_factory.run!
          existing_files = Hyrax.custom_queries.find_child_file_sets(resource: object)
          expect(object_factory.update).to_not be_nil
          expect(existing_files.count).to eq 0
        end
      end
    end
  end

  describe "#apply_depositor_metadata" do
    context "valid object and user" do
      let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
      let(:attributes) do
        {title: "Test title",
         visibility: "open"}
      end
      let(:source_identifier) { "source_id" }

      it "publishes without errors" do
        object = object_factory.run!
        expect(object_factory.apply_depositor_metadata(object, user)).not_to be_nil
        expect(object)
          .to have_attributes(depositor: "comet-admin@library.ucsb.edu")
      end
    end
  end

  describe "#file_attributes" do
    context "return hash when klass is a collection" do
      include_context "create a collection"

      let(:source_identifier) { "spec_collection" }

      it "klass is a collection" do
        object_factory.run!
        expect(object_factory.file_attributes(false)).to eq({})
      end
    end

    context "file is present", :integration do
      include_context "create files in s3 bucket"

      it "file is present" do
        object_factory.run!
        expect(object_factory.file_attributes(false))
          .to eq({"remote_files" => [{file_name: "demo.jpg", url: "demo_files/demo.jpg"}], "use:PreservationFile" => [{"url" => "demo_files/demo.jpg"}]})
      end
    end

    context "using file attributes that are not permitted", :integration do
      include_context "create files in s3 bucket"

      let(:attributes) do
        {title: "Test file attributes method"}
      end

      let(:source_identifier) { "source_id" }

      it "file attributes not starting with 'use'" do
        object_factory.run!
        expect(object_factory.file_attributes(false))
          .to eq({})
      end
    end
  end

  describe "#parsed_remote_files", :integration do
    context "remote file is a Hash" do
      include_context "create files in s3 bucket"

      let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
      let(:source_identifier) { "source_id" }

      it "file value is a Hash type" do
        object_factory.run!
        expect(object_factory.parsed_remote_files("use:PreservationFile")).to eq([{"url" => "demo_files/demo.jpg"}])
      end
    end

    context "file attribute is a String", :integration do
      include_context "create files in s3 bucket"

      let(:user) { User.find_or_create_by(email: "comet-admin@library.ucsb.edu") }
      let(:source_identifier) { "source_id" }

      it "file value is a String" do
        object_factory.run!
        expect(object_factory.parsed_remote_files("remote_files")).to eq([{url: "demo_files/demo.jpg", file_name: "demo.jpg"}])
      end
    end

    context "file attribute is neither Hash nor a String" do
      include_context "with a mock fog connection for bulk file staging"

      let(:s3_bucket) { S3Configurations::StagingArea.bucket }
      let(:file) { Tempfile.new("image.jpg").tap { |f| f.write("An image!") } }
      let(:s3_key) { 123 }
      let(:source_identifier) { "object_1" }
      let(:title) { "Test title" }
      let(:attributes) do
        {
          :title => title,
          "remote_files" => 123
        }
      end

      before do
        staging_area_upload(fog_connection: fog_connection,
          bucket: s3_bucket, s3_key: s3_key, source_file: file)
      end

      it "file value is an integer" do
        # using a test logger here
        log_output = StringIO.new
        original_logger = Rails.logger
        Rails.logger = Logger.new(log_output)
        log_output.rewind
        object_factory.run!
        result = object_factory.parsed_remote_files("remote_files")
        expect(log_output.string).to include("skipped remote file 123 because we do not recognize the type")
        expect(result).to eq([])
        Rails.logger = original_logger
      end
    end
  end
end
