# frozen_string_literal: true

require "rails_helper"
require "comet/characterization_service"

RSpec.describe Comet::CharacterizationService do
  let(:file_set) { FactoryBot.valkyrie_create(:file_set, title: "image.jpg") }
  let(:file) { File.open(Rails.root.join("spec", "fixtures", "image.jpg")) }
  let(:file_upload) { Hyrax.storage_adapter.upload(resource: file_set, file: file, original_filename: "image.jpg") }
  let(:file_metadata) do
    FactoryBot.valkyrie_create(:file_metadata,
      file_set_id: file_set.id,
      file_identifier: file_upload.id,
      original_filename: "image.jpg")
  end

  before do
    file_set.file_ids = [file_upload.id]
    saved_file_set = Hyrax.persister.save(resource: file_set)
    saved_file_metadata = Hyrax.persister.save(resource: file_metadata)
    Hyrax.index_adapter.save(resource: saved_file_set)

    described_class
      .run(metadata: saved_file_metadata, file: saved_file_metadata.file, **Hyrax.config.characterization_options)
  end

  describe "#run" do
    it "successfully sets the property values" do
      expect(Hyrax.query_service.find_by(id: file_metadata.id))
        .to have_attributes(compression: contain_exactly("JPEG"),
          format_label: contain_exactly("JPEG File Interchange Format"),
          height: contain_exactly("93"),
          width: contain_exactly("150"))
    end

    context "with valkyrie shrine storage adaptor", :integration do
      xit "successfully sets the property values" do
        expect(Hyrax.query_service.find_by(id: file_metadata.id))
          .to have_attributes(compression: contain_exactly("JPEG"),
            format_label: contain_exactly("JPEG File Interchange Format"),
            height: contain_exactly("93"),
            width: contain_exactly("150"))
      end
    end
  end
end
