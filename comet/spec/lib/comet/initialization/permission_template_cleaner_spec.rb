# frozen_string_literal: true

require "rails_helper"
require "hyrax/specs/shared_specs/factories/administrative_sets"
require "hyrax/specs/shared_specs/factories/permission_templates"
require "comet/initialization/permission_template_cleaner"

module Comet
  module Initialization
    RSpec.shared_examples "retaining valid templates" do
      let(:projects) do
        3.times.map do |i|
          FactoryBot.valkyrie_create(:hyrax_admin_set, title: "Test Project #{i}").tap do |resource|
            FactoryBot.create(:permission_template, with_workflows: true, source_id: resource.id.to_s)
          end
        end
      end

      let(:collections) do
        3.times.map do |i|
          FactoryBot.valkyrie_create(:collection, title: "Test Collection #{i}").tap do |resource|
            FactoryBot.create(:permission_template, with_workflows: true, source_id: resource.id.to_s)
          end
        end
      end

      let(:source_ids) do
        (projects + collections).map { |source| source.id.to_s }
      end

      def valid_templates
        Hyrax::PermissionTemplate.where(source_id: source_ids).to_a
      end

      def valid_workflows
        Sipity::Workflow.where(permission_template: valid_templates).to_a
      end

      before do
        expect(valid_templates).not_to be_empty # just to be sure
        expect(valid_workflows).not_to be_empty # just to be sure

        {
          Hyrax.config.admin_set_class => projects,
          Hyrax.config.collection_class => collections
        }.each do |model_class, resources_expected|
          resources_actual = Hyrax.query_service.find_all_of_model(model: model_class)
          expect(resources_actual).to contain_exactly(*resources_expected)
        end
      end

      it "does not delete valid templates and workflows" do
        valid_templates_expected = valid_templates
        valid_workflows_expected = valid_workflows

        PermissionTemplateCleaner.clean!
        expect(valid_templates).to contain_exactly(*valid_templates_expected)
        expect(valid_workflows).to contain_exactly(*valid_workflows_expected)
      end
    end

    RSpec.shared_examples "deleting invalid templates" do
      let(:deleted_projects) do
        3.times.map do |i|
          FactoryBot.valkyrie_create(:hyrax_admin_set, title: "Deleted Test Project #{i}").tap do |resource|
            FactoryBot.create(:permission_template, with_workflows: true, source_id: resource.id.to_s)
            Hyrax.persister.delete(resource: resource)
          end
        end
      end

      let(:deleted_collections) do
        3.times.map do |i|
          FactoryBot.valkyrie_create(:collection, title: "Deleted Test Collection #{i}").tap do |resource|
            FactoryBot.create(:permission_template, with_workflows: true, source_id: resource.id.to_s)
            Hyrax.persister.delete(resource: resource)
          end
        end
      end

      let(:deleted_source_ids) do
        (deleted_projects + deleted_collections).map { |source| source.id.to_s }
      end

      def orphaned_templates
        Hyrax::PermissionTemplate.where(source_id: deleted_source_ids).to_a
      end

      def orphaned_workflows
        Sipity::Workflow.where(permission_template: orphaned_templates).to_a
      end

      before do
        expect(orphaned_templates).not_to be_empty # just to be sure
        expect(orphaned_workflows).not_to be_empty # just to be sure

        expect(Hyrax.query_service.find_many_by_ids(ids: deleted_source_ids)).to be_empty # just to be sure
      end

      it "deletes the invalid templates and workflows" do
        PermissionTemplateCleaner.clean!

        expect(orphaned_templates).to be_empty
        expect(orphaned_workflows).to be_empty
      end
    end

    RSpec.describe PermissionTemplateCleaner do
      context "with only valid templates" do
        it_behaves_like "retaining valid templates"
      end

      context "with only invalid templates" do
        it_behaves_like "deleting invalid templates"
      end

      context "with both valid and invalid templates" do
        it_behaves_like "retaining valid templates"
        it_behaves_like "deleting invalid templates"
      end
    end
  end
end
