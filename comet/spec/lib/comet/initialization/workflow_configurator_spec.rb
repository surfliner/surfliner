# frozen_string_literal: true

require "rails_helper"
require "comet/initialization/workflow_configurator"

module Comet
  module Initialization
    RSpec.shared_examples "workflows_subpath is valid" do
      it "sets path_to_workflow_files" do
        orig_path = Rails.root.join("config/workflows/*.json")
        allow(Hyrax::Workflow::WorkflowImporter).to receive(:path_to_workflow_files).and_return(orig_path)

        expected_path = Rails.root.join("config/workflows/#{workflows_subpath}/*.json")
        expect(Hyrax::Workflow::WorkflowImporter).to receive(:path_to_workflow_files=).with(expected_path)
        configurator.configure!
      end
    end

    RSpec.shared_examples "default_active_workflow_name is valid" do
      it "sets default_active_workflow_name" do
        expect(Hyrax.config).to receive(:default_active_workflow_name=).with(default_active_workflow_name)
        configurator.configure!
      end
    end

    RSpec.shared_examples "workflows_subpath is not set" do
      it "does not change path_to_workflow_files" do
        expect(Hyrax::Workflow::WorkflowImporter).not_to receive(:path_to_workflow_files=)
        configurator.configure!
      end
    end

    RSpec.shared_examples "default_active_workflow_name is not set" do
      it "does not change default_active_workflow_name" do
        expect(Hyrax.config).not_to receive(:default_active_workflow_name=)
        configurator.configure!
      end
    end

    RSpec.describe WorkflowConfigurator do
      before do
        allow(Hyrax::Workflow::WorkflowImporter).to receive(:path_to_workflow_files=)
        allow(Hyrax.config).to receive(:default_active_workflow_name=)

        allow(ENV).to receive(:[]).and_call_original
      end

      describe "#new" do
        context "with invalid workflows_subpath" do
          it "raises ArgumentError" do
            workflows_subpath = "no_such_subpath"
            expect do
              WorkflowConfigurator.new(workflows_subpath: workflows_subpath, default_active_workflow_name: nil)
            end.to raise_error(ArgumentError)
          end
        end

        context "with invalid default_active_workflow_name" do
          it "raises ArgumentError" do
            default_active_workflow_name = "no such workflow"
            expect do
              WorkflowConfigurator.new(workflows_subpath: nil, default_active_workflow_name: default_active_workflow_name)
            end.to raise_error(ArgumentError)
          end
        end
      end

      describe "#configure!" do
        context "with valid values" do
          let(:workflows_subpath) { "ucsb" }
          let(:default_active_workflow_name) { "surfliner_default" }

          let(:configurator) do
            WorkflowConfigurator.new(workflows_subpath: workflows_subpath,
              default_active_workflow_name: default_active_workflow_name)
          end

          it_behaves_like "workflows_subpath is valid"
          it_behaves_like "default_active_workflow_name is valid"
        end

        context "with nil workflows_subpath" do
          let(:default_active_workflow_name) { "surfliner_default" }
          let(:configurator) do
            WorkflowConfigurator.new(workflows_subpath: nil, default_active_workflow_name: default_active_workflow_name)
          end

          it_behaves_like "workflows_subpath is not set"
          it_behaves_like "default_active_workflow_name is valid"
        end

        context "with nil default_active_workflow_name" do
          let(:workflows_subpath) { "ucsb" }

          let(:configurator) do
            # subpath won't work with the 'default default'
            allow(Hyrax.config).to receive(:default_active_workflow_name).and_return("surfliner_default")
            WorkflowConfigurator.new(workflows_subpath: workflows_subpath, default_active_workflow_name: nil)
          end

          it_behaves_like "workflows_subpath is valid"
          it_behaves_like "default_active_workflow_name is not set"
        end

        context "with nil values" do
          let(:configurator) do
            WorkflowConfigurator.new(workflows_subpath: nil, default_active_workflow_name: nil)
          end

          it_behaves_like "workflows_subpath is not set"
          it_behaves_like "default_active_workflow_name is not set"
        end

        context "from environment" do
          let(:workflows_subpath) { "ucsb" }
          let(:default_active_workflow_name) { "surfliner_default" }

          # in this case we're calling the class method
          let(:configurator) { WorkflowConfigurator }

          context "with valid values" do
            before do
              {
                "COMET_WORKFLOWS_SUBPATH" => workflows_subpath,
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => default_active_workflow_name
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is valid"
            it_behaves_like "default_active_workflow_name is valid"
          end

          context "with nil workflows subpath" do
            before do
              {
                "COMET_WORKFLOWS_SUBPATH" => nil,
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => default_active_workflow_name
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is not set"
            it_behaves_like "default_active_workflow_name is valid"
          end

          context "with empty workflows subpath" do
            before do
              {
                "COMET_WORKFLOWS_SUBPATH" => "",
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => default_active_workflow_name
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is not set"
            it_behaves_like "default_active_workflow_name is valid"
          end

          context "with nil default_active_workflow_name" do
            before do
              # subpath won't work with the 'default default'
              allow(Hyrax.config).to receive(:default_active_workflow_name).and_return("surfliner_default")

              {
                "COMET_WORKFLOWS_SUBPATH" => workflows_subpath,
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => nil
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is valid"
            it_behaves_like "default_active_workflow_name is not set"
          end

          context "with empty default_active_workflow_name" do
            before do
              # subpath won't work with the 'default default'
              allow(Hyrax.config).to receive(:default_active_workflow_name).and_return("surfliner_default")

              {
                "COMET_WORKFLOWS_SUBPATH" => workflows_subpath,
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => ""
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is valid"
            it_behaves_like "default_active_workflow_name is not set"
          end

          context "with nil values" do
            before do
              {
                "COMET_WORKFLOWS_SUBPATH" => nil,
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => nil
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is not set"
            it_behaves_like "default_active_workflow_name is not set"
          end

          context "with empty values" do
            before do
              {
                "COMET_WORKFLOWS_SUBPATH" => "",
                "COMET_DEFAULT_ACTIVE_WORKFLOW_NAME" => ""
              }.each do |k, v|
                allow(ENV).to receive(:[]).with(k).and_return(v)
              end
            end

            it_behaves_like "workflows_subpath is not set"
            it_behaves_like "default_active_workflow_name is not set"
          end
        end
      end
    end
  end
end
