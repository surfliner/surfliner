# Comet

Comet is a staff-facing digital object management platform based on [Hyrax](https://hyrax.samvera.org/about/), a front-end application that provides a user interface for common repository features. Hyrax is based on the [Samvera](https://samvera.org/samvera-open-source-repository-framework/) open-source framework.

The scope of the Comet platform includes creating, adding, updating, editing, aggregating, and arranging digital collections managed by the library. The user interface, or admin dashboard, supports collaboration between users with an approval process through robust, configurable workflow controls.

## Developing

Run `make build && make up` or, from the root directory of the repository,
```
docker compose --profile comet build
docker compose --profile comet up
```

The application will be available at <http://localhost:3000>.

Please see [CONTRIBUTING.md][contributing] for information about contributing to
this project, and [the manual](../docs/themanual) for technical documentation.
By participating, you agree to abide by the [UC Principles of
Community][principles].

[contributing]: ../CONTRIBUTING.md
[principles]: https://ucnet.universityofcalifornia.edu/working-at-uc/our-values/principles-of-community.html
