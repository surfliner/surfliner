# frozen_string_literal: true

# This migration comes from valkyrie_engine (originally 20161007101725), but
# modified to use bigint id’s (the default in 5.1 and later).
class CreateOrmResources < ActiveRecord::Migration[5.1]
  def change
    create_table :orm_resources do |t|
      t.jsonb :metadata, null: false, default: {}
      t.timestamps
    end
    add_index :orm_resources, :metadata, using: :gin
  end
end
