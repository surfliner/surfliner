# This migration added by comet for option to exclude empty fields from export
class AddExcludeEmptyFieldsToBulkraxExporters < ActiveRecord::Migration[5.1]
  def change
    add_column :bulkrax_exporters, :exclude_empty_fields, :boolean, default: false unless column_exists?(:bulkrax_exporters, :exclude_empty_fields)
  end
end
