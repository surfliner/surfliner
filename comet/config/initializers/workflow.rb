require "comet/initialization/workflow_configurator"
require "comet/initialization/permission_template_cleaner"

Comet::Initialization::WorkflowConfigurator.configure!
Comet::Initialization::PermissionTemplateCleaner.clean!
