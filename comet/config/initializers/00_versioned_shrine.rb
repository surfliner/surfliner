module Valkyrie
  module Storage
    class VersionedShrine < Shrine
      # @param feature [Symbol] Feature to test for.
      # @return [Boolean] true if the adapter supports the given feature
      def supports?(feature)
        return true if feature == :versions || feature == :version_deletion
        false
      end

      # TODO: retireve all file versions from Minio/S3
      # @param id [Valkyrie::ID]
      # @return [Array(Valkyrie::StorageAdapter::StreamFile)]
      # @raise Valkyrie::StorageAdapter::FileNotFound if nothing is found
      def find_versions(id:)
        Array(find_by(id: id))
      end

      # Upload a new version file
      # @param id [Valkyrie::ID] ID of the Valkyrie::StorageAdapter::File to version.
      # @param file [IO]
      def upload_version(id:, file:, **upload_options)
        # S3 adapter validates options, so we have to remove this one used in
        # the shared specs.
        upload_options.delete(:fake_upload_argument)
        identifier = shrine_id_for(id)
        shrine.upload(file, identifier, **upload_options)
        find_by(id: "#{protocol_with_prefix}#{identifier}").tap do |result|
          if verifier
            raise Valkyrie::Shrine::IntegrityError unless verifier.verify_checksum(file, result)
          end
        end
      end
    end
  end
end
