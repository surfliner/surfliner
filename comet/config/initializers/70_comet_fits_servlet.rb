# frozen_string_literal: true

require "hydra/file_characterization/exceptions"
require "hydra/file_characterization/characterizer"
require "logger"
module Hydra::FileCharacterization::Characterizers
  class CometFitsServlet < Hydra::FileCharacterization::Characterizer
    protected

    def command
      "curl -s --get -k --data-binary file=#{filename} #{ENV["FITS_SERVLET_URL"]}/examine"
    end
  end
end
