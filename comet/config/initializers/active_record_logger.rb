# frozen_string_literal: true

# Ignore logging message with CACHE from ActiveRecord
class ActiveRecordCacheFreeLogger < ::Logger
  def debug(message, *args, &block)
    super unless message&.include?("CACHE")
  end
end

# @overwrite ActiveRecord::Base logge
if ActiveModel::Type::Boolean.new.cast(ENV.fetch("RAILS_LOG_TO_STDOUT", true))
  ActiveRecord::Base.logger = ActiveSupport::TaggedLogging.new(
    ActiveRecordCacheFreeLogger.new($stdout)
  )
end
