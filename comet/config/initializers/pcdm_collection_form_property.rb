ActiveSupport::Reloader.to_prepare do
  Hyrax::Forms::PcdmCollectionForm.class_eval do
    property :alternate_ids, default: [], type: Valkyrie::Types::Array
  end
end
