require "cgi"
require "comet"

Rails.application.config.to_prepare do
  # At time of writing, Hyrax provides a minimal compatibility +::Collection+
  # which will likely be autoloaded by the Rails autoloader. We’d rather this
  # didn’t happen, so, attempt to load it, and then remove the loaded const
  # before proceeding.
  #
  # When reloading, temporarily set +Hyrax.config.collection_model+ to something
  # other than +::Collection+ so that it doesn’t create an endless loop trying
  # to constantize.
  Hyrax.config.collection_model = "::PcdmCollection"
  "::Collection".safe_constantize
  Object.send(:remove_const, :Collection) if Object.const_defined?(:Collection)
  Hyrax.config.collection_model = Valkyrie.config.resource_class_resolver.call("http://pcdm.org/models#Collection").to_s

  # Set +Bulkrax::Collection+ to be +Hyrax.config.collection_class+.
  #
  # This is a hack to take advantage of Ruby const resolution rules to
  # overrule the many times +Collection+ appears unspecified in Bulkrax code.
  Bulkrax.send(:remove_const, :Collection) if Bulkrax.const_defined?(:Collection, false)
  Bulkrax.const_set(:Collection, Hyrax.config.collection_class)

  # Redefine the necessary controllers for each M3 resource class name. This
  # will undefine any existing definitions; the hope is that this will help to
  # protect against old versions of controllers hanging around when reloading in
  # the development environment.
  #
  # This code also (necessarily) ensures toplevel consts are defined for all of
  # the classes defined by the schema (which we want).
  ::SchemaLoader.new.availabilities.filter { |class_name|
    !%i[file_set file_metadata].include?(class_name) # remove “special” keys
  }.each do |class_name|
    model_name = class_name.to_s.camelize.to_sym
    model_class = Valkyrie.config.resource_class_resolver.call(model_name)
    next unless model_class < ::PcdmObject # only define controllers for pcdm objects
    controller_name = :"#{model_name.to_s.pluralize}Controller"
    Hyrax.remove_const(controller_name) if Hyrax.const_defined?(controller_name)
    controller_class = Class.new(Hyrax::ResourcesController) do
      self.curation_concern_type = model_class
    end
    Hyrax.const_set(controller_name, controller_class)
  end

  # These configurations point to actual class objects and so should be reset
  # every autoload.
  Hyrax.config do |config|
    # Form overrides…
    config.pcdm_collection_form = ::Forms::PcdmCollectionForm
    "Forms::PcdmObjectForm".constantize # autoload
    config.pcdm_object_form_builder = ->(model) { ::Forms::PcdmObjectForm(model) }

    # Indexer overrides…
    config.pcdm_collection_indexer = ::Indexers::PcdmCollectionIndexer
    "Indexers::PcdmObjectIndexer".constantize # autoload
    config.pcdm_object_indexer_builder = ->(model) { ::Indexers::PcdmObjectIndexer(model) }
  end
end

Hyrax.config do |config|
  ::SchemaLoader.new.availabilities.filter { |class_name|
    !%i[file_set file_metadata].include?(class_name) # remove “special” keys
  }.each do |class_name|
    # Register curation concerns for each PCDM object defined in the schema,
    # providing the Hyrax routing
    model_name = class_name.to_s.camelize.to_sym
    model_class = Valkyrie.config.resource_class_resolver.call(model_name)
    next unless model_class < ::PcdmObject
    config.register_curation_concern(class_name)
  end

  # Disable Wings!
  config.disable_wings = true
  config.disable_freyja = true if config.respond_to?(:disable_freyja)
  config.disable_frigg = true if config.respond_to?(:disable_frigg)
  config.valkyrie_transition = false if config.respond_to?(:valkyrie_transition)

  # Configure models to use for certain Hyrax concepts.
  config.collection_model = Valkyrie.config.resource_class_resolver.call("http://pcdm.org/models#Collection").to_s
  config.admin_set_model = "Hyrax::AdministrativeSet"

  # Disable Hyrax NOIDs. This is really just a config switch, since we don't use
  # models that auto-mint NOIDs.
  config.enable_noids = false

  # Allow works to be created without files.
  config.work_requires_files = false

  # Enable & configure the IIIF image service.
  config.iiif_image_server = true
  config.iiif_image_url_builder = lambda do |file_id, _base_url, size, format|
    fs = Hyrax.query_service.find_by(id: file_id)
    fid = CGI.escape(fs.file_ids.first.to_s)
    "#{ENV["IIIF_BASE_URL"]}/iiif/2/comet:#{fid}/full/#{size}/0/default.#{format}"
  end
  config.iiif_info_url_builder = lambda do |file_id, _base_url|
    fs = Hyrax.query_service.find_by(id: file_id)
    fid = CGI.escape(fs.file_ids.first.to_s)
    "#{ENV["IIIF_BASE_URL"]}/iiif/2/comet:#{fid}"
  end

  # use comet's valkyrie index for search
  config.query_index_from_valkyrie = false
  config.index_adapter = :comet_index

  # disable browse_everything. we may want to re-add this later,
  # or consider other mechanisms for uploading external cloud content.
  config.browse_everything = nil

  # use comet's characterization service
  config.characterization_service = Comet::CharacterizationService
  config.characterization_options = {ch12n_tool: ENV.fetch("CH12N_TOOL", "fits_servlet").to_sym}

  ##
  # Set the system-wide virus scanner
  config.virus_scanner = Hyrax::VirusScanner

  ##
  # A mapping from visibility string values to permissions
  config.visibility_map = ::VisibilityMap.instance

  # in dev we use a single redis for comet and starilght
  config.redis_namespace = "hyrax" unless Rails.env.production?
end

Date::DATE_FORMATS[:standard] = "%m/%d/%Y"

Qa::Authorities::Local.register_subauthority("subjects", "Qa::Authorities::Local::TableBasedAuthority")
Qa::Authorities::Local.register_subauthority("languages", "Qa::Authorities::Local::TableBasedAuthority")
Qa::Authorities::Local.register_subauthority("genres", "Qa::Authorities::Local::TableBasedAuthority")

[Hyrax::CustomQueries::Navigators::CollectionMembers,
  Hyrax::CustomQueries::Navigators::ChildFileSetsNavigator,
  Hyrax::CustomQueries::Navigators::ChildWorksNavigator,
  Hyrax::CustomQueries::Navigators::ParentWorkNavigator,
  Hyrax::CustomQueries::FindAccessControl,
  Hyrax::CustomQueries::FindCollectionsByType,
  Hyrax::CustomQueries::FindManyByAlternateIds,
  Hyrax::CustomQueries::FindIdsByModel,
  Hyrax::CustomQueries::FindFileMetadata,
  Hyrax::CustomQueries::FindByDateRange,
  CustomQueries::FindFileMetadata].each do |handler|
  Hyrax.query_service.custom_queries.register_query_handler(handler)
end

Hyrax::Resource.class_eval do
  def self._hyrax_default_name_class
    Hyrax::Name
  end
end

class CometTransactionContainer
  extend Dry::Container::Mixin

  namespace "file_set" do |ops|
    ops.register "upload_files" do
      Transactions::Steps::AddBulkraxFiles.new
    end

    ops.register "create" do
      Transactions::CreateFileSet.new
    end
  end

  namespace "work_resource" do |ops|
    ops.register "create_with_bulk_behavior" do
      steps = Hyrax::Transactions::WorkCreate::DEFAULT_STEPS.dup
      steps[steps.index("work_resource.add_file_sets")] = "work_resource.add_bulkrax_files"

      Hyrax::Transactions::WorkCreate.new(steps: steps)
    end

    ops.register "update_with_bulk_behavior" do
      steps = Hyrax::Transactions::WorkUpdate::DEFAULT_STEPS.dup
      steps[steps.index("work_resource.add_file_sets")] = "work_resource.add_bulkrax_files"
      steps.prepend("work_resource.destroy_file_sets")

      Hyrax::Transactions::WorkUpdate.new(steps: steps)
    end

    ops.register "destroy_with_recursive_delete_orphans" do
      steps = Hyrax::Transactions::WorkDestroy::DEFAULT_STEPS.dup
      steps.prepend("work_resource.delete_orphan_member_objects")

      Hyrax::Transactions::WorkDestroy.new(steps: steps)
    end

    ops.register "delete_orphan_member_objects" do
      Transactions::Steps::DeleteOrphanMemberObjects.new
    end

    ops.register "add_file_sets" do
      Hyrax::Transactions::Steps::AddFileSets.new(handler: InlineUploadHandler)
    end

    ops.register "add_bulkrax_files" do
      Transactions::Steps::AddBulkraxFiles.new
    end

    ops.register "destroy_file_sets" do
      Transactions::Steps::DestroyFileSets.new
    end
  end

  namespace "change_set" do |ops|
    ops.register "save" do
      Comet::Transactions::Steps::Save.new
    end
  end
end

Hyrax::Transactions::Container.merge(CometTransactionContainer)

Hyrax::Engine.routes.prepend do
  get "/collections/:id", to: "dashboard/collections#show", as: :collection
end

Qa::Authorities::FindWorks.search_builder_class = CometFindWorksSearchBuilder

Hyrax::Publisher.include(Comet::PublisherOtelBehavior)
Dry::Events::Bus.prepend(Comet::PublisherBusOtelBehavior)
