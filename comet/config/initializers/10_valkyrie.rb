# frozen_string_literal: true

require "shrine/storage/s3"
require "valkyrie/storage/shrine"
require "valkyrie/shrine/checksum/s3"

# skip much of this setup if we're just building the app image
building = (ENV["DB_ADAPTER"] == "nulldb")

if building
  Valkyrie::MetadataAdapter.register(
    Valkyrie::Persistence::Memory::MetadataAdapter.new,
    :comet_metadata_store
  )

  Valkyrie::MetadataAdapter.register(
    Valkyrie::Persistence::Memory::MetadataAdapter.new,
    :app_data
  )

  Valkyrie::StorageAdapter.register(
    Valkyrie::Storage::Memory.new,
    :repository_s3
  )
else
  database = ENV.fetch("METADATA_DATABASE_NAME", "comet_metadata")
  Rails.logger.info "Establishing connection to postgresql on: " \
                    "#{ENV["POSTGRESQL_HOST"]}:#{ENV["POSTGRESQL_PORT"]}.\n" \
                    "Using database: #{database}."
  connection = Sequel.connect(
    user: ENV["DB_USERNAME"],
    password: ENV["DB_PASSWORD"],
    host: ENV["POSTGRESQL_HOST"],
    port: ENV["POSTGRESQL_PORT"],
    database: database,
    max_connections: ENV.fetch("METADATA_DATABASE_POOL", 5),
    pool_timeout: ENV.fetch("METADATA_DATABASE_TIMEOUT", 5),
    adapter: :postgres
  )

  Valkyrie::MetadataAdapter.register(
    Valkyrie::Sequel::MetadataAdapter.new(connection: connection),
    :comet_metadata_store
  )

  Valkyrie::MetadataAdapter.register(
    Valkyrie::Persistence::Postgres::MetadataAdapter.new,
    :app_data
  )

  # These values can come from a variety of ENV vars
  shrine_s3_options = {
    access_key_id: S3Configurations::Default.access_key,
    bucket: S3Configurations::Default.bucket,
    endpoint: S3Configurations::Default.endpoint,
    force_path_style: S3Configurations::Default.force_path_style,
    region: S3Configurations::Default.region,
    secret_access_key: S3Configurations::Default.secret_key
  }

  Valkyrie::StorageAdapter.register(
    Valkyrie::Storage::VersionedShrine.new(Shrine::Storage::S3.new(**shrine_s3_options)),
    :repository_s3
  )
end

Valkyrie.config.metadata_adapter = :comet_metadata_store
Valkyrie.config.resource_class_resolver = ::SchemaLoader.new.resource_class_resolver
Valkyrie.config.storage_adapter = :repository_s3
Valkyrie::IndexingAdapter.register(CometIndexingAdapter.new, :comet_index)

Hyrax.query_service.instance_variable_set(:@id_type, :bigint)

[Hyrax::CustomQueries::FindCountBy, Hyrax::CustomQueries::FindModelsByAccess].each do |query_handler|
  Valkyrie.config.metadata_adapter.query_service.custom_queries.register_query_handler(query_handler)
end

Valkyrie::MetadataAdapter.find(:app_data)
  .query_service
  .custom_queries
  .register_query_handler(CustomQueries::AppData::FindPendingRelationship)

Valkyrie::MetadataAdapter.find(:app_data)
  .query_service
  .custom_queries
  .register_query_handler(CustomQueries::AppData::DeleteBinaries)

# rubocop:ignore Lint/Void
Hyrax::ValkyrieCanCanAdapter # just load this to make sure cancancan finds it
