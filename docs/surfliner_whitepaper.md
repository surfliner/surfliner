Project Surfliner Architecture
==============================

This document is written in [`markdown`][markdown] format, and other formats can
be generated using [`pandoc`][pandoc].

You can generate PDFs for the documents with: `./bin/generate_pdfs.sh`.

## What is Surfliner

## Surfliner Architectural Guidelines

## Products

### Comet

#### Content Lifecycle

TK: tj

#### Content Identity

TK: mc

#### Content Structure

TK: ad

#### Metadata Control

TK: ad

#### Access Controls

TK: mc

#### Import & Export

TK: tj

#### Administrative Search & Browse

TK: tj

#### Analytics

TK: mc

### Lark

### Orange Empire

_Orange Empire_ is a `helm` chart, basically. Don't worry about it. Forget it, Jack; it's Kubernetes (`k8s`).

### Shoreline

### Starlight

[markdown]: https://daringfireball.net/projects/markdown/
[pandoc]: https://pandoc.org/
