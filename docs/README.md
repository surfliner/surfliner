# Surfliner Documentation

This directory contains version controlled documents for Project Surfliner. This
is the home for documents that are reasonably long-lived.

## Getting started

- How do I set up a local development environment?
    - The sort version: from a project subdirectory, `make build && make up`
    - [The long version](./themanual/dev/README.md)
- How do I deploy an application in production?
    - [The long version](./themanual/helm/README.md)
- How do I download geospatial fixture data?
    - The short version: `rclone --config ./rclone.conf copy ro:surfliner-fixtures/ minio:comet-staging/`
    - [The long version](./themanual/comet/bulkrax.md#seeding-files-for-csv-fixtures)
- How do all these pieces fit together?
    - The short version: <https://architecture.eks.dld.library.ucsb.edu/>
    - [The long version](./themanual/README.md)
- How do I published a new Helm chart?
    - [The long version](./themanual/README.md#publishing-charts)
