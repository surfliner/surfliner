#!/bin/sh

diagram="${1}"

base="$(basename "${diagram}" .d2)"

d2 "${diagram}" "${base}.svg"

echo "-- Generating ${base}.html"
./embed.sh "${base}.svg" > "${base}.html"
