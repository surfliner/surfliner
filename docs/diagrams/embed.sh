#!/bin/sh

input="${1}"

cat <<EOF
<!doctype html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>
      ${input}
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
$(sed 's|^<?xml[^>]*>||' < "${input}")
  </body>
</html>
EOF
