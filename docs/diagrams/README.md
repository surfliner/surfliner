# Surfliner architectural diagrams

Surfliner is a large project with many interlocking systems.  Comprehensive
visual documentation helps developers, SMEs, and stakeholders understand each system
and its relationships to the rest.  And since the systems evolve and new ones
are added over time, it’s necessary that this documentation be easily edited by
the developers making those changes (and, ideally, SMEs as well).

For that reason, diagrams are written in [D2](https://d2lang.com/), which is a
plain-text format that is then rendered to various image formats.  This means
that making changes to diagrams does not require any expertise in the art of
diagram construction.  (The trade-off, of course, is that these diagrams will
not be as elegant or thoughtfully designed as ones created by hand.)

Changes are made to the `.d2` files in this directory; the generated diagrams
are produced by the [`d2`](https://d2lang.com/tour/install) binary, which can be
run directly or via our Makefile:

```
make all
```

This will produce PNG and SVG versions of the diagrams, which can be embedded in
Markdown documentation.  It will also produce an HTML version with the SVG
embedded, which can be served as a static page.  By linking between diagrams, we
can create an interactive overview of the Surfliner project:
<https://architecture.eks.dld.library.ucsb.edu/>
