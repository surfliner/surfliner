## Authentication in Comet and Starlight

The environment variable `AUTH_METHOD` determines how users log in.  The options
are:

- `google`: Authenticates via Google OAuth
- `developer`: Authenticates via YAML file (for local testing only)
- `database`: Authenticates via standard Rails/Devise db-backed Users

### Authentication with Google

Users authenticate using Google.  To configure CometStarlight to connect to
Google using OAuth:

1. Create a new application at <https://console.developers.google.com/>.
1. Complete the OAuth consent form under the Credentials tab.
1. In the Credentials tab, select `Create credentials -> OAuth client ID`.  The
   type should be “Web application” and the Authoritized redirect URI should be
   your application’s hostname followed by `/users/auth/google_oauth2/callback`.
1. Set the environment variables `GOOGLE_AUTH_ID` and `GOOGLE_AUTH_SECRET` to
   the provided OAuth client ID and client secret, respectively.
