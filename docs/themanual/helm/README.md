# Deploying applications with Helm charts

Deploying Surfliner applications into a production environment requires some
familiarity with [Helm](https://helm.sh/docs/intro/quickstart/).

- [Chart guidelines and conventions](#chart-guidelines-and-conventions)
    - [Secrets](#secrets)
- [Publishing charts](#publishing-charts)
    - [Adding a new chart to CI](#adding-a-new-chart-to-ci)
- [Deploying Comet](#deploying-comet)
- [Helm tips and tricks](#helm-tips-and-tricks)
    - [Completion](#completion)
    - [Always update the chart dependencies](#always-update-the-chart-dependencies)
    - [Modifying an existing deployment](#modifying-an-existing-deployment)
        - [One-off deploy of a branch](#one-off-deploy-of-a-branch)
        - [Check values against local chart changes](#check-values-against-local-chart-changes)
    - [Resources](#resources)

## Chart guidelines and conventions

### Secrets

Secrets (i.e., sensitive data like passwords that cannot be committed to the
repository history) should be placed in pre-generated
[`Secret`](https://kubernetes.io/docs/concepts/configuration/secret/) objects,
either manually or with something like [Sealed
Secrets](https://github.com/bitnami-labs/sealed-secrets).  These `Secret`
objects can then be [referenced by the
charts](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/#define-container-environment-variables-using-secret-data).

- Use a single `Secret` for multiple pieces of data when possible (e.g.,
  passwords that are only used by a single component).  For things that are used
  by multiple components, like database passwords, create a dedicated `Secret`
  for them, and have each component reference that `Secret` instead of
  duplicated the data across multiple `Secret`s.

- `Secret` objects specify their data in key-value for.  Always have the key be
  the environment variable that you wish to populate with the accompanying
  value.  (Some charts follow [different
  conventions](https://github.com/bitnami/charts/tree/main/bitnami/solr#solr-parameters)
  – in such cases, when the data in question is also used by a local
  application, override the `existingSecretPasswordKey` (or similar) of the
  upstream chart to use the local environment variable as the key.)

## Publishing charts

We publish our charts to the Gitlab container registry, so that institutions can
deploy a specific version, instead of running whatever is on `trunk`.  We use a
CI job to [automatically publish
charts](https://gitlab.com/surfliner/surfliner/-/blob/8c01ba13f437978afc82cccf51e1d1863536b7da/.gitlab-ci.yml#L270-291)
when their version is incremented.

### Adding a new chart to CI

Adding a new chart to the CI build pipeline requires publishing it manually
once, since that will create the container repository that we need to reference
for automatic builds.  To do this for e.g. Comet:

```shell
cd charts/comet
helm dependency build .
helm package .
helm push hyrax-1.0.1.tgz oci://registry.gitlab.com/surfliner/surfliner/charts
```

This will create the repository; we can now query the `repository_id` that the
CI job needs (to check what chart versions have already been published):

```
curl --header "PRIVATE-TOKEN: <redacted>" \
     "https://gitlab.com/api/v4/projects/surfliner%2Fsurfliner/registry/repositories?per_page=100" | jq
```

Take the `id` value and create a new CI job:

```yaml
comet:build:chart:
  extends:
    - .helm-publish-chart
  stage: build
  needs: []
  variables:
    CHART_DIR: "charts/comet"
    REPOSITORY_ID: "8211500"
  rules:
    - !reference [.comet-chart-default-branch-rules, rules]
```

## Deploying Comet

See the [Comet chart README](../../../charts/comet/README.md).

## Helm tips and tricks

### Completion

Most package managers _should_ install the [Helm completions][helm-completion] automatically when installed the `helm`
package. However, if for some reason your package manager doesn't, the documentation provides instructions for installing the completion
file(s) for your shell of choice.

This let's you use tab-completion when typing out `helm` commands in the shell, which can save a lot of time and errors.

### Always update the chart dependencies

If you are working on an existing chart, prior to starting development it is always a good idea to ensure you're working
with the latest versions of the chart dependencies locally. Not taking this step can cause you to not notice changes,
and potentially bugs, until a `review` application is built in CI.

For this we use the [`helm dependency update`][helm-dep-up] command.

Taking the `shoreline` chart for example:

```sh
$ cd surfliner/charts/shoreline
$ helm dependency update . # or shorthand -- helm dep up .
```

### Modifying an existing deployment

When modifying a chart configuration, it can be helpful to do some testing using real values from existing deployments,
such as `staging` or `production`.

Helm provides a helpful command, [`helm get values`][helm-get-values] to quickly gather all the values of an _existing_ deployment so that those values
may be used locally for testing and development.

Using this in a surfliner workflow might look like the following:

First save the values of the current `shoreline` `staging` deployment to a file `/tmp/shoreline-staging.yaml`

```sh
$ helm get values --namespace shoreline-staging surfliner-shoreline-stage > /tmp/shoreline-staging.yaml
```

Now that you have values stored. Here are some ways you can use it.

#### One-off deploy of a branch

Sometimes it's helpful to try out a new version of the application deployed in an environment, like `staging`, with real
production data.

General steps:
- Locate and copy the image tag from the `build` job in the branch pipeline
- Replace the `image.tag` value in the file downloaded via `helm get values`, in our case `/tmp/shoreline-staging.yaml`
- Run a helm deployment referencing your modified `/tmp/shoreline-staging.yaml` values file

```sh
# edit image.tag in your editor
$ cd surfliner/charts/shoreline
$ helm upgrade --install --debug --atomic --values /tmp/shoreline-staging.yaml --namespace shoreline-staging surfliner-shoreline-stage ./
```

#### Check values against local chart changes

If you are editing a Helm chart locally, say adding a new template or modifying a template, it can be very helpful to
validate what the generated template(s) and values will look like when your changes are applied. For this you can
leverage the `--dry-run` flag. This will render the templates and values using your local chart changes and you can use
the `--values` flag to reference your saved `/tmp/shoreline-staging.yaml` values.

```sh
$ cd surfliner/charts/shoreline
$ helm upgrade --install --dry-run --debug --values /tmp/shoreline-staging.yaml --namespace shoreline-staging surfliner-shoreline-stage ./
```

See [`helm.sh Debugging Templates`][helm-debugging] for more debugging ideas.

### Resources

[helm.sh Tips and Tricks](https://helm.sh/docs/howto/charts_tips_and_tricks/)

[helm-completion]:https://helm.sh/docs/helm/helm_completion/#helm
[helm-debugging]:https://helm.sh/docs/chart_template_guide/debugging/#helm
[helm-dep-up]:https://helm.sh/docs/helm/helm_dependency_update/#helm
[helm-get-values]:https://helm.sh/docs/helm/helm_get_values/#helm
