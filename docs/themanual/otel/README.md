# Open Telemetry

- [Honeycomb in production environments](#honeycomb-in-production-environments)
- [Jaeger in development environments](#jaeger-in-development-environments)

## Honeycomb in production environments

In Helm deployments to review, staging and production environments in Kubernetes, we configure the open telemetry(OTEL)
instrumentation to send information to an [OTEL Collector][otel-collector] running in the relevant Kubernetes cluster.
This OTEL Collector is meant to be configured to send data along to [Honeycomb][honeycomb]. However, in practice, this
could be configured to send data along to [jaeger][jaeger] or another OTEL UI of choice.

In a Helm values snippet, this URL needs to be set as follows.

```yaml
telemetry:
  enabled: true
  otlp_endpoint: "http://<collector-release-name>.<kubernetes-namespace>:<service-port>"
```

Example for UCSD's review environment/cluster:

```yaml
telemetry:
  enabled: true
  otlp_endpoint: "http://otel-collector-review-opentelemetry-collector.opentelemetry-review:4318"
```

## Jaeger in development environments

For local development, we include a [jaeger container image][jaeger] which can serve as both the [OTEL
Collector][otel-collector] as well as the UI for browsing the OTEL data sent by the application(s) in docker-compose.

The OTEL endpoint in docker-compose will typically be: `OTEL_EXPORTER_OTLP_ENDPOINT=http://jaeger:4318`

The Jaeger UI can be accessed at the following URL: http://localhost:16686

[honeycomb]:https://docs.honeycomb.io/send-data/opentelemetry/
[jaeger]:https://www.jaegertracing.io/
[otel-collector]:https://opentelemetry.io/docs/collector/

