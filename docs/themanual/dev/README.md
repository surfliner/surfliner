# Developing locally with Docker Compose

Local development uses [docker compose](https://docs.docker.com/compose/).
Because Surfliner consists of multiple applications, which interact with each
other and often share dependencies, we maintain [a shared set of configuration
files](../../../docker-compose) and use
[profiles](https://docs.docker.com/compose/profiles/) to allow developers to run
one or more applications at a time.

For example, to bring up a local Comet and Shoreline instance, in order to test
the ingest of geospatial materials, run the following commands from the root of
the monorepo:

```sh
docker compose --profile comet --profile shoreline build
docker compose --profile comet --profile shoreline up
```

For running a single application, we use Makefiles to provide shortcuts.  From
the subdirectory of a given project (for example, `comet`), you can instead run:

```sh
make build
make up # this will start the services in the background
```

See the [root Makefile](../../../GNUmakefile) for the full set of commands.

## Modular compose configurations

Because the Surfliner project contains so many different services, the docker
compose configurations have been split into pieces, which are managed in
[`/docker-compose`](../../../docker-compose).

**If you make changes to a docker compose service definition, be sure that it
works for each project that relies on it.**

## Environment variables

Wherever possible, applications are configured using environment variables.
Variables for local development are managed by files in
[`/docker-compose/env`](../../../docker-compose/env) and loaded by docker
compose at run time.  When adding a new variable, please add a comment
describing its purpose, if it isn’t obvious.

Docker Compose documentation recommends naming these variable files `.env`, but
this should be discouraged for several reasons:

- it requires all variables be combined into a single large file instead of
  being organized by purpose.
- it conflates environment variables, [build
  arguments](https://docs.docker.com/build/guide/build-args/) and [template
  variables](https://docs.docker.com/compose/compose-file/12-interpolation/).
  Variables set in a file called `.env` are not only injected into the container
  at runtime, but are also used to set variables defined in the `Dockerfile`
  that are used at buildtime, and are _also_ used to replace any `$`-style
  variables in the `docker-compose.yaml` itself.  To avoid the resulting
  confusion, we do not use `.env`.
