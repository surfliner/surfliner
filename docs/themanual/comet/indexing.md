# Indexing and Re-indexing

## reindexing on merge

By adding a [trailer](https://git-scm.com/docs/git-interpret-trailers) message
of `ci: reindex` to a commit, Comet will trigger a reindex.
