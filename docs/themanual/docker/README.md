# docker/container guidelines and conventions

- [we use pre-existing images when we can](#we-use-pre-existing-images-when-we-can)
- [we follow these rules when we write our own images](#we-follow-these-rules-when-we-write-our-own-images)
- [we use shared base images for consistency and to reduce duplication](#we-use-shared-base-images-for-consistency-and-to-reduce-duplication)

Because most of the applications we develop involve multiple interlocking
services, and because our team works in a variety of environments, we use
[containers][container] for both development and production.

Instead of asking developers to configure and run all the various services
(Rails, PostgreSQL, Solr, etc.) directly on their machine, we use use [docker
compose][docker-compose] to run container images of each service in a
coordinated way (this is often referred to as “container orchestration”).

In production, we use [Helm charts][helm] to orchestrate the deployment of
containers in a [Kubernetes][k8s] cluster.

## we use pre-existing images when we can

Maintaining Docker images for containers requires ongoing work, so when there is
a suitable image available, we try to use it.  For many of the services our
applications depend on, we use [Bitnami][bitnami-docker] images.  They have the
advantage of being well-maintained and offer a consistent set of conventions
(e.g., data is always written to `/bitnami/<image-name>`).

## we follow these rules when we write our own images

When there is no suitable image already existing, we try to follow best
practices while writing our own.  This includes:

- [using `.dockerignore` to keep non-essential files out of the build context](../../../.dockerignore)
- [using multi-stage builds to keep the final image size small](https://www.augmentedmind.de/2022/02/06/optimize-docker-image-size/)
- [running as a non-root
  user](https://github.com/hexops/dockerfile#run-as-a-non-root-user)
- [capturing signals and processes with
  `tini`](https://github.com/hexops/dockerfile#use-tini-as-your-entrypoint)

See our [Comet image definition](../../../comet/Dockerfile) for an example of
these in practice.

## we use shared base images for consistency and to reduce duplication

Because many of our applications share the same basic structure (a Ruby on Rails
app that depends on shared gems from our monorepo), we have two base images that
each application image inherits from, to avoid duplicating the same
configuration in each application’s `Dockerfile`.  These images are

- [`docker/ruby/base/Dockerfile`](../../../docker/ruby/base/Dockerfile) – this
  is used as the foundation for all build and dev images; it can be included in
  another Dockerfile with a list of build-time dependencies that can then be
  used for compiling application dependencies:

    ```dockerfile
    ARG BUILD_TIME_APKS='build-base curl gcompat git libxml2 libxml2-dev nodejs postgresql-dev tzdata yarn'
    FROM registry.gitlab.com/surfliner/surfliner/ruby/base:3.2.2 as comet-build
    ```

- [`docker/ruby/prod/Dockerfile`](../../../docker/ruby/prod/Dockerfile) – this
  is used as a base for production images.  Nothing should be compiled in an
  image that inherits from this; instead, compiled dependencies are expected to
  be copied from a previous build [stage][multistage].  In a production image,
  only run-time dependencies should be installed:

    ```dockerfile
    ARG RUN_TIME_APKS='bash curl libpq nodejs postgresql-client tini tzdata zip'
    FROM registry.gitlab.com/surfliner/surfliner/ruby-prod:3.2.2 as comet-prod
    ```

[bitnami-docker]: https://hub.docker.com/u/bitnami/
[container]: https://www.redhat.com/en/topics/containers/whats-a-linux-container
[docker-compose]: https://docs.docker.com/compose/
[helm]: https://helm.sh/docs/
[k8s]: https://kubernetes.io
[multistage]: https://docs.docker.com/build/building/multi-stage/
