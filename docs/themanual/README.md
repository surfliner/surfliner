# The Surliner Technical Manual

- [Surfliner architecture](#surfliner-architecture)
    - [Comet](#comet)
    - [Starlight](#starlight)
    - [Lark](#lark)
    - [Superskunk](#superskunk)
    - [Tidewater](#tidewater)
- [Development and deployment](#development-and-deployment)
    - [Developing locally](#developing-locally)
    - [Deploying in production](#deploying-in-production)
    - [Open Telemetry](#open-telemetry)

## Surfliner architecture

* [Message Queuing in Surfliner](./message_queue/README.md)
* [Authentication](./auth/README.md)

<a>![An overview of the Surfliner architecture](../images/d2/surfliner-architecture.svg "An overview of the Surfliner architecture")</a>

### Comet

* [Configuring Comet to use an external PostgreSQL database](./comet/external-db.md)
* [Indexing and Re-indexing](./comet/indexing.md)
* [Ingesting with Bulkrax](./comet/bulkrax.md)
* [Metadata in Comet](./comet/metadata.md)
* [Setting up a development environment.](./comet/devsetup.md).
* [Storage in Comet](./comet/storage.md)
* [The Access Control Model in Comet](./comet/access_control.md)

### Starlight

* [Migrating a Starlight instance](./starlight/migration.md)

### Lark

* [Data Model](./lark/data-model.md)

### Superskunk

* [Access Controls in Superskunk](./superskunk/acl.md)
* [Aardvark Metadata Profile in Superskunk](./superskunk/profiles/aardvark.md)

### Tidewater

* [Tidewater API Endpoints](./tidewater/api.md)
* [Configuring Tidewater](./tidewater/configuration.md)
* [The Tidewater Database](./tidewater/database.md)

## Development and deployment

* [Container guidelines and conventions](./docker/README.md)

### Developing locally

* [Docker Compose configurations](./dev/README.md)

### Deploying in production

* [Deploying applications with Helm charts](./helm/README.md)

### Open Telemetry

* [Open Telemetry](./otel/README.md)
