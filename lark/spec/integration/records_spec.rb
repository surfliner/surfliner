# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Records" do
  let(:adapter) do
    Valkyrie::MetadataAdapter.find(Lark.config.index_adapter)
  end
  let(:persister) { adapter.persister }

  let(:user_agent) { "surfliner.lark.test_suite" }
  let(:authority_id) { "test_authority_id" }
  let(:headers) do
    {
      "HTTP_ACCEPT" => "application/ld+json",
      "HTTP_USER_AGENT" => user_agent
    }
  end

  before do
    persister.wipe!
  end

  it "returns 404 response" do
    get "/#{authority_id}", headers: headers

    expect(response).to have_http_status(:not_found)
  end

  context "with existing authority record" do
    let!(:authority) do
      persister.save(resource: FactoryBot.create(:concept,
        id: "test_authority_id",
        pref_label: "Test authority label"))
    end

    it "returns the authority" do
      get "/#{authority_id}", headers: headers

      result_json = JSON.parse(response.body.to_str)

      expect(response).to have_http_status(:ok)
      expect(result_json).to include(
        "id" => "test_authority_id",
        "pref_label" => ["Test authority label"]
      )
    end
  end
end
