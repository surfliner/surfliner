# frozen_string_literal: true

require "rails_helper"

RSpec.describe "GET /", type: :request do
  before { get "/" }

  it "returns 200" do
    expect(response.status).to eq 200
  end

  it "defaults to a json response" do
    expect(response.headers)
      .to include "Content-Type" => "application/json"
  end

  it "provides an api service description" do
    expect(JSON.parse(response.body))
      .to include "version" => an_instance_of(String)
  end

  it "has header for CORS request" do
    expect(response.headers)
      .to include "Access-Control-Allow-Origin" => "*"
  end
end
