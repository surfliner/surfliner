# frozen_string_literal: true

require "rails_helper"

RSpec.describe RecordsController do
  let(:adapter) do
    Valkyrie::MetadataAdapter.find(Lark.config.index_adapter)
  end
  let(:persister) { adapter.persister }

  let(:headers) { {"CONTENT_TYPE" => "application/json"} }

  # let(:request) do
  #   instance_double(Rack::Request,
  #    body: body,
  #    env: headers)
  # end

  let(:body) { StringIO.new('{ "pref_label": "moomin"}') }

  describe "#create" do
    subject(:controller) { described_class.new(request: request) }

    xit "gives a 201 status" do
      expect(controller.create.first).to eq 201
    end

    context "with a listener" do
      let(:listener) { FakeListener.new }

      before { Lark.config.event_stream.subscribe(listener) }

      xit "adds an event to the stream" do
        expect { controller.create }
          .to change(listener, :events)
          .to include have_attributes(id: "created")
      end
    end

    context "with an unknown media type" do
      let(:headers) { {"CONTENT_TYPE" => "application/fake"} }

      xit "gives a 415 status" do
        expect(controller.create.first).to eq 415
      end
    end

    context "when the minter fails" do
      before do
        allow(Lark.config).to receive(:minter).and_return(FailureMinter.new)
      end

      xit "gives a 500 status" do
        expect(controller.create.first).to eq 500
      end
    end
  end

  describe "#update" do
    subject(:controller) do
      described_class.new(request: request, params: params)
    end

    let(:authority) do
      persister.save(resource: FactoryBot.create(:concept))
    end
    let(:params) { {"id" => authority.id.to_s} }
    let(:body) { StringIO.new(serializer.serialize(record: authority)) }
    let(:serializer) { Lark::RecordSerializer.for(content_type: "application/json") }

    context "with a listener" do
      let(:listener) { FakeListener.new }

      before { Lark.config.event_stream.subscribe(listener) }

      xit "adds an update event to the stream" do
        expect { controller.update }
          .to change { listener.events.count }
          .by 1
      end
    end
    # rubocop:enable RSpec/MultipleMemoizedHelpers

    context "with an unknown media type" do
      let(:headers) { {"CONTENT_TYPE" => "application/fake"} }

      xit "gives a 415 status" do
        expect(controller.update.first).to eq 415
      end
    end

    context "with an invalid attribute" do
      let(:body) { StringIO.new({pref_label: ["blah"], not_an_attribute: :oh_no}.to_json) }

      xit "gives a 400 status" do
        expect(controller.update.first).to eq 400
      end
    end

    context "with existing authority record" do
      let(:authority) do
        persister.save(resource: FactoryBot.create(:concept))
      end

      let(:body) do
        authority.pref_label = "Label edited"
        StringIO.new(serializer.serialize(record: authority))
      end

      xit "gives a 200 OK status" do
        expect(controller.update.first).to eq 200
      end
    end
  end

  describe "#show" do
    let(:user_agent) { "surfliner.lark.test_suite" }
    let(:authority_id) { "test_authority_id" }
    let(:headers) do
      {
        "HTTP_ACCEPT" => "application/ld+json",
        "HTTP_USER_AGENT" => user_agent
      }
    end

    before do
      persister.wipe!
      request.headers.merge headers
    end

    it "returns 404 response" do
      get :show, params: {id: authority_id}

      expect(response).to have_http_status(:not_found)
    end

    context "with existing authority record" do
      let!(:authority) do
        persister.save(resource: FactoryBot.create(:concept,
          id: "test_authority_id",
          pref_label: "Test authority label"))
      end

      it "returns the authority" do
        get :show, params: {id: authority_id}
        result_json = JSON.parse(response.body.to_str)

        expect(response).to have_http_status(:ok)
        expect(result_json).to include(
          "id" => "test_authority_id",
          "pref_label" => ["Test authority label"]
        )
      end
    end
  end
end
