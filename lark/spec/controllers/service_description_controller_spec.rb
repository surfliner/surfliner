# frozen_string_literal: true

require "rails_helper"

RSpec.describe ServiceDescriptionController do
  describe "#show" do
    before { get :show }

    it "gives a 200 status" do
      expect(response).to have_http_status(:ok)
    end

    it "gives a JSON response" do
      expect(response.headers)
        .to include "Content-Type" => "application/json"
    end

    it "provides an api service description" do
      expect(JSON.parse(response.body))
        .to include "version" => an_instance_of(String)
    end
  end
end
