# frozen_string_literal: true

##
# This controller provides a service description of the API.
class ServiceDescriptionController < ApplicationController
  ##
  # Show the API service description
  def show
    service_description = ServiceDescription.new

    render json: service_description.to_json
  end

  def ok_content_type
    "application/json"
  end
end
