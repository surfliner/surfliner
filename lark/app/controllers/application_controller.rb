##
# An abstract controller encapsulating application-wide controller behavior.
class ApplicationController < ActionController::API
  include LdApi::ControllerBehavior::ContentType
  include LdApi::ControllerBehavior::Cors
  include LdApi::ControllerBehavior::Errors

  rescue_from Lark::BadRequest, with: :bad_request
  rescue_from Lark::RequestError, with: :render_error
  rescue_from Lark::NotFound, with: :not_found

  private

  ##
  # Raise an appropriate error for transaction a failure
  #
  # @param [Dry::Monads::Result::Failure] a hash-like object with a `:reason`
  #
  # @return [void] this method should never return; it should always raise
  # @raise [Lark::RequestError]
  def raise_error_for(failure)
    case failure[:reason]
    when :minter_failed
      raise Lark::RequestError, failure[:message]
    when :invalid_attributes, :unknown_attribute
      raise Lark::BadRequest, failure[:message]
    else
      raise Lark::RequestError, "Unknown Error: #{failure[:message]}"
    end
  end
end
