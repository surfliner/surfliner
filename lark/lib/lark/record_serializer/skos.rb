# frozen_string_literal: true

module Lark
  class RecordSerializer
    ##
    # A serializer for SKOS JSON‐LD content input through the API.
    class Skos < Lark::RecordSerializer
      VOCAB = "http://www.w3.org/2004/02/skos/core#"
      PREFIXES = {"skosxl" => "http://www.w3.org/2008/05/skos-xl"}

      ##
      # @see Lark::RecordSerializer#serialize
      def serialize(record:)
        record.mapped_to("http://www.w3.org/2004/02/skos/core").each_with_object({}) { |(iri, val), result|
          name = if iri.start_with?(VOCAB)
            iri.delete_prefix(VOCAB)
          else
            prefix = PREFIXES.find { |(key, val)| iri.start_with?(val) }
            prefix ? "#{prefix[0]}:#{iri.delete_prefix(prefix[1])}" : iri
          end
          value = (val.respond_to?(:to_a) ? val.to_a : [val]).map do |obj|
            case obj
            when Valkyrie::Resource
              serialize(record: obj)
            else
              obj
            end
          end
          result[name] = (value.size == 1) ? value[0] : value
        }.merge({"@context" => self.class.context}).to_json
      end

      def self.context
        {"@vocab" => VOCAB}.merge(PREFIXES)
      end
    end
  end
end
