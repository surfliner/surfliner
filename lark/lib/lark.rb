##
# The top-level module for the Lark project.
#
# @example configuration
#   Lark.config.index_adapter = :a_registered_valkyrie_adapter
#
module Lark
  extend Dry::Configurable

  require "lark/base_label"
  require "lark/event_stream"
  require "lark/ezid_minter"
  require "lark/indexer"
  require "lark/listeners"
  require "lark/minter"
  require "lark/queries"
  require "lark/record_parser"
  require "lark/record_serializer"
  require "lark/reindexer"
  require "lark/schema_loader"
  require "lark/version"

  setting :database
  setting :event_adapter, default: :sql
  setting :index_adapter, default: :solr
  setting :minter, default: Lark::Minter
  setting :event_stream, default: EventStream.instance

  ##
  # A generic HTTP error. This error can be handled by a Controller or a Rack
  # middleware to emit appropriate HTTP response codes and messages.
  class RequestError < RuntimeError
    STATUS = 500

    ##
    # @return [Integer] an HTTP status code for this error
    def status
      self.class::STATUS
    end
  end

  ##
  # @see RequestError
  class UnsupportedMediaType < RequestError
    STATUS = 415
  end

  ##
  # @see RequestError
  class BadRequest < RequestError
    STATUS = 400
  end

  ##
  # @see RequestError
  class NotFound < RequestError
    STATUS = 404
  end
end
