Lark.config.database = {
  user: ENV.fetch("POSTGRESQL_USERNAME"),
  password: ENV.fetch("POSTGRESQL_PASSWORD"),
  host: ENV.fetch("POSTGRESQL_HOST"),
  port: ENV.fetch("POSTGRESQL_PORT"),
  database: ENV.fetch("POSTGRESQL_DATABASE"),
  adapter: "postgres"
}.freeze
