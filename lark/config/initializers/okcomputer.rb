# frozen_string_literal: true

# Mount the health endpoint at /healthz
# Review all health checks at /healthz/all
OkComputer.mount_at = "healthz"

OkComputer::Registry.deregister "database"

if Lark.config.event_adapter == :sql
  OkComputer::Registry.register "sequel", OkComputer::SequelCheck.new(
    database: Sequel.connect("postgresql://#{Lark.config.database[:user]}:#{Lark.config.database[:password]}@#{Lark.config.database[:host]}:#{Lark.config.database[:port]}/#{Lark.config.database[:database]}"),
    migration_directory: "db/migrations"
  )
end
