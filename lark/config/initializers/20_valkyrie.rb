require "valkyrie/persistence/solr/model_converter/label_value"

Lark.config.event_adapter = ENV["EVENT_ADAPTER"].to_sym
Lark.config.index_adapter = ENV["INDEX_ADAPTER"].to_sym

Valkyrie::MetadataAdapter.register(
  Valkyrie::Persistence::Memory::MetadataAdapter.new,
  :memory
)

if Lark.config.event_adapter == :sql
  ##
  # Register postgres metadata adapter
  Valkyrie::MetadataAdapter.register(
    Valkyrie::Sequel::MetadataAdapter.new(
      connection: Sequel.connect(Lark.config.database)
    ),
    :sql
  )

  # Register custom queries for event adapter
  # (see Valkyrie::Persistence::CustomQueryContainer)
  [Lark::Queries::FindByEventDataProperty, Lark::Queries::FindByEventProperty].each do |q|
    Valkyrie::MetadataAdapter
      .find(Lark.config.event_adapter)
      .query_service
      .custom_queries
      .register_query_handler(q)
  end
end

if Lark.config.index_adapter == :solr
  ##
  # Register solr metadata adapter with RSolr connection for SOLR persistence
  Valkyrie::MetadataAdapter.register(
    Valkyrie::Persistence::Solr::MetadataAdapter.new(
      connection: RSolr.connect(
        url: "http://#{ENV["SOLR_ADMIN_USER"]}:#{ENV["SOLR_ADMIN_PASSWORD"]}@#{ENV["SOLR_HOST"]}:#{ENV["SOLR_PORT"]}/solr/#{ENV["SOLR_COLLECTION_NAME"]}"
      )
    ),
    :solr
  )

  # Register custom queries for the default Valkyrie metadata adapter
  # (see Valkyrie::Persistence::CustomQueryContainer)
  Valkyrie::MetadataAdapter
    .find(Lark.config.index_adapter)
    .query_service
    .custom_queries
    .register_query_handler(Lark::Queries::FindByStringProperty)

  # Prepend custom Solr mappers needed for indexing to the Solr value mapper.
  Valkyrie::Persistence::Solr::ModelConverter::SolrMapperValue.value_casters.prepend(
    Valkyrie::Persistence::Solr::ModelConverter::LabelValue
  )
end

Valkyrie.config.resource_class_resolver = Lark::SchemaLoader.new.resource_class_resolver
