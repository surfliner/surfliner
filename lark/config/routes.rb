Rails.application.routes.draw do
  get "/", controller: "service_description", action: :show
  get "/search", controller: "search", action: :show
  get "/:id", controller: "records", action: :show

  #    post "/batch_edit" do
  #      BatchController.new(request: request).batch_update
  #    end
  #
  #    post "/batch_import" do
  #      BatchController.new(request: request).batch_import
  #    end
  #
  #    post "/" do
  #      RecordController.new(request: request).create
  #    end
  #
  #    put "/:id" do
  #      RecordController.new(request: request, params: params).update
  #    end
  #
  #    options "/" do
  #      ServiceDescriptionController.new(params: params).options
  #    end
  #
  #    options "/search" do
  #      SearchController.new(params: params).options
  #    end
  #
  #    options "/batch_edit" do
  #      BatchController.new(params: params).options
  #    end
  #
  #    options "/:id" do
  #      RecordController.new(params: params).options
  #    end
end
