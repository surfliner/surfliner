require_relative "boot"

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
# require "active_record/railtie"
# require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
# require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Lark
  class Application < Rails::Application
    config.load_defaults 7.0

    config.api_only = true
    config.action_dispatch.ignore_accept_header = true

    config.log_level = ENV.fetch("RAILS_LOG_LEVEL", "info").to_sym
    config.log_formatter = ::Logger::Formatter.new

    if ENV["RAILS_LOG_TO_STDOUT"].present?
      logger = ActiveSupport::Logger.new($stdout)
      logger.formatter = config.log_formatter
      config.logger = ActiveSupport::TaggedLogging.new(logger)
    end

    # Configure SSL based on environment variable.
    # This makes it easier to deploy across environments, where SSL termiation
    # may take place before we hit Ruby.
    config.force_ssl = ActiveModel::Type::Boolean.new.cast(ENV.fetch("RAILS_FORCE_SSL", false))
  end
end
