# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=3.3.1
ARG IMAGE_REGISTRY='docker.io/'
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine as lark

ARG PROJECTPATH=lark
ARG DOCKERROOT=/home/${PROJECTPATH}

RUN apk update && apk add \
    build-base \
    curl \
    gcompat \
    git \
    libc-dev \
    libxml2 \
    libxml2-dev \
    libxslt \
    libxslt-dev \
    postgresql \
    postgresql-dev \
    tzdata \
    yaml-dev \
    zip

RUN gem update bundler

WORKDIR ${DOCKERROOT}/app

COPY ${PROJECTPATH}/Gemfile* ${DOCKERROOT}/app/
COPY gems ${DOCKERROOT}/gems/
RUN bundle check || bundle install --jobs "$(nproc)"

COPY ${PROJECTPATH} ${DOCKERROOT}/app/
COPY scripts ${DOCKERROOT}/scripts/
ENV PATH="$DOCKERROOT/scripts:$PATH"

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["rails-entrypoint.sh"]
