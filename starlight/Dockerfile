# renovate: datasource=docker depName=alpine
ARG ALPINE_VERSION=3.19
# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=3.2.2

ARG IMAGE_REGISTRY='docker.io/'


# =================
# Shared base image
# =================
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION} as build-base

COPY scripts/docker/*.sh /home/starlight/app/

# used in alpine-install.sh
ENV APKS='build-base gcompat git libxml2-dev libxslt-dev nodejs postgresql-dev shared-mime-info tzdata yarn'
RUN /home/starlight/app/alpine-install.sh

COPY starlight/Gemfile* /home/starlight/app/
COPY gems /home/starlight/gems/

WORKDIR /home/starlight/app


# ==========================
# Build base for prod images
# ==========================
FROM build-base as ruby-build-prod

RUN /home/starlight/app/ruby-bundle-prod.sh
# used in ruby-bundle-cleanup.sh for locating the bundle cache
ENV RUBY_ABI=3.2.0
ENV PROJECT_PATH=/home/starlight/app
RUN /home/starlight/app/ruby-bundle-cleanup.sh

COPY starlight /home/starlight/app
# There are symlinks into this folder which aren’t followed by COPY; it needs to be copied in separately.
COPY themes /home/starlight/themes

RUN \
  APP_URL='http://localhost' \
  DATABASE_URL='postgresql://fake' \
  REDIS_URL='redis://redis:6379' \
  DB_ADAPTER=nulldb \
  SKIP_TRANSLATION=yes \
  bundle exec rake assets:precompile


# ==========================
# Prod image for web service
# ==========================
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION} as starlight-prod

COPY scripts/docker/*.sh /home/starlight/app/

ENV APKS='curl imagemagick imagemagick-heic imagemagick-jpeg imagemagick-pdf imagemagick-raw imagemagick-svg imagemagick-tiff imagemagick-webp libpq nodejs shared-mime-info tini tzdata zip'
RUN /home/starlight/app/alpine-install.sh

USER 10000
WORKDIR /home/starlight/app

COPY --chown=10000:10001 gems /home/starlight/gems/
COPY --chown=10000:10001 starlight /home/starlight/app
COPY --chown=10000:10001 scripts/* /home/starlight/app/scripts/
ENV PATH="/home/starlight/app/scripts:${PATH}"

COPY --from=ruby-build-prod --chown=10000:10001 /home/starlight/app/public/assets /home/starlight/app/public/assets
COPY --from=ruby-build-prod --chown=10000:10001 /home/starlight/app/public/uv /home/starlight/app/public/uv
COPY --from=ruby-build-prod --chown=10000:10001 /home/starlight/app/vendor /home/starlight/app/vendor

RUN bundle config set deployment 'true'
RUN bundle config set without 'test development'

# TODO: should these be set by helm/docker-compose?
ENV RAILS_ROOT=/home/starlight/app
ENV RAILS_SERVE_STATIC_FILES=1
ENV BUNDLE_PATH="/home/starlight/app/vendor/bundle"

ENTRYPOINT ["tini", "--", "bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]


# ===============
# Local dev image
# ===============
FROM build-base as starlight-dev

RUN apk --no-cache add \
  curl \
  imagemagick \
  imagemagick-heic \
  imagemagick-jpeg \
  imagemagick-pdf \
  imagemagick-raw \
  imagemagick-svg \
  imagemagick-tiff \
  imagemagick-webp \
  libpq \
  nodejs \
  postgresql-client \
  tzdata \
  zip

RUN gem update bundler
RUN bundle install --jobs "$(nproc)"

COPY starlight /home/starlight/app

# There are symlinks into this folder which aren’t followed by COPY; it needs to be copied in separately.
COPY themes /home/starlight/themes

# put in DOCKER_ROOT rather than DOCKER_PROJECT so that docker-compose volume
# mounting doesn't overwrite them
COPY scripts/* /home/starlight/scripts/
ENV PATH="/home/starlight/scripts:${PATH}"

ENV RAILS_ROOT=/home/starlight/app
ENV RAILS_SERVE_STATIC_FILES=1

RUN \
  APP_URL='http://localhost' \
  DATABASE_URL='postgresql://fake' \
  DB_ADAPTER=nulldb \
  REDIS_URL='redis://fake' \
  SKIP_TRANSLATION=yes \
  bundle exec rake assets:precompile

ENTRYPOINT ["bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
