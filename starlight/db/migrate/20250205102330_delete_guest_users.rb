class DeleteGuestUsers < ActiveRecord::Migration[6.1]
  def up
    stmt = <<~SQL
      DELETE FROM users
            WHERE provider IS NULL
              AND email LIKE 'guest_%'
    SQL
    exec_delete(stmt)
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
