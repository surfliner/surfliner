module LdApi
  class Error < StandardError
    require "ld_api/error/bad_accept"
  end
end
