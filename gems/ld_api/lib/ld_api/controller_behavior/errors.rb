module LdApi
  module ControllerBehavior
    module Errors
      ##
      # 400 BAD REQUEST
      #
      # Use this for badly‐formed requests (e.g. headers which don’t match the
      # HTTP syntax).
      def bad_request(exception = nil)
        if exception.is_a?(LdApi::Error::BadAccept)
          render_error text: "Bad Accept Header", status: 400, exception: exception
        else
          render_error text: "Bad Request", status: 400, exception: exception
        end
      end

      ##
      # 403 FORBIDDEN
      #
      # Use this for authentication errors, but use +not_found+ for authorization
      # failures to avoid leaking information.
      def forbidden(exception = nil)
        render_error text: "Forbidden", status: 403, exception: exception
      end

      ##
      # 404 NOT FOUND
      #
      # A resource may be not found because it doesn’t exist, or because the
      # requester is not authorized to view the resource.
      def not_found(exception = nil)
        render_error text: "Not Found", status: 404, exception: exception
      end

      ##
      # Renders an error in a reasonable format.
      def render_error(exception: nil, text: "Server Error", status: 500)
        error_type = "text/plain"
        begin
          error_type = error_content_type if respond_to?(:error_content_type)
        rescue LdApi::Error::BadAccept
          # Don’t throw an error for a bad accept header when we’re already
          # rendering an error for other reasons.
        end

        if error_type == "application/json"
          render json: {"error" => text, "status" => status}, status: status
        else
          render plain: text, status: status
        end
      end

      class << self
        private

        def included(descendant)
          super
          descendant.rescue_from LdApi::Error::BadAccept, with: :bad_request
        end
      end
    end
  end
end
