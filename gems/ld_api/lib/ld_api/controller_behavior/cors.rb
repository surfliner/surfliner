module LdApi
  module ControllerBehavior
    module Cors
      ##
      # Modifies the response headers to allow crossorigin requests.
      #
      # Define +allow_cors?+ to be false if this shouldn’t happen.
      def allow_cors
        response.headers["Access-Control-Allow-Origin"] = "*" unless respond_to?(:allow_cors?) && allow_cors? == false
      end

      class << self
        private

        def included(descendant)
          super
          descendant.after_action :allow_cors
        end
      end
    end
  end
end
