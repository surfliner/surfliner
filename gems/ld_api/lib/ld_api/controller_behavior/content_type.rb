module LdApi
  module ControllerBehavior
    ##
    # Common behaviours for including in a Rails Application Controller.
    module ContentType
      ##
      # Returns an AcceptReader for the current request.
      def accept_reader
        @accept_reader ||= LdApi::AcceptReader.new(request.headers["Accept"])
      end

      ##
      # Returns the default type for responses (ignoring profiles and other
      # considerations).
      #
      # This is intended as a fallback in the case that no more specific type is
      # acceptable, and is used to determine the format of errors. Possible
      # return values are +:unknown+, +:plaintext+, or +:json+.
      #
      # Rendering methods can support other types or profiles, but should fallback
      # to these types in the case that no other type or profile is recognized.
      # API’s which don’t need complicated type/profile support can just use this
      # method directly.
      def default_type
        return @default_type unless @default_type.nil?
        best_recognized_type = accept_reader.best_type_of(types: [
          "application/ld+json",
          "application/json",
          "application/*",
          "text/plain",
          "text/*"
        ])&.[](:type)
        @default_type = if best_recognized_type.nil?
          :unknown
        elsif best_recognized_type.start_with?("text/")
          :plaintext
        else
          :json
        end
      end

      ##
      # Set the Content-Type of the resource based on whether or not it is
      # successful.
      #
      # This is intended to be used as a Rails +:after_action+ to appropriately
      # set the content type of responses regardless of whether an error appears.
      def set_content_type
        response.headers["Content-Type"] = if response.ok?
          ok_content_type
        else
          error_content_type
        end
      end

      ##
      # The Content-Type of an ok response.
      #
      # Override this if a different Content-Type is desired!!
      def ok_content_type
        "text/plain"
      end

      ##
      # The Content-Type of a non‐ok response.
      #
      # This will match the output of +render_error+, so there’s no reason to
      # override it unless you are using custom error handling.
      def error_content_type
        default_type == :json ? "application/json" : "text/plain"
      end

      class << self
        private

        def included(descendant)
          super
          descendant.after_action :set_content_type
        end
      end
    end
  end
end
