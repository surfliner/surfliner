module LdApi
  class Error
    ##
    # This error is raised when the +Accept+ header is invalid.
    class BadAccept < LdApi::Error
    end
  end
end
