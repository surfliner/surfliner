module LdApi
  ##
  # Encapsulates controller behaviours.
  module ControllerBehavior
    require "ld_api/controller_behavior/content_type"
    require "ld_api/controller_behavior/cors"
    require "ld_api/controller_behavior/errors"
  end
end
