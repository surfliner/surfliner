# ld_api

## Installation

Add this line to your application's Gemfile:

```ruby
gem "ld_api", path: "../gems/ld_api"
```

And then execute:

    $ bundle install

## Local Development

You can use the local `Dockerfile` to build an image for testing changes to the gem.

You can build an image locally:

```sh
❯ cd gems/ld_api
❯ docker build -t ld_api .
```

By default, running the image will run the test suite:

```sh
❯ cd gems/ld_api
❯ docker run --rm -it ld_api:latest

Randomized with seed 7846
...

Finished in 0.00456 seconds (files took 0.09002 seconds to load)
3 examples, 0 failures

```

You can also volume mount your local code into the image, for quicker iterative development test running tests:

```sh
❯ cd gems/ld_api
❯ docker run --rm -it --volume "$(pwd)/app" ld_api:latest
```


