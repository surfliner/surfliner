require "spec_helper"

describe LdApi::AcceptReader do
  let(:allowed_profiles) { ["example:good", "example:ok"] }
  let(:allowed_types) { ["application/json", "application/ld+json", "text/plain", "application/*", "text/*", "*/*"] }
  let(:header) { nil }
  let(:reader) { LdApi::AcceptReader.new(header) }

  describe "#best_type_of" do
    let(:best_type) { reader.best_type_of(types: allowed_types, ld_profiles: allowed_profiles) }

    describe "with a simple header containing an allowed type" do
      let(:header) { "application/ld+json" }

      it "returns the requested type" do
        expect(best_type).to include(type: "application/ld+json", profile: nil)
      end
    end

    describe "with a simple header containing an allowed profile" do
      let(:header) { "application/ld+json;profile=\"example:good\"" }

      it "returns the requested type" do
        expect(best_type).to include(type: "application/ld+json", profile: "example:good")
      end
    end

    describe "with a simple header containing a disallowed type" do
      let(:header) { "text/x.record-jar" }

      it "returns nil" do
        expect(best_type).to be_nil
      end
    end

    describe "with a simple header containing a disallowed profile" do
      let(:header) { "application/ld+json;profile=\"example:bad\"" }

      it "returns nil" do
        expect(best_type).to be_nil
      end
    end

    describe "with a complex header with a simple type as highest priority" do
      let(:header) { "application/ld+json;profile=\"example:bad\";q=1,text/x.record-jar;q=1,application/ld+json;profile=\"example:good\";q=0.7,application/ld+json;profile=\"example:ok\";q=0.8,application/json;q=0.9,text/plain;q=0.9" }

      it "returns the best requested type" do
        expect(best_type).to eq(type: "application/json", profile: nil)
      end
    end

    describe "with a complex header with a profile highest priority" do
      let(:header) { "application/ld+json;profile=\"example:bad\";q=1,text/x.record-jar;q=1,application/ld+json;profile=\"example:good\";q=0.7,application/ld+json;profile=\"example:ok\";q=0.8,application/json;q=0.6,text/plain;q=0.6" }

      it "returns the best requested type" do
        expect(best_type).to eq(type: "application/ld+json", profile: "example:ok")
      end
    end

    describe "with a complex header with no matches" do
      let(:header) { "application/ld+json;profile=\"example:bad\";q=1,text/x.record-jar;q=1" }

      it "returns nil" do
        expect(best_type).to be_nil
      end
    end

    describe "with an invalid header" do
      let(:header) { "**/**" }

      it "raises an error" do
        expect { best_type }.to raise_error(LdApi::Error::BadAccept)
      end
    end
  end
end
