require "spec_helper"

describe LdApi::ControllerBehavior::Cors do
  it "registers :allow_cors as an :after_action" do
    controller_class = Class.new
    expect(controller_class).to receive(:after_action).with(:allow_cors)
    controller_class.include(LdApi::ControllerBehavior::Cors)
  end

  describe "when included into a controller" do
    let(:controller_class) { Class.new }
    let(:controller) { controller_class.new }

    before do
      allow(controller_class).to receive(:after_action)
      controller_class.include(LdApi::ControllerBehavior::Cors)
    end

    describe "#allow_cors" do
      let(:response) { double }
      let(:response_headers) { {} }

      before do
        allow(controller).to receive(:response).and_return(response)
        allow(response).to receive(:headers).and_return(response_headers)
      end

      it "adds a cors header by default" do
        controller.allow_cors
        expect(response_headers["Access-Control-Allow-Origin"]).to eq "*"
      end

      it "adds a cors header when allow_cors? is true" do
        allow(controller).to receive(:allow_cors?).and_return(true)
        controller.allow_cors
        expect(response_headers["Access-Control-Allow-Origin"]).to eq "*"
      end

      it "does not add a cors header when allow_cors? is false" do
        allow(controller).to receive(:allow_cors?).and_return(false)
        controller.allow_cors
        expect(response_headers["Access-Control-Allow-Origin"]).to be_nil
      end
    end
  end
end
