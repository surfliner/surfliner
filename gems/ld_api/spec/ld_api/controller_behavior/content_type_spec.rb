require "spec_helper"

describe LdApi::ControllerBehavior::ContentType do
  it "registers :set_content_type as an :after_action" do
    controller_class = Class.new
    expect(controller_class).to receive(:after_action).with(:set_content_type)
    controller_class.include(LdApi::ControllerBehavior::ContentType)
  end

  describe "when included into a controller" do
    let(:controller_class) { Class.new }
    let(:controller) { controller_class.new }
    let(:request) { double }

    before do
      allow(controller_class).to receive(:after_action)
      controller_class.include(LdApi::ControllerBehavior::ContentType)
      allow(controller).to receive(:request).and_return(request)
      allow(request).to receive(:headers).and_return(headers)
    end

    describe "#default_type" do
      describe "for an ok request with a json content type" do
        let(:headers) { {"Accept" => "application/json"} }

        it "selects the correct default type" do
          expect(controller.default_type).to eq :json
        end
      end

      describe "for an ok request with an unrecognized content type" do
        let(:headers) { {"Accept" => "text/x.record-jar"} }

        it "selects the correct default type" do
          expect(controller.default_type).to eq :unknown
        end
      end
    end

    describe "#error_content_type" do
      describe "for an ok request with a json content type" do
        let(:headers) { {"Accept" => "application/json"} }

        it "chooses the correct corresponding error type" do
          expect(controller.error_content_type).to eq "application/json"
        end
      end

      describe "for an ok request with an unrecognized content type" do
        let(:headers) { {"Accept" => "text/x.record-jar"} }

        it "chooses the correct corresponding error type" do
          expect(controller.error_content_type).to eq "text/plain"
        end
      end
    end

    describe "#set_content_type" do
      let(:headers) { {"Accept" => "application/json"} }
      let(:response) { double }
      let(:response_headers) { {} }

      before do
        allow(controller).to receive(:response).and_return(response)
        allow(response).to receive(:headers).and_return(response_headers)
      end

      it "picks the right type when the response is ok" do
        allow(response).to receive(:ok?).and_return(true)
        controller.set_content_type
        expect(response_headers["Content-Type"]).to eq "text/plain"
      end

      it "picks the right type when the response is not ok" do
        allow(response).to receive(:ok?).and_return(false)
        controller.set_content_type
        expect(response_headers["Content-Type"]).to eq "application/json"
      end
    end
  end
end
