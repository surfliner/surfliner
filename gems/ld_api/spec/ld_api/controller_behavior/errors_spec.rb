require "spec_helper"

describe LdApi::ControllerBehavior::Errors do
  it "registers :bad_request as a :rescue_from for LdApi::Error::BadAccept" do
    controller_class = Class.new
    expect(controller_class).to receive(:rescue_from).with(LdApi::Error::BadAccept, with: :bad_request)
    controller_class.include(LdApi::ControllerBehavior::Errors)
  end

  describe "when included into a controller" do
    let(:controller_class) { Class.new }
    let(:controller) { controller_class.new }

    before do
      allow(controller_class).to receive(:rescue_from)
      controller_class.include(LdApi::ControllerBehavior::Errors)
    end

    describe "#render_error" do
      let(:error_text) { "An error" }
      let(:error_status) { 417 }

      it "renders plaintext by default" do
        expect(controller).to receive(:render).with(plain: error_text, status: error_status)
        controller.render_error(text: error_text, status: error_status)
      end

      it "renders json when the error content type is json" do
        allow(controller).to receive(:error_content_type).and_return("application/json")
        error_hash = {"error" => error_text, "status" => error_status}
        expect(controller).to receive(:render).with(json: error_hash, status: error_status)
        controller.render_error(text: error_text, status: error_status)
      end
    end
  end
end
