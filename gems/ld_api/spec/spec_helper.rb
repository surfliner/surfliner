# frozen_string_literal: true

require "ld_api"

RSpec.configure do |config|
  config.order = :random
  Kernel.srand config.seed
end
