# Treat structs as hashes for the sake of +Dry::Types+ type validations!

Dry::Types::PrimitiveInferrer::Compiler.alias_method(:visit_struct, :visit_hash)
Dry::Types::PredicateInferrer::Compiler.alias_method(:visit_struct, :visit_hash)
