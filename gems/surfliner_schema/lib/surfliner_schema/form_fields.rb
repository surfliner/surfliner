# frozen_string_literal: true

module SurflinerSchema
  ##
  # Builds a module which provides Reform::Form fields as well as a few
  # additional useful (class) methods for form processing.
  #
  # The additional methods are :—
  #
  # [form_definition]
  #
  #   Returns a +SurflinerSchema::FormDefinition+ corresponding to the provided
  #   property name, if such a property is available through the schema on the
  #   form; otherwise, returns nil.
  #
  # [primary_division]
  #
  #   Returns a +SurflinerSchema::Division+ for the form’s availability, limited
  #   to only primary fields.
  #
  # [secondary_division]
  #
  #   Returns a +SurflinerSchema::Division+ for the form’s availability, limited
  #   to only nonprimary fields.
  #
  # @param model_class [#reader,#availability]
  #
  # @return [Module] a mixin module providing properties, validations, and
  #   implementations of the above methods for a +Reform::Form+.
  #
  # @example
  #   class MonographForm < Reform::Form
  #     include SurflinerSchema::FormFields(Monograph)
  #   end
  def self.FormFields(model_class)
    SurflinerSchema::FormFields.new(model_class)
  end

  ##
  # @api private
  #
  # A module providing properties, validations, and additional methods to a
  # +Reform::Form+.
  #
  # This class provides the internals for the recommended module builder syntax:
  # +SurflinerSchema::FormFields(:my_schema_name)+.
  #
  # @see .FormFields
  class FormFields < Module
    autoload :ClassBehaviours, "surfliner_schema/form_fields/class_behaviours"

    attr_reader :model_class, :availability, :reader, :definitions

    ##
    # @param availability [Symbol]
    # @param loader [#properties_for]
    def initialize(model_class)
      @model_class = model_class
      @availability = model_class.availability
      @reader = model_class.reader
      @definitions = @reader.form_definitions(availability: @availability)

      module_eval do
        def deserialize!(params)
          contract = self.class.schema_contract.new
          keys = contract.schema.key_map.map { |key| key.name.to_sym }

          # For any params that are not in the provided hash but are in the
          # schema, add the current existing values in the form.
          schema_params = keys.each_with_object({}) do |name, obj|
            obj[name] = if params&.include?(name)
              params[name]
            elsif params&.include?(name.to_s)
              params[name.to_s]
            else
              self[name]
            end
          end

          # Attempt to coerce all of the params into approptiate values, and run
          # schema validations on them. This is a bit more nuanced than the
          # builtin Reform coercion support, which doesn’t have error handling.
          result = contract.call(schema_params)

          # +@result+ is defined by Reform::Contract and collects the results of
          # the validation process. +#to_results+ gives the actual used array of
          # result objects. The actual error used is an incredibly hacked
          # +Reform::Contract::CustomError+; ideally we would just use the Dry
          # error object natively, but the A·P·I’s don’t quite match.
          #
          # The +Reform::Contract::CustomError+ initializer only supports a
          # single key, but we want all the keys, so we circumvent it here.
          if result.failure?
            errors = result.errors.to_h
            error = Reform::Contract::CustomError.allocate # do not initialize
            error.instance_variable_set(:@errors, errors)
            error.instance_variable_set(:@messages, errors)
            error.instance_variable_set(:@hint, {})
            error.instance_variable_set(:@results, [result])
            @result.to_results << error
          end

          # Replace schema params with their coerced result, persisting any
          # params which were not specified in the schema through to the form.
          (params.try(:with_indifferent_access) || params || {}).merge(result.to_h)
        end
      end
    end

    def class_behaviours
      @class_behaviours ||= SurflinerSchema::FormFields::ClassBehaviours.new(self)
    end

    ##
    # @return [String]
    def inspect
      "SurflinerSchema::FormFields(#{availability})"
    end

    private

    def included(descendant)
      super

      descendant.singleton_class.include(class_behaviours)

      definitions.keys.each do |property_name|
        descendant.property property_name, default: []
      end
    end
  end
end
