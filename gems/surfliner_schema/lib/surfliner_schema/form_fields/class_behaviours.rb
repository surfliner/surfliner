# frozen_string_literal: true

module SurflinerSchema
  class FormFields
    ##
    # Class behaviours for +SurflinerSchema::FormFields+ instances, included
    # automatically as part of including the latter.
    #
    # Do not include these modules manually.
    class ClassBehaviours < Module
      attr_reader :base_module

      def initialize(base_module)
        @base_module = base_module

        availability = base_module.availability
        define_method(:schema_availability, -> { availability })

        definitions = base_module.definitions
        define_method(:form_definition, ->(name) { definitions[name.to_sym] })

        reader = base_module.reader
        define_method(:schema_reader, -> { reader })

        module_eval do
          ##
          # Returns a +SurflinerSchema::Division+ containing only primary
          # properties.
          #
          # @return [SurflinerSchema::Division]
          def primary_division
            @primary_division ||= schema_reader.class_division(availability: schema_availability) do |property|
              form_definition(property.name).primary?
            end
          end

          ##
          # Returns a +SurflinerSchema::Division+ containing only nonprimary
          # properties.
          #
          # @return [SurflinerSchema::Division]
          def secondary_division
            @secondary_division ||= schema_reader.class_division(availability: schema_availability) do |property|
              !form_definition(property.name).primary?
            end
          end

          ##
          # A +Dry::Validation::Contract+ for schema‐defined properties in this
          # form.
          #
          # By default, this just applies schema processing, but additional rules
          # can be defined with +.rule+.
          def schema_contract
            schema = schema_reader.dry_schema(availability: schema_availability)
            @schema_contract ||= Class.new(SurflinerSchema::Contract) do
              params(schema)
            end
          end
        end
      end

      ##
      # @return [String]
      def inspect
        "SurflinerSchema::FormFields(#{availability}).class_behaivours"
      end
    end
  end
end
