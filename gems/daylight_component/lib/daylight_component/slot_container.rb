class DaylightComponent
  ##
  # A small wrapper object for associating a +DaylightComponent+ with a
  # +Nokogiri::XML::DocumentFragment+ of slotted elements.
  class SlotContainer
    attr_reader :component
    attr_reader :slotted_elements

    ##
    # Initialize a new +DaylightComponent::SlotContainer+ given a
    # +DaylightComponent+.
    #
    # @param component [DaylightComponent]
    def initialize(component)
      @component = component
      @slotted_elements = component.empty_fragment
    end

    ##
    # Wraps the provided HTML in a slotted wrapper element and appends it to the
    # +#slotted_elements+ of this +DaylightComponent::SlotContainer+.
    #
    # @param html [Nokogiri::XML::Node, Nokogiri::XML::NodeSet, String, #to_json]
    # @param name [#to_s]
    # @param wrapper [#to_s]
    def append(html, name: :"daylight-component", wrapper: :div)
      return unless component.result_document
      slotted = component.result_document.create_element(wrapper.to_s,
        {"xmlns" => "http://www.w3.org/1999/xhtml",
         "slot" => name.to_s})
      slotted <<
        if html.is_a?(Nokogiri::XML::Node) || html.is_a?(Nokogiri::XML::NodeSet)
          html
        elsif html.is_a?(String)
          component.class.parse_html(html, fragment: true)
        else
          component.jsonify_and_wrap(html)
        end
      slotted_elements.add_child(slotted)
    end
  end
end
