class DaylightComponent
  class Railtie < Rails::Railtie
    initializer "daylight_component.view_helpers" do
      ActiveSupport.on_load(:action_view) do
        include DaylightComponent::ViewHelpers
      end
    end
  end
end
