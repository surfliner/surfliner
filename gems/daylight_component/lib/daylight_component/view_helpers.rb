class DaylightComponent
  module ViewHelpers
    ##
    # Renders the named component to an HTML string, and includes its metadata
    # tags in the +:daylight_component_metadata+ content for this view, if it
    # hasn’t already done so.
    #
    # If a block is provided, then the first custom element in the component
    # will be provided with the result of the block, wrapped in a
    # +<div slot="daylight-component">+. So, for a component like :—
    #
    #   <my-daylight-component></my-daylight-component>
    #
    # —: called from Ruby with the following :—
    #
    #   render_daylight_component(name: :my_daylight_component) do
    #     "<em>Amazing!</em>"
    #   end
    #
    # —: the result will be :—
    #
    #   <my-daylight-component>
    #     <div slot="daylight-component"><em>Amazing!</em></div>
    #   </my-daylight-component>
    #
    # +#slot_into+ can be used inside the block to slot contents into slots
    # other than just +daylight-component+; see that method for more.
    #
    # The result will be an HTML string, unless +to_string+ is false.
    #
    # @param to_string [Boolean]
    # @yieldreturn [Nokogiri::XML::Node, Nokogiri::XML::NodeSet, String, #to_json]
    # @return [String, Nokogiri::XML::DocumentFragment]
    def render_daylight_component(name:, path: "", params: {}, to_string: true)
      @daylight_slot_stack ||= []
      @included_daylight_compontents ||= Set.new
      component = DaylightComponent.new(name: name, path: path, params: params)
      url_sym = component.url.to_s.to_sym
      if component.head_content && !@included_daylight_compontents.include?(url_sym)
        # If this view has not already included this component, add its head
        # content to the component metadata.
        content_for(:daylight_component_metadata) do
          component.class.serialize(component.head_content)
        end
        @included_daylight_compontents << url_sym
      end
      if !block_given?
        # No block is given; just render the component without insertions.
        to_string ? component.class.serialize(component.body_content) : component.body_content.dup
      else
        # A block is given; create a new slot container and then call it,
        # prepending any resulting slots into the first custom element in the
        # component. (The block is called regardless of whether any such custom
        # elements exist.)
        slot_container = DaylightComponent::SlotContainer.new(component)
        @daylight_slot_stack << slot_container
        block_result = yield
        @daylight_slot_stack.delete(slot_container)
        slot_container.append(block_result) unless block_result.nil?
        result = component.body_content.dup
        custom_element = result.xpath("descendant-or-self::html:*[contains(local-name(), '-')][1]",
          {html: "http://www.w3.org/1999/xhtml"}).first
        custom_element&.prepend_child(slot_container.slotted_elements)
        to_string ? component.class.serialize(result) : result
      end
    end

    ##
    # Wraps the provided HTML in a wrapper slotted into the slot with the
    # provided name.
    #
    # If the HTML is a +Nokogiri::XML::Node+ or a +Nokogiri::XML::NodeSet+, it
    # will be used verbatim; if it is a string, it will be parsed; otherwise, it
    # will be converted to Json and wrapped in an approprite
    # +<script type="application/json">+ element.
    #
    # This only has an effect in the block passed to
    # +#render_daylight_component+, which sets up the document fragment which
    # will hold the resulting slotted elements. In the case of nested
    # +#render_daylight_component+s, the elements will be slotted into the
    # fragment corresponding to the most recent call.
    #
    # @param name [#to_s]
    # @param html [Nokogiri::XML::Node, Nokogiri::XML::NodeSet, String, #to_json]
    # @param wrapper [#to_s]
    def slot_into(name, html, wrapper: :div)
      slot_container = @daylight_slot_stack&.last
      return unless slot_container
      slot_container.append(html, name: name, wrapper: wrapper)
      nil
    end
  end
end
