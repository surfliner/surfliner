# frozen_string_literal: true

require "nokogiri"
require "open-uri" # also requires "uri"

##
# A single daylight component.
#
# Components are specific to views, but the same component can (and should) be
# re·used multiple times in a given view.
class DaylightComponent
  require "daylight_component/railtie" if defined? Rails
  require "daylight_component/slot_container"
  require "daylight_component/version"
  require "daylight_component/view_helpers"

  class << self
    include Enumerable

    ##
    # Returns a new component given a microfrontend name and optional path and
    # params.
    def new(name:, path: "", params: {})
      @component_cache ||= {}
      base_uri = self[name.to_sym]
      if base_uri.nil?
        super(nil)
      else
        url = URI.join(base_uri, path.to_s)
        unless params.nil? || params.empty?
          final_params = URI.decode_www_form(url.query || "").to_h.merge(params.to_h)
          url.query = URI.encode_www_form(final_params)
        end
        url.fragment = nil
        @component_cache[url.to_s.to_sym] ||= super(url)
      end
    end

    ##
    # Get the base URL associated with a given daylight component microfrontend
    # name.
    #
    # @param name [#to_sym]
    # @return [URI]
    def [](name)
      @sources&.[](name.to_sym).dup
    end

    ##
    # Set the base URL associated with a given daylight component microfrontend
    # name.
    #
    # @param name [#to_sym]
    # @param url [#to_s]
    def []=(name, url)
      @sources ||= {}
      key = name.to_sym
      val = URI(url.to_s)
      val.query = nil
      val.fragment = nil
      @sources[key] = val
    end

    ##
    # Iterate over each registered microfrontend name and its corresponding base
    # URL.
    #
    # @yield [(Symbol, URI)]
    def each
      @sources.each do |(name, url)|
        yield [name, url.dup]
      end
      self
    end

    ##
    # Returns whether the provided microfrontend name has already been
    # registered.
    #
    # @return [Boolean]
    def include?(name)
      @sources.include?(name)
    end

    ##
    # Modifies the provided result +Nokogiri::XML::Document+, performing any
    # necessary transformations, and then returns it.
    #
    # By default, this resolves any relative links in +<link>+ or +<script>+
    # elements according to the provided +base_url+.
    #
    # @param document [Nokogiri::XML::Document]
    # @param base_url [URI]
    # @return [Nokogiri::XML::Document]
    def transform_result(document, base_url:)
      html_base = document.xpath("//html:base[@href][1]",
        {html: "http://www.w3.org/1999/xhtml"}).first
      document_base_url = html_base ? base_url + html_base["href"] : base_url
      document.xpath("//*[self::html:link or self::html:script]",
        {html: "http://www.w3.org/1999/xhtml"}).each do |element|
        if element.has_attribute?("href")
          element["href"] = (document_base_url + element["href"]).to_s
        elsif element.has_attribute?("src")
          element["src"] = (document_base_url + element["src"]).to_s
        end
      end
      document
    end

    ##
    # Returns a +Nokogiri::XML::NodeSet+ of metadata nodes in the provided
    # context.
    #
    # By default, this is any +<link>+, +<script>+, +<style>+, or +<template>+
    # elements.
    #
    # @param context [Nokogiri::XML::Searchable]
    # @return [Nokogiri::XML::NodeSet]
    def metadata_nodes(context)
      context.xpath("*[self::html:link or self::html:script or self::html:style or self::html:template]",
        {html: "http://www.w3.org/1999/xhtml"})
    end

    ##
    # Returns a +Nokogiri::XML::NodeSet+ of content nodes in the provided
    # context.
    #
    # By default, this matches all elements and text nodes.
    #
    # @param context [Nokogiri::XML::Searchable]
    # @return [Nokogiri::XML::NodeSet]
    def content_nodes(context)
      context.xpath("*|text()")
    end

    ##
    # Parse the provided value as HTML.
    #
    # @param value [#to_s]
    # @param fragment [Boolean]
    # @return [Nokogiri::XML::Document, Nokogiri::XML::DocumentFragment]
    def parse_html(value, fragment: false)
      # Nokogiri’s HTML5 parser doesn’t follow the spec and assign HTML
      # elements to their proper namespaces. Get around this by converting the
      # HTML to XHTML and then reparsing.
      raw = value.to_s
      return Nokogiri::XML(Nokogiri::HTML5(raw).root.to_xhtml) unless fragment
      wrapper = Nokogiri::XML("<div xmlns='http://www.w3.org/1999/xhtml'>#{
        Nokogiri::HTML5.fragment(raw).to_xml
      }</div>")
      result = Nokogiri::XML::DocumentFragment.new(wrapper)
      result << wrapper.root.children
    end

    ##
    # Serializes the given value to an HTML string.
    #
    # Nokogiri’s handling of namespaces re: XML and HTML is bad, so we need to
    # manually strip them.
    #
    # In environments like Rails, the resulting string is marked as HTML‐safe.
    # Don’t ever call this method with a value which might contain unsanitized
    # user input!
    #
    # @param value [#to_xml]
    def serialize(value)
      wrapper = Nokogiri::XML("<div xmlns='http://www.w3.org/1999/xhtml'>#{
        value.to_xml
      }</div>")
      wrapper.remove_namespaces!
      html = wrapper.root.children.to_html
      if html.respond_to?(:html_safe)
        html.html_safe
      else
        html
      end
    end
  end

  ##
  # An object which implements a +#content_for method akin to
  # +ActionView::Base+.
  #
  # @return [#content_for]
  attr_reader :view_context

  ##
  # A +Nokogiri::XML::Document+ providing the result of fetching the path for
  # this component from its microfrontend.
  #
  # This will be +nil+ instead if there was an error in processing the document.
  #
  # @return [Nokogiri::XML::Document, nil]
  attr_reader :result_document

  ##
  # The +URI+ for this component.
  #
  # @return [URI]
  attr_reader :url

  ##
  # Initializes a new component using the provided url.
  #
  # @param url [URI, nil]
  def initialize(url)
    if url.nil?
      @body_content = format_error(error_text: "Could not resolve component")
    else
      @url = url
      fetch
    end
  end

  ##
  # A +Nokogiri::XML::DocumentFragment+ of those elements in the
  # +#result_document+ which should be included in the +<head>+ of any views.
  #
  # @return [Nokogiri::XML::DocumentFragment]
  def head_content
    return @head_content if @head_content
    @head_content = empty_fragment
    return @head_content unless result_document
    head = result_document.xpath("/html:html/html:head[1]",
      {html: "http://www.w3.org/1999/xhtml"})
    @head_content << self.class.metadata_nodes(head)
  end

  ##
  # A +Nokogiri::XML::DocumentFragment+ of those elements in the
  # +#result_document+ which should be included in the +<body>+ of any views.
  #
  # If an error occurs, this will contain a +<script>+ element with a class of
  # +daylight_component+ and +error+ and a +@data-error-text+ attribute with the
  # error text.
  #
  # @return [Nokogiri::XML::DocumentFragment]
  def body_content
    return @body_content if @body_content
    @body_content = empty_fragment
    return @body_content unless result_document
    body = result_document.xpath("/html:html/html:body|/html:body",
      {html: "http://www.w3.org/1999/xhtml"})
    body = result_document.xpath("/") if body.empty?
    @body_content << self.class.content_nodes(body)
  end

  ##
  # Returns a new, empty +Nokogiri::XML::DocumentFragment+.
  #
  # @return [Nokogiri::XML::DocumentFragment]
  def empty_fragment(doc = nil)
    base = doc || result_document || Nokogiri::XML::Document.new
    Nokogiri::XML::DocumentFragment.new(base)
  end

  ##
  # Returns an HTML element representing an error.
  #
  # An error text of +"My error"+ will produce output like the following :—
  #
  #   <script class="daylight_component error" data-error-text="My error">
  #     console.error(currentScript.dataset.errorText)
  #   </script>
  #
  # The intention is that this will be processible by backends as well as
  # provide a reasonable frontend fallback if no processing occurs.
  #
  # @param error_text [#to_s]
  # @return [Nokogiri::XML::DocumentFragment]
  def format_error(error_text:)
    error_doc = Nokogiri::XML("<script xmlns='http://www.w3.org/1999/xhtml'/>")
    error_elt = error_doc.root
    error_elt["class"] = "daylight_component error"
    error_elt["data-error-text"] = error_text.to_s
    error_elt.content = "console.error(currentScript.dataset.errorText)"
    empty_fragment << error_elt
  end

  ##
  # Converts the provided object to Json and wraps it in a
  # +<script class="daylight_component data" type="application/json">+ element.
  #
  # @param obj [#to_json]
  # @return [Nokogiri::XML::DocumentFragment]
  def jsonify_and_wrap(obj)
    json_doc = Nokogiri::XML("<script xmlns='http://www.w3.org/1999/xhtml'/>")
    json_elt = json_doc.root
    json_elt["class"] = "daylight_component data"
    json_elt["type"] = "application/json"
    json_elt.content = obj.to_json
    empty_fragment << json_elt
  rescue err
    format_error(error_text: "Could not jsonify #{obj.inspect}: #{err}")
  end

  private

  ##
  # Fetches the url and parses the result into the result document.
  def fetch
    url.open("Accept" => "application/xhtml+xml,text/html,application/xml") do |response|
      content_type = response.meta["content-type"].split(";", 2).first.strip.downcase
      body = response.read
      if content_type == "text/html"
        @result_document = self.class.parse_html(body)
      elsif content_type == "application/xml" || content_type.end_with?("+xml")
        @result_document = Nokogiri::XML(body)
      else
        raise StandardError, "Unrecognized Content-Type `#{content_type}` in response."
      end
      canonical = response.metas["link"].to_a.any? do |link_text|
        # Search the link headers for a link with +rel=canonical+ and no other
        # parameters.
        #
        # This uses +#any?+ instead of +#each+ just to ensure a falsey result if
        # no link matches; the loop will break with the actual value of the link
        # if one is found.
        match = /^<(?<href>[^>]*)>[ \t]*;[ \t]*rel[ \t]*=[ \t]*(?:"canonical"|canonical)$/i.match(link_text)
        break match[:href] if match
      end
      base_url = canonical ? url + canonical : url
      self.class.transform_result(@result_document, base_url: base_url)
    end
  rescue => err
    @body_content = format_error(error_text: err)
  end
end
