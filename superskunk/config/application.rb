require_relative "boot"

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
# require "active_record/railtie"
# require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
# require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
# require "rails/test_unit/railtie"
require_relative "../lib/superskunk/response_logger"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Superskunk
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true
    config.action_dispatch.ignore_accept_header = true

    config.log_level = ENV.fetch("RAILS_LOG_LEVEL", "info").to_sym
    config.log_formatter = ::Logger::Formatter.new

    config.to_prepare do
      # Autoload EDTF literal support.
      "RDF::EDTF::Literal".constantize
    end

    config.superskunk_api_base = ENV.fetch("SUPERSKUNK_API_BASE") { "http://superskunk" }

    config.hosts = [URI(config.superskunk_api_base).host]
    config.host_authorization = {exclude: ->(request) { request.path =~ /healthz/ }}

    if ENV["RAILS_LOG_TO_STDOUT"].present?
      logger = ActiveSupport::Logger.new($stdout)
      logger.formatter = config.log_formatter
      config.logger = ActiveSupport::TaggedLogging.new(logger)
    end

    # Configure SSL based on environment variable.
    # This makes it easier to deploy across environments, where SSL termiation
    # may take place before we hit Ruby.
    config.force_ssl = ActiveModel::Type::Boolean.new.cast(ENV.fetch("RAILS_FORCE_SSL", false))

    config.middleware.use(Superskunk::ResponseLogger)
  end
end
