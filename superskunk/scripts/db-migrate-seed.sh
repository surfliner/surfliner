#!/bin/sh
set -e

db-wait.sh "$POSTGRESQL_HOST:$POSTGRESQL_PORT"

export PGPASSWORD="${POSTGRESQL_PASSWORD}"
echo "Ensuring database: $POSTGRESQL_DATABASE exists"

psql -h "${POSTGRESQL_HOST}" -U "${POSTGRESQL_USERNAME}" -d postgres \
     -tc "SELECT 1 FROM pg_database WHERE datname = '$POSTGRESQL_DATABASE'" | \
  grep -q 1 || \
  psql -h "${POSTGRESQL_HOST}" -U "${POSTGRESQL_USERNAME}" -d postgres \
       -tc "CREATE DATABASE $POSTGRESQL_DATABASE;"

psql_base="postgres://${POSTGRESQL_USERNAME}:${POSTGRESQL_PASSWORD}@${POSTGRESQL_HOST}"
database_url="${psql_base}/${POSTGRESQL_DATABASE}"

bundle exec sequel -m ../db/sequel "$database_url"

if [[ -n "${SUPERSKUNK_SEED_DATA:+1}" ]]; then
  bundle exec rake superskunk:seed
fi
