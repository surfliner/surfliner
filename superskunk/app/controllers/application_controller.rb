##
# An abstract controller encapsulating application-wide controller behavior.
class ApplicationController < ActionController::API
  include LdApi::ControllerBehavior::ContentType
  include LdApi::ControllerBehavior::Cors
  include LdApi::ControllerBehavior::Errors

  rescue_from DiscoveryPlatform::AuthError, with: :forbidden
  rescue_from Valkyrie::Persistence::ObjectNotFoundError, with: :not_found
end
