namespace :superskunk do
  desc "Load demo seed data"
  task seed: :environment do
    Rails.logger.info { "Loading demo seed data" }
    persister = Valkyrie::MetadataAdapter.find(:comet_metadata_store).persister

    (1..10).each do |i|
      resource = Valkyrie.config.resource_class_resolver.call(:GenericObject).new(
        title: "GenericObject #{i}",
        creator: ["Author #{i}"],
        ark: "ark:/fk4/abc#{i}"
      )

      persister.save(resource: resource).tap do |r|
        Rails.logger.info { "Created a resource with id: #{r.id} and ARK: #{r.ark}" }
        permission = Hyrax::Acl::Permission.new(access_to: r.id, mode: :discover, agent: "group/surfliner.daylight")
        persister.save(resource: Hyrax::Acl::AccessControl.new(access_to: r.id, permissions: [permission]))
        Rails.logger.info { "Granted #{permission.mode} access on #{permission.access_to} to #{permission.agent}" }
      end
    end
  end
end
