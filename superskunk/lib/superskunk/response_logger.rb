module Superskunk
  class ResponseLogger
    def initialize(app)
      @app = app
    end

    def call(env)
      status, _, body = response = @app.call(env)
      Rails.logger.debug { "Response Status: #{status}\n\tBody: #{body.body}" }
      response
    end
  end
end
