import arcpy
import os
import xml.etree.ElementTree as ET
import csv
import re
import traceback


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the .pyt file)"""
        self.label = "Metadata Tools"
        self.alias = ""

        # List of tool classes associated with this toolbox
        self.tools = [Extract_Metadata_to_CSV, Create_Derivative_Files, Validate_Files, Delete_Old_Files]


def remove_HTML_Tags(text):
    #Strip embedded html tags from parameter text    
    tags = re.compile('<.*?>')
    return re.sub(tags, '', text)

        
class Extract_Metadata_to_CSV(object):
    def __init__(self):
        #Define the tool (tool name is the name of the class).
        
        self.label = "4. Extract Metadata to CSV"
        self.description = "For each shapefile or TIFF in a user-selected folder(and subfolders), export specific metadata values to a CSV file. View results window for shapefile issues."
        self.canRunInBackground = False

    def getParameterInfo(self):
        #Define parameter definitions    

        # First parameter
        param0 = arcpy.Parameter(
            displayName="Input Workspace",
            name="in_workspace",
            datatype="Workspace",
            parameterType="Required",
            direction="Input")

        params = [param0]
        return params

    def execute(self, parameters, messages):
        """The source code of the tool."""

        # Dictionary of constraint categories
        constraint = {'000': 'Empty','001': 'Copyright','002': 'Patent','003': 'Patent Pending','004': 'Trademark','005': 'License','006': 'Intellectual Property Rights', '007': 'Restricted','008' : 'Other Restrictions'}
        
        try:
            # arcpy environments
            arcpy.env.overwriteOutput = "True"

            # Set the input workspace
            arcpy.env.workspace = parameters[0].valueAsText

            # create new csv output file
            csvFile = os.path.join(parameters[0].valueAsText, "Metadata_Extract.csv") 

            csvWriter = csv.writer(open(csvFile, 'w', newline=''))
            
            # create header list
            row = ["source_identifier", "model", "parents", "collectionTitle1", "collectionTitle", "use:PreservationFile", "use:IntermediateFile", "use:ServiceFile", "visibility", "format1", "format_geo", 
            "resource_class_geo", "resource_type_geo", "title1", "title", "title_alternative_geo", "date1", "date_index_geo", "date_issued_geo", "subject_temporal_geo", "creatorName", "creatorOrg",
            "creator_geo", "description_geo_1", "description_geo_2", "language_geo", "publisherName", "publisherOrg", "publisher_geo", "subject1", "subject_topic_geo", "subject_keyword_geo", "spatialCoverage1",
            "subject_spatial_geo", "useLimitation1", "useLimitation2", "accessConst", "useConst", "otherConst", "rights_note_geo", "license_geo", "rights_holder_geo", "isPartOf", "source_geo", "replaces_geo",
            "is_replaced_by_geo", "is_version_of_geo", "relation_geo", "bounding_box_west_geo", "bounding_box_east_geo", "bounding_box_north_geo", "bounding_box_south_geo", "georeferenced_geo"]

            # write header to csv file
            csvWriter.writerow([v for v in row])

            # Step through the user-selected folder and extract metadata values
            for dirPath, dirNames, fileNames in os.walk(arcpy.env.workspace):
                dirNames.sort()
                for file in fileNames:
                    if file.lower().endswith(".shp.xml") or file.lower().endswith(".tif.xml"):
                        formatText = titleText = dateText = originatorNameText = originatorOrgText = descriptionText = languageText = publisherNameText = publisherOrgText = ""
                        subjectText = collectionTitleText = subjectSpatialText = useLimitationText1 = useLimitationText2 = accessConstText = useConstText = otherConstText = ""
                        westText = eastText = northText = southText = ""
                        arcpy.AddMessage("******************************************")

                        # read in XML
                        tree = ET.parse(os.path.join(dirPath, file))
                        root = tree.getroot()

                        # upgrade FGDC metadata content to ArcGIS metadata format
                        metadataUpgradeNeeded = True
                        EsriElt = root.find('Esri')
                        if ET.iselement(EsriElt):
                            ArcGISFormatElt = EsriElt.find('ArcGISFormat')
                            if ET.iselement(ArcGISFormatElt):
                                arcpy.AddMessage(file + ": metadata is already in ArcGIS format.")
                                metadataUpgradeNeeded = False

                        if metadataUpgradeNeeded:
                            #arcpy.AddMessage(file + ": metadata is being upgraded to ArcGIS format...")
                            #result = arcpy.UpgradeMetadata_conversion(os.path.join(dirPath, file), "FGDC_TO_ARCGIS")
                            arcpy.AddMessage("***********************************************************************************************************************")
                            arcpy.AddMessage(file + ": METADATA NEEDS UPGRADE TO ARCGIS FORMAT! UPGRADE METADATA FOR THIS FILE IN ARCCATALOG, THEN RERUN THIS TOOL.")
                            arcpy.AddMessage("***********************************************************************************************************************")

                            # reload xml tree after upgrade
                            #tree = ET.parse(os.path.join(dirPath, file))
                            #root = tree.getroot()

                        crucialEltsFound = True
                        dataIdInfoElt = root.find('dataIdInfo')
                        if ET.iselement(dataIdInfoElt):
                            idCitationElt = dataIdInfoElt.find('idCitation')
                            if not ET.iselement(idCitationElt):
                                crucialEltsFound = False
                            else:
                                crucialEltsFound = False
                                for child in dataIdInfoElt:
                                    if child.tag == 'dataExt':
                                        geoEleElt = child.find('geoEle')
                                        if ET.iselement(geoEleElt):
                                            GeoBndBoxElt = geoEleElt.find('GeoBndBox')
                                            if ET.iselement(GeoBndBoxElt):
                                                if GeoBndBoxElt.get('esriExtentType') == 'search': crucialEltsFound = True
                        else: crucialEltsFound = False
                        
                        if crucialEltsFound == False:
                            arcpy.AddMessage("*********************************************************************************************************************************")
                            arcpy.AddMessage(file + ": CRUCIAL METADATA ELEMENTS MISSING! OPEN METADATA FOR THIS FILE IN ARCCATALOG TO UPDATE, THEN RERUN THIS TOOL.")
                            arcpy.AddMessage("*********************************************************************************************************************************")
                            continue


                        ##################################################################################################
                        # Extract format from XML file

                        distInfoElt = root.find('distInfo')
                        if ET.iselement(distInfoElt):
                            distFormatElt = distInfoElt.find('distFormat')
                            if ET.iselement(distFormatElt):
                                formatNameElt = distFormatElt.find('formatName')
                                if ET.iselement(formatNameElt):
                                    formatText = formatNameElt.text

                        ##################################################################################################
                        # Extract description from XML file
                         
                        idAbsElt = dataIdInfoElt.find('idAbs')
                        if ET.iselement(idAbsElt):
                            descriptionText = remove_HTML_Tags(idAbsElt.text)

                        ##################################################################################################
                        # Extract language from XML file

                        dataLangElt = dataIdInfoElt.find('dataLang')
                        if ET.iselement(dataLangElt):
                            languageCodeElt = dataLangElt.find('languageCode')
                            if ET.iselement(languageCodeElt):
                                languageText = languageCodeElt.get('value')
                                
                        if not languageText: arcpy.AddMessage(file + ": note - no data language found")

                        ##################################################################################################
                        # Extract theme and place keywords from XML file
                        
                        for child1 in dataIdInfoElt:
                            if child1.tag == 'themeKeys':
                                for child2 in child1:
                                    if child2.tag == 'keyword':
                                        subjectText = subjectText + "|" + child2.text.rstrip()

                        subjectText = subjectText[1:len(subjectText)]
                                
                        for child1 in dataIdInfoElt:
                            if child1.tag == 'placeKeys':
                                for child2 in child1:
                                    if child2.tag == 'keyword':
                                        subjectSpatialText = subjectSpatialText + "|" + child2.text.rstrip()

                        subjectSpatialText = subjectSpatialText[1:len(subjectSpatialText)]
                        if not subjectSpatialText: arcpy.AddMessage(file + ": note - no spatial coverage keywords found")

                        ##################################################################################################
                        # Extract general constraints from XML file

                        for child in dataIdInfoElt:
                            if child.tag == 'resConst':
                                constsElt = child.find('Consts')
                                if ET.iselement(constsElt):
                                    useLimitElt = constsElt.find('useLimit')
                                    if ET.iselement(useLimitElt):
                                        useLimitationText1 = useLimitationText1 + "|" + remove_HTML_Tags(useLimitElt.text)

                        if useLimitationText1.startswith("|"): useLimitationText1 = useLimitationText1[1:]
                        if not useLimitationText1: arcpy.AddMessage(file + ": note - no general use limitation found")

                        ##################################################################################################
                        # Extract legal constraints from XML file

                        for child in dataIdInfoElt:
                            if child.tag == 'resConst':
                                legConstsElt = child.find('LegConsts')
                                if ET.iselement(legConstsElt):
                                    for subElt in legConstsElt:
                                        if subElt.tag == 'accessConsts':
                                            restrictCdElt = subElt.find('RestrictCd')
                                            if ET.iselement(restrictCdElt):
                                                #e.g. <RestrictCd value="001"/>
                                                listValue = constraint[restrictCdElt.get('value')]
                                                accessConstText = accessConstText + "|" + listValue
                                        elif subElt.tag == 'useConsts':
                                            restrictCdElt = subElt.find('RestrictCd')
                                            if ET.iselement(restrictCdElt):
                                                #e.g. <RestrictCd value="001"/>
                                                listValue = constraint[restrictCdElt.get('value')]
                                                useConstText = useConstText + "|" + listValue
                                        elif subElt.tag == 'useLimit':
                                            useLimitationText2 = useLimitationText2 + "|" + subElt.text
                                        elif subElt.tag == 'othConsts':
                                            otherConstText = otherConstText + "|" + subElt.text

                        if accessConstText.startswith("|"): accessConstText = accessConstText[1:]
                        if useConstText.startswith("|"): useConstText = useConstText[1:]
                        if useLimitationText2.startswith("|"): useLimitationText2 = useLimitationText2[1:]
                        if otherConstText.startswith("|"): otherConstText = otherConstText[1:]

                        if not accessConstText: arcpy.AddMessage(file + ": note - no legal access constraint found")
                        if not useConstText: arcpy.AddMessage(file + ": note - no legal use constraints found")
                        if not useLimitationText2: arcpy.AddMessage(file + ": note - no legal use limitation found")
                        if not otherConstText: arcpy.AddMessage(file + ": note - no legal other constraint found")

                        ##################################################################################################
                        # Extract bounding box extent values from XML file

                        for child in dataIdInfoElt:
                            if child.tag == 'dataExt':
                                geoEleElt = child.find('geoEle')
                                if not ET.iselement(geoEleElt): continue
                                GeoBndBoxElt = geoEleElt.find('GeoBndBox')
                                if not ET.iselement(GeoBndBoxElt): continue
                                if GeoBndBoxElt.get('esriExtentType') != 'search': continue
                                westBLElt = GeoBndBoxElt.find('westBL')
                                if ET.iselement(westBLElt): westText = westBLElt.text
                                eastBLElt = GeoBndBoxElt.find('eastBL')
                                if ET.iselement(eastBLElt): eastText = eastBLElt.text
                                northBLElt = GeoBndBoxElt.find('northBL')
                                if ET.iselement(northBLElt): northText = northBLElt.text
                                southBLElt = GeoBndBoxElt.find('southBL')
                                if ET.iselement(southBLElt): southText = southBLElt.text
                                
                        ##################################################################################################
                        # Extract title from XML file

                        resTitleElt = idCitationElt.find('resTitle')
                        if ET.iselement(resTitleElt): titleText = resTitleElt.text
                        if not titleText:
                            idinfoElt = root.find('idinfo')
                            if ET.iselement(idinfoElt):
                                citationElt = idinfoElt.find('citation')
                                if ET.iselement(citationElt):
                                    citeinfoElt = citationElt.find('citeinfo')
                                    if ET.iselement(citeinfoElt):
                                        titleElt = citeinfoElt.find('title')
                                        if ET.iselement(titleElt):
                                            titleText = titleElt.text

                        if not titleText: arcpy.AddMessage(file + ": note - no title found")

                        ##################################################################################################
                        # Extract originator from XML file

                        for child in idCitationElt:
                            if child.tag == 'citRespParty':
                                roleElt = child.find('role')
                                if ET.iselement(roleElt):
                                    roleCdElt = roleElt.find('RoleCd')
                                    if ET.iselement(roleCdElt):
                                        if roleCdElt.get('value') == '006':
                                            rpIndNameElt = child.find('rpIndName')
                                            if ET.iselement(rpIndNameElt):
                                                originatorNameText = originatorNameText + "|" + rpIndNameElt.text

                                            rpOrgNameElt = child.find('rpOrgName')
                                            if ET.iselement(rpOrgNameElt):
                                                originatorOrgText = originatorOrgText + "|" + rpOrgNameElt.text

                        if originatorNameText.startswith("|"): originatorNameText = originatorNameText[1:]
                        if originatorOrgText.startswith("|"): originatorOrgText = originatorOrgText[1:]

                        if not originatorNameText: arcpy.AddMessage(file + ": note - no originator name found")
                        if not originatorOrgText: arcpy.AddMessage(file + ": note - no originator organization found")
                        
                        ##################################################################################################
                        # Extract publisher from XML file

                        for child in idCitationElt:
                            if child.tag == 'citRespParty':
                                roleElt = child.find('role')
                                if ET.iselement(roleElt):
                                    roleCdElt = roleElt.find('RoleCd')
                                    if ET.iselement(roleCdElt):
                                        if roleCdElt.get('value') == '010':
                                            rpIndNameElt = child.find('rpIndName')
                                            if ET.iselement(rpIndNameElt):
                                                publisherNameText = publisherNameText + "|" + rpIndNameElt.text

                                            rpOrgNameElt = child.find('rpOrgName')
                                            if ET.iselement(rpOrgNameElt):
                                                publisherOrgText = publisherOrgText + "|" + rpOrgNameElt.text

                        if publisherNameText.startswith("|"): publisherNameText = publisherNameText[1:]
                        if publisherOrgText.startswith("|"): publisherOrgText = publisherOrgText[1:]

                        if not publisherNameText: arcpy.AddMessage(file + ": note - no publisher name found")
                        if not publisherOrgText: arcpy.AddMessage(file + ": note - no publisher organization found")
                        
                        ##################################################################################################
                        # Extract collection title from XML file

                        collTitleElt = idCitationElt.find('collTitle')
                        if ET.iselement(collTitleElt):
                            collectionTitleText = collTitleElt.text

                        if not collectionTitleText: arcpy.AddMessage(file + ": note - no collection title found")

                        ##################################################################################################
                        # Extract published date from XML file

                        dateElt = idCitationElt.find('date')
                        if ET.iselement(dateElt):
                            pubDateElt = dateElt.find('pubDate')
                            if ET.iselement(pubDateElt):
                                dateText = pubDateElt.text
                                if dateText:
                                    if dateText.find("T"):
                                        temp = pubDateElt.text.split("T")
                                        dateText = temp[0]

                        if not dateText:
                            arcpy.AddMessage(file + ": note - no published date found")
                            dateText = ""

                        ##################################################################################################                                
                        # Create source_identifier and use file names

                        folderName = os.path.basename(dirPath)
                        sourceIdentifier = 'gis' + folderName
                        parentName = 'gis' + folderName[0:folderName.find('_')]
                        preservationFilename = 'geodata/gis' + folderName + '-a.zip'
                        serviceFilename = 'geodata/gis' + folderName + '-d.zip'

                        # Write extracted data to CSV file
                        """
                        # csv header listed here for reference
                        ["source_identifier", "model", "parents", "collectionTitle1", "collectionTitle", "use:PreservationFile", "use:IntermediateFile", "use:ServiceFile", "visibility", "format1",
                        "format_geo", "resource_class_geo", "resource_type_geo", "title1", "title", "title_alternative_geo", "date1", "date_index_geo", "date_issued_geo", "subject_temporal_geo",
                        "creatorName", "creatorOrg", "creator_geo", "description_geo_1", "description_geo_2", "language_geo", "publisherName", "publisherOrg", "publisher_geo", "subject1",
                        "subject_topic_geo", "subject_keyword_geo", "spatialCoverage1", "subject_spatial_geo", "useLimitation1", "useLimitation2", "accessConst", "useConst", "otherConst",
                        "rights_note_geo", "license_geo", "rights_holder_geo", "isPartOf", "source_geo", "replaces_geo", "is_replaced_by_geo", "is_version_of_geo", "relation_geo",
                        "bounding_box_west_geo", "bounding_box_east_geo", "bounding_box_north_geo", "bounding_box_south_geo", "georeferenced_geo"]
                        """

                        row = [sourceIdentifier, "GeospatialObject", parentName, collectionTitleText, "", preservationFilename, preservationFilename, serviceFilename, "open", formatText,
                        "", "", "", titleText, "", "", dateText, "", "", "", originatorNameText,
                        originatorOrgText, "", descriptionText, "Title supplied by cataloger.", languageText, publisherNameText, publisherOrgText, "", subjectText, "", "",
                        subjectSpatialText, "", useLimitationText1, useLimitationText2, accessConstText, useConstText, otherConstText, "", "", "", "",
                        "", "", "", "", "", westText, eastText, northText, southText, ""]

                        csvWriter.writerow([v for v in row])
            
            del csvFile
            del csvWriter
            del tree
            del root

        except Exception as err:
            arcpy.AddMessage(arcpy.GetMessages(2))
            print (arcpy.GetMessages(2))
            arcpy.AddMessage(traceback.format_exc())
            raise arcpy.ExecuteError
            return
        
    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return


####################################################################################################################################################################################################
####################################################################################################################################################################################################


class Validate_Files(object):
    def __init__(self):
        #Define the tool (tool name is the name of the class).

        self.label = "1. Validate Files"
        self.description = "Checks each shapefile and TIFF in a user-selected folder (and subfolders) for undefined projection, missing crucial metadata elements, and metadata requiring upgrade to ArcGIS format."
        self.canRunInBackground = False

    def getParameterInfo(self):
        #Define parameter definitions    

        # First parameter
        param0 = arcpy.Parameter(
            displayName="Input Workspace",
            name="in_workspace",
            datatype="Workspace",
            parameterType="Required",
            direction="Input")
                                
        params = [param0]
        return params


    def execute(self, parameters, messages):
        """The source code of the tool."""

        try:
            # arcpy environments
            arcpy.env.overwriteOutput = "True"

            # Set the input workspace
            arcpy.env.workspace = parameters[0].valueAsText
            arcpy.AddMessage("workspace = " + arcpy.env.workspace)

            arcpy.AddMessage("Checking shapefiles and TIFFs for undefined coordinate systems")
            arcpy.AddMessage("****************************")

            #for dirPath, dirNames, fileNames in arcpy.da.Walk(arcpy.env.workspace, datatype="FeatureClass"):
            for dirPath, dirNames, fileNames in os.walk(arcpy.env.workspace):
                for file in fileNames:
                    if file.lower().endswith(".shp") or file.lower().endswith(".tif"):
                        if arcpy.Exists(os.path.join(dirPath, file) + ".xml") == False:
                            arcpy.AddMessage("*****************************************************************************************************************")
                            arcpy.AddMessage(file + ": METADATA FILE MISSING! OPEN METADATA FOR THIS FILE IN ARCCATALOG TO CREATE FILE, THEN RERUN THIS TOOL.")
                            arcpy.AddMessage("*****************************************************************************************************************")

                        dsc = arcpy.Describe(os.path.join(dirPath, file))
                        coordSys = dsc.spatialReference
                        if coordSys.Name == "Unknown":
                            arcpy.AddMessage("Coord Sys: " + coordSys.Name)
                            arcpy.AddMessage("*****************************************************************************************************************")
                            arcpy.AddMessage(file + ": COORDINATE SYSTEM UNDEFINED! RUN DEFINE PROJECTION TOOL ON THIS FILE.")
                            arcpy.AddMessage("*****************************************************************************************************************")

                    
            # Step through the user-selected folder and check for missing metadata values
            for dirPath, dirNames, fileNames in os.walk(arcpy.env.workspace):
                dirNames.sort()
                for file in fileNames:
                    if file.lower().endswith(".shp.xml") or file.lower().endswith(".tif.xml"):
                        # read in XML
                        tree = ET.parse(os.path.join(dirPath, file))
                        root = tree.getroot()

                        # upgrade FGDC metadata content to ArcGIS metadata format
                        metadataUpgradeNeeded = True
                        EsriElt = root.find('Esri')
                        if ET.iselement(EsriElt):
                            ArcGISFormatElt = EsriElt.find('ArcGISFormat')
                            if ET.iselement(ArcGISFormatElt):
                                arcpy.AddMessage(file + ": metadata is already in ArcGIS format.")
                                metadataUpgradeNeeded = False

                        if metadataUpgradeNeeded:
                            #arcpy.AddMessage(file + ": metadata is being upgraded to ArcGIS format...")
                            #result = arcpy.UpgradeMetadata_conversion(os.path.join(dirPath, file), "FGDC_TO_ARCGIS")
                            arcpy.AddMessage("***********************************************************************************************************************")
                            arcpy.AddMessage(file + ": METADATA NEEDS UPGRADE TO ARCGIS FORMAT! UPGRADE METADATA FOR THIS FILE IN ARCCATALOG, THEN RERUN THIS TOOL.")
                            arcpy.AddMessage("***********************************************************************************************************************")

                            # reload xml tree after upgrade
                            #tree = ET.parse(os.path.join(dirPath, file))
                            #root = tree.getroot()
                            
                        crucialEltsFound = True
                        dataIdInfoElt = root.find('dataIdInfo')
                        if ET.iselement(dataIdInfoElt):
                            idCitationElt = dataIdInfoElt.find('idCitation')
                            if not ET.iselement(idCitationElt):
                                crucialEltsFound = False
                            else:
                                crucialEltsFound = False
                                for child in dataIdInfoElt:
                                    if child.tag == 'dataExt':
                                        geoEleElt = child.find('geoEle')
                                        if ET.iselement(geoEleElt):
                                            GeoBndBoxElt = geoEleElt.find('GeoBndBox')
                                            if ET.iselement(GeoBndBoxElt):
                                                if GeoBndBoxElt.get('esriExtentType') == 'search': crucialEltsFound = True
                        else: crucialEltsFound = False
                        
                        if crucialEltsFound == False:
                            arcpy.AddMessage("*********************************************************************************************************************************")
                            arcpy.AddMessage(file + ": CRUCIAL METADATA ELEMENTS MISSING! OPEN METADATA FOR THIS FILE IN ARCCATALOG TO UPDATE.")
                            arcpy.AddMessage("*********************************************************************************************************************************")
                    
            del file
            
        except Exception as err:
            arcpy.AddMessage(arcpy.GetMessages(2))
            print (arcpy.GetMessages(2))
            arcpy.AddMessage(traceback.format_exc())
            raise arcpy.ExecuteError
            return
        
    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return


####################################################################################################################################################################################################
####################################################################################################################################################################################################

class Create_Derivative_Files(object):
    def __init__(self):
        #Define the tool (tool name is the name of the class).

        self.label = "2. Create WGS 1984 Derivatives"
        self.description = "For each shapefile or GeoTIFF in a user-selected folder (and subfolders), create a copy with spatial reference 'WGS 1984'. \
                            The original shapefile or GeoTIFF is renamed with suffix '_OLD'. \
                            Note - if the projection for an original file is undefined, the derivative is not created."
        self.canRunInBackground = False

    def getParameterInfo(self):
        #Define parameter definitions    

        # First parameter
        param0 = arcpy.Parameter(
            displayName="Input Workspace",
            name="in_workspace",
            datatype="Workspace",
            parameterType="Required",
            direction="Input")
                                
        params = [param0]
        return params


    def execute(self, parameters, messages):
        """The source code of the tool."""

        try:
            # arcpy environments
            arcpy.env.overwriteOutput = "True"

            # Set the input workspace
            arcpy.env.workspace = parameters[0].valueAsText
            arcpy.AddMessage("Workspace = " + arcpy.env.workspace)

            # Step through the user-selected folder and reproject shapefiles if coordinate system is defined
            for (dirPath, dirNames, fileNames) in os.walk(arcpy.env.workspace):
                for file in fileNames:
                    if file.endswith('.shp') or file.endswith('.tif'):
                        dsc = arcpy.Describe(os.path.join(dirPath, file))
                        coordSys = dsc.spatialReference
                        
                        if coordSys.Name != "Unknown":
                            # set output coordinate system (EPSG 4326)
                            spatialRef = arcpy.SpatialReference("WGS 1984")
                            
                            if file.endswith('.shp'): oldFilename = file.replace(".shp", "_OLD.shp")
                            elif file.endswith('.tif'): oldFilename = file.replace(".tif", "_OLD.tif")
                            
                            fileOld = os.path.join(dirPath, oldFilename)
                            fileProject = os.path.join(dirPath, file)
                            arcpy.Rename_management(os.path.join(dirPath, file), os.path.join(dirPath, oldFilename))
                            
                            arcpy.AddMessage("**************")
                            arcpy.AddMessage("Reprojecting file: " + file)
                            
                            # run project tool
                            if file.endswith('.shp'): arcpy.Project_management(fileOld, fileProject, spatialRef)
                            elif file.endswith('.tif'): arcpy.ProjectRaster_management(fileOld, fileProject, spatialRef)
                            
                            # run synchronize metadata tool
                            #arcpy.SynchronizeMetadata_conversion(fileProject, "SELECTIVE")
                        else:
                            arcpy.AddMessage("*****************************************************************************************************************")
                            arcpy.AddMessage(file + ": COORDINATE SYSTEM UNDEFINED! RUN DEFINE PROJECTION TOOL ON THIS FILE, THEN REPROJECT TO WGS 1984 MANUALLY.")
                            arcpy.AddMessage("*****************************************************************************************************************")
            
            del dsc
            del file
            del fileOld
            del fileProject

        except Exception as err:
            arcpy.AddMessage(arcpy.GetMessages(2))
            print (arcpy.GetMessages(2))
            arcpy.AddMessage(traceback.format_exc())
            raise arcpy.ExecuteError
            return
        
    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

####################################################################################################################################################################################################
####################################################################################################################################################################################################

class Delete_Old_Files(object):
    def __init__(self):
        #Define the tool (tool name is the name of the class).

        self.label = "3. Delete _OLD Files"
        self.description = "Deletes each shapefile or TIFF with the suffix '_OLD' in a user-selected folder (and subfolders)."
        self.canRunInBackground = False

    def getParameterInfo(self):
        #Define parameter definitions    

        # First parameter
        param0 = arcpy.Parameter(
            displayName="Input Workspace",
            name="in_workspace",
            datatype="Workspace",
            parameterType="Required",
            direction="Input")
                                
        params = [param0]
        return params


    def execute(self, parameters, messages):
        """The source code of the tool."""

        try:
            # arcpy environments
            arcpy.env.overwriteOutput = "True"

            # Set the input workspace
            arcpy.env.workspace = parameters[0].valueAsText
            arcpy.AddMessage("workspace = " + arcpy.env.workspace)

            # Step through the user-selected folder
            for (dirPath, dirNames, fileNames) in os.walk(arcpy.env.workspace):
                for file in fileNames:
                    if file.endswith('_OLD.shp') or file.endswith('_OLD.tif'):
                        arcpy.AddMessage("**************")
                        arcpy.AddMessage("Deleting: " + file)
                        arcpy.Delete_management(os.path.join(dirPath, file))
                    
            del file

        except Exception as err:
            arcpy.AddMessage(arcpy.GetMessages(2))
            print (arcpy.GetMessages(2))
            arcpy.AddMessage(traceback.format_exc())
            raise arcpy.ExecuteError
            return
        
    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return


