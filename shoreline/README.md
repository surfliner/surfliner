# Shoreline

Shorline is a [GeoBlacklight][geoblacklight] frontend for the Shoreline
geospatial system.

Please see [CONTRIBUTING.md][contributing] for information about contributing to
this project, and [the manual](../docs/themanual) for technical documentation.
By participating, you agree to abide by the [UC Principles of
Community][principles].

## Developing

Run `make build && make up` or, from the root directory of the repository,
```
docker compose --profile shoreline build
docker compose --profile shoreline up
```

The application will be available at <http://localhost:3001>.

For running tests:
```
docker-compose --profile shoreline exec web bundle exec rspec
```

See the [`docker-compose` CLI
reference](https://docs.docker.com/compose/reference/overview/) for more on commands.

## [Deployment](doc/deploy.md)

## [Ingesting objects](doc/ingest.md)

[contributing]: ../CONTRIBUTING.md
[geoblacklight]: https://github.com/geoblacklight/geoblacklight
[localhost]: http://localhost:3000
[principles]: https://ucnet.universityofcalifornia.edu/working-at-uc/our-values/principles-of-community.html
[rails]: https://rubyonrails.org/
[solr]: http://lucene.apache.org/solr/
