# frozen_string_literal: true

require "rails_helper"

RSpec.describe ItemController do
  describe "#index", :integration do
    let(:source_iri) { "http://superskunk.example.com/1234" }
    let(:aardvark) do
      JSON.parse(File.read(Rails.root.join(
        "spec",
        "fixtures",
        "aardvark-metadata",
        "gford-20140000-010002_lakes.json"
      )))
    end

    it "routes to /item" do
      expect(get: "/item").to be_routable
    end

    it "gives bad request when no source_iri param provided" do
      expect { get :index }.to raise_error(ActionController::BadRequest)
    end

    context "with valid source_iri" do
      let(:superskunk_uri) { "http://superskunk/id1234" }
      let(:document_id) { "gford-20140000-010002_lakes" }
      before {
        metadata_with_superskunk_uri = aardvark.merge({"superskunk_uri_s" => superskunk_uri})
        Importer.publish_to_geoblacklight(metadata: metadata_with_superskunk_uri)
      }

      it "redirects to the catalog controller using the catalog id" do
        get :index, params: {source_iri: superskunk_uri}

        expect(response).to have_http_status(302)
        expect(response).to redirect_to %r{/catalog/#{document_id}}
      end
    end

    context "with invalid source_iri" do
      it "raises error ActiveRecord::RecordNotFound" do
        expect {
          get :index, params: {source_iri: "invalid-source-iri"}
        }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
