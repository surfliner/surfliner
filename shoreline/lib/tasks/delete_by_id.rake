# frozen_string_literal: true

$stdout.sync = true

require "rsolr"

namespace :shoreline do
  desc "Delete objects from Solr by ID"
  task delete_by_ids: :environment do
    abort "--- Error: environment variable SOLR_DELETE_IDS is unset" if ENV["SOLR_DELETE_IDS"].blank?

    solr_url = "http://#{ENV["SOLR_ADMIN_USER"]}:#{ENV["SOLR_ADMIN_PASSWORD"]}@#{ENV["SOLR_HOST"]}:#{ENV["SOLR_PORT"]}/solr/#{ENV["SOLR_COLLECTION_NAME"]}"

    client = RSolr.connect(url: solr_url)
    client.delete_by_id ENV["SOLR_DELETE_IDS"].split(",")
    client.commit
  end
end
