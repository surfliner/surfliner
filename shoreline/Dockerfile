# renovate: datasource=docker depName=alpine
ARG ALPINE_VERSION=3.19
# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=3.2.2

ARG IMAGE_REGISTRY='docker.io/'
# =================
# Shared base image
# =================
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION} as build-base

COPY scripts/docker/*.sh /home/shoreline/app/

# used in alpine-install.sh
ENV APKS='build-base libxml2-dev libxslt-dev nodejs postgresql-dev shared-mime-info yarn'
RUN /home/shoreline/app/alpine-install.sh

COPY shoreline/Gemfile* /home/shoreline/app/
COPY gems /home/shoreline/gems/

WORKDIR /home/shoreline/app

# ==========================
# Build base for prod images
# ==========================
FROM build-base as ruby-build-prod
RUN /home/shoreline/app/ruby-bundle-prod.sh
# used in ruby-bundle-cleanup.sh for locating the bundle cache
ENV RUBY_ABI=3.2.0
ENV PROJECT_PATH=/home/shoreline/app
RUN /home/shoreline/app/ruby-bundle-cleanup.sh

COPY shoreline /home/shoreline/app
# There are symlinks into this folder which aren’t followed by COPY; it needs to be copied in separately.
COPY themes /home/shoreline/themes

RUN bundle exec rake assets:precompile


# ==========================
# Prod image for web service
# ==========================
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION} as shoreline-prod

COPY scripts/docker/*.sh /home/shoreline/app/

ENV APKS='curl libpq nodejs shared-mime-info tini tzdata zip'
RUN /home/shoreline/app/alpine-install.sh

USER 10000
WORKDIR /home/shoreline/app

COPY --chown=10000:10001 gems /home/shoreline/gems/
COPY --chown=10000:10001 shoreline /home/shoreline/app
COPY --chown=10000:10001 scripts/* /home/shoreline/app/scripts/
ENV PATH="/home/shoreline/app/scripts:${PATH}"

COPY --from=ruby-build-prod --chown=10000:10001 /home/shoreline/app/public/assets /home/shoreline/app/public/assets
COPY --from=ruby-build-prod --chown=10000:10001 /home/shoreline/app/vendor /home/shoreline/app/vendor
ENV BUNDLE_PATH="/home/shoreline/app/vendor/bundle"

RUN bundle config set deployment 'true'
RUN bundle config set without 'test development'

# TODO: should these be set by helm/docker-compose?
ENV RAILS_ROOT=/home/shoreline/app
ENV RAILS_SERVE_STATIC_FILES=1
ENTRYPOINT ["tini", "--", "bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]


# ===============
# Local dev image
# ===============
FROM build-base as shoreline-dev

RUN apk --no-cache add \
  curl \
  less \
  libpq \
  nodejs \
  shared-mime-info \
  tzdata \
  zip

RUN gem update bundler
RUN bundle install --jobs "$(nproc)"

COPY shoreline /home/shoreline/app

COPY scripts/* /home/shoreline/app/scripts/
ENV PATH="/home/shoreline/app/scripts:${PATH}"

# There are symlinks into this folder which aren’t followed by COPY; it needs to be copied in separately.
COPY themes /home/shoreline/themes

ENV RAILS_ROOT=/home/shoreline/app
ENV RAILS_SERVE_STATIC_FILES=1

RUN bundle exec rake assets:precompile

ENTRYPOINT ["bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
