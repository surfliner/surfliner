module GeoserverDownloadUrl
  # by default this is
  # @document.references.send(@options[:service_type]).endpoint which is the
  # external URL, while we need our internal one
  def url
    url = "#{ENV["GEOSERVER_INTERNAL_URL"]}/geoserver/#{@options[:service_type]}"
    url += "/reflect" if @options[:reflect]
    url
  end
end

Geoblacklight::Download.class_eval do
  prepend GeoserverDownloadUrl
end
