class ItemController < ApplicationController
  def index
    source_iri = params.fetch(:source_iri)

    response = Blacklight.default_index.connection.get("select",
      params: {df: "superskunk_uri_s", q: source_iri})
    result = response["response"]["docs"].first

    raise ActiveRecord::RecordNotFound.new if result.nil?

    redirect_to solr_document_path(id: result["id"])
  rescue ActionController::ParameterMissing
    raise ActionController::BadRequest.new, "Parameter source_iri is missing."
  end
end
