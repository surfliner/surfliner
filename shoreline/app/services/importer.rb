# frozen_string_literal: true

require "cgi"
require "fileutils"

##
# @see doc/deploy.md
module Importer
  SHAPEFILE = "shapefile"
  RASTER = "raster"
  REFERENCES = {
    wfs: "#{ENV["GEOSERVER_URL"]}/geoserver/wfs",
    wms: "#{ENV["GEOSERVER_URL"]}/geoserver/wms"
  }

  # @param [String] id Of the resource to delete
  def self.delete(id:)
    Rails.logger.debug "Deleting resource document from Solr index with id: #{id}"

    client = RSolr.connect(url: ENV["SOLR_URL"])
    solrdoc = client.get "select", params: {q: "id:#{id}"}
    file_id = solrdoc["response"]["docs"].first["gbl_wxsIdentifier_s"]&.sub(/^public:/, "")

    begin
      client.delete_by_id(id)
      client.commit
    rescue => e
      Rails.logger.error("Error deleting document with id #{id} from Solr: #{e}")
    end

    unless file_id.nil?
      begin
        Rails.logger.debug "Deleting resource files from GeoServer: #{file_id}"
        conn = Geoserver::Publish::Connection.new(
          "url" => "#{ENV["GEOSERVER_INTERNAL_URL"]}/geoserver/rest",
          "user" => ENV["GEOSERVER_ADMIN_USER"],
          "password" => ENV["GEOSERVER_ADMIN_PASSWORD"]
        )
        workspace = ENV.fetch("GEOSERVER_WORKSPACE", "public")
        Geoserver::Publish::DataStore.new(conn).delete(
          workspace_name: workspace,
          data_store_name: file_id
        )
      rescue => e
        Rails.logger.error("Error deleting #{file_id} from GeoServer: #{e}")
      end
    end
  end

  # @param [Hash] metadata
  # @param [String] file_url
  def self.ingest(metadata:, file_url: nil)
    Rails.logger.debug "Received metadata: #{metadata}"
    parsed = if metadata.is_a? String
      JSON.parse(metadata)
    else
      metadata
    end.except("@context")

    merged_metadata = if file_url.nil?
      parsed
    else
      parsed.merge({dct_references_s: geoserver_references(metadata: metadata)})
    end.with_indifferent_access

    unless file_url.nil?
      Rails.logger.info "Importing file #{file_url}"

      # GeoServer claims to support uploading shapefiles by providing a url and
      # passing `url` as the method (instead of `file` and uploading a binary
      # stream), but this is not true.  Instead we fetch the file ourselves and
      # upload the binary data.
      service_file_io = fetch_s3_service_file(uri: URI.parse(file_url), logger: Rails.logger)

      begin # go ahead and keep ingesting metadata if the file ingest fails
        # TODO: testing
        rename_file_and_publish(file_io: service_file_io, id: merged_metadata[:id])

        merged_metadata = merged_metadata.merge(resource_type(id: merged_metadata[:id], metadata: merged_metadata))
        merged_metadata = merged_metadata.merge(hash_from_geoserver(id: merged_metadata[:id]))
      rescue => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace
      end
    end

    Rails.logger.debug "Parsed metadata #{merged_metadata}"

    unless is_metadata_valid?(merged_metadata)
      raise "--- ERROR: metadata failed validation against Aardvark schema: #{metadata}"
    end

    publish_to_geoblacklight(metadata: merged_metadata)
  rescue => e
    Rails.logger.error "Error ingesting into Shoreline"
    Rails.logger.error "#{e.message}: #{e.inspect}"
    Rails.logger.error e.backtrace
    raise e
  end

  # @param [IO] file
  # @param [String] file_id
  # @param [String] format dct_format_s should be a supported value of https://opengeometadata.org/ogm-aardvark/#format-values
  def self.publish_to_geoserver(file:, file_id:, format:)
    conn = Geoserver::Publish::Connection.new(
      "url" => "#{ENV["GEOSERVER_INTERNAL_URL"]}/geoserver/rest",
      "user" => ENV["GEOSERVER_ADMIN_USER"],
      "password" => ENV["GEOSERVER_ADMIN_PASSWORD"]
    )
    workspace = ENV.fetch("GEOSERVER_WORKSPACE", "public")

    Rails.logger.info "-- Publishing to GeoServer as #{file_id} using format #{format}"

    Geoserver::Publish.create_workspace(workspace_name: workspace, connection: conn)

    case format
    when SHAPEFILE
      Geoserver::Publish::DataStore.new(conn).upload(
        workspace_name: workspace,
        data_store_name: file_id,
        file: file,
        method: "file"
      )
    when RASTER
      Geoserver::Publish::CoverageStore.new(conn).upload(
        workspace_name: workspace,
        coverage_store_name: file_id,
        format: "geotiff",
        file: file,
        method: "file"
      )
    else
      raise "Unsupported format #{format}"
    end
  rescue => e
    Rails.logger.error "Error ingesting into GeoServer"
    Rails.logger.error "#{e.message}: #{e.inspect}"
    raise e
  end

  def self.publish_to_geoblacklight(metadata:)
    solrdoc = JSON.parse(metadata.to_json)

    Rails.logger.debug "Publishing to Solr: #{solrdoc}"

    Blacklight.default_index.connection.add(solrdoc)
    Rails.logger.info "Committing changes to Solr"
    Blacklight.default_index.connection.commit
  end

  # retrieve resource type from geoserver if not present in m3 mapping
  def self.resource_type(id:, metadata:)
    unless metadata[:gbl_resourceType_sm].present?
      metadata = metadata.merge({gbl_resourceType_sm: get_layer_type("public:#{id}")})
    end
    metadata
  end

  def self.hash_from_geoserver(id:)
    {
      gbl_wxsIdentifier_s: "public:#{id}"
    }
  end

  # @param [IO] file_io
  # @param [String] id
  # Rename each file in the service zip file to use the id/ark instead of the original filename. This is needed to match
  # with the catalog ID in solr and various assumptions in the geoblacklight codebase.
  def self.rename_file_and_publish(file_io:, id:)
    format = nil

    zipout = Rails.root.join("tmp", id)
    FileUtils.mkdir_p zipout

    og = Rails.root.join("tmp", "#{id}-original.zip")

    f = File.new(og, "wb")
    f.write(file_io)
    f.close

    # rubyzip doesn't work
    Kernel.system "unzip -qq -j -d #{zipout} #{og}"

    Dir.each_child(zipout).each do |entry|
      if entry.end_with?(".shp")
        format = SHAPEFILE
      elsif entry.end_with?(".tif")
        format = RASTER
      end

      new_filename = id + entry.slice(entry.index("."), entry.length)

      Kernel.system "mv #{zipout}/#{entry} #{zipout}/#{new_filename}"
    end

    newzip = Rails.root.join("tmp", "#{id}.zip")
    Kernel.system "zip -j -r #{newzip} #{zipout}"

    # for reasons known only to itself, GeoServer seems to treat the provided ID
    # field differently when it's a geotiff versus a shapefile
    file_id = case format
    when SHAPEFILE
      "#{id}.zip"
    else
      id
    end

    publish_to_geoserver(file: File.read(newzip), file_id: file_id, format: format)
  rescue => e
    Rails.logger.error "Error renaming service file with id #{id}"
    Rails.logger.error "#{e.message}: #{e.inspect}"
    Rails.logger.error e.backtrace
    raise e
  end

  def self.fetch_s3_service_file(uri:, logger:)
    req = Net::HTTP::Get.new(uri)
    logger.debug "Querying #{uri} ..."

    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") { |http|
      http.request(req)
    }

    case res
    when Net::HTTPSuccess
      res.body
    when Net::HTTPRedirection
      logger.debug "Got a 30x response: #{res}"
      fetch_s3_service_file(uri: URI(res["location"]), logger: logger)
    else
      logger.debug "Got a non-success HTTP response:"
      logger.debug res.inspect

      raise "Failed to fetch #{uri}"
    end
  end

  # Validate the provided metadata against geoblacklight-schema-aardvark
  # @param [Hash] metadata
  def self.is_metadata_valid?(metadata)
    schema_path = Gem::Specification.find_by_name("geoblacklight").full_gem_path +
      "/schema/geoblacklight-schema-aardvark.json"
    schema = Pathname.new(schema_path)
    errors = JSONSchemer.schema(schema).validate(metadata.with_indifferent_access)
    if errors.count > 0
      puts "-- Aardvark validation errors: #{errors.map { |e| JSONSchemer::Errors.pretty(e) }}"
      false
    else
      true
    end
  end

  def self.get_layer_type(name)
    connection = Faraday.new(headers: {"Content-Type" => "application/json"}) do |conn|
      conn.request :authorization, :basic,
        ENV["GEOSERVER_ADMIN_USER"],
        ENV["GEOSERVER_ADMIN_PASSWORD"]
    end

    url = "#{ENV["GEOSERVER_INTERNAL_URL"]}/geoserver/rest/layers/#{name}.json"

    ready = false
    tries = 10
    until ready || tries == 0
      begin
        JSON.parse(connection.get(url).body)
        ready = true
      rescue => e
        warn "#{name} not ready: #{e}"
        tries -= 1
      end
      sleep 5
    end
    raise "--- ERROR: failed to get #{name} from GeoServer" if tries == 0

    json = JSON.parse(connection.get(url).body)
    rtype = json["layer"]["defaultStyle"]["name"].titlecase

    ["#{rtype} data"]
  end

  def self.geoserver_references(metadata:)
    refs = metadata["dct_references_s"] || "{}"

    JSON.parse(refs.gsub("http://localhost:8080", ENV["GEOSERVER_URL"])).merge(
      transform_reference_uris(REFERENCES)
    ).to_json
  end

  # @param [Hash] references
  # @return [Hash]
  def self.transform_reference_uris(references)
    references.map do |k, v|
      {Geoblacklight::Constants::URI[k] => v}
    rescue => e
      warn "Transforming #{k} to URI failed, skipping: #{e.message}"
    end.reduce(&:merge)
  end
end
