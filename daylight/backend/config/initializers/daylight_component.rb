ENV.select { |key, _| key.starts_with?("COMPONENT_URI__") }.each do |key, value|
  component = key.delete_prefix("COMPONENT_URI__").downcase.to_sym
  DaylightComponent[component] = value
end
