class HighlightQueries
  attr_reader :index, :limit

  def initialize(index: Blacklight.default_index, limit: 4)
    @index = index
    @limit = limit
  end

  ##
  # @return <Enumerable[Hash]>
  def current_highlights
    index.search(rows: limit).documents.map do |doc|
      {id: doc.id, label: doc.label}
    end
  end
end
