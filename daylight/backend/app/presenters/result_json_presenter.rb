class ResultJsonPresenter
  attr_reader :document

  def initialize(document)
    @document = document
  end

  def as_json
    {
      id: document["id"],
      label: label,
      metadata: {
        title: document["title_tesim"],
        creator: document["creator_ssim"]
      }
    }
  end

  def id
    document["id"]
  end

  def label
    document["title_tesim"].first || ""
  end
end
