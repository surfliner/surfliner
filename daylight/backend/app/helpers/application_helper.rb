module ApplicationHelper
  def component_json(data)
    case data
    when Hash
      data
    when NilClass
      {}
    else
      Rails.logger.info("We don't know what #{data.class}: #{data} is, but we're blindly passing it to a component's slot.")
      data
    end
  end
end
