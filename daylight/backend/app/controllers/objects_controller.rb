class ObjectsController < ApplicationController
  IIIF_TEMPLATE = ENV.fetch("DAYLIGHT_IIIF_MANIFEST_TEMPLATE")

  def show
    params.require(:id)

    @iiif_manifest = manifest(id: params[:id].parameterize)
  end

  private

  ##
  # @param id [#to_str]
  def manifest(id:)
    IIIF_TEMPLATE.sub("%s", id)
  end
end
