# renovate: datasource=docker depName=alpine
ARG ALPINE_VERSION=3.19
# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=3.2.2

ARG IMAGE_REGISTRY='docker.io/'
# =================
# Shared base image
# =================
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION} as build-base

COPY scripts/docker/*.sh /home/daylight/backend/app/

# used in alpine-install.sh
ENV APKS='build-base gcompat libxml2-dev libxslt-dev postgresql-dev tzdata'
RUN /home/daylight/backend/app/alpine-install.sh

COPY daylight/backend/Gemfile* /home/daylight/backend/app/
COPY gems /home/daylight/backend/gems/

WORKDIR /home/daylight/backend/app


# ==========================
# Build base for prod images
# ==========================
FROM build-base as ruby-build-prod
RUN /home/daylight/backend/app/ruby-bundle-prod.sh
# used in ruby-bundle-cleanup.sh for locating the bundle cache
ENV RUBY_ABI=3.2.0
ENV PROJECT_PATH=/home/daylight/backend/app
RUN /home/daylight/backend/app/ruby-bundle-cleanup.sh

COPY daylight/backend /home/daylight/backend/app


# ==========================
# Prod image for web service
# ==========================
FROM ${IMAGE_REGISTRY}ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION} as daylight-prod

COPY scripts/docker/*.sh /home/daylight/backend/app/

ENV APKS='curl libpq tini tzdata zip'
RUN /home/daylight/backend/app/alpine-install.sh

USER 10000
WORKDIR /home/daylight/backend/app

COPY --chown=10000:10001 gems /home/daylight/backend/gems/
COPY --chown=10000:10001 daylight/backend /home/daylight/backend/app
COPY --chown=10000:10001 scripts/* /home/daylight/backend/app/scripts/

COPY --from=ruby-build-prod --chown=10000:10001 /home/daylight/backend/app/vendor /home/daylight/backend/app/vendor

RUN bundle config set deployment 'true'
RUN bundle config set without 'test development'
ENV BUNDLE_PATH="/home/daylight/backend/app/vendor/bundle"

# TODO: should these be set by helm/docker-compose?
ENV PATH="/home/daylight/backend/app/scripts:${PATH}"
ENV RAILS_ROOT=/home/daylight/backend/app
ENV RAILS_SERVE_STATIC_FILES=1
ENTRYPOINT ["tini", "--", "bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]


# ===============
# Local dev image
# ===============
FROM build-base as daylight-dev

RUN apk --no-cache add \
  curl \
  less \
  libpq \
  postgresql-client \
  shared-mime-info \
  tzdata \
  zip

RUN gem update bundler
RUN bundle install --jobs "$(nproc)"

COPY daylight/backend /home/daylight/backend/app

COPY scripts/* /home/daylight/backend/app/scripts/
ENV PATH="/home/daylight/backend/app/scripts:${PATH}"

ENV RAILS_ROOT=/home/daylight/backend/app
ENV RAILS_SERVE_STATIC_FILES=1

ENTRYPOINT ["bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
