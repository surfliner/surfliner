const starthereTemplate = document.getElementById("urn:fdc:daylight:starthere");

export class MyComponentElement extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = starthereTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  update(data) {
    const shadowRoot = this.#shadowRoot;
    const startLink = shadowRoot.getElementById("start-link");
    if (data.name) {
      startLink.textContent = data.name;
    }
    if (data.url) {
      startLink.href = data.url;
    }
    // Add more logic here to build the component from parsed JSON data
  }
}

customElements.define("starthere-component", MyComponentElement);
