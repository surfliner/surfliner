const topnavTemplate = document.getElementById("urn:fdc:daylight:topnavigation");

console.log('topnav js ran')
export class MyComponentElement extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = topnavTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  update(data) {
    console.log("data: ", data);
    const shadowRoot = this.#shadowRoot;
    const navUL = shadowRoot.getElementById("site-nav");
    navUL.innerHTML = "";

    data.navItems.forEach(item => {
      const li = document.createElement('li');
      li.classList.add('nav-item');
    
      const a = document.createElement('a');
      a.classList.add('nav-link');
    
      a.href = item.url;
    
      if (item.bookmarkNum !== undefined) {
        a.id = 'bookmarks_nav'; // Assuming you want the id only for the first item
        const span = document.createElement('span');
        span.classList.add('badge', 'badge-secondary', 'bg-secondary');
        span.dataset.role = 'bookmark-counter';
        span.textContent = item.bookmarkNum;
        a.appendChild(document.createTextNode(item.text));
        a.appendChild(span);
      } else {
        a.textContent = item.text;
      }
    
      li.appendChild(a);
      navUL.appendChild(li);
    })
  } //update
} //class

customElements.define("topnavigation-component", MyComponentElement);
