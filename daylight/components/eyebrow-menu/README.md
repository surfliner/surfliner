# Eyebrow Component

## Overview
The **Eyebrow Component** is a custom web component designed to display a top navigation bar with a campus name and configurable links.

## Features
- Toggleable navigation menu for smaller screens
- Fully accessible with keyboard navigation

## Installation
Include the component in your HTML file:

```html
<eyebrow-component></eyebrow-component>
```

Ensure the JavaScript file is included and loaded as a module:

```html
<script src="eyebrow.js" type="module"></script>
```

## Expected JSON Structure
The component expects a JSON structure within a `<script>` tag inside a `<slot>` element:

```json
{
  "campus": {
    "title": "University of Anywhere, Anywhere",
    "url": "https://www.example.com"
  },
  "navigation": [
    {"title": "About", "href": "about.html", "active": false},
    {"title": "FAQ", "href": "faq.html", "active": false},
    {"title": "Usage Guidelines", "href": "usage.html", "active": false},
    {"title": "Contact", "href": "contact.html", "active": false}
  ]
}
```

To emit the campus link, remove the campus key/pair value:

```json
{
  "navigation": [
    {"title": "About", "href": "about.html", "active": false},
    {"title": "FAQ", "href": "faq.html", "active": false},
    {"title": "Usage Guidelines", "href": "usage.html", "active": false},
    {"title": "Contact", "href": "contact.html", "active": false}
  ]
}
```