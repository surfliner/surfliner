// components/eyebrow/eyebrow.js
const eyebrowTemplate = document.getElementById(
  "urn:fdc:adrl:20240625:adrl:eyebrow",
);
class MyComponentElement extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = eyebrowTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ 
      mode: "open",
      delegatesFocus: true,
     });

    const css = `
       @import url('https://webfonts.brand.ucsb.edu/webfont.min.css');

        :host {
            display: block;
            width: 100%;
            margin: 0;
            padding: 0;
            background-color: var(--eyebrow-bg-color, #033660);
        }

        #eyebrow {
            background-color: var(--eyebrow-bg-color, #033660);
            color: #fff;
            font-family: "Nunito Sans", sans-serif;
            font-style: normal;
            font-size: 12px;
            font-weight: 100;
            letter-spacing: 0.7pt;
            line-height: 24px;
            position: relative;
            display: flex;
            justify-content: space-between;
            align-items: center;
            flex-direction: row-reverse;
            max-width: 1380px;
            min-height: 36px;
            margin: 0 auto;
            padding: 0 1em;
        }

        #eyebrow a {
            color: #033660;
            text-decoration: none;
        }
        #eyebrow a:hover {
            text-decoration: underline;
        }



        a {
          display: inline-block;
          padding: 16px; /* Expands the clickable area */
          margin: -16px; /* Keeps visual alignment within the parent */
        }

        #eyebrow a#edu-link {
            display: none;
            color: white;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        .navbar-nav {
            display: none;
            margin: 0;
        }

        .hide {
            display: none;
        }

        .show {
            display: block !important;
        }

        ul.navbar-nav.show {
            display: flex;
            position: absolute;
            left: 0;
            top: 36px;
            background: #edeff1;
            z-index: 9998;
            width: 100%;
            width: -moz-available;          /* WebKit-based browsers will ignore this. */
            width: -webkit-fill-available;  /* Mozilla-based browsers will ignore this. */
            width: fill-available;
            color: #003360;
            padding-left: 50px;
        }

        li {
            display: flex;
            align-items: center;
            min-height: 44px;
        }

        .navbar-toggle {
            position: relative;
            cursor: pointer;
            float: right;
            padding: 9px 10px;
            margin: 8px 15px 8px 0;
            background-color: transparent;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .navbar-toggle span {
            height: 2px;
            width: 25px;
            background-color: #fff;
            margin: 7px 0;
            display: block;
        }

        .close-icon {
            cursor: pointer;
            display: none;
        }

        /* Responsive Styles */
        @media (min-width: 768px) {

            .close-icon {
                display: none !important;
            }

            ul.navbar-nav.show {
                display: inline-flex;
                align-items: center;
                position: unset;
                background: unset;
                width: unset;
                color: white;
                padding-left: unset;
            }
            
            #eyebrow {
              flex-direction: row;
            }

            #eyebrow a {
                color: white;
            }
          #eyebrow a#edu-link {
                display: inline-block;
           }
            #eyebrow a:hover {
                text-decoration: none;
            }

            .navbar-toggle {
                display: none;
            }

            .navbar-nav {
                display: inline-block;
            }

            li {
                display: inline-flex;
                min-height: 30px;
                padding: 0 10px;
            }

            li:last-child {
                padding-right: 0;
            }

        }

            

        
    `;

    // Inject the CSS into a <style> tag in the shadow DOM
    const style = document.createElement("style");
    style.textContent = css;
    shadowRoot.appendChild(style);
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );

    this.update(defaultData);

    const navbarToggle = shadowRoot.querySelector(".navbar-toggle");
    navbarToggle?.addEventListener("click", () => {
      this.toggleMenu();
    });

    const closeToggle = shadowRoot.querySelector(".close-icon");
    closeToggle?.addEventListener("click", () => {
        this.toggleMenu();
      });


    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    })

  }
  update(data) {
    const shadowRoot = this.#shadowRoot;
    const eyebrowNav = shadowRoot.getElementById("eyebrow-nav");
    const eyebrowCampusLink = shadowRoot.getElementById("edu-link");
    if (eyebrowCampusLink) {
      if(data.campus){
          eyebrowCampusLink.href = data.campus.url;
          eyebrowCampusLink.title = data.campus.title;
          eyebrowCampusLink.textContent = data.campus.title;
      }else{
        eyebrowCampusLink.style.display = "none";
      }
    }
    // Collect <li> elements in an array
    const listItems = data.navigation.map((navItem, i) => {
      
      const li = document.createElement("li");
      li.setAttribute("role", "listitem"); // Set the role attribute
      li.innerHTML = `<a href="${navItem.href}" tabindex="${i+1}">${navItem.title}</a>`;
      // Add the "active" class if the item is active
      if (navItem.active === true) {
        li.classList.add("active");
      }

      return li;
    });

    // Clear existing content in the <ul>
    eyebrowNav.innerHTML = "";
    // Append all new <li> elements
    eyebrowNav.append(...listItems);


  } 
 toggleMenu(event) {
    // If triggered by a keyboard event, allow only Enter key to activate
    if (event && event.type === "keydown") {
        if (event.key === "Tab") {
            return; // Allow normal tabbing behavior
        }
        if (event.key !== "Enter") {
            return; // Ignore other keys
        }
    }

    const shadowRoot = this.#shadowRoot;
    const navbarNav = shadowRoot.querySelector(".navbar-nav");
    const navbarToggle = shadowRoot.querySelector(".navbar-toggle");
    const closeToggle = shadowRoot.querySelector(".close-icon");

    // Determine the current state
    const isExpanded = !navbarNav.classList.contains("show");
    console.log("::: ", isExpanded);

    // Toggle classes
    navbarNav.classList.toggle("show", isExpanded);
    navbarToggle.classList.toggle("hide", isExpanded);
    closeToggle.classList.toggle("show", isExpanded);

    // Update aria-expanded attributes
    navbarToggle.setAttribute("aria-expanded", !isExpanded);
    closeToggle.setAttribute("aria-expanded", isExpanded);

    // Focus management
    if (isExpanded) {
        const firstFocusable = navbarNav.querySelector("a, button, input");
        if (firstFocusable) firstFocusable.focus();
    } else {
        navbarToggle.focus();
    }
}
  
  connectedCallback() {
      const shadowRoot = this.shadowRoot;
      const navbarToggle = shadowRoot.querySelector(".navbar-toggle");
      const closeToggle = shadowRoot.querySelector(".close-icon");
  
      // Ensure the toggle button is focusable on page load
      navbarToggle.setAttribute("tabindex", "0");
  
      // Add keyboard event listeners to allow toggling with Enter key
      navbarToggle.addEventListener("keydown", this.toggleMenu.bind(this));
      closeToggle.addEventListener("keydown", this.toggleMenu.bind(this));
  }
  
  disconnectedCallback() {
      const shadowRoot = this.shadowRoot;
      const navbarToggle = shadowRoot.querySelector(".navbar-toggle");
      const closeToggle = shadowRoot.querySelector(".close-icon");
  
      // Remove event listeners when component is removed
      navbarToggle.removeEventListener("keydown", this.toggleMenu.bind(this));
      closeToggle.removeEventListener("keydown", this.toggleMenu.bind(this));
  }
  

connectedCallback() {
    const shadowRoot = this.shadowRoot;
    const navbarToggle = shadowRoot.querySelector(".navbar-toggle");
    const closeToggle = shadowRoot.querySelector(".close-icon");

    // Ensure the toggle button is focusable on page load
    navbarToggle.setAttribute("tabindex", "0");
    closeToggle.setAttribute("tabindex", "0");

    // Add keyboard event listeners to allow toggling with Enter key
    navbarToggle.addEventListener("keydown", this.toggleMenu.bind(this));
    closeToggle.addEventListener("keydown", this.toggleMenu.bind(this));
}

disconnectedCallback() {
    const shadowRoot = this.shadowRoot;
    const navbarToggle = shadowRoot.querySelector(".navbar-toggle");
    const closeToggle = shadowRoot.querySelector(".close-icon");

    // Remove event listeners when component is removed
    navbarToggle.removeEventListener("keydown", this.toggleMenu.bind(this));
    closeToggle.removeEventListener("keydown", this.toggleMenu.bind(this));
}

}

customElements.define("eyebrow-component", MyComponentElement);
