const objectTemplate = document.getElementById("DAYLIGHT_OBJECT_CAROUSEL");

export class MyComponentElement extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = objectTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  update(data) {
    const shadowRoot = this.#shadowRoot;
    const objectContent = shadowRoot.getElementById("blacklight-object");

    // Check if objectContent exists; if not, log an error and return.
    if (!objectContent) {
      console.error("Element with ID 'blacklight-object' not found in the shadow DOM.");
      return;
    }

    for (const [key, value] of Object.entries(data)) {
      // Create a container for each dt and dd pair
      const container = document.createElement('div');
      container.className = 'dt-dd-container'; // Add class for styling

      const dt = document.createElement('dt');
      const dd = document.createElement('dd');

      // Format the key: remove underscores and capitalize each word
      const formattedKey = key.replace(/_/g, ' ').replace(/\b\w/g, char => char.toUpperCase());


      // Check if the key is "title" and add a specific class
      if (key.toLowerCase() === 'title') {
        dt.className = 'title'; // Add the 'title' class to hide this dt
        dd.className = 'title-value'; // Optional class for styling the dd if needed
      }

      dt.textContent = `${formattedKey}:`; // Set the formatted key as the dt content
      dd.textContent = value;

      container.appendChild(dt);
      container.appendChild(dd);
      objectContent.appendChild(container);
    }
  }

}

customElements.define("object-carousel-component", MyComponentElement);
