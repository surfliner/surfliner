const featuredListTemplate =
  document.getElementById("FEATURED-LIST");
const itemTemplate =
  document.getElementById("FEATURED-LIST-ITEM");

class ComponentElement extends HTMLElement {
  #shadowRoot;

  constructor() {
    super();
    const content = featuredListTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({
      mode: "open",
      delegatesFocus: true,
    });
    
    const css = `
        * {
          font-family: sans-serif;
        }
        h1 {    
          margin-left: 15px;
          margin-bottom: 0px;
        }
        .featured-list-container {
            text-align: left;
        }
        .featured-list-wrapper {
            display: flex;
            gap: 20px;
            justify-content: left;
            flex-wrap: wrap;
        }
        .card {
            min-width: 165px;
            padding: 15px;
            text-align: left;
            flex: 1 1 275px; 
            max-width: 275px;
        }
        .card:hover, .card:focus-within {
            outline: 2px solid #033660;
        }
        .card img {
            width: auto;
            height: auto;
            max-width: 300px;
            max-height: 185px;
        }
        .card-title {
            font-size: 18px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 8px;
            margin-left: 0px;
        }
        .card-title a {
            text-decoration: none;
            color: #033660;
        }
        .card-title a:hover, .card-title a:focus {
            text-decoration: underline;
        }
        .card-description {
            font-size: 16px;
            margin-top: 0px;
            color: #555;
        }
        @media (min-width: 768px) {
            .card-container {
                flex-wrap: nowrap; 
                justify-content: space-between;
            }
        }
`
    const style = document.createElement("style");
    style.textContent = css;
    shadowRoot.appendChild(style);
    shadowRoot.appendChild(content);


    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );

    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    })



  }
  update(data) {
    const shadowRoot = this.#shadowRoot;
    if (data) {
      const { header, featured } = data;
      shadowRoot.querySelector('h1').textContent = header;

      const wrapper = shadowRoot.getElementById("featured-list-wrapper");

      for (const item of featured) {
        const newItem = itemTemplate.content.cloneNode(true).firstElementChild;
        const { image, title, description, alt_text, link } = item;
        const img = newItem.querySelector("img");
        const links = newItem.querySelectorAll('a');
        const descr = newItem.querySelector("p");
        img.src = image;
        img.alt = alt_text;
        links.forEach(a => a.href = link);
        links[1].textContent = title;
        descr.textContent = description;

        wrapper.appendChild(newItem);
      }
    }
  }
}

customElements.define("featured-list", ComponentElement);