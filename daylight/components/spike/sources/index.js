const myComponentTemplate = document.getElementById(
  "urn:fdc:surfliner.ucsd.edu:20240313:daylight:spike:index",
);

export class MyComponentElement extends HTMLElement {
  #shadowRoot;

  /**
   * Construct a new `<component-element>`.
   *
   * This initializes the shadow root and sets up listeners.
   */
  constructor() {
    super();
    const content = myComponentTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);
    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);
    shadowRoot.addEventListener("slotchange", ({ target }) => {
      // Watch for changes to slot contents.
      //
      // This will also handle the initial setting of slots, since that occurs
      // *after* the element is first created.
      if (target.name == "daylight-component") {
        // The contents of the `daylight-component` slot changed.
        for (const node of target.assignedNodes()) {
          const data = node.matches("script[type='application/json']")
            ? node
            : node.querySelector("script[type='application/json']");
          if (data) {
            // The slot contains Json data.
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  /**
   * Update the custom element with new data.
   */
  update(data) {
    const shadowRoot = this.#shadowRoot;
    const navList = shadowRoot.querySelector(".navigation>ul");
    const pane = shadowRoot.querySelector(".pane");
    navList.textContent = pane.textContent = "";
    for (const [title, content] of Object.entries(data)) {
      const navItem = document.createElement("li");
      navItem.textContent = title;
      navItem.addEventListener("click", () => {
        pane.textContent = content;
      });
      navList.appendChild(navItem);
    }
  }
}

customElements.define("my-component", MyComponentElement);
