const myComponentTemplateIdentifier = 'results-list';
const myComponentTemplate = document.getElementById(myComponentTemplateIdentifier);
export class MyComponentElement extends HTMLElement {
  #shadowRoot;

  /**
   * Construct a new `<component-element>`.
   *
   * This initializes the shadow root and sets up listeners.
   */
  constructor() {

    super();
    const content = myComponentTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ 
      mode: "open",
      delegatesFocus: true,
     });
    shadowRoot.appendChild(content);
    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    
    this.update(defaultData);
    this.data = defaultData;

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      // Watch for changes to slot contents.
      //
      // This will also handle the initial setting of slots, since that occurs
      // *after* the element is first created.
      if (target.name == "daylight-component") {
        // The contents of the `daylight-component` slot changed.
        for (const node of target.assignedNodes()) {
          const data = node.matches("script[type='application/json']")
            ? node
            : node.querySelector("script[type='application/json']");
          if (data) {
            // The slot contains Json data.
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }
  
  filterResults(results, query, searchField = null) {
    if (!query) return results; // Return all items if no query is given
  
    if (searchField === "all_fields") {
      return results.filter(item => {
        // Convert all field values to a single string for each item
        const allFieldsString = Object.values(item)
          .map(value => (typeof value === 'object' ? JSON.stringify(value) : value)) // Handle nested objects
          .join(" ")
          .toLowerCase();
  
        return allFieldsString.includes(query.toLowerCase());
      });
    }
  
    // Filter by a specific field
    return results.filter(item => {
      const value = this.getValueByKey(item, searchField);
      return value && value.toString().toLowerCase().includes(query.toLowerCase());
    });
  }

  getValueByKey(item, key){
    if (key in item) return item[key];
    if (key in item.metadata) return item.metadata[key];
    return null;
  }
  
  flattenValues(obj) {
    const values = [];
    for (const key in obj) {
      if (typeof obj[key] === "object" && obj[key] !== null) {
        values.push(...this.flattenValues(obj[key])); // Recursively extract nested values
      } else {
        values.push(obj[key]); 
      }
    }
    return values;
  }

  displayResults(data) {
    const shadowRoot = this.#shadowRoot;
    if (!shadowRoot) return;
  
    const { current_page, per_page, total_results } = data.pagination;
    const results = data.results;
  
    if (!results || Object.keys(results).length === 0) {
      shadowRoot.querySelector("#search-results").innerHTML = `
        <div id="search-results">
          <h2>No results found for your search</h2>
          <h3>Try modifying your search</h3>
          <ul>
            <li>Use fewer keywords to start, then refine your search using the links on the left.</li>
          </ul>
        </div>
      `;
      return;
    }
  
    let startIndex = (current_page - 1) * per_page;
    let endIndex = startIndex + per_page;
  
    // Adjust for the special case where last item should be on second page
    if (total_results === 11 && current_page === 2) {
      startIndex = 10;
      endIndex = 11;
    }
  
    const paginatedResults = results.slice(startIndex, endIndex);
  
    let result = '<ol>';
    for (const item of paginatedResults) {
      result += `
        <li>
          <div>
            <h2><a href="#">${item.title}</a></h2>
            <ul>
              <li><strong>Title:</strong> ${item.title}</li>
              <li><strong>Author:</strong> ${item.metadata.author}</li>
              <li><strong>Format:</strong> ${item.metadata.format}</li>
              <li><strong>Creation Date:</strong> ${item.metadata.date_created}</li>
            </ul>
          </div>
          <div>
            <label for="bookmark-${item.id}">
              <input id="bookmark-${item.id}" type="checkbox" data-bookmark="${item.ITEM_ID}">
              Bookmark
            </label>
          </div>
        </li>
      `;
    }
    result += '</ol>';
  
    shadowRoot.querySelector("#search-results").innerHTML = result;
  }
  

   updateSlot(data) {
      // Select the slot element inside the component
      const component = document.querySelector("results-list");
      const slotScript = component.shadowRoot.querySelector('slot script');
  
      if (slotScript) {
          slotScript.textContent = JSON.stringify(data); // Update slot content
      }
    // Ensure event fires after DOM updates
    requestAnimationFrame(() => {
        this.dispatchEvent(new CustomEvent("content-updated", {
            detail: data,
            bubbles: true
        }));
      });

  }

	/**
	 * Update the custom element with new data.
	 */
    update(data) {
      const result_item_list = data.results;
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const userSearchParams = {};
      
      // Iterate over all key-value pairs in the URL
      urlParams.forEach((value, key) => {
        userSearchParams[key] = value;
      });    

      // Add search results filter logic for q param
      if(userSearchParams['q']){
        const userSearchTerm = userSearchParams['q'];
        const searchField = userSearchParams['search_field'];
        const filtered_results = this.filterResults(result_item_list, userSearchTerm, searchField)
        if (filtered_results.length == 0){
          //no results found
          data.results = {};
          data.pagination.total_results = filtered_results.length;
        } 
        else {
          data.results = filtered_results;
          data.pagination.total_results = filtered_results.length;
        }
        this.updateSlot(data); 
             

      }
      this.displayResults(data);
    
	}
  connectedCallback() {
    //listen for changes in the search results list
    document.addEventListener("pagination-updated", (e) => {
      this.data.pagination = e.detail.pagination
        this.update(this.data); 
    });
}

}

customElements.define("results-list", MyComponentElement);
