export {
  assert,
  assertStrictEquals,
} from "https://jsr.io/@std/assert/1.0.0/mod.ts";
export { describe, it } from "https://jsr.io/@std/testing/0.225.3/bdd.ts";
export { DOMParser, XMLSerializer } from "npm:@xmldom/xmldom@0.8.10";
export { jsdom, setupJSDOM } from "../../../scripts/jsdom-setup/index.js";
