import {
  assert,
  assertStrictEquals,
  describe,
  it,
  setupJSDOM,
} from "./dev-deps.js";

/* Set up JSDOM */
await setupJSDOM();

/* Load the component element. */
const MyComponentElement = await (async () => {
  // This happens using an async `import()` because it needs to come _after_
  // JSDOM is loaded.
  const exports = await import("./index.js");
  return exports.MyComponentElement;
})();

describe("MyComponentElement for the", () => {
  it("Object Component can be constructed", () => {
    assert(new MyComponentElement() instanceof MyComponentElement);
  });

});
