const objectTemplate = document.getElementById("DAYLIGHT_OBJECT_METADATA");

export class MyComponentElement extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = objectTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }


	update(data) {
		const shadowRoot = this.#shadowRoot;
		const objectContent = shadowRoot.getElementById("daylight-object-metadata");

		// Check if objectContent exists; if not, log an error and return.
		if (!objectContent) {
			console.error("Object content not found in the shadow DOM.");
			return;
		}

		const title = data.title;
		const id = data.id;
		const metadata = data.hasNote;
		const ul = document.createElement('ul');

		ul.className = 'metadata-items';

		for (const [key, item] of Object.entries(metadata)) {
			const li = document.createElement('li');
			li.innerHTML = `<div class="item-label">${item.note.displayLabel}</div><div class="item-value">${item.note.value}</div>`;
			ul.appendChild(li);
		}

		objectContent.appendChild(ul);
	}

}

customElements.define("object-metadata-component", MyComponentElement);
