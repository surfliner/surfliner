# Results Simple Pager Component

## Overview
The **Results Simple Pager** is a custom web component designed to provide basic pagination controls, allowing users to navigate through a list of results.

## Features
- Displays "Previous" and "Next" links for navigation
- Shows the current number of results displayed and the total count
- Listens for changes in the result list
- Dispatches an event when the user clicks "Next" or "Previous"

## Installation
Include the component in your HTML file:

```html
<results-simple-pager></results-simple-pager>
```

Or use it within a daylight component:

```html
<render-daylight-component name="results-simple-pager"></render-daylight-component>
```

Ensure the JavaScript file is included and loaded as a module:

```html
<script src="index.js" type="module"></script>
```

## Expected JSON Structure
The component expects a JSON structure within a `<script>` tag inside a `<slot>` element:

```json
 						{
							"current_page": 1,
							"per_page": 10,
							"total_results": 11
						  }
```

### Event Dispatch
The component emits a custom event when the user interacts with the pagination controls:

- **Event Name:** `pagination-updated`
- **Detail Payload:**
  ```json
 						{
							"current_page": 2,
							"per_page": 10,
							"total_results": 11
						  }
  ```

