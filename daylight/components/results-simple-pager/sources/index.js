const myComponentTemplateIdentifier = 'RESULTS-SIMPLE-PAGER';
const myComponentTemplate = document.getElementById(myComponentTemplateIdentifier);

export class MyComponentElement extends HTMLElement {
  #shadowRoot;

  /**
   * Construct a new `<component-element>`.
   *
   * This initializes the shadow root and sets up listeners.
   */
  constructor() {
    super();
    const content = myComponentTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);
    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      // Watch for changes to slot contents.
      //
      // This will also handle the initial setting of slots, since that occurs
      // *after* the element is first created.
      if (target.name == "daylight-component") {
        // The contents of the `daylight-component` slot changed.
        for (const node of target.assignedNodes()) {
          const data = node.matches("script[type='application/json']")
            ? node
            : node.querySelector("script[type='application/json']");
          if (data) {
            // The slot contains Json data.
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }
  /**
   * Dispatch new results list dynamically based on page change
   */
  dispatchNewResults(data) {
    this.update(data); // Re-render the pagination UI

    // Dispatch a custom event to notify other components of the change
    this.dispatchEvent(new CustomEvent("pagination-updated", {
        detail: { pagination: data },
        bubbles: true,
        composed: true
    }));
}
	/**
	 * Update the custom element with new data.
	 */
	update(data) {
		const shadowRoot = this.#shadowRoot;

		const total_results = parseInt(data.total_results);
		const results_per_page = parseInt(data.per_page);
		const results_offset = parseInt(data.current_page - 1);
		const resultItem_start = results_offset * results_per_page +1;
    const page_end = Math.min(resultItem_start - 1 + results_per_page, total_results);

    //Disable prev/next links
    const prevDisabled = data.current_page > 1;
    const nextDisabled = page_end < total_results;

		const result = `
        <ul>
            <li>
                 ${prevDisabled ? `<a href="#" id="prev-btn" >Previous</a> |` : ''}
                <strong>${resultItem_start} - ${page_end}</strong> of <strong>${total_results}</strong>
                ${nextDisabled ? `| <a href="#" id="next-btn">Next</a>` : ''}
            </li>
        </ul>
			`;

		//shadowRoot.querySelector("#search-pager-mini").textContent = '';
    shadowRoot.querySelector("#search-pager-mini").innerHTML = result;
        
    // Add event listeners for prev & next links
    shadowRoot.querySelector("#prev-btn")?.addEventListener("click", (event) => {
        event.preventDefault();
        if (prevDisabled) {
             data.current_page = data.current_page - 1;
             this.dispatchNewResults(data);
         }
    });
  
    shadowRoot.querySelector("#next-btn")?.addEventListener("click", (event) => {
        event.preventDefault();
        if (nextDisabled) {
          //last result #, pagination object
         // {current_page: 2, per_page: 10, total_results: 11}
            data.current_page = data.current_page + 1;
            this.dispatchNewResults(data);
            
         }
    });


	}

  connectedCallback() {
    //listen for changes in the search results list
    document.addEventListener("content-updated", (e) => {
        this.update(e.detail.pagination); 
    });
}

}

customElements.define("results-simple-pager", MyComponentElement);
