// components/quicklinks/quicklinks.js
const quicklinksTemplate = document.getElementById(
  "QUICKLINKS",
);
class quicklinksComponent extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = quicklinksTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({
      mode: "open",
      delegatesFocus: true,
    });

    const css = `
        @import url('https://webfonts.brand.ucsb.edu/webfont.min.css');
        /*** Quicklinks Styles ***/

        #quick-links {
            font-family: "Nunito Sans", sans-serif;
            font-size: 16px;
            font-weight: 400;
            line-height: 1.75;
        }
        a {
           color: var(--quicklink-a-color, #111517);
        }
        a:hover {
            color: var(--quicklink-hover-color, #FEBC11);
        }

        `;
    // Inject the CSS into a <style> tag in the shadow DOM
    const style = document.createElement("style");
    style.textContent = css;
    shadowRoot.appendChild(style);
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=quicklinks-component] script[type='application/json']",
      ).textContent,
    );

    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "quicklinks-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  update(parsedJson) {
    const shadowRoot = this.#shadowRoot;
    const quicklinksList = shadowRoot.getElementById("quick-links");
    const linkList = [];

    for (const { title, href, alt, active } of parsedJson["quicklinks"]) {
      let link = `<a href="${href}" alt="${alt}" class="">${title}</a>`;
      if (active) {
        link = `<a href="${href}" alt="${alt}" class="active">${title}</a>`;
      }
      linkList.push(link);
    }
    let htmlString = `Or browse by ${linkList.slice(0, -1).join(", ")}`;

    // Add "or" before the last item in the array
    if (linkList.length > 1) {
      htmlString += ` or ${linkList[linkList.length - 1]}`;
    }

    // Set the innerHTML of quicklinksList
    quicklinksList.innerHTML = `<h2>${htmlString}</h2>`;
  }
}
customElements.define("quick-links", quicklinksComponent);
