const headerTemplate =
  document.getElementById("urn:fdc:surfliner.ucsd.edu:20240417:daylight:header");

// Define the header component
class HeaderComponent extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();

    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    const headerContent = headerTemplate.content.cloneNode(true);

    shadowRoot.appendChild(headerContent);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name == "daylight-component") {
        for (const node of target.assignedNodes()) {
          const data = node.matches("script[type='application/json']") ? node : node.querySelector("script[type='application/json']")
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  update(data) {
    const shadowRoot = this.#shadowRoot;

    { // Fill out the navigation.
      const navUL = shadowRoot.querySelector(".navigation ul");
      navUL.textContents = "";
      for (const {active, href, title} of (data.navigation ?? [])) {
        const li = this.ownerDocument.createElement("li");
        li.className = "nav-item";
        const a = this.ownerDocument.createElement("a");
        a.className = active ? "nav-link active" : "nav-link";
        a.href = href;
        a.textContent = title;
        li.append(a);
        navUL.append(li);
      }
    }
  }
}

// Define the custom element
customElements.define('header-component', HeaderComponent);
