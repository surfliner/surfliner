# Results Facet Menu Component

## Overview
The **Results Facet Menu Component** is a custom web component that displays a menu of associated facets for quickly filtering search results.

## Features
- Displays a list of facets for refining search results
- Fully accessible with keyboard navigation

## Installation
Include the component in your HTML file:

```html
<results-facet-menu></results-facet-menu>
```

Ensure the JavaScript file is included and loaded as a module:

```html
<script src="result-facet-menu.js" type="module"></script>
```

