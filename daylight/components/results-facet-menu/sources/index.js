const facetTemplate = document.getElementById("RESULTS-FACET-MENU");

export class MyComponentElement extends HTMLElement {
  #shadowRoot;
  constructor() {
    super();
    const content = facetTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({
      mode: "open",
      delegatesFocus: true,
    });
    shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);
    shadowRoot.addEventListener("slotchange", ({ target }) => {

      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  update(data) {
    const shadowRoot = this.#shadowRoot;
    const header = shadowRoot.querySelector(".facets-heading");
    header.textContent = "";
    header.textContent = data.header;


    // Clear the existing facets container (assuming you have a container element for facets)
    const facetsContainer = shadowRoot.querySelector(".facets-container");
    facetsContainer.innerHTML = "";

    const facets = data.allFacets;

    for (const facet of facets) {
      // Create the details element
      const details = document.createElement("details");
      details.className = "facet-limit";

      // Create the summary element
      const summary = document.createElement("summary");
      summary.className = "facet-field-heading";
      summary.textContent = facet.header;

      // Create the div element for facet content
      const facetContent = document.createElement("div");
      facetContent.className = "facet-content";

      // Create the ul element for facet values
      const ul = document.createElement("ul");
      ul.className = "facet-values";

      // Loop through facet values and create li elements
      for (const value of facet.values) {
        const li = document.createElement("li");

        const a = document.createElement("a");
        a.href = value.href;
        a.textContent = value.label;

        const span = document.createElement("span");
        span.className = "facet-count";
        span.textContent = value.count.toLocaleString();

        li.appendChild(a);
        li.appendChild(span);
        ul.appendChild(li);
      }

      // Append the ul to the facet content div
      facetContent.appendChild(ul);

      // Append the summary and facet content div to the details element
      details.appendChild(summary);
      details.appendChild(facetContent);

      // Append the details element to the facets container
      facetsContainer.appendChild(details);
    }
  }
}
customElements.define("results-facet-menu", MyComponentElement);
