const myComponentTemplateIdentifier = 'RESULTS_COMPLEX_PAGER';
const myComponentTemplate = document.getElementById(myComponentTemplateIdentifier);

export class MyComponentElement extends HTMLElement {
  #shadowRoot;

  /**
   * Construct a new `<component-element>`.
   *
   * This initializes the shadow root and sets up listeners.
   */
  constructor() {
    super();
    const content = myComponentTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);
    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);
		console.log(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      // Watch for changes to slot contents.
      //
      // This will also handle the initial setting of slots, since that occurs
      // *after* the element is first created.
      if (target.name == "daylight-component") {
        // The contents of the `daylight-component` slot changed.
        for (const node of target.assignedNodes()) {
          const data = node.matches("script[type='application/json']")
            ? node
            : node.querySelector("script[type='application/json']");
          if (data) {
            // The slot contains Json data.
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

	/**
	 * Update the custom element with new data.
	 */
	update(data) {
		const shadowRoot = this.#shadowRoot;

		// Demo quality pager 😜
		let total_results = parseInt(data.SEARCH_RESULTS_TOTAL);
		let results_per_page = parseInt(data.SEARCH_RESULTS_PER_PAGE);
		let results_offset = parseInt(data.SEARCH_RESULTS_OFFSET);
		let prev_button = '<a href="#">« Previous</a>';
		let next_button = '<a href="#">Next »</a>';
		let total_pages = Math.floor(total_results/results_per_page);
		let total_pages_remainder = total_results%results_per_page;
		total_pages = (total_pages_remainder !== 0) ? total_pages + 1 : total_pages;
		let page_list = '';
		let output = '';

		for (let i = 0; i < total_pages; i++) {
			if (i < 5 || i === total_pages-2 || i === total_pages-1) {
				if (i >= 5 && i === total_pages-2 && !(total_results > 60 && total_results <= 70)) {
					page_list += `<a href="#">…</a>`;
				}
				page_list += `<a href="#">${i+1}</a>`;
			}
		}

		output = `
		<ul>
			<li>${prev_button}${next_button}${page_list}</li>
		</ul>
		`;

		shadowRoot.querySelector("#search-pager").textContent = '';
		shadowRoot.querySelector("#search-pager").insertAdjacentHTML('afterbegin', output);

	}
}

customElements.define("results-complex-pager", MyComponentElement);

// « Previous Next » 1 2 3 4 5 … 143 144
// « Previous Next » 1 2 3 4 5
// Results 1 - 3 of 3