`daylight-highlights`
=====================

`daylight-highlights` is a [`daylight` microfrontend][daylight-frontend]
component that provides a view of "highlighted"/"promoted" objects, suitable
for a digital collections homepage.

it exposes a single Custom Element: `highlights-component`.

## `<slot>` Data

the `hightlight-component` provides a [`<slot>`][slot]. following the
[`daylight_component`][daylight-component] convention, it expects a
`script[type='application/json']`. the JSON passed to this slot is used
to build the display:

it accepts data like:

```json
{
  "header": "Highlights",
  "highlights": [
    { "id": "1", "label": "Comet in Moominland" },
    { "id": "2", "label": "Finn Family Moomintroll" }
  ]
}
```


[daylight-component]: ../../gems/daylight_component/README.md
[daylight-frontend]: ../README.md
[slot]: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/slot
