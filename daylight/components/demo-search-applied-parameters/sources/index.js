const myComponentTemplateIdentifier = 'DAYLIGHT_DEMO_SEARCH_APPLIED_PARAMETERS_TEMPLATE';
const myComponentTemplate = document.getElementById(myComponentTemplateIdentifier);

export class MyComponentElement extends HTMLElement {
  #shadowRoot;

  /**
   * Construct a new `<component-element>`.
   *
   * This initializes the shadow root and sets up listeners.
   */
  constructor() {
    super();
    const content = myComponentTemplate.content.cloneNode(true);
    const shadowRoot = this.#shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(content);
    const defaultData = JSON.parse(
      shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);
		console.log(defaultData);

    shadowRoot.addEventListener("slotchange", ({ target }) => {
      // Watch for changes to slot contents.
      //
      // This will also handle the initial setting of slots, since that occurs
      // *after* the element is first created.
      if (target.name == "daylight-component") {
        // The contents of the `daylight-component` slot changed.
        for (const node of target.assignedNodes()) {
          const data = node.matches("script[type='application/json']")
            ? node
            : node.querySelector("script[type='application/json']");
          if (data) {
            // The slot contains Json data.
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

	/**
	 * Update the custom element with new data.
	 */
	update(data) {
		const shadowRoot = this.#shadowRoot;

		let result = `
			<p>You searched for:</p>
			<ul>
				<li class="filter-value"><span>${data.SEARCH_KEYWORDS}</span><a href="#">✖</a></li>
				<li><a href="#">Start Over</a></li>
			</ul>
			`;

		shadowRoot.querySelector("#search-applied-parameters").textContent = '';
		shadowRoot.querySelector("#search-applied-parameters").insertAdjacentHTML('afterbegin', result);

	}
}

customElements.define("demo-search-applied-parameters-component", MyComponentElement);
