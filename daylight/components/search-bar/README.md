# Daylight: `search-bar` component documentation

This component draws a search bar. The search bar consists of text input control followed by a submit button control that can be prefixed with an optional search option select list.

## JSON Config (sources/index.html)

### ACTION_URL

(String) This parameter sets the value of the search form's 'action' attribute.

E.g., `"ACTION_URL": "https://example.com/cgi-bin/old-school-program"`

### SELECT_LIST

(Array) This parameter contains the contents of the optional search option select list. Array items are arrays that contain three strings: the first is the option value, the second is the option label, and the third is the placeholder text used to describe this option in the search control.

E.g., `"SELECT_LIST": [["option_value", "Option Label", "This is placeholder text…"]]`
