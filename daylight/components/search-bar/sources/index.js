const searchTemplate = document.getElementById("DAYLIGHT_SEARCH_BAR");

export class MyComponentElement extends HTMLElement {
  #shadowRoot;

  constructor() {
    super();
    const content = searchTemplate.content.cloneNode(true);
    this.#shadowRoot = this.attachShadow({ mode: "open" });
    this.#shadowRoot.appendChild(content);

    const defaultData = JSON.parse(
      this.#shadowRoot.querySelector(
        "slot[name=daylight-component] script[type='application/json']",
      ).textContent,
    );
    this.update(defaultData);

    this.#shadowRoot.addEventListener("slotchange", ({ target }) => {
      if (target.name === "daylight-component") {
        for (const node of target.assignedNodes()) {
          const element = node;
          const data = element.matches("script[type='application/json']")
            ? element
            : element.querySelector("script[type='application/json']");
          if (data) {
            this.update(JSON.parse(data.textContent));
            return; // only take the first result
          }
        }
        this.update(defaultData); // fallback
      }
    });
  }

  logValues(event) {
    event.preventDefault();
    console.log(
      "Dropdown & Search Input:",
      this.searchInput.value,
      this.searchField.value,
    );
  }

	updatePlaceholder(event) {
		event.preventDefault();
		const data_placeholder = this.#shadowRoot.querySelector(`option[value='${this.searchField.value}']`).getAttribute("data-placeholder");
		this.#shadowRoot.querySelector("#q").setAttribute("placeholder", data_placeholder + '…');
		this.#shadowRoot.querySelector("label[for='q']").innerHTML = data_placeholder + ':';
	}

  update(data) {

		// Sets the form's `action` attribute
		if ('ACTION_URL' in data && data.ACTION_URL !== '')
		{
			const action_url = data.ACTION_URL;
			this.#shadowRoot.querySelector("#main-search").setAttribute("action", action_url);
		}

		// Adds select list to the search bar
		if ('SELECT_LIST' in data && data.SELECT_LIST.length)
		{
			const label = document.createElement("label");
			const select = document.createElement("select");

			label.setAttribute("class","sr-only visually-hidden");
			label.setAttribute("for","search_field");
			label.innerHTML = "Search for:";

			select.setAttribute("id","search_field");
			select.setAttribute("name","search_field");
			select.setAttribute("class","custom-select form-select search-field");
			select.setAttribute("title","Facet search options");

			for (const i in data.SELECT_LIST) {
				const option = document.createElement("option");
				option.value = data.SELECT_LIST[i][0];
				option.innerText = data.SELECT_LIST[i][1];
				option.setAttribute("data-placeholder", data.SELECT_LIST[i][2]);
				select.append(option);
			}

			const mainSearch = this.#shadowRoot.querySelector("#main-search");
			const inputGroup = this.#shadowRoot.querySelector(".input-group");

			//remove existing drop down
			const existingLabel = mainSearch.querySelector("label");
			if (existingLabel) existingLabel.remove();
		
			const existingSelect = inputGroup.querySelector("select");
			if (existingSelect) existingSelect.remove();

			mainSearch.prepend(label);
			inputGroup.prepend(select);
			
			this.#shadowRoot.querySelector("#q").classList.add("form-control-sans-select");
		}

		//---
		// VALLEY OF THE EVENT LISTENERS
		//---

    // Attach event listeners after shadow DOM is updated
    this.searchField = this.#shadowRoot.querySelector("#search_field");

		if (this.searchField !== null) {
			this.searchField.addEventListener("change", this.updatePlaceholder.bind(this));
		}

	}
}

customElements.define("search-bar-component", MyComponentElement);
