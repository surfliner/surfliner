import {
  assert,
  assertExists,
  assertStrictEquals,
  describe,
  it,
  setupJSDOM,
} from "./dev-deps.js";

/* Set up JSDOM */
await setupJSDOM();

/* Load the component element. */
const MyComponentElement = await (async () => {
  // This happens using an async `import()` because it needs to come _after_
  // JSDOM is loaded.
  const exports = await import("./index.js");
  return exports.MyComponentElement;
})();



describe("MyComponentElement", () => {
  it("Search can be constructed", () => {
    assert(new MyComponentElement() instanceof MyComponentElement);
  });

  describe("Search initialization", () => {
    it("should instantiate without errors", () => {
      // Create an instance of the component
      const component = new MyComponentElement();

      // Check that the component is created without errors
      assertExists(component);

      // Ensure shadow DOM is created
      assertExists(component.shadowRoot);

      // Verify initial content is correct
      const searchField = component.shadowRoot.querySelector("#search_field");
      const searchInput = component.shadowRoot.querySelector("#q");

      assertExists(searchField);
      assertExists(searchInput);
      assertStrictEquals(searchInput.placeholder, "Search...");
    });
  });

  describe("Search custom events", () => {
    it("emits custom events correctly", () => {
      const component = new MyComponentElement();
      document.body.appendChild(component);

      const searchButton = component.shadowRoot.querySelector("#search");
      const form = component.shadowRoot.querySelector("form");

      let eventTriggered = false;
      form.addEventListener("submit", (event) => {
        event.preventDefault();
        eventTriggered = true;
      });

      // Simulate clicking the search button
      searchButton.click();

      // Verify that the form submit event was triggered
      assertStrictEquals(eventTriggered, true);

      document.body.removeChild(component);
    });
  });
});

