`clover_view`
=============

`clover_view` is a frontend microservice for serving (pcdm) Object view from
a IIIF manifest.

To Build in Isolation
---------------------

This service is intended to be run in the container image specified by
`Dockerfile`:

```sh
cd daylight/frontend/clover_view
docker build -t clover_view .
docker run -it --env-file=.env -p 8081:80 --rm clover_view
```

This creates a mimimal `nginx:alpine` container to serve the app.

To Run with [`daylight/backend`][backend]
-----------------------------

The `daylight` product provides a "`backend`" application as a default wrapper
around its microfrontend toolkit. To run that service locally, see
[`daylight/backend` development][backend-dev].

That development environment supports integration with this service by default,
but without code reloading. When developing this frontend with integration in
mind, you may want to use `docker compose --profile backend up` to start the
backend services. You can then run this frontend with networking arguments to
make it available to the backend:

```sh
docker run -it -p 8081:80 --env-file .env --rm --network backend_daylight --network-alias clover clover_view
```

[backend]: ../../backend/README.md
[backend-dev]: ../../backend/README.md#developing-locally
