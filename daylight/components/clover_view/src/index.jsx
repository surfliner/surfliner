import { StrictMode } from "react";
import { createRoot } from "react-dom/client";

import App from "./App";

class CloverViewComponentElement extends HTMLElement {
  connectedCallback() {
    const dataElement = this.querySelector(
      "div[slot=daylight-component] script[type='application/json']",
    );
    const data = dataElement ? JSON.parse(dataElement.textContent) : null;
    const rootElement = document.createElement("div");
    this.replaceChildren(rootElement);
    const reactRoot = createRoot(rootElement);
    reactRoot.render(
      <StrictMode>
        <App manifestId={data?.manifest_url} />
      </StrictMode>
    );
  }
}

customElements.define("clover-view", CloverViewComponentElement);
