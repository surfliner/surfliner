import Object from "./Object";

export default function App({ manifestId }) {
  return (
    <div className="clover-iiif">
      <Object manifestId={manifestId} />
    </div>
  );
}
