import { useEffect, useState } from "react";
import Viewer from "@samvera/clover-iiif/viewer";
import {
  Homepage,
  Label,
  Metadata,
  PartOf,
  RequiredStatement,
  SeeAlso,
  Summary,
} from "@samvera/clover-iiif/primitives";
import Slider from "@samvera/clover-iiif/slider";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

const Object = ({ manifestId }) => {
  const [manifest, setManifest] = useState();

  const collectionId = manifest?.partOf?.[0]?.id;

  useEffect(() => {
    if (!manifestId) {
      setManifest(null);
    } else {
      (async () => {
        const response = await fetch(manifestId);
        const json = await response.json();
        setManifest(json);
      })();
    }
  }, [manifestId]);

  if (!manifest) return <></>;

  return (
    <article>
      <Viewer iiifContent={manifestId} />
      <div>
        <Label label={manifest.label} as="h1" />
        <Summary summary={manifest.summary} as="p" />
        <Metadata metadata={manifest.metadata} />
        <RequiredStatement requiredStatement={manifest.requiredStatement} />
        <PartOf partOf={manifest.partOf} />
        <SeeAlso seeAlso={manifest.seeAlso} />
        <Homepage homepage={manifest.homepage} />
      </div>
      <Slider iiifContent={collectionId} />
    </article>
  );
};

export default Object;
