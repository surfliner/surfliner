class FooterComponent extends HTMLElement {
  constructor() {
    super();

    // Attach the shadow DOM to the element
    this.attachShadow({ mode: 'open' });

    // Get the template content and clone it into the shadow DOM
    const template = document.getElementById('urn:fdc:surfliner.ucsd.edu:20240417:daylight:footer');
    const templateContent = template.content.cloneNode(true);
    this.shadowRoot.appendChild(templateContent);

  }
}

// Define the custom element "header-component"
customElements.define('footer-component', FooterComponent);
