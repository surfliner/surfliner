/**
 * This code is loosely based on the Deno example at
 * <https://docs.deno.com/runtime/tutorials/file_server/> (MIT license).
 */

import "./deps.js"; // add any dependencies here

const PUBLIC = "./public";

const FILE_TYPE_MAPPINGS = {
  css: "text/css",
  html: "text/html;charset=UTF-8",
  js: "application/ecmascript",
  xhtml: "application/xhtml+xml",
  xml: "application/xml",
};

const getMediaType = (filepath) => {
  for (const [extension, mapping] of Object.entries(FILE_TYPE_MAPPINGS)) {
    if (filepath.endsWith(`.${extension}`)) return mapping;
  }
  return "text/plain;charset=UTF-8";
};

const handler = async (request) => {
  // Use the request pathname as filepath
  const url = new URL(request.url);
  const filepath = decodeURIComponent(url.pathname);
  // Try opening the file
  let file = null;
  let mediaType = "text/plain";
  if (filepath.endsWith("/")) {
    for (const index of ["index.xhtml", "index.html", "index.xml"]) {
      try {
        file = await Deno.open(PUBLIC + filepath + index, { read: true });
        mediaType = getMediaType(index);
        break;
      } catch {
        // Ignore errors; a 404 will be returned if no page resolves correctly.
      }
    }
  } else {
    try {
      file = await Deno.open(PUBLIC + filepath, { read: true });
      mediaType = getMediaType(filepath);
    } catch {
      // Ignore errors; a 404 will be returned if no page resolves correctly.
    }
  }

  const headers = { "Access-Control-Allow-Origin": "*" };

  // If no file was opened, 404.
  if (!file) {
    return new Response("404 Not Found", {
      status: 404,
      headers: {
        ...headers,
        "Content-Type": "text/plain;charset=UTF-8",
      },
    });
  }

  // Build a readable stream so the file doesn't have to be fully loaded into
  // memory while we send it
  const readableStream = file.readable;

  // Build and send the response
  return new Response(readableStream, {
    headers: {
      ...headers,
      "Content-Type": mediaType,
    },
  });
};

// Start listening on port 8080 of localhost.
const options = { port: 8080 };
Deno.serve(options, handler);
