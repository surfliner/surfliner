import { parseArgs } from "./deps.js";
import bundler from "../bundle/bundler.js";

// This script watches for changes to any of the files or directories provided
// on the command line, and rebundles accordingly.
//
// It assumes that an initial build has already taken place.

const parsedArgs = parseArgs(Deno.args);
for (const flag of Object.keys(parsedArgs)) {
  if (flag != "_") {
    throw new Error(`Unrecognized flag: #{flag}`);
  }
}

const watcher = Deno.watchFs(parsedArgs._, { recursive: true });
console.log(`Watching [${parsedArgs._}]…`);

// Run the actual file server.
await import("../serve/main.js");

for await (const watchEvent of watcher) {
  console.log(`Noticed a change to paths [${watchEvent.paths}]; rebuilding.`);
  bundler();
}
