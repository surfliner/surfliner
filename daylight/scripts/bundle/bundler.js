import { bundle, esbuild } from "./deps.js";

const ensurePublicDirectory = async () => {
  try {
    if ((await Deno.stat("public")).isDirectory) return;
  } catch {
    await Deno.mkdir("public");
  }
};

const writeHtmlFiles = async () => {
  for await (const entry of Deno.readDir('sources')) {
    if (entry.isFile && entry.name.endsWith('.html')) {
      await Deno.copyFile(`sources/${entry.name}`, `public/${entry.name}`);
    }
  }
};

const writeIndexJs = async () => {
  const indexFile = await (async () => {
    for (const index of ["index.ts", "index.js"]) {
      try {
        const info = await Deno.stat(`sources/${index}`);
        if (info.isFile) return index;
      } catch {
        // File does not exist; try the next one.
      }
    }
  })();
  const bundled = await bundle(`sources/${indexFile}`);
  const result = await esbuild.transform(bundled.code, {
    format: "esm",
    minify: true,
    treeShaking: true,
  });
  await Deno.writeTextFile("public/index.js", result.code);
};


export default async () => {
  await ensurePublicDirectory();
  await Promise.allSettled([
    writeHtmlFiles(),
    writeIndexJs(),
  ]);
};
