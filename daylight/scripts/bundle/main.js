import { esbuild } from "./deps.js";
import bundler from "./bundler.js";
await bundler();
await esbuild.stop();
