import { jsdom } from "./deps.js";
export { jsdom };
const { JSDOM } = jsdom;

/* Set up JSDOM */
export const setupJSDOM = async (source = "public/index.html") => {
  const jsdomInstance = new JSDOM(await Deno.readTextFile(source), {
    contentType: "text/html;charset=UTF-8",
    resources: "usable",
    runScripts: "dangerously",
  });
  for (
    // Iterate over every property on `jsdomInstance.window`, even
    // non‐enumerable ones.
    const term of new Set(
      Reflect.ownKeys(jsdomInstance.window).concat([
        // JSDOM’s window proxy incompletely lists its properties when you use
        // `Reflect.ownKeys`, so manually add a few values here.
        //
        // This is probably a problem with JSDOM; it works fine in browser.
        "HTMLElement",
      ]),
    )
  ) {
    if (!(term in globalThis)) {
      // If the property doesn’t exist in `globalThis`, copy it over.
      const descriptor = Object.getOwnPropertyDescriptor(
        jsdomInstance.window,
        term,
      );
      Object.defineProperty(globalThis, term, {
        configurable: descriptor.configurable,
        enumerable: descriptor.enumerable,
        get: () => jsdomInstance.window[term],
      });
    }
  }
  return jsdomInstance;
};
