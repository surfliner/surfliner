require "spec_helper"

module DaylightMetadataConsumer
  RSpec.shared_context "payload helpers" do
    # rubocop:disable Lint/ConstantDefinitionInBlock
    HANDLERS_FOR_STATES = # TODO: something cleaner
      {
        updated: Handlers::IndexHandler,
        published: Handlers::IndexHandler,
        unpublished: Handlers::DeleteHandler,
        deleted: Handlers::DeleteHandler
      }.freeze
    # rubocop:enable Lint/ConstantDefinitionInBlock

    let(:resource_url) { "https://comet.test/resources/12345/67890" }

    def payload_with(status:)
      JSON.generate({"status" => status, "resourceUrl" => resource_url})
    end
  end
end
