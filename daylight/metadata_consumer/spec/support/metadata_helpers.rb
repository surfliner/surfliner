require "spec_helper"
require "support/payload_helpers"

module DaylightMetadataConsumer
  RSpec.shared_context "metadata helpers" do
    include_context "payload helpers"

    let(:resource_metadata) do
      {
        "@id" => resource_url,
        "title" => "Test Resource",
        "creator" => "Jane Q. Test",
        "ark" => "ark:/99999/98765_4321"
      }
    end
  end
end
