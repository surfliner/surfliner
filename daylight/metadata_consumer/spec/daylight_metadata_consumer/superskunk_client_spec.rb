require "spec_helper"
require "support/payload_helpers"
require "support/metadata_helpers"

module DaylightMetadataConsumer
  describe SuperskunkClient do
    include_context "payload helpers"
    include_context "metadata helpers"

    let(:jsonld_profile) { SuperskunkClient::DEFAULT_JSONLD_PROFILE }
    let(:accept_header) { "application/ld+json;profile=\"#{jsonld_profile}\"" }
    let(:user_agent_header) { "surfliner.daylight" }
    let(:headers) do
      {
        "Accept" => accept_header,
        "User-Agent" => user_agent_header
      }
    end
    let(:body) { JSON.generate(resource_metadata) }

    let(:error_states) do
      Net::HTTPResponse::CODE_TO_OBJ.each_key.filter_map do |code_str|
        status = code_str.to_i
        status if status < 200 || status >= 400
      end
    end

    describe "get" do
      it "makes a request" do
        stub_request(:get, resource_url).with(headers:).to_return(body:)

        response = SuperskunkClient.get(resource_url)
        expect(response).to eq(resource_metadata)
      end

      it "follows redirects" do
        redirect_url = resource_url + "/new"

        stub_request(:get, resource_url).with(headers:).to_return(
          status: 302,
          headers: {"Location" => redirect_url}
        )
        stub_request(:get, redirect_url).with(headers:).to_return(body:)

        response = SuperskunkClient.get(resource_url)
        expect(response).to eq(resource_metadata)
      end

      it "raises an error in the event of an HTTP error" do
        error_states.each do |status|
          stub_request(:get, resource_url).with(headers:).to_return(status: status)

          expect { SuperskunkClient.get(resource_url) }.to raise_error(SuperskunkClient::UnexpectedResponse, /#{status}/)
        end
      end
    end
  end
end
