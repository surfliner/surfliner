require "spec_helper"
require "support/payload_helpers"
require "support/metadata_helpers"

module DaylightMetadataConsumer
  module Handlers
    RSpec.describe IndexHandler do
      include_context "payload helpers"
      include_context "metadata helpers"

      describe "handle" do
        let(:payload) { payload_with(status: :updated) }
        let(:payload_data) { Payload.new(payload) }

        it "retrieves and indexes the document" do
          expect(SuperskunkClient).to receive(:get).with(resource_url).and_return(resource_metadata)

          solr_connection = instance_double(RSolr::Client)
          expect(Consumer).to receive(:solr_connection).and_return(solr_connection)

          doc_expected = {
            id: resource_metadata["@id"].split("/").last,
            title_tesim: resource_metadata["title"],
            creator_ssim: resource_metadata["creator"],
            ark_si: resource_metadata["ark"],
            superskunk_uri_si: resource_metadata["@id"]
          }
          expect(solr_connection).to receive(:add).with(
            [doc_expected], add_attributes: IndexHandler::SOLR_ATTRIBUTES
          )

          handler = IndexHandler.new(payload_data)
          handler.handle
        end
      end
    end
  end
end
