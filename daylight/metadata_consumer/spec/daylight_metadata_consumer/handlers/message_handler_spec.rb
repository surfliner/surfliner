require "spec_helper"
require "support/payload_helpers"

module DaylightMetadataConsumer
  module Handlers
    RSpec.describe MessageHandler do
      include_context "payload helpers"

      describe "KNOWN_STATUSES" do
        it "contains the known statuses" do
          # just to be sure
          expected_statuses = HANDLERS_FOR_STATES.keys
          expect(Payload::KNOWN_STATUSES).to contain_exactly(*expected_statuses)
        end
      end

      describe "for" do
        it "returns the correct handler for each known state" do
          aggregate_failures "handler types" do
            HANDLERS_FOR_STATES.each do |status, handler_expected|
              payload = payload_with(status:)
              handler_actual = MessageHandler.for(payload:)
              expect(handler_actual).to be_a(handler_expected)
            end
          end
        end

        it "raises #{ArgumentError} for unknown states" do
          payload = payload_with(status: "some unknown status")
          expect { MessageHandler.for(payload:) }.to raise_error(ArgumentError)
        end
      end

      describe "handle" do
        context "as an instance method" do
          it "is abstract" do
            Payload::KNOWN_STATUSES.each do |status|
              payload = payload_with(status:)
              handler = MessageHandler.new(payload)
              expect { handler.handle }.to raise_error(NotImplementedError)
            end
          end
        end

        context "as a class method" do
          shared_examples "it creates and delegates to the correct handler" do |status, handler_expected|
            specify do
              payload = payload_with(status:)
              payload_data = Payload.new(payload)
              expect(Payload).to receive(:new).with(payload).and_return(payload_data)

              handler = instance_double(handler_expected)
              expect(handler_expected).to receive(:new).with(payload_data).and_return(handler)

              expect(handler).to receive(:handle)
              MessageHandler.handle(payload:)
            end
          end

          HANDLERS_FOR_STATES.each do |status, handler_expected|
            it_behaves_like "it creates and delegates to the correct handler", status, handler_expected
          end
        end
      end
    end
  end
end
