require "spec_helper"
require "support/payload_helpers"

module DaylightMetadataConsumer
  module Handlers
    RSpec.describe DeleteHandler do
      include_context "payload helpers"

      describe "handle" do
        let(:payload) { payload_with(status: :deleted) }
        let(:payload_data) { Payload.new(payload) }

        it "raises #{NotImplementedError}" do
          handler = DeleteHandler.new(payload_data)
          expect { handler.handle }.to raise_error(NotImplementedError)
        end
      end
    end
  end
end
