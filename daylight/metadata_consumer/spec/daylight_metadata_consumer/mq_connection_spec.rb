require "spec_helper"

module DaylightMetadataConsumer
  RSpec.describe MqConnection do
    let(:rabbitmq_host) { "rabbitmq" }
    let(:rabbitmq_port) { "5672" }
    let(:rabbitmq_password) { "bitnami" }
    let(:rabbitmq_user) { "user" }

    let(:connection) { instance_double(Bunny::Session) }
    let(:channel) { instance_double(Bunny::Channel) }
    let(:exchange) { instance_double(Bunny::Exchange) }
    let(:queue) { instance_double(Bunny::Queue) }

    let(:topic) { "surfliner.metadata" } # TODO: use different values, at least for testing
    let(:queue_name) { "surfliner.metadata" } # TODO: use different values, at least for testing
    let(:routing_key) { "surfliner.metadata.daylight" }

    let(:connection_uri) { URI("amqp://#{rabbitmq_user}:#{rabbitmq_password}@#{rabbitmq_host}:#{rabbitmq_port}") }
    let(:redacted_url) { connection_uri.to_s.sub(rabbitmq_password, "REDACTED") }

    let(:env_rabbitmq) do
      {
        "RABBITMQ_HOST" => rabbitmq_host,
        "RABBITMQ_NODE_PORT_NUMBER" => rabbitmq_port,
        "RABBITMQ_PASSWORD" => rabbitmq_password,
        "RABBITMQ_QUEUE" => queue_name,
        "RABBITMQ_PLATFORM_ROUTING_KEY" => routing_key,
        "RABBITMQ_TOPIC" => topic,
        "RABBITMQ_USERNAME" => rabbitmq_user
      }
    end

    let(:logger) do
      instance_double(Logger).tap do |logger|
        allow(logger).to receive(:info)
      end
    end

    subject { MqConnection.new(logger: logger) }

    before do
      allow(Bunny).to receive(:new).and_return(connection)
      env_rabbitmq.each { |k, v| allow(ENV).to receive(:fetch).with(k).and_return(v) }
    end

    RSpec::Matchers.define :connect_successfully do
      supports_block_expectations

      match(notify_expecation_failures: true) do |actual|
        expect(connection).to receive(:start).ordered
        expect(connection).to receive(:status).and_return(:open).ordered
        expect(connection).to receive(:create_channel).and_return(channel).ordered
        expect(channel).to receive(:topic).with(topic, auto_delete: true).and_return(exchange).ordered
        expect(channel).to receive(:queue).with(queue_name, durable: true).and_return(queue).ordered

        expect(queue).to receive(:bind).with(exchange, routing_key: routing_key).ordered

        actual.call # should return a truthy value
      end
    end

    describe :connect do
      it "connects" do
        expect { subject.connect }.to connect_successfully
      end

      it "tries to reconnect in the event of a transient failure with errors" do
        expect(connection).to receive(:start).and_raise("some transient failure").ordered
        expect(connection).to receive(:status).and_return(:not_connected).ordered

        sleep_time_expected = 1  # TODO: make sleep time configurable
        expect(subject).to receive(:sleep).with(sleep_time_expected).ordered

        expect { subject.connect }.to connect_successfully
      end

      it "tries to reconnect in the event of a transient failure without errors" do
        expect(connection).to receive(:start).ordered
        expect(connection).to receive(:status).and_return(:not_connected).ordered

        sleep_time_expected = 1  # TODO: make sleep time configurable
        expect(subject).to receive(:sleep).with(sleep_time_expected).ordered

        expect { subject.connect }.to connect_successfully
      end

      it "fails eventually if sleep timeout is exceeded" do
        timeout_expected = 120 # TODO: make timeout configurable

        timeout_expected.times do
          expect(connection).to receive(:start).ordered
          expect(connection).to receive(:status).and_return(:not_connected).ordered

          sleep_time_expected = 1  # TODO: make sleep time configurable
          expect(subject).to receive(:sleep).with(sleep_time_expected).ordered
        end

        expect { subject.connect }.to raise_error(/Failed/)
      end

      it "raises an error if already connected" do
        # TODO: refactor so we don't need to cheat here
        subject.instance_variable_set(:@connection, connection)

        expect(connection).to receive(:status).and_return(:open)
        expect { subject.connect }.to raise_error(/already open/) # TODO: more specific exception
      end

      it "logs an error in the event of a #{Bunny::TCPConnectionFailed}" do
        err_expected = Bunny::TCPConnectionFailed.new("oops")

        # TODO: IRL :status doesn't raise the error, :start does; but we eat that
        allow(connection).to receive(:start)
        expect(connection).to receive(:status).and_raise(err_expected)

        expect(logger).to receive(:error).with(/#{redacted_url}/)
        expect { subject.connect }.to raise_error(err_expected)
      end

      it "logs an error in the event of a #{Bunny::PossibleAuthenticationFailureError}" do
        err_expected = Bunny::PossibleAuthenticationFailureError.new(
          rabbitmq_user,
          rabbitmq_host,
          rabbitmq_password.size
        )

        # TODO: IRL :status doesn't raise the error, :start does; but we eat that
        allow(connection).to receive(:start)

        expect(connection).to receive(:status).and_raise(err_expected)

        expect(logger).to receive(:error).with(/#{redacted_url}/)
        expect { subject.connect }.to raise_error(err_expected)
      end
    end

    describe :open do
      it "connects" do
        expect do
          allow(channel).to receive(:close).ordered
          allow(connection).to receive(:close).ordered
          subject.open do |q|
            expect(q).to be(queue)
          end
        end.to connect_successfully
      end
    end

    describe :close do
      context "before connecting" do
        it "succeeds even if not connected" do
          expect { subject.close }.not_to raise_exception
        end

        context "when connected" do
          before do
            # TODO: refactor so we don't need to cheat here
            subject.instance_variable_set(:@channel, channel)
            subject.instance_variable_set(:@connection, connection)
          end

          it "closes the channel and connection" do
            expect(channel).to receive(:close)
            expect(connection).to receive(:close)

            subject.close
          end

          it "closes the connection even if closing the channel fails" do
            err_expected = "nope"
            expect(channel).to receive(:close).and_raise(err_expected)
            expect(connection).to receive(:close)

            expect { subject.close }.to raise_error(err_expected)
          end
        end
      end
    end
  end
end
