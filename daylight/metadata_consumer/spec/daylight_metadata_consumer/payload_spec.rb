require "spec_helper"
require "support/payload_helpers"

module DaylightMetadataConsumer
  RSpec.describe Payload do
    include_context "payload helpers"

    describe "status" do
      it "raises #{Payload::UnknownStatus} when status is undefined" do
        payload = JSON.generate({"resourceUrl" => resource_url})
        payload_data = Payload.new(payload) # TODO: why not blow up right here?
        expect { payload_data.status }.to raise_error(Payload::UnknownStatus)
      end
    end

    describe "to_s" do
      it "returns the data as a string" do
        payload = payload_with(status: "updated")
        payload_data = Payload.new(payload)

        string_expected = JSON.parse(payload).to_s
        expect(payload_data.to_s).to eq(string_expected)
      end
    end
  end
end
