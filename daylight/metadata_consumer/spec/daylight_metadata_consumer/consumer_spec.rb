require "spec_helper"
require "support/payload_helpers"

require "opentelemetry-api"

module DaylightMetadataConsumer
  RSpec.describe Consumer do
    include_context "payload helpers"

    let(:tracer) { instance_double(OpenTelemetry::Trace::Tracer) }

    let(:logger) do
      instance_double(Logger).tap do |logger|
        allow(logger).to receive(:info)
      end
    end

    let(:queue) { instance_double(Bunny::Queue) }
    let(:connection) { instance_double(MqConnection) }

    let(:delivery_info) { instance_double(Bunny::DeliveryInfo) }
    let(:msg_properties) { instance_double(Bunny::MessageProperties) }

    let(:span) { instance_double(OpenTelemetry::Trace::Span) }

    let(:payload) { payload_with(status: "test status") }

    describe "run" do
      RSpec::Matchers.define :subscribe_to_the_queue do
        supports_block_expectations

        match(notify_expecation_failures: true) do |actual|
          expect(connection).to receive(:open).and_yield(queue)
          expect(queue).to receive(:subscribe).with(block: true).and_yield(delivery_info, msg_properties, payload)
          expect(tracer).to receive(:in_span).with(/consumer/).and_yield(span)
          expect(Handlers::MessageHandler).to receive(:handle).with(payload:)

          actual.call || true # should return nil, but RSpec will fail on a non-truthy return value
        end
      end

      RSpec::Matchers.define :log_handling_failures do
        supports_block_expectations

        match(notify_expecation_failures: true) do |actual|
          expect(connection).to receive(:open).and_yield(queue)
          expect(queue).to receive(:subscribe).with(block: true).and_yield(delivery_info, msg_properties, payload)
          expect(tracer).to receive(:in_span).with(/consumer/).and_yield(span)

          err_expected = ArgumentError.new("oops")
          expect(Handlers::MessageHandler).to receive(:handle).with(payload:).and_raise(err_expected)
          expect(logger).to receive(:error).with(/#{err_expected}/)

          actual.call || true # should return nil, but RSpec will fail on a non-truthy return value
        end
      end

      context "as a class method" do
        before do
          expect(MqConnection).to receive(:new).with(logger:).and_return(connection)
        end

        it "subscribes to the queue" do
          expect { Consumer.run(tracer:, logger:) }.to subscribe_to_the_queue
        end

        it "logs handling failures w/o failing" do
          expect { Consumer.run(tracer:, logger:) }.to log_handling_failures
        end
      end

      context "as an instance method" do
        let(:consumer) do
          expect(MqConnection).to receive(:new).with(logger:).and_return(connection)
          Consumer.new(tracer:, logger:)
        end

        it "subscribes to the queue" do
          expect { consumer.run }.to subscribe_to_the_queue
        end

        it "logs handling failures w/o failing" do
          expect { consumer.run }.to log_handling_failures
        end
      end
    end

    # TODO: why is this here, and not in say IndexHandler?
    describe "solr_connection" do
      let(:solr_admin_user) { "admin" }
      let(:solr_admin_password) { "admin" }
      let(:solr_host) { "solr" }
      let(:solr_port) { "8983" }
      let(:solr_collection_name) { "daylight-dev" }
      let(:solr_connection) { instance_double(RSolr::Client) }

      let(:env_solr) do
        {
          "SOLR_HOST" => solr_host,
          "SOLR_PORT" => solr_port,
          "SOLR_COLLECTION_NAME" => solr_collection_name
        }
      end

      let(:env_solr_auth) do
        {
          "SOLR_ADMIN_USER" => solr_admin_user,
          "SOLR_ADMIN_PASSWORD" => solr_admin_password
        }
      end

      before do
        env_solr.each { |k, v| allow(ENV).to receive(:fetch).with(k).and_return(v) }
      end

      after do
        Consumer.remove_instance_variable(:@solr_connection)
      end

      RSpec::Matchers.define :connect_to_solr do |url:|
        supports_block_expectations

        match(notify_expecation_failures: true) do |actual|
          expect(RSolr).to receive(:connect).with(url:).and_return(solr_connection)
          result = expect(actual.call).to be(solr_connection)
          result || true
        end
      end

      context "with authorization" do
        before do
          env_solr_auth.each { |k, v| allow(ENV).to receive(:fetch).with(k, nil).and_return(v) }
        end

        it "connects to solr" do
          url_expected = "http://#{solr_admin_user}:#{solr_admin_password}@#{solr_host}:#{solr_port}/solr/#{solr_collection_name}"
          expect { Consumer.solr_connection }.to connect_to_solr(url: url_expected)
        end
      end

      context "without authorization" do
        before do
          env_solr_auth.each_key { |k| allow(ENV).to receive(:fetch).with(k, nil).and_return(nil) }
        end

        it "connects to solr" do
          url_expected = "http://#{solr_host}:#{solr_port}/solr/#{solr_collection_name}"
          expect { Consumer.solr_connection }.to connect_to_solr(url: url_expected)
        end
      end
    end
  end
end
