Daylight Metadata Consumer
==========================

this service consumes a surfliner metadata message queue, retrieves metadata for
`:published`/`:updated` events, transforms results for indexing, and updates
a target Solr index.

## Local development

The default `rake` task performs the following, with each step proceeding only
if prior steps succeed.

1. runs unit tests, with code coverage
2. checks code style
3. builds the gem

### Testing

- `rake spec` to run unit tests
  - test results are written to the console
  - test reports are written to `artifacts/rspec` in JUnit format
- `rake coverage` to run unit tests with coverage
  - summary coverage statistics are written to the console
  - detailed coverage reports are written to `artifacts/coverage` in HTML format;
    open `artifacts/coverage/index.html` to view

### Enforcing code style

- `rake standard` to check code style
- `rake standard:fix` to make safe fixes automatically

### Building the gem

- `rake gem` builds the gem
  - gem package is written to `artifacts/daylight_metadata_consumer-<VERSION>.gem`
  - version is set in `lib/daylight_metadata_consumer/version.rb`

### Running the service

Build the listener Dockerfile and tag it with the name `dmc-listener`:

```none
docker build . -t dmc-listener
```

Run the listener container:

```none
docker run --env-file ../../docker-compose/env/daylight-listener.sh dmc-listener
```
