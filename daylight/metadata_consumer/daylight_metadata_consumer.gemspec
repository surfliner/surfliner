require_relative "lib/daylight_metadata_consumer/version"

Gem::Specification.new do |spec|
  spec.name = "daylight_metadata_consumer"
  spec.version = DaylightMetadataConsumer::VERSION
  spec.authors = ["Project Surfliner"]

  spec.homepage = "https://gitlab.com/surfliner/surfliner"
  spec.license = "MIT"
  spec.summary = "Daylight metadata consumer"

  spec.required_ruby_version = Gem::Requirement.new(">= 3.3.1")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/surfliner/surfliner.git"

  spec.files = Dir["lib/**/*.rb"] + Dir["bin/*"] + Dir["[A-Z]*"]

  ["dmc-listen", "dmc-publish"].each do |script|
    spec.executables << script
  end

  spec.add_dependency "bunny", "~> 2.23"
  # TODO: Figure out why we get "The otlp exporter cannot be configured - please add opentelemetry-exporter-otlp to your Gemfile"
  spec.add_dependency "opentelemetry-exporter-otlp", "~> 0.26.3"
  spec.add_dependency "opentelemetry-instrumentation-all", "~> 0.60.0"
  spec.add_dependency "opentelemetry-sdk", "~> 1.4.1"
  spec.add_dependency "rsolr", ">= 1.0", "< 3"

  spec.add_development_dependency "debug", "~> 1.9.2"
  spec.add_development_dependency "rspec", "~> 3.13"
  spec.add_development_dependency "standard", "~> 1.31"
  spec.add_development_dependency "ci_reporter_rspec", "~> 1.0"
  spec.add_development_dependency "colorize", "~> 0.8"
  spec.add_development_dependency "dotenv", "~> 2.7"
  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "simplecov", "~> 0.21"
  spec.add_development_dependency "webmock", "~> 3.12"
end
