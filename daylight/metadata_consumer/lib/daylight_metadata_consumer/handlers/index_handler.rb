require "json"
require "net/http"
require "rsolr"

require "daylight_metadata_consumer/handlers/message_handler"
require "daylight_metadata_consumer/superskunk_client"

module DaylightMetadataConsumer
  module Handlers
    class IndexHandler < MessageHandler
      SOLR_ATTRIBUTES = {commitWithin: 10}.freeze

      def handle
        index(build_document(SuperskunkClient.get(payload.resource_url)))
      end

      private

      ##
      # @return [Hash]
      def build_document(data)
        index_document = {id: data["@id"].split("/").last}
        index_document[:title_tesim] = data["title"]
        index_document[:creator_ssim] = data["creator"]
        index_document[:ark_si] = data["ark"]
        index_document[:superskunk_uri_si] = data["@id"]
        index_document
      end

      ##
      # @param doc [Hash]
      # @return void
      def index(doc)
        Consumer.solr_connection.add([doc], add_attributes: SOLR_ATTRIBUTES)
      end
    end
  end
end
