require "json"
require "daylight_metadata_consumer/payload"

module DaylightMetadataConsumer
  module Handlers
    class MessageHandler
      attr_reader :payload

      ##
      # @param payload [String] JSON message payload
      #
      # @return [#handle]
      def self.for(payload:)
        payload_data = Payload.new(payload)

        case payload_data.status
        when :published, :updated
          IndexHandler.new(payload_data)
        when :unpublished, :deleted
          DeleteHandler.new(payload_data)
        else
          raise ArgumentError, "Couldn't handle message with payload status: #{payload_data.status}"
        end
      end

      def self.handle(payload:)
        self.for(payload: payload).handle
      end

      ##
      # @param payload [Payload]
      def initialize(payload)
        @payload = payload
      end

      ##
      # @abstract
      def handle
        raise NotImplementedError
      end
    end
  end
end
