require "daylight_metadata_consumer/handlers/message_handler"

module DaylightMetadataConsumer
  module Handlers
    class DeleteHandler < MessageHandler
      def handle
        raise NotImplementedError, "IMPLEMENT ME: This consumer can't delete yet!"
      end
    end
  end
end
