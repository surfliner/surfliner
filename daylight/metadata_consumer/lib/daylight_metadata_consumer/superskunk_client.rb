module DaylightMetadataConsumer
  class SuperskunkClient
    DEFAULT_JSONLD_PROFILE = "tag:surfliner.gitlab.io,2022:api/oai_dc"

    class << self
      ##
      # @param uri [URI]
      # @return [Hash] parsed JSON response data
      #
      # @raise [UnexpectedResponse]
      def get(uri)
        uri = URI(uri)
        req = Net::HTTP::Get.new(uri)
        req["Accept"] = "application/ld+json;profile=\"#{jsonld_profile}\""
        req["User-Agent"] = ENV.fetch("USER_AGENT_PRODUCT_NAME") { "surfliner.daylight" }

        response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") do |http|
          http.request(req)
        end

        case response
        when Net::HTTPSuccess
          JSON.parse(response.body)
        when Net::HTTPRedirection
          get(URI(response["location"]))
        else
          raise UnexpectedResponse, "Failed to fetch data; status #{response.code}"
        end
      end

      private

      ##
      # @return [#to_s]
      def jsonld_profile
        DEFAULT_JSONLD_PROFILE
      end
    end

    class UnexpectedResponse < RuntimeError; end
  end
end
