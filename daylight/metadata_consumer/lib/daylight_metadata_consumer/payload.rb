module DaylightMetadataConsumer
  class Payload
    KNOWN_STATUSES = [:published, :updated, :unpublished, :deleted]

    def initialize(json)
      @data = JSON.parse(json)
    end

    def to_s
      @data.to_s
    end

    def resource_url
      @data.fetch("resourceUrl")
    end

    def status
      status = @data.fetch("status") { raise(UnknownStatus, "Payload status is not defined in payload: #{@data}") }.to_sym

      status.to_sym
    end

    class UnknownStatus < RuntimeError; end
  end
end
