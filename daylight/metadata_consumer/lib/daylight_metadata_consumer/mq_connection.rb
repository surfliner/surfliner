require "bunny"

module DaylightMetadataConsumer
  class MqConnection
    attr_reader :channel, :connection, :connection_url, :exchange, :logger, :queue, :routing_key

    def initialize(logger:)
      @logger = logger
      rabbitmq_host = ENV.fetch("RABBITMQ_HOST")
      rabbitmq_password = ENV.fetch("RABBITMQ_PASSWORD")
      rabbitmq_port = ENV.fetch("RABBITMQ_NODE_PORT_NUMBER")
      rabbitmq_user = ENV.fetch("RABBITMQ_USERNAME")

      @routing_key = ENV.fetch("RABBITMQ_PLATFORM_ROUTING_KEY")
      @queue = ENV.fetch("RABBITMQ_QUEUE")
      @topic = ENV.fetch("RABBITMQ_TOPIC")

      @connection_url = URI("amqp://#{rabbitmq_user}:#{rabbitmq_password}@#{rabbitmq_host}:#{rabbitmq_port}")
    end

    def connect # TODO: more specific exception
      raise "RabbitMQ connection #{connection} already open." if connection&.status == :open

      logger.info("Rabbitmq message broker connection url: #{redacted_url}")
      @connection = Bunny.new(connection_url.to_s, logger: logger)
      connect_on(connection)
      @channel = connection.create_channel
      @exchange = channel.topic(@topic, auto_delete: true)
      @queue = channel.queue(@queue, durable: true) # TODO: stop using @queue to mean two different things
      queue.bind(exchange, routing_key: @routing_key)

      self
    rescue Bunny::TCPConnectionFailed => err
      # TODO: realistically, this only happens in connection.start, where we're eating it
      logger.error("Connection to #{redacted_url} failed")
      raise err
    rescue Bunny::PossibleAuthenticationFailureError => err
      # TODO: realistically, this only happens in connection.start, where we're eating it
      logger.error("Failed to authenticate to #{redacted_url}")
      raise err
    end

    def open
      connect
      yield queue
    ensure
      close
    end

    def close
      channel&.close
    ensure
      connection&.close
    end

    private

    def connect_on(connection, timeout = 120)
      timer = 0
      logger.info "Trying to open queue connection with timeout=#{timeout}"
      while timer < timeout

        begin
          connection.start
        rescue
          # TODO: do we actually want to rescue from everything?
        end
        return connection if connection.status == :open
        sleep 1
        timer += 1
      end
      raise "Failed to connect to queue."
    end

    def redacted_url
      "#{connection_url.scheme}://#{connection_url.user}:REDACTED@" \
        "#{connection_url.host}:#{connection_url.port}"
    end
  end
end
