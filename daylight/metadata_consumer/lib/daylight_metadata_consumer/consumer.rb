require "rsolr"
require "daylight_metadata_consumer/mq_connection"
require "daylight_metadata_consumer/handlers/message_handler"

module DaylightMetadataConsumer
  class Consumer
    class << self
      # TODO: why is this here, and not in say IndexHandler?
      def solr_connection
        @solr_connection ||= begin
          solr_host = ENV.fetch("SOLR_HOST")
          solr_port = ENV.fetch("SOLR_PORT")
          solr_collection_name = ENV.fetch("SOLR_COLLECTION_NAME")

          solr_url = "http://#{solr_auth}#{solr_host}:#{solr_port}/solr/#{solr_collection_name}"
          RSolr.connect(url: solr_url)
        end
      end

      private

      def solr_auth
        solr_admin_user = ENV.fetch("SOLR_ADMIN_USER", nil)
        solr_admin_password = ENV.fetch("SOLR_ADMIN_PASSWORD", nil)
        return "" unless solr_admin_user && solr_admin_password

        "#{solr_admin_user}:#{solr_admin_password}@"
      end
    end

    attr_reader :connection, :logger, :tracer

    def initialize(tracer:, logger:)
      @connection = MqConnection.new(logger: logger)
      @logger = logger
      @tracer = tracer
    end

    def self.run(tracer:, logger:)
      new(tracer: tracer, logger: logger).run
    end

    def run
      connection.open do |queue|
        queue.subscribe(block: true) do |_delivery_info, _properties, payload|
          tracer.in_span("daylight metadata consumer message") do |span|
            logger.info(" [  ] message received with payload: #{payload}")

            Handlers::MessageHandler.handle(payload: payload)
          end
        rescue => err
          logger.error(" [❌] failed to handle message: #{err}\n#{err.backtrace}")
        end
      end
    end
  end
end
