Daylight Frontend Components
============================

The Surfliner team is exploring strategies for shipping
[Micro Frontends][micro-frontends].

## I want to create a new component

**1. Clone the Surfliner monorepo:**

Daylight is found within the [Surfliner Project](https://surfliner.ucsd.edu/).

```
git clone https://gitlab.com/surfliner/surfliner.git
```

**2. Add a new folder:**

-  Navigate to the `daylight/components` directory
  - `cd daylight/components/`
- Create a new folder with the component name in `daylight/components`.
- Create a new directory for your component
  - `mkdir COMPONENT_NAME`

**3. Create a `sources` directory:**

- Navigate to your new component directory
  - `cd COMPONENT_NAME`
- Create a sources directory.
  - `mkdir sources`

**4. Create the component HTML/CSS/JS:**

- Navigate into `sources`, create `index.html` & `index.js` or index.ts
    - Include scripts, styles, and templates in the `<head>` of the html document.
    - The `<body>` may be an HTML custom element or standard HTML. If it is a custom element, slots passed to `<render-daylight-component>` will be forwarded; otherwise, slots are not supported.
    - View the component `starthere` or `spike` for an example
    - [Web API - Web Components][web-components]
    - In `index.js` (or `index.ts`), import and re-export external dependencies from a `deps.js` (or `deps.ts`) file.
      - Add dependencies to the `deps.js`

**6. Copy build files:**

- Copy `build`, `lint`, `live-serve`, `serve`, `test`, and the `Dockerfile` from the `starthere` component.
- Update the `WORKDIR` in the `Dockerfile` to match the new component path within daylight.


**7. Add component locally:**

- In `/docker-compose/daylight.yaml`, add the following snippet:
- Replace COMPONENT_NAME and PORT_NUMBER below with your component's respective information
  - PORT_NUMBER must be unique
  - COMPONENT_NAME should no include uppercase characters

```

COMPONENT_NAME:
  profiles:
    - daylight-components
  build:
    context: ../daylight
    dockerfile: components/COMPONENT_NAME/Dockerfile
  environment:
    - DAYLIGHT_PUBLIC_URL=http://localhost:PORT_NUMBER
  command:
    - ./live-serve
  ports:
    - "PORT_NUMBER:8080"
  volumes:
    - ../daylight:/app #change, if needed
  networks:
    - surfliner

```

**8. Test your component**
To ensure that the component will function with other Daylight features, load the component within the development sandbox.

- Navigate to `daylight/development`
- Open `daylight/development/sources/index.html`
- Add this html code snippet:
```
<render-daylight-component name="COMPONENT_NAME">
  <div slot="daylight-component"> /* If a slot is used, the slot value should match the name value from the template slot */
    <script type="application/json">{data: "Add json to include any dynamic content"}</script>
  </div>
</render-daylight-component>

```
- Register your component with js
  - Open `daylight/development/source/index.js`
    - Add this code js snippet:
    ```
    daylightComponentRegistry.register("COMPONENT_NAME", "http://localhost:PORT_NUMBER/");

    ```


## I want to start a local development environment
To bring up a local dev environment:

- From the root Daylight directory `/daylight`
- Execute the following commands in your terminal:

```
make build
make up

```

To stop the local dev environment, run the command:

```
make down

```

- Note: Make Build fails if component name contains uppercase characters

## Helpful Links

- [Learn more about web components][web-components]
- [Deno](https://deno.com/)
- [bitnami-nginx](https://github.com/bitnami/charts/tree/main/bitnami/nginx)
- [Micro Frontends][micro-frontends]
- [nginx](https://nginx.org/)

[web-components]: https://developer.mozilla.org/en-US/docs/Web/API/Web_components
[micro-frontends]: https://micro-frontends.org/
