import { daylightComponentRegistry } from "./deps.js";
daylightComponentRegistry.register("spike", "http://localhost:4001/");
daylightComponentRegistry.register("starthere", "http://localhost:4002/");
// DT
daylightComponentRegistry.register("demo-footer", "http://localhost:4003/");
daylightComponentRegistry.register("eyebrow-menu", "http://localhost:4004/");
daylightComponentRegistry.register("demo-home-content", "http://localhost:4005/");
daylightComponentRegistry.register("results-list", "http://localhost:4006/");
daylightComponentRegistry.register("demo-search-applied-parameters", "http://localhost:4007/");
daylightComponentRegistry.register("results-complex-pager", "http://localhost:4008/");
daylightComponentRegistry.register("results-simple-pager", "http://localhost:4009/");
daylightComponentRegistry.register("result-search-sort", "http://localhost:4010/");
daylightComponentRegistry.register("object-carousel", "http://localhost:4011/");
// AP
daylightComponentRegistry.register("search-bar", "http://localhost:4052/");
daylightComponentRegistry.register("results-facet-menu", "http://localhost:4053/");
daylightComponentRegistry.register("object-metadata", "http://localhost:4054/");
daylightComponentRegistry.register("featured-list", "http://localhost:4055/");
daylightComponentRegistry.register("quicklinks", "http://localhost:4056/");
