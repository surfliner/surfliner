#!/bin/sh

chmod +rwt /tmp

apk --no-cache add ${APKS}

getent group app || addgroup -S --gid 10001 app
getent passwd app || adduser -S -G app -u 10000 -s /bin/sh -h /app app
