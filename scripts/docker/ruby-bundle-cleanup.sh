#!/bin/sh

if [ -z "${PROJECT_PATH+x}" ]; then
  echo "--- ERROR: missing PROJECT_PATH variable"
  exit 1
fi

if [ -z "${RUBY_ABI+x}" ]; then
  echo "--- ERROR: missing RUBY_ABI variable"
  exit 1
fi

rm -rf "$PROJECT_PATH/vendor/bundle/ruby/$RUBY_ABI/cache"
# gems installed with git include their entire history, which can be very large
find "$PROJECT_PATH/vendor" -name ".git" -type d -print0 | xargs -n1 rm -rf
