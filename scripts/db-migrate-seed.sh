#!/bin/sh
set -e

PGPASSWORD=$DB_PASSWORD PGHOST=$POSTGRESQL_HOST PGUSER=$DB_USERNAME PGDATABASE=$DB_NAME
export PGPASSWORD PGHOST PGUSER PGDATABASE

while ! db-wait.sh "$POSTGRESQL_HOST:$POSTGRESQL_PORT"; do echo "-- Waiting for PGSQL ..."; sleep 5s; done

echo "-- Creating ${DB_NAME} ..."
psql -tc "SELECT 1 FROM pg_database WHERE datname = '$DB_NAME'" | grep -q 1 || createdb -e -w $DB_NAME

echo "-- Creating ${METADATA_DATABASE_NAME} ..."
psql -tc "SELECT 1 FROM pg_database WHERE datname = '$METADATA_DATABASE_NAME'" | grep -q 1 || createdb -e -w $METADATA_DATABASE_NAME

# shellcheck source=/dev/null
. db-setup.sh

echo "-- Running sequel migrations ..."
bundle exec sequel -m db/sequel "postgres://${DB_USERNAME}:${DB_PASSWORD}@${POSTGRESQL_HOST}:${POSTGRESQL_PORT}/${METADATA_DATABASE_NAME}"

db-wait.sh "$SOLR_HOST:$SOLR_PORT"

while ! curl --silent "http://$SOLR_ADMIN_USER:$SOLR_ADMIN_PASSWORD@$SOLR_HOST:$SOLR_PORT/solr/$SOLR_COLLECTION_NAME/admin/ping" | tee /dev/fd/2 | grep -q 'OK'
do
  echo "Waiting for Solr core/collection to be pingable on http://$SOLR_ADMIN_USER:REDACTED@$SOLR_HOST:$SOLR_PORT/solr/$SOLR_COLLECTION_NAME/admin/ping"
  sleep 10
done

bundle exec rails db:seed
