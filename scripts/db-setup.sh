#!/bin/sh
set -e

db_host=${POSTGRESQL_HOST}
db_port=${POSTGRESQL_PORT}

if [ -z "${db_host}" ]; then
  echo "No database host information found; skipping db setup"
else
  if [ -z "${db_port}" ]; then
    db_port=5432
  fi

  db-wait.sh "$db_host:$db_port"

  if [ -z "${DATABASE_COMMAND}" ]; then
    echo "DATABASE_COMMAND is not supplied, skipping database rake tasks"
  else
    for db_command in $DATABASE_COMMAND; do
      bundle exec rake "$db_command"
    done
  fi
fi
