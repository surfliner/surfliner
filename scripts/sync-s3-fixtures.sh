#!/usr/bin/env sh
export AWS_DEFAULT_REGION=us-west-2
export AWS_SHARED_CREDENTIALS_FILE=/tmp/awscredentials
# needed to support multiple s3 endpoints and different sets of credentials
cat > $AWS_SHARED_CREDENTIALS_FILE << EOF
[destination]
aws_access_key_id=${FIXTURE_DESTINATION_ACCESS_KEY_ID}
aws_secret_access_key=${FIXTURE_DESTINATION_SECRET_ACCESS_KEY}

EOF

source_profile=""
if [ ! -z "${FIXTURE_SOURCE_ACCESS_KEY_ID}" ]; then
  source_profile="--profile source"

  cat >> $AWS_SHARED_CREDENTIALS_FILE << EOF
[source]
aws_access_key_id=${FIXTURE_SOURCE_ACCESS_KEY_ID}
aws_secret_access_key=${FIXTURE_SOURCE_SECRET_ACCESS_KEY}
EOF
fi

# list the files
files=$(aws s3 ls "s3://${FIXTURE_SOURCE_BUCKET}/" --recursive --human-readable --endpoint-url "${FIXTURE_SOURCE_ENDPOINT_URL}" "${source_profile}" --no-sign-request |\
  awk '{print $5}')

for f in $files; do
  echo "syncing s3://${FIXTURE_SOURCE_BUCKET}/$f to s3://${FIXTURE_DESTINATION_BUCKET}/$f"
  aws s3 cp "s3://${FIXTURE_SOURCE_BUCKET}/$f" --endpoint-url "${FIXTURE_SOURCE_ENDPOINT_URL}"  "${source_profile}" --no-sign-request - |\
  aws s3 cp - "s3://${FIXTURE_DESTINATION_BUCKET}/$f" --profile destination --endpoint-url "${FIXTURE_DESTINATION_ENDPOINT_URL}"
done
