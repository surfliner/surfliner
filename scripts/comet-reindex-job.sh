job_name="${HELM_RELEASE_NAME}-reindex-$(date '+%s')"

cat <<EOF
apiVersion: batch/v1
kind: Job
metadata:
  name: ${job_name}
  namespace: ${KUBE_NAMESPACE}
spec:
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ${job_name}
        app.kubernetes.io/instance: ${HELM_RELEASE_NAME}
    spec:
      restartPolicy: Never
      containers:
        - name: comet-rake-reindex
          image: "${DEPLOY_IMAGE}:${DEPLOY_TAG}"
          imagePullPolicy: IfNotPresent
          envFrom:
            - configMapRef:
                name: ${HELM_RELEASE_NAME}-hyrax-env
            - configMapRef:
                name: ${HELM_RELEASE_NAME}-hyrax-comet-extra-env
            - secretRef:
                name: ${HELM_RELEASE_NAME}-hyrax
            - secretRef:
                name: ${HELM_RELEASE_NAME}-hyrax-comet-extra-secret
            - secretRef:
                name: comet-postgres
          args: ["/bin/sh", "-c", "bundle exec rake comet:index:reindex"]
EOF
